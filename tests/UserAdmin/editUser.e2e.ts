import {
  BASE_URL,
  USER_ADMIN_URL,
} from '../constants';
import {
  USERS_HEADER,
  CREATE_USER_BUTTON,
  USERS_TABLE,
  EDIT_USER_BUTTON,
  EDIT_HEADER,
  CANCEL_EDIT_BUTTON,
  EDIT_SAVE_BUTTON,
  EDIT_USERNAME_INPUT_LABEL,
  EDIT_USERNAME_INPUT,
  EDIT_USER_GROUP_LABEL,
  EDIT_GROUP_INPUT,
  EDIT_EMAIL_LABEL,
  EDIT_EMAIL_INPUT,

} from './selectors';
import { login } from '../helperFunctions';
import {
  Browser,
  Page,
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import { reset } from 'redux-form';

const puppeteer = require('puppeteer');

const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type UsersE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    title?: string,
    headerText?: string,
    createUserButton?: ElementHandle,
    usersTable?: ElementHandle,
    editHeader?: string,
    editUserButton?: ElementHandle,
    editSaveButton?: ElementHandle,
    cancelEditButton?: ElementHandle,
    editUsernameLabel?: boolean,
    editGroupLabel?: boolean,
    editEmailLabel?: boolean,
    editUsernameInput?: boolean,
    editUserGroupInput?: boolean,
    editUserEmail?: boolean,
  },
};

const elementQuerySetup = async (testObject: UsersE2eTestObject) => {

  // Headers
  testObject.results.title = await testObject.page.title();
  testObject.results.headerText = await testObject.page.$eval(USERS_HEADER, header => header.innerHTML);
  testObject.results.editHeader = await testObject.page.$eval(EDIT_HEADER, (header: HTMLHeadingElement) => header.textContent);

  // Buttons
  testObject.results.createUserButton = await testObject.page.$(CREATE_USER_BUTTON);
  testObject.results.editUserButton = await testObject.page.$(EDIT_USER_BUTTON);
  testObject.results.editSaveButton = await testObject.page.$(EDIT_SAVE_BUTTON);
  testObject.results.cancelEditButton = await testObject.page.$(CANCEL_EDIT_BUTTON),

  // Labels
  testObject.results.editUsernameLabel =
   await testObject.page.$eval(EDIT_USERNAME_INPUT_LABEL, (userLabel:HTMLLabelElement) => userLabel.textContent);
  testObject.results.editGroupLabel =
   await testObject.page.$eval(EDIT_USER_GROUP_LABEL, (groupLabel:HTMLLabelElement) => groupLabel.textContent);
  testObject.results.editEmailLabel =
   await testObject.page.$eval(EDIT_EMAIL_LABEL, (emailLabel:HTMLLabelElement) => emailLabel.textContent);

  // Inputs
  testObject.results.editUsernameInput = await testObject.page.$(EDIT_USERNAME_INPUT) !== null;
  testObject.results.editUserGroupInput = await testObject.page.$(EDIT_GROUP_INPUT) !== null;
  testObject.results.editUserEmail = await testObject.page.$(EDIT_EMAIL_INPUT) !== null;

  // Tables
  testObject.results.usersTable = await testObject.page.$(USERS_TABLE);
};

const elementQueryTest = (getTestObject: () => UsersE2eTestObject) => {
  describe('Edit User Modal', () => {
    // Edit Contents
    it('Edit Header', async () => {
      expect(getTestObject().results.editHeader).not.toBeNull();
    });
    it('Username Label', async () => {
      expect(getTestObject().results.editUsernameLabel).toEqual('Username *');
    });
    it('User Group Label', async () => {
      expect(getTestObject().results.editGroupLabel).toEqual('User Group *');
    });
    it('Edit Email Label', async () => {
      expect(getTestObject().results.editEmailLabel).toEqual('Email *');
    });
    it('Username Input', async () => {
      expect(getTestObject().results.editUsernameInput).not.toBeNull();
    });
    it('User Group Input', async () => {
      expect(getTestObject().results.editUserGroupInput).not.toBeNull();
    });
    it('User Email Input', async () => {
      expect(getTestObject().results.editUserEmail).not.toBeNull();
    });
    it('Save Button', async () => {
      expect(getTestObject().results.editSaveButton).not.toBeNull();
    });
    it('Cancel Button', async () => {
      expect(getTestObject().results.cancelEditButton).not.toBeNull();
    });
    it('Edit Button', async () => {
      expect(getTestObject().results.editUserButton).not.toBeNull();
    });

    // Original Page Contents
    it('correct page title', async () => {
      expect(getTestObject().results.title).toEqual('User Administration - iterrex');
    });

    it('header with "Users" text', async () => {
      expect(getTestObject().results.headerText).toEqual('Users');
    });

    it('Create User button', async () => {
      expect(getTestObject().results.createUserButton).not.toBeNull();
    });

    it('table containing users', async () => {
      expect(getTestObject().results.usersTable).not.toBeNull();
    });
  });
};

// Test Setup For Edit Functionality
const editSaveUserSetup = async (testObject: UsersE2eTestObject) => {
  await testObject.page.click(EDIT_USER_BUTTON);
  await testObject.page.type(EDIT_HEADER, 'Edit');
  await testObject.page.click(EDIT_SAVE_BUTTON);
  await testObject.page.click(CANCEL_EDIT_BUTTON);
};

// Test For Edit, Save
const editSaveUserTest = async (getTestObject: () => UsersE2eTestObject) => {
  describe('Edit Button Click & Save', () => {
    it('Edit Button Clicked', () => {
      expect(getTestObject().results.editUserButton.click);
    });
    it('Save Edit Clicked', () => {
      expect(getTestObject().results.editSaveButton.click);
    });
  });
};

// Test Setup For Edit Fucntionality
const editCancelUserCancelSetup = async (testObject: UsersE2eTestObject) => {
  await testObject.page.click(EDIT_USER_BUTTON);
  await testObject.page.type(EDIT_HEADER, 'Edit');
  await testObject.page.click(CANCEL_EDIT_BUTTON);
};
// Test For Edit, Cancel
const editCancelUserCancelTest = async (getTestObject: () => UsersE2eTestObject) => {
  describe('Edit Button Click, Cancel', () => {
    it('Edit Button Clicked', () => {
      expect(getTestObject().results.editUserButton.click);
    });
    it('Cancel Button Clicked', () => {
      expect(getTestObject().results.cancelEditButton.click);
    });
  });
};

// TO-DO: set up tests for create and delete users when working

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<UsersE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (UsersE2eTestObject) => any = async (testObject: UsersE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(USER_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USERS_HEADER),
    testObject.page.waitFor(CREATE_USER_BUTTON),
    testObject.page.waitFor(USERS_TABLE),
    testObject.page.waitFor(EDIT_USER_BUTTON),
  ]);

  await testObject.page.click(EDIT_USER_BUTTON);

  await Promise.all([
    testObject.page.waitFor(EDIT_HEADER),
    testObject.page.waitFor(EDIT_USERNAME_INPUT_LABEL),
    testObject.page.waitFor(EDIT_USER_GROUP_LABEL),
    testObject.page.waitFor(EDIT_EMAIL_LABEL),
    testObject.page.waitFor(EDIT_USERNAME_INPUT),
    testObject.page.waitFor(EDIT_GROUP_INPUT),
    testObject.page.waitFor(EDIT_EMAIL_INPUT),
    testObject.page.waitFor(EDIT_SAVE_BUTTON),
    testObject.page.waitFor(CANCEL_EDIT_BUTTON),
  ]);
};

const before00Setup = async (testObject: UsersE2eTestObject) => {
  dLog('before00Setup');

  testObject.browser = await puppeteer.launch({
    // headless:false,
    // sloMo: 800,
  });

  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');

  await testObject.page.goto(BASE_URL);

  await login(testObject);
  dLog('At login');

  await testObject.page.goto(USER_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USERS_HEADER),
    testObject.page.waitFor(CREATE_USER_BUTTON),
    testObject.page.waitFor(USERS_TABLE),
    testObject.page.waitFor(EDIT_USER_BUTTON),
  ]);

  await testObject.page.click(EDIT_USER_BUTTON);

  await Promise.all([
    testObject.page.waitFor(EDIT_HEADER),
    testObject.page.waitFor(EDIT_USERNAME_INPUT_LABEL),
    testObject.page.waitFor(EDIT_USER_GROUP_LABEL),
    testObject.page.waitFor(EDIT_EMAIL_LABEL),
    testObject.page.waitFor(EDIT_USERNAME_INPUT),
    testObject.page.waitFor(EDIT_GROUP_INPUT),
    testObject.page.waitFor(EDIT_EMAIL_INPUT),
    testObject.page.waitFor(EDIT_SAVE_BUTTON),
    testObject.page.waitFor(CANCEL_EDIT_BUTTON),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: UsersE2eTestObject) => {
  await elementQuerySetup(testObject);
  await editSaveUserSetup(testObject);
  // await editCancelUserCancelSetup(testObject);
};

const beforeTests = async (testObject: UsersE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => UsersE2eTestObject) => {
  elementQueryTest(getTestObject);
  editSaveUserTest(getTestObject);
 // editCancelUserCancelTest(getTestObject);
};

const after00Teardown = async (testObject: UsersE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: UsersE2eTestObject) => {
  after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end user admin page test',
  parallelBefore: false,
});
