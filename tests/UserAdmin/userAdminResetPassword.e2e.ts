import {
  BASE_URL,
  USER_ADMIN_URL,
} from '../constants';
import {
  USERS_HEADER,
  CREATE_USER_BUTTON,
  USERS_TABLE,
  RESET_PASSWORD,
  RESET_PASSWORD_BUTTON,
  CANCEL_RESET_BUTTON,
  RESET_PASSWORD_HEADER,
  RESET_MODULE,
} from './selectors';
import { login } from '../helperFunctions';
import {
  Browser,
  Page,
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import { reset } from 'redux-form';

const puppeteer = require('puppeteer');

const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type UsersE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    title?: string,
    headerText?: string,
    createUserButton?: ElementHandle,
    usersTable?: ElementHandle,
    reset_button?: ElementHandle,
    reset_password_button?: ElementHandle,
    cancel_reset_button?: ElementHandle,
    reset_pass_header?: boolean,
    reset_module?: boolean,
  };
};

const elementQuerySetup = async (testObject: UsersE2eTestObject) => {

  // Headers
  testObject.results.title = await testObject.page.title();
  testObject.results.headerText = await testObject.page.$eval(USERS_HEADER, header => header.innerHTML);
  testObject.results.reset_pass_header =
  await testObject.page.$eval(RESET_PASSWORD_HEADER, (resetHeader: HTMLHeadingElement) => resetHeader.textContent);

  // Buttons
  testObject.results.createUserButton = await testObject.page.$(CREATE_USER_BUTTON);
  testObject.results.reset_button = await testObject.page.$(RESET_PASSWORD);
  testObject.results.reset_password_button = await testObject.page.$(RESET_PASSWORD_BUTTON);
  testObject.results.cancel_reset_button =
  await testObject.page.$eval(CANCEL_RESET_BUTTON, (clicked: HTMLButtonElement) => clicked.click);

  // Module
  testObject.results.reset_module = await testObject.page.$(RESET_MODULE) !== null;

  // Tables
  testObject.results.usersTable = await testObject.page.$(USERS_TABLE);
};

const elementQueryTest = (getTestObject: () => UsersE2eTestObject) => {
  describe('Reset Password Pop-Up', () => {

    // Reset Password Tests
    it('Reset Module Opens', async () => {
      expect(getTestObject().results.reset_module).not.toBeNull();
    });
    it('Reset Password Button', async () => {
      expect(getTestObject().results.reset_password_button).not.toBeNull();
    });
    it('Reset Button', async () => {
      expect(getTestObject().results.reset_button).not.toBeNull();
    });
    it('Cancel Reset Button', async () => {
      expect(getTestObject().results.cancel_reset_button).not.toBeNull();
    });
    it('Reset Password Header', async () => {
      expect(getTestObject().results.reset_pass_header).toEqual('Reset Password');
    });

    // Original Page Contents
    it('correct page title', async () => {
      expect(getTestObject().results.title).toEqual('User Administration - iterrex');
    });

    it('header with "Users" text', async () => {
      expect(getTestObject().results.headerText).toEqual('Users');
    });

    it('Create User button', async () => {
      expect(getTestObject().results.createUserButton).not.toBeNull();
    });

    it('table containing users', async () => {
      expect(getTestObject().results.usersTable).not.toBeNull();
    });
  });
};

// Reset Password Cancel Functionality
const resetPassCancelSetup = async (testObject: UsersE2eTestObject) => {
  await testObject.page.click(RESET_PASSWORD_BUTTON);
  await testObject.page.type(RESET_PASSWORD_HEADER, 'Reset Password');
  await testObject.page.click(CANCEL_RESET_BUTTON);
};
// Reset Password And Cancel Funstionality
const resetPassCancelTest = async (getTestObject: () => UsersE2eTestObject) => {
  describe('Reset Password Click, Cancel', () => {
    it('Reset Pass Clicked', async () => {
      expect(getTestObject().results.reset_password_button.click);
    });
    it('Cancel Clicked', async () => {
      expect(getTestObject().results.cancel_reset_button.click);
    });
  });
};

// Reset Password Cancel Functionality
const resetPassSetup = async (testObject: UsersE2eTestObject) => {
  await testObject.page.click(RESET_PASSWORD_BUTTON);
  await testObject.page.type(RESET_PASSWORD_HEADER, 'Reset Password');
  await testObject.page.click(RESET_PASSWORD);
};
// Reset Password And Cancel Funstionality
const resetPassTest = async (getTestObject: () => UsersE2eTestObject) => {
  describe('Reset Password Click, Cancel', () => {
    it('Reset Pass Clicked', async () => {
      expect(getTestObject().results.reset_password_button.click);
    });
    it('Reset Clicked', async () => {
      expect(getTestObject().results.reset_button.click);
    });
  });
};

// TO-DO: set up tests for create and delete users when working

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<UsersE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (UsersE2eTestObject) => any = async (testObject: UsersE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(USER_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USERS_HEADER),
    testObject.page.waitFor(CREATE_USER_BUTTON),
    testObject.page.waitFor(USERS_TABLE),
    testObject.page.waitFor(RESET_PASSWORD_BUTTON),
  ]);

  await testObject.page.click(RESET_PASSWORD_BUTTON);
  await Promise.all([
    testObject.page.waitFor(RESET_MODULE),
    testObject.page.waitFor(RESET_PASSWORD_HEADER),
    testObject.page.waitFor(RESET_PASSWORD),
    testObject.page.waitFor(CANCEL_RESET_BUTTON),
  ]);
};

const before00Setup = async (testObject: UsersE2eTestObject) => {
  dLog('before00Setup');
  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo: 800,
  });
  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');

  await testObject.page.goto(BASE_URL);

  await login(testObject);
  dLog('At login');
  await testObject.page.goto(USER_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USERS_HEADER),
    testObject.page.waitFor(CREATE_USER_BUTTON),
    testObject.page.waitFor(USERS_TABLE),
    testObject.page.waitFor(RESET_PASSWORD_BUTTON),
  ]);

  await testObject.page.click(RESET_PASSWORD_BUTTON);
  await Promise.all([
    testObject.page.waitFor(RESET_MODULE),
    testObject.page.waitFor(RESET_PASSWORD_HEADER),
    testObject.page.waitFor(RESET_PASSWORD),
    testObject.page.waitFor(CANCEL_RESET_BUTTON),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: UsersE2eTestObject) => {
  await elementQuerySetup(testObject);
  // await resetPassCancelSetup(testObject);
  await resetPassSetup(testObject);
};

const beforeTests = async (testObject: UsersE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => UsersE2eTestObject) => {
  elementQueryTest(getTestObject);
  // resetPassCancelTest(getTestObject);
  resetPassTest(getTestObject);
};

const after00Teardown = async (testObject: UsersE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: UsersE2eTestObject) => {
  after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end user admin page test',
  parallelBefore: false,
});
