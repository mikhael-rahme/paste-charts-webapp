
export const USERS_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 > div > h3';
export const CREATE_USER_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 > div > div > div:nth-child(1) > div > div > button';
export const USERS_TABLE =
'#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 > div > div > div:nth-child(2) > div:nth-child(3) > table';

// Create User Admin
export const CREATE_USER_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div > div > h3';
export const USERNAME_LABEL =
`#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 >
 div > div > div > div > form > div.bottom-buttons.row > div > div:nth-child(1) > label`;
export const USER_GROUP_LABEL =
`#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 >
div > div > div > div > form > div.bottom-buttons.row > div > div:nth-child(2) > label`;
export const EMAIL_LABEL =
`#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 >
div > div > div > div > form > div.bottom-buttons.row > div > div:nth-child(3) > label`;
export const USERNAME_INPUT =
`#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div > div >
 div > div > form > div.bottom-buttons.row > div > div:nth-child(1) > div:nth-child(2) > input`;
export const USER_GROUP_DROPDOWN =
`#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div > div > div > div > form >
 div.bottom-buttons.row > div > div:nth-child(2) > div:nth-child(2) > div > div.rw-widget-input.rw-widget-picker.rw-widget-container > div`;
export const EMAIL_INPUT =
`#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 >
 div > div > div > div > form > div.bottom-buttons.row > div > div:nth-child(3) > div:nth-child(2) > input`;
export const SAVE_CREATE_USER_BUTTON =
`#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 >
 div > div > div > div > form > div.form-inline.save-buttons.row > div > div > button`;
export const CANCEL_CREATE_USER_BUTTON =
`#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div >
div > div > div > form > div.form-inline.save-buttons.row > div > button`;

// Delete User Admin
export const DELETE_USER_HEADER =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-header > h4';
export const CONFIRM_DELETE_CHECKBOX =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > label > input';
export const DELETE_BUTTON =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > button.btn.btn-danger';
export const CANCEL_DELETE_BUTTON =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > button.btn.btn-secondary';
export const DELETE_USER_BUTTON =
`#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 > div > div >
 div:nth-child(2) > div:nth-child(3) > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > button.btn.btn-danger > span`;

 // Edit User
export const EDIT_USER_BUTTON =
 `#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 > div > div > div:nth-child(2) >
  div:nth-child(3) > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > button.btn.btn-warning`;
export const EDIT_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div > div > h3';
export const EDIT_USERNAME_INPUT_LABEL =
`#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div >
 div > div > div > form > div.bottom-buttons.row > div > div:nth-child(1) > label`;
export const EDIT_USER_GROUP_LABEL =
 `#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 >
  div > div > div > div > form > div.bottom-buttons.row > div > div:nth-child(2) > label`;
export const EDIT_EMAIL_LABEL =
`#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 >
 div > div > div > div > form > div.bottom-buttons.row > div > div:nth-child(3) > label`;
export const EDIT_SAVE_BUTTON =
 `#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 >
  div > div > div > div > form > div.form-inline.save-buttons.row > div > div > button`;
export const CANCEL_EDIT_BUTTON =
`#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 >
 div > div > div > div > form > div.form-inline.save-buttons.row > div > button`;
export const EDIT_USERNAME_INPUT =
`#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div >
 div > div > div > form > div.bottom-buttons.row > div > div:nth-child(1) > div:nth-child(2) > input`;
export const EDIT_GROUP_INPUT =
`#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 >
 div > div > div > div > form > div.bottom-buttons.row > div > div:nth-child(2) > div:nth-child(2)`;
export const EDIT_EMAIL_INPUT =
`#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div > div >
 div > div > form > div.bottom-buttons.row > div > div:nth-child(3) > div:nth-child(2) > input`;

 // Reset Password
export const RESET_PASSWORD_BUTTON =
`#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 > div > div > div:nth-child(2) >
 div:nth-child(3) > table > tbody > tr:nth-child(6) > td:nth-child(4) > div > button.btn.btn-secondary`;
export const RESET_PASSWORD_HEADER =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-header > h4';
export const RESET_PASSWORD =
`body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > button.btn.btn-danger`;
export const CANCEL_RESET_BUTTON =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > button.btn.btn-secondary';

// Page Navigation
export const NEXT_PAGE_NAVIGATION =
`#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 >
 div > div > div:nth-child(2) > div:nth-child(4) > nav > ul > li:nth-child(4) > a`;
export const PREVIOUS_PAGE_NAV =
`#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 >
 div > div > div:nth-child(2) > div:nth-child(4) > nav > ul > li:nth-child(1) > a`;
export const NEXT_PAGE =
`#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 >
 div > div > div:nth-child(2) > div:nth-child(4) > nav > ul > li:nth-child(3) > a`;
export const CURRENT_PAGE =
`#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 > div >
 div > div:nth-child(2) > div:nth-child(4) > nav > ul > li.page-item.active > a`;

 // Data Toggle Arrows
export const USERNAME_ARROW_TOGGLE =
`#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 > div >
 div > div:nth-child(2) > div:nth-child(3) > table > thead > tr > th:nth-child(1)`;
export const EMAIL_ARROW_TOGGLE =
`#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 >
 div > div > div:nth-child(2) > div:nth-child(3) > table > thead > tr > th:nth-child(2)`;
export const GROUP_ARROW_TOGGLE =
`#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 > div >
 div > div:nth-child(2) > div:nth-child(3) > table > thead > tr > th:nth-child(3)`;
export const RESET_MODULE =
 'body > div:nth-child(7) > div > div.modal.fade.show > div > div';
