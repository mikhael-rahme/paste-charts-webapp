import {
  BASE_URL,
  USER_ADMIN_URL,
} from '../constants';
import {
  USERS_HEADER,
  CREATE_USER_BUTTON,
  USERS_TABLE,
  USERNAME_ARROW_TOGGLE,
  EMAIL_ARROW_TOGGLE,
  GROUP_ARROW_TOGGLE,
} from './selectors';
import { login } from '../helperFunctions';
import {
  Browser,
  Page,
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';

const puppeteer = require('puppeteer');
const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type UsersE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    title?: string,
    headerText?: string,
    createUserButton?: ElementHandle,
    usersTable?: ElementHandle,
    username_arrow_tog?: ElementHandle,
    email_arrow_tog?: ElementHandle,
    groups_arrow_tog?: ElementHandle,
  },
};

const elementQuerySetup = async (testObject: UsersE2eTestObject) => {
  // Headers
  testObject.results.title = await testObject.page.title();
  testObject.results.headerText = await testObject.page.$eval(USERS_HEADER, header => header.innerHTML);

  // Buttons
  testObject.results.createUserButton = await testObject.page.$(CREATE_USER_BUTTON);
  testObject.results.username_arrow_tog = await testObject.page.$(USERNAME_ARROW_TOGGLE);
  testObject.results.email_arrow_tog = await testObject.page.$(EMAIL_ARROW_TOGGLE);
  testObject.results.groups_arrow_tog = await testObject.page.$(GROUP_ARROW_TOGGLE);

  // Table
  testObject.results.usersTable = await testObject.page.$(USERS_TABLE);
};

const elementQueryTest = (getTestObject: () => UsersE2eTestObject) => {
  describe('Toggle Arrows', () => {

    // Toggle Arrows
    it('Username Toggle Arrow', async () => {
      expect(getTestObject().results.username_arrow_tog).not.toBeNull();
    });
    it('Email Toggle', async () => {
      expect(getTestObject().results.email_arrow_tog).not.toBeNull();
    });
    it('Group Toggle', async () => {
      expect(getTestObject().results.groups_arrow_tog).not.toBeNull();
    });

    // Original Page Contents
    it('correct page title', async () => {
      expect(getTestObject().results.title).toEqual('User Administration - iterrex');
    });

    it('header with "Users" text', async () => {
      expect(getTestObject().results.headerText).toEqual('Users');
    });

    it('Create User button', async () => {
      expect(getTestObject().results.createUserButton).not.toBeNull();
    });

    it('table containing users', async () => {
      expect(getTestObject().results.usersTable).not.toBeNull();
    });
  });
};

const dataToggleSetup = async (testObject: UsersE2eTestObject) => {
  await testObject.page.click(USERNAME_ARROW_TOGGLE);
  await testObject.page.click(EMAIL_ARROW_TOGGLE);
  await testObject.page.click(GROUP_ARROW_TOGGLE);
};
// Create User Cancel Test
const dataToggleTest = async (getTestObject: () => UsersE2eTestObject) => {
  describe('Data Toggle Functionality', () => {
    it('Username Arrow Toggle', async () => {
      expect(getTestObject().results.username_arrow_tog.click);
    });
    it('Email Arrow Data Toggle', async () => {
      expect(getTestObject().results.email_arrow_tog.click);
    });
    it('Group Arrow Toggle', async () => {
      expect(getTestObject().results.groups_arrow_tog.click);
    });
  });
};

// TO-DO: set up tests for create and delete users when working

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<UsersE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (UsersE2eTestObject) => any = async (testObject: UsersE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(USER_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USERS_HEADER),
    testObject.page.waitFor(CREATE_USER_BUTTON),
    testObject.page.waitFor(USERS_TABLE),
    testObject.page.waitFor(USERNAME_ARROW_TOGGLE),
    testObject.page.waitFor(EMAIL_ARROW_TOGGLE),
    testObject.page.waitFor(GROUP_ARROW_TOGGLE),
  ]);
};

const before00Setup = async (testObject: UsersE2eTestObject) => {
  dLog('before00Setup');

  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo: 800,
  });
  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');

  await testObject.page.goto(BASE_URL);

  await login(testObject);
  dLog('At login');
  await testObject.page.goto(USER_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USERS_HEADER),
    testObject.page.waitFor(CREATE_USER_BUTTON),
    testObject.page.waitFor(USERS_TABLE),
    testObject.page.waitFor(USERNAME_ARROW_TOGGLE),
    testObject.page.waitFor(EMAIL_ARROW_TOGGLE),
    testObject.page.waitFor(GROUP_ARROW_TOGGLE),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: UsersE2eTestObject) => {
  await elementQuerySetup(testObject);
  await dataToggleSetup(testObject);
};

const beforeTests = async (testObject: UsersE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => UsersE2eTestObject) => {
  elementQueryTest(getTestObject);
  dataToggleTest(getTestObject);
};

const after00Teardown = async (testObject: UsersE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: UsersE2eTestObject) => {
  after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end user admin page test',
  parallelBefore: false,
});
