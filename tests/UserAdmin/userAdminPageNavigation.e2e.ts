import {
  BASE_URL,
  USER_ADMIN_URL,
} from '../constants';
import {
  USERS_HEADER,
  CREATE_USER_BUTTON,
  USERS_TABLE,
  NEXT_PAGE_NAVIGATION,
  PREVIOUS_PAGE_NAV,
  CURRENT_PAGE,
  NEXT_PAGE,
} from './selectors';
import { login } from '../helperFunctions';
import {
  Browser,
  Page,
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import { reset } from 'redux-form';

const puppeteer = require('puppeteer');
const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type UsersE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    title?: string,
    headerText?: string,
    createUserButton?: ElementHandle,
    usersTable?: number,
    next_page_nav?: ElementHandle,
    prev_page_nav?: ElementHandle,
    next_page?: ElementHandle,
    current_page?: ElementHandle,
  },
};

const elementQuerySetup = async (testObject: UsersE2eTestObject) => {

  // Headings
  testObject.results.title = await testObject.page.title();
  testObject.results.headerText = await testObject.page.$eval(USERS_HEADER, header => header.innerHTML);

  // Buttons
  testObject.results.createUserButton = await testObject.page.$(CREATE_USER_BUTTON);

  // Table/Navigation
  testObject.results.usersTable =
    await testObject.page.$eval(USERS_TABLE, (table: HTMLTableElement) => table.childNodes[1].childNodes.length);
  testObject.results.next_page_nav = await testObject.page.$(NEXT_PAGE_NAVIGATION);
  testObject.results.prev_page_nav = await testObject.page.$(PREVIOUS_PAGE_NAV);
  testObject.results.next_page = await testObject.page.$(NEXT_PAGE),
  testObject.results.current_page = await testObject.page.$(CURRENT_PAGE);
};

const elementQueryTest = (getTestObject: () => UsersE2eTestObject) => {
  describe('Table Navigation', () => {

    // Table Navigation Contents
    it('Next Page Navigation', async () => {
      expect(getTestObject().results.next_page_nav).not.toBeNull();
    });
    it('Previous Page Navigation', async () => {
      expect(getTestObject().results.prev_page_nav).not.toBeNull();
    });
    it('Next Page', async () => {
      expect(getTestObject().results.next_page).not.toBeNull();
    });
    it('Current Page', async () => {
      expect(getTestObject().results.current_page).not.toBeNull();
    });

    // Original Page Contents
    it('correct page title', async () => {
      expect(getTestObject().results.title).toEqual('User Administration - iterrex');
    });

    it('header with "Users" text', async () => {
      expect(getTestObject().results.headerText).toEqual('Users');
    });

    it('Create User button', async () => {
      expect(getTestObject().results.createUserButton).not.toBeNull();
    });

    it('table containing users', async () => {
      expect(getTestObject().results.usersTable).toBeGreaterThan(0);
    });
  });
};

// Page Navigation Setup
const navigationSetup = async (testObject: UsersE2eTestObject) => {
  await testObject.page.click(NEXT_PAGE_NAVIGATION);
  await testObject.page.click(PREVIOUS_PAGE_NAV);
};
// Page Navigation Test
const navigationTest = async (getTestObject: () => UsersE2eTestObject) => {
  describe('Page Navigation Function', () => {
    it('Next Page Navigation Click', async () => {
      expect(getTestObject().results.next_page_nav.click);
    });
    it('Previous Page Navigation', async () => {
      expect(getTestObject().results.prev_page_nav.click);
    });
  });
};

// TO-DO: set up tests for create and delete users when working

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<UsersE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (UsersE2eTestObject) => any = async (testObject: UsersE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(USER_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USERS_HEADER),
    testObject.page.waitFor(CREATE_USER_BUTTON),
    testObject.page.waitFor(USERS_TABLE),
    testObject.page.waitFor(NEXT_PAGE_NAVIGATION),
    testObject.page.waitFor(PREVIOUS_PAGE_NAV),
    testObject.page.waitFor(NEXT_PAGE),
    testObject.page.waitFor(CURRENT_PAGE),
  ]);
};

const before00Setup = async (testObject: UsersE2eTestObject) => {
  dLog('before00Setup');
  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo: 500,
  });
  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');

  await testObject.page.goto(BASE_URL);

  await login(testObject);
  dLog('At login');
  await testObject.page.goto(USER_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USERS_HEADER),
    testObject.page.waitFor(CREATE_USER_BUTTON),
    testObject.page.waitFor(USERS_TABLE),
    testObject.page.waitFor(NEXT_PAGE_NAVIGATION),
    testObject.page.waitFor(PREVIOUS_PAGE_NAV),
    testObject.page.waitFor(NEXT_PAGE),
    testObject.page.waitFor(CURRENT_PAGE),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: UsersE2eTestObject) => {
  await elementQuerySetup(testObject);
  await navigationSetup(testObject);
};

const beforeTests = async (testObject: UsersE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => UsersE2eTestObject) => {
  elementQueryTest(getTestObject);
  navigationTest(getTestObject);
};

const after00Teardown = async (testObject: UsersE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: UsersE2eTestObject) => {
  after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end user admin page test',
  parallelBefore: false,
});
