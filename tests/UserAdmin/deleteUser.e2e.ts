import {
  BASE_URL,
  USER_ADMIN_URL,
} from '../constants';
import {
  USERS_HEADER,
  CREATE_USER_BUTTON,
  USERS_TABLE,

  DELETE_USER_HEADER,
  DELETE_USER_BUTTON,
  DELETE_BUTTON,
  CONFIRM_DELETE_CHECKBOX,
  CANCEL_DELETE_BUTTON,
} from './selectors';
import { login } from '../helperFunctions';
import {
  Browser,
  Page,
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';

const puppeteer = require('puppeteer');

const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type UsersE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    title?: string,
    headerText?: string,
    createUserButton?: ElementHandle,
    usersTable?: ElementHandle,
    deleteUserHeader?: string,
    deleteUserButton?: ElementHandle,
    deleteButton?: ElementHandle,
    confirmDeleteCheckbox?: ElementHandle,
    cancelDeleteButton?: ElementHandle,
  },
};

const elementQuerySetup = async (testObject: UsersE2eTestObject) => {

  // Headers
  testObject.results.title = await testObject.page.title();
  testObject.results.headerText = await testObject.page.$eval(USERS_HEADER, header => header.innerHTML);
  testObject.results.deleteUserHeader = await testObject.page.$eval(DELETE_USER_HEADER, (header:HTMLHeadingElement) => header.textContent);

  // Buttons
  testObject.results.createUserButton = await testObject.page.$(CREATE_USER_BUTTON);
  testObject.results.deleteUserButton = await testObject.page.$(DELETE_USER_BUTTON);
  testObject.results.deleteButton = await testObject.page.$(DELETE_BUTTON);
  testObject.results.cancelDeleteButton = await testObject.page.$(CANCEL_DELETE_BUTTON);
  testObject.results.confirmDeleteCheckbox = await testObject.page.$(CONFIRM_DELETE_CHECKBOX);

  // Table
  testObject.results.usersTable = await testObject.page.$(USERS_TABLE);

};

const elementQueryTest = (getTestObject: () => UsersE2eTestObject) => {
  describe('Delete Pop Up Should Have', () => {

    // Delete Pop Up Contents
    it('Delete User Header', async () => {
      expect(getTestObject().results.deleteUserHeader).toEqual('Permanently Delete User');
    });

    it('Confirm Delete Checkbox', async () => {
      expect(getTestObject().results.confirmDeleteCheckbox).not.toBeNull();
    });

    it('Delete Button', async () => {
      expect(getTestObject().results.deleteButton).not.toBeNull();
    });

    it('Cancel Delete Button', async () => {
      expect(getTestObject().results.cancelDeleteButton).not.toBeNull();
    });

    it('Delete User Button On Page', async () => {
      expect(getTestObject().results.deleteUserButton).not.toBeNull();
    });

    // Original Page Contents
    it('correct page title', async () => {
      expect(getTestObject().results.title).toEqual('User Administration - iterrex');
    });

    it('header with "Users" text', async () => {
      expect(getTestObject().results.headerText).toEqual('Users');
    });

    it('Create User button', async () => {
      expect(getTestObject().results.createUserButton).not.toBeNull();
    });

    it('table containing users', async () => {
      expect(getTestObject().results.usersTable).not.toBeNull();
    });
  });
};

// Test Setup For Delete Box, Confirm & Delete
const deleteUserSetup = async (testObject: UsersE2eTestObject) => {
  await testObject.page.click(DELETE_USER_BUTTON);
  await testObject.page.type(DELETE_USER_HEADER, 'Permanently Delete User Group');
  await testObject.page.click(CONFIRM_DELETE_CHECKBOX);
  await testObject.page.click(DELETE_BUTTON);
};

// Test For Delete Box, Confirm & Delete
const deleteUserTest = async (getTestObject: () => UsersE2eTestObject) => {
  describe('Delete Button Functionality, Confirm Check, Delete', () => {

    it('Delete User Button Click', () => {
      expect(getTestObject().results.deleteUserButton.click);
    });
    it('Confirm Checkbox Clicked', () => {
      expect(getTestObject().results.confirmDeleteCheckbox.click);
    });
    it('Delete Button Clicked', () => {
      expect(getTestObject().results.deleteButton.click);
    });
  });
};

// Test Setup For Delete Box, Cancel
const deleteUserCancelSetup = async (testObject: UsersE2eTestObject) => {
  await testObject.page.click(DELETE_USER_BUTTON);
  await testObject.page.type(DELETE_USER_HEADER, 'Permanently Delete User Group');
  await testObject.page.click(CANCEL_DELETE_BUTTON);
};

// Test For Delete Box, Cancel
const deleteUserCancelTest = async (getTestObject: () => UsersE2eTestObject) => {
  describe('Delete Button Functionality, Cancel Button', () => {

    it('Delete User Button Click', () => {
      expect(getTestObject().results.deleteUserButton.click);
    });
    it('Cancel Button Clicked', () => {
      expect(getTestObject().results.cancelDeleteButton.click);
    });
  });
};

// TO-DO: set up tests for create and delete users when working

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<UsersE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (UsersE2eTestObject) => any = async (testObject: UsersE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(USER_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USERS_HEADER),
    testObject.page.waitFor(CREATE_USER_BUTTON),
    testObject.page.waitFor(USERS_TABLE),
    testObject.page.waitFor(DELETE_USER_BUTTON),
  ]);
  await testObject.page.click(DELETE_USER_BUTTON),
    await Promise.all([
      testObject.page.waitFor(DELETE_USER_HEADER),
      testObject.page.waitFor(DELETE_BUTTON),
      testObject.page.waitFor(CONFIRM_DELETE_CHECKBOX),
      testObject.page.waitFor(CANCEL_DELETE_BUTTON),
    ]);
  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before00Setup = async (testObject: UsersE2eTestObject) => {
  dLog('before00Setup');

  testObject.browser = await puppeteer.launch({
    headless: false,
    sloMo: 800,
  });

  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');

  await testObject.page.goto(BASE_URL);

  await login(testObject);
  dLog('At login');
  await testObject.page.goto(USER_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USERS_HEADER),
    testObject.page.waitFor(CREATE_USER_BUTTON),
    testObject.page.waitFor(USERS_TABLE),
    testObject.page.waitFor(DELETE_USER_BUTTON),
  ]);
  await testObject.page.click(DELETE_USER_BUTTON),
    await Promise.all([
      testObject.page.waitFor(DELETE_USER_HEADER),
      testObject.page.waitFor(DELETE_BUTTON),
      testObject.page.waitFor(CONFIRM_DELETE_CHECKBOX),
      testObject.page.waitFor(CANCEL_DELETE_BUTTON),
    ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: UsersE2eTestObject) => {
  await elementQuerySetup(testObject);
  await deleteUserSetup(testObject);
  // await deleteUserCancelSetup(testObject);
};

const beforeTests = async (testObject: UsersE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => UsersE2eTestObject) => {
  elementQueryTest(getTestObject);
  deleteUserTest(getTestObject);
  // deleteUserCancelTest(getTestObject);
};

const after00Teardown = async (testObject: UsersE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: UsersE2eTestObject) => {
  after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end user admin page test',
  parallelBefore: false,
});
