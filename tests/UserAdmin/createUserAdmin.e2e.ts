import {
  BASE_URL,
  USER_ADMIN_URL,
} from '../constants';
import {
  USERS_HEADER,
  CREATE_USER_BUTTON,
  USERS_TABLE,
  CREATE_USER_HEADER,
  USERNAME_LABEL,
  USERNAME_INPUT,
  USER_GROUP_DROPDOWN,
  USER_GROUP_LABEL,
  EMAIL_LABEL,
  EMAIL_INPUT,
  SAVE_CREATE_USER_BUTTON,
  CANCEL_CREATE_USER_BUTTON,
  DELETE_USER_BUTTON,

} from './selectors';
import { login } from '../helperFunctions';
import {
  Browser,
  Page,
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import expect from 'expect';

const puppeteer = require('puppeteer');

const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type UsersE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    title?: string,
    headerText?: string,
    createUserButton?: ElementHandle,
    usersTable?: number,
    createUserHeader?: string,
    userNameLabel?: string,
    userGroupLabel?: string,
    emailLabel?: string,
    userNameInput?: boolean,
    userGroupInput?: boolean,
    emailInput?: boolean,
    saveButton?: ElementHandle,
    cancelButton?: ElementHandle,

  },
};

const elementQuerySetup = async (testObject: UsersE2eTestObject) => {

  // Headers
  testObject.results.title = (await testObject.page.title());
  testObject.results.headerText = (await testObject.page.$eval(USERS_HEADER, header => header.innerHTML));
  testObject.results.createUserHeader =
  (await testObject.page.$eval(CREATE_USER_HEADER, (header:HTMLHeadingElement) => header.textContent));

  // Buttons
  testObject.results.createUserButton = (await testObject.page.$(CREATE_USER_BUTTON));
  testObject.results.cancelButton = (await testObject.page.$(CANCEL_CREATE_USER_BUTTON));
  testObject.results.saveButton = (await testObject.page.$(SAVE_CREATE_USER_BUTTON));

  // Labels
  testObject.results.userNameLabel = (await testObject.page.$eval(USERNAME_LABEL, (label:HTMLLabelElement) => label.textContent));
  testObject.results.userGroupLabel = (await testObject.page.$eval(USER_GROUP_LABEL, (label:HTMLLabelElement) => label.textContent));
  testObject.results.emailLabel = (await testObject.page.$eval(EMAIL_LABEL, (label:HTMLLabelElement) => label.textContent));

  // Input
  testObject.results.userNameInput = (await testObject.page.$(USERNAME_INPUT)) !== null;
  testObject.results.userGroupInput = (await testObject.page.$(USER_GROUP_DROPDOWN)) !== null;
  testObject.results.emailInput = (await testObject.page.$(EMAIL_INPUT)) !== null;

  // Tables
  testObject.results.usersTable =
  await testObject.page.$eval(USERS_TABLE, (table: HTMLTableElement) => table.childNodes[1].childNodes.length);
};

const elementQueryTest = (getTestObject: () => UsersE2eTestObject) => {
  describe('Opened Create Group Page Has:', () => {

    // Headers
    it('correct page title', async () => {
      expect(getTestObject().results.title).toEqual('User Administration - iterrex');
    });
    it('Create User Header', async () => {
      expect(getTestObject().results.createUserHeader).toEqual('Create User');
    });
    it('header with "Users" text', async () => {
      expect(getTestObject().results.headerText).toEqual('Users');
    });

    // Labels
    it('Username Label *', async () => {
      expect(getTestObject().results.userNameLabel).toEqual('Username *');
    });
    it('User Group *', async () => {
      expect(getTestObject().results.userGroupLabel).toEqual('User Group *');
    });
    it('Email *', async () => {
      expect(getTestObject().results.emailLabel).toEqual('Email *');
    });

    // Buttons
    // it('Save Button', async () => {
    //   expect(getTestObject().results.saveButton).not.toBeNull();
    // });
    // it('Cancel Button', async () => {
    //   expect(getTestObject().results.cancelButton).not.toBeNull();
    // });
    // it('Create User Button', async() => {
    //   expect(getTestObject().results.createUserButton).not.toBeNull();
    // });

    // Table
    it('User Table', async () => {
      expect(getTestObject().results.usersTable).toBeGreaterThan(0);
    });

    // Input
    it('Username Input', async () => {
      expect(getTestObject().results.userNameInput).toEqual(true);
    });
    it('User Group Input', async () => {
      expect(getTestObject().results.userGroupInput).toEqual(true);
    });
    it('Email Input', async () => {
      expect(getTestObject().results.emailInput).toEqual(true);
    });
  });
};

// Create User Setup Save Button Function
const createUserSetup = async (testObject: UsersE2eTestObject) => {
  await testObject.page.click(CREATE_USER_BUTTON);
  await testObject.page.type(CREATE_USER_HEADER, 'test header');
  await testObject.page.type(USERNAME_INPUT, 'test username input');
  await testObject.page.click(USER_GROUP_DROPDOWN);
  await testObject.page.type(EMAIL_INPUT, 'test email input');
  await testObject.page.click(SAVE_CREATE_USER_BUTTON);
};
// Create User Setup Save Button Click Test
const createUserTest = async (getTestObject: () => UsersE2eTestObject) => {
  describe('Creating New User Admin Save Function', () => {
    it('Create User Clicked', async () => {
      expect(getTestObject().results.createUserButton.click);
    });
    it('Save Button Clicked', async () => {
      expect(getTestObject().results.saveButton.click);
    });
  });
};

// Create User Setup Cancel
const cancelCreateUserSetup = async (testObject: UsersE2eTestObject) => {
  await testObject.page.click(CREATE_USER_BUTTON);
  await testObject.page.click(CANCEL_CREATE_USER_BUTTON);
};
// Create User Cancel Test
const cancelCreateUserTest = async (getTestObject: () => UsersE2eTestObject) => {
  describe('Creating New User Admin Cancel Function', () => {
    it('Create User Clicked', async () => {
      expect(getTestObject().results.createUserButton.click);
    });
    it('Cancel Button Clicked', async () => {
      expect(getTestObject().results.cancelButton.click);
    });
  });
};

// TO-DO: set up tests for create and delete users when working

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<UsersE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (UsersE2eTestObject) => any = async (testObject: UsersE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(USER_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USERS_HEADER),
    testObject.page.waitFor(CREATE_USER_BUTTON),
    testObject.page.waitFor(USERS_TABLE),
  ]);

  await testObject.page.click(CREATE_USER_BUTTON);

  await Promise.all([
    testObject.page.waitFor(CREATE_USER_HEADER),
    testObject.page.waitFor(USERS_TABLE),
    testObject.page.waitFor(USERS_HEADER),
    testObject.page.waitFor(USERNAME_INPUT),
    testObject.page.waitFor(USER_GROUP_DROPDOWN),
    testObject.page.waitFor(EMAIL_INPUT),
    testObject.page.waitFor(USERNAME_LABEL),
    testObject.page.waitFor(USER_GROUP_LABEL),
    testObject.page.waitFor(EMAIL_LABEL),
    testObject.page.waitFor(SAVE_CREATE_USER_BUTTON),
    testObject.page.waitFor(CANCEL_CREATE_USER_BUTTON),
  ]);
  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before00Setup = async (testObject: UsersE2eTestObject) => {
  dLog('before00Setup');

  testObject.browser = await puppeteer.launch({
    // headless:false,
    // sloMo: 800,
  });

  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');

  await testObject.page.goto(BASE_URL);

  await login(testObject);
  await testObject.page.goto(USER_ADMIN_URL);
  dLog('At login');

  await Promise.all([
    testObject.page.waitFor(USERS_HEADER),
    testObject.page.waitFor(CREATE_USER_BUTTON),
    testObject.page.waitFor(USERS_TABLE),
  ]);

  await testObject.page.click(CREATE_USER_BUTTON);

  await Promise.all([
    testObject.page.waitFor(CREATE_USER_HEADER),
    testObject.page.waitFor(USERS_TABLE),
    testObject.page.waitFor(USERS_HEADER),
    testObject.page.waitFor(USERNAME_INPUT),
    testObject.page.waitFor(USER_GROUP_DROPDOWN),
    testObject.page.waitFor(EMAIL_INPUT),
    testObject.page.waitFor(USERNAME_LABEL),
    testObject.page.waitFor(USER_GROUP_LABEL),
    testObject.page.waitFor(EMAIL_LABEL),
    testObject.page.waitFor(SAVE_CREATE_USER_BUTTON),
    testObject.page.waitFor(CANCEL_CREATE_USER_BUTTON),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: UsersE2eTestObject) => {
  await elementQuerySetup(testObject);
  await createUserSetup(testObject);
  await cancelCreateUserSetup(testObject);
};

const beforeTests = async (testObject: UsersE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => UsersE2eTestObject) => {
  elementQueryTest(getTestObject);
  createUserTest(getTestObject);
  cancelCreateUserTest(getTestObject);
};

const after00Teardown = async (testObject: UsersE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: UsersE2eTestObject) => {
  after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end user admin page test',
  parallelBefore: false,
});
