import {
  createHtmlId,
} from '../../app/utils/createHtmlId';
import {
  createInputSelector,
} from '../testHelpers/createInputSelector';

describe('createInputSelector', () => {
  it('should not throw', () => {
    const formFileName = 'SomeFile';
    const formFieldLabel = 'someField';
    expect(() => createInputSelector({
      formFileName,
      formFieldLabel,
      inputType: 'input',
    })).not.toThrow();
  });
  it('should return the correct id', () => {
    const formFileName = 'file';
    const formFieldLabel = 'field';
    const path = `${formFileName}/${formFieldLabel}`;
    const expected = `input[id='${createHtmlId(path)}']`;
    expect(createInputSelector({
      formFileName,
      formFieldLabel,
      inputType: 'input',
    })).toEqual(expected);
  });
});
