import {
  E2ETestObject,
} from '../E2ETestObject';
import {
  createInputSelector,
} from '../testHelpers/createInputSelector';

export const hubbellLogin = async (testObject: E2ETestObject) => {
  try {
    await testObject.page.type(
      createInputSelector({
        formFieldLabel: 'username',
        formFileName: 'LoginForm',
        inputType: 'input',
      }),
      testObject.user.username);
    await testObject.page.type(
        createInputSelector({
          formFieldLabel: 'password',
          formFileName: 'LoginForm',
          inputType: 'input',
        }),
        testObject.user.password);
    await testObject.page.click(
      createInputSelector({
        formFieldLabel: 'login',
        formFileName: 'LoginForm',
        inputType: 'button',
      }),
    );
    // await testObject.page.click(HUBBELL_LOGIN_BUTTON);

  } catch (err) {
    console.log(err.message);
  }
};
