import {
  createInputSelector,
} from '../../testHelpers/createInputSelector';
import { reduceConstants as rc } from '../../testHelpers/reduceConstants';
const formFileName = 'LineItemForm';
const reduceConstants = rc(formFileName);

type DropdownInputType = {
  PRODUCTION_ROUTE?: string,
  QUALITY?: string,
  GALVANIZING_LOCATION?: string,
};
type ComboboxInputType = {
  DESCRIPTION?: string,
  DESCRIPTION1?: string,
  DESCRIPTION2?: string,
  DESCRIPTION3?: string,
};
type CheckboxInputType = {
  DOUBLE_DIP?: string,
  PARTIAL_DIP?: string,
  PLUG_HOLES?: string,
  SPECIAL_PACKAGING?: string,
  VENT_DRAIN?: string,
  HANGING_HOLES?: string,
  OK_FOR_HOLES?: string,
  AIR_COOL?: string,
  AIR_COOL_THEN_QUENCH?: string,
  CREATE_ANOTHER_LINE_ITEM?: string
  HAS_LOST_PIECES?: string,
};
type TextareaInputType = {
  LINE_ITEM_NOTES?: string,
};
type ButtonInputType = {
  SAVE_BUTTON?: string,
  CANCEL_BUTTON?: string,
};
type MultiselectInputType= {
  PROBLEM?: string,
};
type NumberPickerInputType= {
  QUANTITY?: string,
  ESTIMATED_WEIGHT?: string,
  LOST_QUANTITY?: string,
};
export type InputConstantType = DropdownInputType
  & ComboboxInputType
  & CheckboxInputType
  & TextareaInputType
  & ButtonInputType
  & MultiselectInputType
  & NumberPickerInputType;

const inputsByType = {
  dropdownlist: [
    'productionRoute',
    'quality',
    'galvanizingLocation',
  ],
  combobox: [
    'description',
    'description1',
    'description2',
    'description3',
  ],
  numberpicker: [
    'quantity',
    'estimatedWeight',
    'lostQuantity',
  ],
  textarea: [
    'lineItemNotes',
  ],
  checkbox: [
    'doubleDip',
    'partialDip',
    'plugHoles',
    'specialPackaging',
    'ventDrain',
    'hangingHoles',
    'OKForHoles',
    'airCool',
    'airCoolThenQuench',
    'createAnotherLineItem',
    'hasLostPieces',
  ],
  multiselect: [
    'problem',
  ],
  button: [
    'saveButton',
    'cancelButton',
  ],
};

export const LINE_ITEM_HEADER = createInputSelector({
  formFileName,
  formFieldLabel: 'lineItemHeader',
  inputType: 'h5',
});

export const CHECKBOXES: CheckboxInputType = inputsByType.checkbox.reduce(reduceConstants('checkbox'), {});
export const TEXTAREA: TextareaInputType = inputsByType.textarea.reduce(reduceConstants('textarea'), {});
export const DROPDOWNS: DropdownInputType = inputsByType.dropdownlist.reduce(reduceConstants('dropdownlist'), {});
export const NUMBERPICKERS: NumberPickerInputType = inputsByType.numberpicker.reduce(reduceConstants('numberpicker'), {});
export const MULTISELECTS: MultiselectInputType = inputsByType.multiselect.reduce(reduceConstants('multiselect'), {});

const labels = Object.keys(inputsByType).reduce((acc, type) => [...acc, ...inputsByType[type]], []);
export const LABELS: InputConstantType = labels.reduce(reduceConstants('label', 'Label'), {});

export const BUTTONS: ButtonInputType = inputsByType.button.reduce(reduceConstants('button'), {});

export const FORM = '#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-6 > form';
