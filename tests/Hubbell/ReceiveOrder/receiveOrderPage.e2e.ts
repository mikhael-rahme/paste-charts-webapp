// tslint:disable:max-line-length
import {
  HUBBELL_BASE_URL,
} from '../../Hubbell/constants';
import {
  RECEIVE_ORDER_HOME_BUTTON,
  RECEIVE_ORDER_HEADER,
  CHECKBOXES,
  TEXTAREA,
  TEXT,
  BUTTONS,
  DROPDOWNS,
  LABELS,
  COMBOBOXES,
  SIGNATURES,
  ORDER_LINE_ITEMS_HEADER,
  REQUIRED_HELP,
  ESTIMATED_WEIGHT,
  LINE_ITEM_TABLE_INFO,
} from './receiveOrderSelectors';
import {
  hubbellLogin,
} from '../hubbellLoginHelper';
import {
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import {
  E2ETestObject,
} from '../../E2ETestObject';
import { DropDownListTester } from '../../../tests/testHelpers/dropdownListSelectorHelper';
import { ComboBoxListSelectorHelp } from '../../../tests/testHelpers/comboBoxListSelectorHelper';
import { ReceiveOrderFormErrorType } from '../../../app/containers/Hubbell/ReceiveOrder/ReceiveOrderForm';
import { noValue } from '../../../app/utils/utilities';
import { BUTTONS as HOME_BUTTONS } from '../Home/selectors';
const puppeteer = require('puppeteer');

// Debug
const DEBUG = false;
const dLog = (message: string) => DEBUG && console.log(message);

export interface ReceivedOrdersE2eTestObject extends E2ETestObject {}

export const recievedOrdersSetup = async (testObject: ReceivedOrdersE2eTestObject) => {};

const recievedOrdersTestSetup = async (getTestObject: () => ReceivedOrdersE2eTestObject) => {
  const getDisabled = async id => getTestObject().page.$eval(id, (input: HTMLInputElement) => input.disabled);
  const getValue = async id => getTestObject().page.$eval(id, (input: HTMLInputElement) => input.value);
  const getInnerText = async id => getTestObject().page.$eval(id, (input: HTMLInputElement) => input.innerText);
  const getChecked = async id => getTestObject().page.$eval(id, (input: HTMLInputElement) => input.checked);

  const checkboxClickable = async (id, clickable, reset = false) => {
    const disabled = await getDisabled(id);
    const preClickState = await getChecked(id);
    await getTestObject().page.click(id);
    const postClickState = await getChecked(id);
    reset && await getTestObject().page.click(id);
    expect(disabled).toEqual(!clickable);
    expect(preClickState).toEqual(false);
    expect(postClickState).toEqual(clickable);
  };

  const labelHasText = async (id, label) => {
    const inner = await getInnerText(id);
    expect(inner).toEqual(label);
  };

  const fieldTakesInput = async (id, inputValue) => {
    const field = await getTestObject().page.$(id);
    await field.type(inputValue);
    const value = await getValue(id);
    expect(value).toEqual(inputValue);
  };

  describe('Tests for headers and labels', () => {
    // Header
    it('header should say "Receive New Order"', async () =>
      expect(await getInnerText(RECEIVE_ORDER_HEADER)).toEqual('Receive New Order'),
    );
    it('header for Line Items should say "Order Line Items"', async () =>
      expect(await getInnerText(ORDER_LINE_ITEMS_HEADER)).toEqual('Order Line Items'),
    );
    // Required Help * = Required
    it('Required small label on top right of Receive New Order Form should say "*=Required"', async() =>
      expect(await getInnerText(REQUIRED_HELP)).toEqual('* = Required'),
    );

    // Total Estimate Weight Label and Given Weight Should be 0 when first opened up
    it('Total Estimate Weight should say "Total Estimate Weight" and given weight', async () =>
      expect(await getInnerText(ESTIMATED_WEIGHT)).toEqual(`Total Estimated Weight: ${0}`),
    );

    // Labels
    it('customer field should have "Customer *" label', async () => labelHasText(LABELS.CUSTOMER, 'Customer *'));
    it('customerContact field should have "Customer Contact" label', async () => labelHasText(LABELS.CUSTOMER_CONTACT, 'Customer Contact'));
    it('customerPO field should have "Customer PO *" label', async () => labelHasText(LABELS.CUSTOMER_PO, 'Customer PO *'));
    it('carrier fields should have "Carrier *" label', async () => labelHasText(LABELS.CARRIER, 'Carrier *'));
    it('personDroppingOff field should have "Person Dropping Off *" label', async () => labelHasText(LABELS.PERSON_DROPPING_OFF, 'Person Dropping Off *'));
    it('personDroppingOffSignature field should have "Signature *" label', async () => labelHasText(LABELS.SIGNATURE, 'Signature *'));
    it('whileYouWait field should have "While You Wait" label', async () => labelHasText(LABELS.WHILE_YOU_WAIT, 'While You Wait'));
    it('orderNotes field should have "Notes" label', async () => labelHasText(LABELS.ORDER_NOTES, 'Notes'));
  });

  describe('Tests for Line Item Table', () => {
    it('Table should include "Tracking #"', async () => {
      const getTableColumnText = num =>  getInnerText(`${LINE_ITEM_TABLE_INFO.LINE_ITEM_TABLE} th:nth-child(${num})`);
      expect(await getTableColumnText(3)).toEqual('Tracking #');
      expect(await getTableColumnText(4)).toEqual('Quantity');
      expect(await getTableColumnText(5)).toEqual('Description');
    });
  });

  // Tests for Input Error Messages
  describe('Tests for input errors', () => {
    it('Error message should show if valid inputs are not entered', async () => {
      const inputs = (values, props) => {
        const errors: ReceiveOrderFormErrorType = {};
        if (values) {
          const requiredFields = [
            'customer',
            'customerPO',
            'carrier',
            'personDroppingOff',
          ];
          requiredFields.forEach((field) => {
            if (noValue(values, field)) {
              errors[field] = 'Required';
            }
          });
          if (props.receiveOrder && Object.keys(errors).length === 0) {
            if (noValue(props.initialValues, 'personDroppingOffSignature')) {
              errors._error = 'Order must be signed for using the signature app';
            }
            if (props.receiveOrder.operationRecords.length === 0) {
              errors._error = 'Must have at least one line item';
            }
          } else {
            errors._error = 'All required order fields must be filled';
          }
        }
        expect(errors);
      };
    });
  });
  // Checkboxes
  describe('Tests to click checkboxes', () => {
    it('"While You Wait" checkbox should be clickable', async () => checkboxClickable(CHECKBOXES.WHILE_YOU_WAIT, true));
    it('"Create Another" checkbox should be clickable', async () => checkboxClickable(CHECKBOXES.CREATE_ANOTHER_ORDER_CHECKBOX, true));
  });
  // Inputs
  describe('Tests to insert text into inputs and text area', () => {
    it('"Customer PO" numberpicker should take an input', async () => fieldTakesInput(TEXT.CUSTOMER_PO, 'ABC123456'));
    it('"Person Dropping Off" numberpicker should take an input', async () => fieldTakesInput(TEXT.PERSON_DROPPING_OFF, 'Bob'));
    it('"Notes" numberpicker should take an input', async () => fieldTakesInput(TEXTAREA.ORDER_NOTES, 'Notes'));
  });
  // Buttons
  describe('Tests to click buttons', () => {
    it('"Save/Update" button should be clickable but have disabled classes', async () => {
      const disabled = await getDisabled(BUTTONS.RECEIVE_ORDER_UPDATE_BUTTON);
      const className = await getTestObject().page.$eval(BUTTONS.RECEIVE_ORDER_UPDATE_BUTTON, (input: HTMLInputElement) => input.className);
      expect(className.split(' ')).toContain('btn-disabled');
      expect(disabled).toEqual(false);
    });
    // Disabled until order draft is initially saved
    it('"Discard Draft" button should be disabled', async () => {
      const disabled = await getDisabled(BUTTONS.DISCARD_BUTTON);
      expect(disabled).toEqual(true);
    });
    // Disabled until order draft is initially saved
    it('"Save Draft" button should be disabled', async () => {
      const disabled = await getDisabled(BUTTONS.SAVE_DRAFT_BUTTON);
      expect(disabled).toEqual(true);
    });
    it('Add New Item button should be clickable', async () => {
      const disabled = await getDisabled(BUTTONS.ADD_NEW_ITEM_BUTTON);
      expect(disabled).toEqual(false);
    });
    it('"Create" customer contact button should be disabled', async () => {
      const disabled = await getDisabled(BUTTONS.DISCARD_BUTTON);
      expect(disabled).toEqual(true);
    });

    it('"Discard" button should clear out form and bring you to home screen', async () => {
      // Insert more stuff to be cleared
      await getTestObject().page.type(TEXT.CUSTOMER_PO, 'abcd');
      await getTestObject().page.type(TEXT.PERSON_DROPPING_OFF, 'abcd');
      await getTestObject().page.type(TEXTAREA.ORDER_NOTES, 'abcd');
      await getTestObject().page.click(CHECKBOXES.WHILE_YOU_WAIT);
      const button = await getTestObject().page.$(BUTTONS.DISCARD_BUTTON);
      expect(button.click);
      expect(getTestObject().page.close);
    });
  });

  // Signatures
  describe('Tests for Signature Display', () => {
    it('Signature display should be recognized', async () => {
      await getTestObject().page.$(SIGNATURES.SIGNATURE);
    });
  });

  // Dropdowns
  describe('Tests for dropdowns', () => {
    // TODO: fix issues with this not working correctly
    // it('"Customer *" dropdown should have dropdown options and search bar', async () => {
    //   // dropdownListSelectorHelper is used to find and select list options
    //   try {
    //     const tester = DropDownListTester({
    //       dropDownListSelector: DROPDOWNS.CUSTOMER,
    //       page: getTestObject().page,
    //       // options: {
    //       //   debug: true,
    //       // },
    //     });
    //     await tester.focusOnInput();
    //     await tester.clickCaretDown();
    //     await tester.type('ALP');
    //     await tester.waitForOptionsLoad();
    //     const options = await tester.getOptionsText();
    //     console.log(`options: ${JSON.stringify(options, null, 2)}`);
    //     await tester.selectDropDownItemByText(options[0]);

    //   } catch (err) {
    //     console.log(err);
    //   }
    // });
    it('"Customer Contact" dropdown should be clickable but have no data', async () => {
      try {
        const tester = DropDownListTester({
          dropDownListSelector: DROPDOWNS.CUSTOMER_CONTACT,
          page: getTestObject().page,
        });
        await tester.clickInput(3);

      } catch (err) {
        console.log(err);
      }
    });
  });
    // Comboboxes
  describe('Tests to click comboboxes', () => {
    it('"Carrier *" combobox should be clickable and have dropdown of options', async () => {
      try {
        const tester = ComboBoxListSelectorHelp({
          comboBoxListSelector: COMBOBOXES.CARRIER,
          page: getTestObject().page,
        });
        await tester.focusOnInput();
        await tester.clickCaretDown();
        await tester.clickInput(3);

      } catch (err) {
        console.log(err);
      }
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<ReceivedOrdersE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    user: {
      username: 'sara@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
      accessKeyId: null,
      secretAccessKey: null,
    },
  };
};

const before00Setup = async (testObject: ReceivedOrdersE2eTestObject) => {
  dLog('before00Setup');
  // Try/Catch for navigation issues
  try {
    testObject.browser = await puppeteer.launch({
      headless: true,
      args: [
        '--proxy-server="direct://"',
        '--proxy-bypass-list=*',
        '--disable-dev-shm-usage',
      ],
    });
    testObject.page = await testObject.browser.newPage();
    dLog('browser launched');

    // Navigation
    await testObject.page.goto(HUBBELL_BASE_URL, { waitUntil: 'load', timeout: 1200000 });
    await testObject.page.waitForNavigation({ waitUntil: 'load', timeout: 1200000 });
    dLog('running hubbellLogin');
    await hubbellLogin(testObject);
    dLog('Logged in, going to home page');
    await testObject.page.waitForNavigation({ waitUntil: 'load', timeout: 1200000 });
    dLog('waiting for selectors');
    await testObject.page.waitFor(HOME_BUTTONS.RECEIVE_ORDER, { waitUntil: 'load', timeout: 1200000 });
    dLog('got selector');
    await testObject.page.click(HOME_BUTTONS.RECEIVE_ORDER);
    dLog('clicked selector');
    await testObject.page.waitFor(RECEIVE_ORDER_HEADER, { waitUntil: 'load', timeout: 1200000 });
    await testObject.page.waitFor(TEXTAREA.ORDER_NOTES, { waitUntil: 'load', timeout: 1200000 });
    dLog('got to page');

  } catch (err) {
    console.log(err.message);
  }
  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: ReceivedOrdersE2eTestObject) => {
  await recievedOrdersSetup(testObject);
};

const beforeTests = async (testObject: ReceivedOrdersE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => ReceivedOrdersE2eTestObject) => {
  recievedOrdersTestSetup(getTestObject);
};

const after00Teardown = async (testObject: E2ETestObject) => {
  await testObject.page.click(BUTTONS.DISCARD_BUTTON);
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: E2ETestObject) => {
  await after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: (global as any).testCredentials,
  testName: 'End-to-end receive order page test',
  parallelBefore: false,
});
