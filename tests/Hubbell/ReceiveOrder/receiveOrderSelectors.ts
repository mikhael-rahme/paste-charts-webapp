// tslint:disable:max-line-length
import { reduceConstants as rc } from '../../testHelpers/reduceConstants';
const formFileName = 'ReceiveOrderForm';
const reduceConstants = rc(formFileName);

type DropdownInputType = {
  CUSTOMER?: string,
  CUSTOMER_CONTACT?: string,
};
type ComboboxInputType = {
  CARRIER?: string,
};
type TextInputType = {
  CUSTOMER_PO?: string,
  PERSON_DROPPING_OFF?: string,
};
type CheckboxInputType = {
  WHILE_YOU_WAIT?: string,
  CREATE_ANOTHER_ORDER_CHECKBOX?: string,
};
type TextareaInputType = {
  ORDER_NOTES?: string,
};
type ButtonInputType = {
  CREATE_BUTTON?: string,
  SAVE_DRAFT_BUTTON?: string,
  DISCARD_BUTTON?: string,
  RECEIVE_ORDER_UPDATE_BUTTON?: string,
  ADD_NEW_ITEM_BUTTON?: string,
};
type SignatureInputType = {
  SIGNATURE?: string,
};
type LineItemTableInputType = {
  LINE_ITEM_TABLE,
};
export type InputConstantType = DropdownInputType
  & ComboboxInputType
  & TextInputType
  & CheckboxInputType
  & TextareaInputType
  & ButtonInputType
  & SignatureInputType
  & LineItemTableInputType;

const inputsByType = {
  dropdownlist: [
    'customer',
    'customerContact',
    'galvanizingLocation',
  ],
  combobox: [
    'carrier',
  ],
  text: [
    'customerPO',
    'personDroppingOff',
  ],
  textarea: [
    'orderNotes',
  ],
  checkbox: [
    'whileYouWait',
    'createAnotherOrderCheckbox',
  ],
  signature: [
    'signature',
  ],
  button: [
    'createButton',
    'saveDraftButton',
    'discardButton',
    'receiveOrderUpdateButton',
    'addNewItemButton',
  ],
  div: [
    'lineItemTable',
  ],
};

export const RECEIVE_ORDER_HOME_BUTTON =
'#app > div > div > div:nth-child(3) > div > div:nth-child(2) > div > div > button';
export const RECEIVE_ORDER_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div > div.col.col-sm-6 > form > h5';
export const ORDER_LINE_ITEMS_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div > div.col.col-sm-6 > form > div.bottom-buttons.receive > div:nth-child(2) > div:nth-child(1) > div.col.col-lg-8 > h4';
export const REQUIRED_HELP =
'#app > div > div > div:nth-child(3) > div > div > div > div.col.col-sm-6 > form > i';
export const ESTIMATED_WEIGHT =
'#app > div > div > div:nth-child(3) > div > div > div > div.col.col-sm-6 > form > div:nth-child(4) > div:nth-child(2) > div:nth-child(1)';

export const CHECKBOXES: CheckboxInputType = inputsByType.checkbox.reduce(reduceConstants('checkbox'), {});
export const TEXT: TextInputType = inputsByType.text.reduce(reduceConstants('text'), {});
export const TEXTAREA: TextareaInputType = inputsByType.textarea.reduce(reduceConstants('textarea'), {});
export const DROPDOWNS: DropdownInputType = inputsByType.dropdownlist.reduce(reduceConstants('dropdownlist'), {});
export const COMBOBOXES: ComboboxInputType = inputsByType.combobox.reduce(reduceConstants('combobox'), {});
export const SIGNATURES: SignatureInputType = inputsByType.signature.reduce(reduceConstants('div'), {});
export const LINE_ITEM_TABLE_INFO: LineItemTableInputType = inputsByType.div.reduce(reduceConstants('div'), {});

const labels = Object.keys(inputsByType).reduce((acc, type) => [...acc, ...inputsByType[type]], []);
export const LABELS: InputConstantType = labels.reduce(reduceConstants('label', 'Label'), {});

export const BUTTONS: ButtonInputType = inputsByType.button.reduce(reduceConstants('button'), {});

export const FORM = '#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-6 > form';
