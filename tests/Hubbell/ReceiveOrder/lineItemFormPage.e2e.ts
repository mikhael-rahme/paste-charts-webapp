// tslint:disable:max-line-length
import {
  HUBBELL_BASE_URL,
} from '../constants';
import {
  LINE_ITEM_HEADER,
  CHECKBOXES,
  LABELS,
  NUMBERPICKERS,
  TEXTAREA,
  BUTTONS,
  DROPDOWNS,
} from './lineItemSelectors';
import {
  BUTTONS as RECEIVE_ORDER_BUTTONS,
  RECEIVE_ORDER_HOME_BUTTON,
  RECEIVE_ORDER_HEADER,
} from './receiveOrderSelectors';
import {
  hubbellLogin,
} from '../hubbellLoginHelper';
import {
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import {
  E2ETestObject,
} from '../../E2ETestObject';
import { DropDownListTester } from '../../testHelpers/dropdownListSelectorHelper';
const puppeteer = require('puppeteer');

// Debug
const DEBUG = false;
const dLog = (message: string) => DEBUG && console.log(message);
const formFileName = 'LineItemForm';

export interface LineItemE2eTestObject extends E2ETestObject {}
export const lineItemsSetup = async (testObject: LineItemE2eTestObject) => {};

const lineItemsTestSetup = async (getTestObject: () => LineItemE2eTestObject) => {
  const getDisabled = async id => getTestObject().page.$eval(id, (input: HTMLInputElement) => input.disabled);
  const getValue = async id => getTestObject().page.$eval(id, (input: HTMLInputElement) => input.value);
  const getInnerText = async id => getTestObject().page.$eval(id, (input: HTMLInputElement) => input.innerText);
  const getChecked = async id => getTestObject().page.$eval(id, (input: HTMLInputElement) => input.checked);

  const checkboxClickable = async (id, clickable, reset = false) => {
    const disabled = await getDisabled(id);
    const preClickState = await getChecked(id);
    await getTestObject().page.click(id);
    const postClickState = await getChecked(id);
    reset && await getTestObject().page.click(id);
    expect(disabled).toEqual(!clickable);
    expect(preClickState).toEqual(false);
    expect(postClickState).toEqual(clickable);
  };

  const labelHasText = async (id, label) => {
    const inner = await getInnerText(id);
    expect(inner).toEqual(label);
  };

  const fieldTakesInput = async (id, inputValue) => {
    const field = await getTestObject().page.$(id);
    await field.type(inputValue);
    const value = await getValue(id);
    expect(value).toEqual(inputValue);
  };

  describe('Tests for headers and labels', () => {
    // Header
    it('header should start with "Order Line Item - Tracking # G001"', async () =>
      expect(await getInnerText(LINE_ITEM_HEADER)).toEqual('Order Line Item - Tracking # G001'),
    );

    // GOING TO WANT TO WRITE A TEST FOR ESTIMATED WEIGHT CLICKS AND IF THE "TOTAL ESTIMATED WEIGHT" LABEL CORRESPONDS

    // Labels
    it('quantity field should have "Quantity *" label', async () => labelHasText(LABELS.QUANTITY, 'Quantity *'));
    it('estimatedWeight field should have "Estimated Weight *" label', async () => labelHasText(LABELS.ESTIMATED_WEIGHT, 'Estimated Weight *'));
    it('productionRoute field should have "Production Route *" label', async () => labelHasText(LABELS.PRODUCTION_ROUTE, 'Production Route *'));
    it('description fields should have "Description *" label', async () => labelHasText(LABELS.DESCRIPTION, 'Description *'));
    it('quality field should have "Quality *" label', async () => labelHasText(LABELS.QUALITY, 'Quality *'));
    it('galvanizingLocation field should have "Galvanizing Location" label', async () => labelHasText(LABELS.GALVANIZING_LOCATION, 'Galvanizing Location'));
    it('notes field should have "Notes" label', async () => labelHasText(LABELS.LINE_ITEM_NOTES, 'Notes'));
    it('doubleDip field should have "Double Dip" label', async () => labelHasText(LABELS.DOUBLE_DIP, 'Double Dip'));
    it('partialDip field should have "Partial Dip" label', async () => labelHasText(LABELS.PARTIAL_DIP, 'Partial Dip'));
    it('plugHoles field should have "Plug Holes" label', async () => labelHasText(LABELS.PLUG_HOLES, 'Plug Holes'));
    it('specialPackaging field should have "Special Packaging" label', async () => labelHasText(LABELS.SPECIAL_PACKAGING, 'Special Packaging'));
    it('ventDrain field should have "Vent/Drain" label', async () => labelHasText(LABELS.VENT_DRAIN, 'Vent/Drain'));
    it('hangingHoles field should have "Hanging Holes" label', async () => labelHasText(LABELS.HANGING_HOLES, 'Hanging Holes'));
    it('OKForHoles field should have "OK for Holes" label', async () => labelHasText(LABELS.OK_FOR_HOLES, 'OK for Holes'));
    it('airCool field should have "Air Cool" label', async () => labelHasText(LABELS.AIR_COOL, 'Air Cool'));
    it('quenchAfterAirCool field should have "Quench After Air Cool" label', async () => labelHasText(LABELS.AIR_COOL_THEN_QUENCH, 'Quench After Air Cool'));
    it('problem field should have "Problem(s)" label', async () => labelHasText(LABELS.PROBLEM, 'Problem(s)'));
  });

  describe('Tests to click checkboxes', () => {
    // Checkboxes
    it('"Create Another Line Item" checkbox should be clickable', async () => checkboxClickable(CHECKBOXES.CREATE_ANOTHER_LINE_ITEM, true));
    it('"Double Dip" checkbox should be clickable', async () => checkboxClickable(CHECKBOXES.DOUBLE_DIP, true));
    it('"Partial Dip" checkbox should be clickable', async () => checkboxClickable(CHECKBOXES.PARTIAL_DIP, true));
    it('"Plug Holes" checkbox should be clickable', async () => checkboxClickable(CHECKBOXES.PLUG_HOLES, true));
    it('"Special Packaging" checkbox should be clickable', async () => checkboxClickable(CHECKBOXES.SPECIAL_PACKAGING, true));
    it('"OK for Holes" checkbox should be disabled', async () => checkboxClickable(CHECKBOXES.OK_FOR_HOLES, false));
    it('"Vent/Drain" checkbox should be clickable', async () => checkboxClickable(CHECKBOXES.VENT_DRAIN, true));
    it('"Hanging Holes" checkbox should be clickable', async () => checkboxClickable(CHECKBOXES.HANGING_HOLES, true));
    it('"OK for Holes" checkbox should be clickable after clicking "Vent/Drain" or "Hanging Holes"', async () => checkboxClickable(CHECKBOXES.OK_FOR_HOLES, true));
    it('"Quench After Air Cool" checkbox should be disabled', async () => checkboxClickable(CHECKBOXES.AIR_COOL_THEN_QUENCH, false));
    it('"Air Cool" checkbox should be clickable', async () => checkboxClickable(CHECKBOXES.AIR_COOL, true));
    it('"Quench After Air Cool" checkbox should be clickable after clicking "Air Cool"', async () => checkboxClickable(CHECKBOXES.AIR_COOL_THEN_QUENCH, true));
    it('"Has Lost Pieces" checkbox should not be present before order is received', async () => {
      expect(await getTestObject().page.$(CHECKBOXES.HAS_LOST_PIECES) === null);
    });
  });

  // Inputs
  describe('Tests to insert text into inputs and text area', () => {
    it('"Quantity" numberpicker should take an input', async () => fieldTakesInput(`${NUMBERPICKERS.QUANTITY} input`, '6'));
    it('"Estimated Weight" numberpicker should take an input', async () => fieldTakesInput(`${NUMBERPICKERS.ESTIMATED_WEIGHT} input`, '600'));
    it('"Notes" input should take an input', async () => fieldTakesInput(TEXTAREA.LINE_ITEM_NOTES, 'Notes'));
    it('"Lost Quantity" numberpicker should not be present before order is received', async () => {
      expect(await getTestObject().page.$(NUMBERPICKERS.LOST_QUANTITY) === null);
    });
  });

  // Buttons
  describe('Tests to click buttons', () => {
    it('"Save Line Item" button should be clickable', async () => {
      const disabled = await getDisabled(BUTTONS.SAVE_BUTTON);
      expect(disabled).toEqual(false);
    });
    it('"Save Line Item" button should have disabled styles', async () => {
      const className = await getTestObject().page.$eval(BUTTONS.SAVE_BUTTON, (input: HTMLInputElement) => input.className);
      expect(className.split(' ')).toContain('btn-disabled');
    });
    it('"Cancel" button should be clickable', async () => {
      const disabled = await getDisabled(BUTTONS.CANCEL_BUTTON);
      expect(disabled).toEqual(false);
    });
  });

  // Dropdowns
  describe('Tests for dropdowns', () => {
    it('"Production Route" dropdown should have dropdown options', async () => {
      try {
        const tester = DropDownListTester({
          dropDownListSelector: DROPDOWNS.PRODUCTION_ROUTE,
          page: getTestObject().page,
        });
        const options = await tester.getOptionsText();
        dLog(`options: ${JSON.stringify(options, null, 2)}`);

      } catch (err) {
        console.log(err);
      }
    });
    it('"Galvanizing Location" dropdown should have dropdown options', async () => {
      try {
        const tester = DropDownListTester({
          dropDownListSelector: DROPDOWNS.GALVANIZING_LOCATION,
          page: getTestObject().page,
        });
        await tester.clickInput(3);

      } catch (err) {
        console.log(err);
      }
    });
    it('"Quality" dropdown should have dropdown options', async () => {
      try {
        const tester = DropDownListTester({
          dropDownListSelector: DROPDOWNS.QUALITY,
          page: getTestObject().page,
        });
        await tester.clickInput(3);

      } catch (err) {
        console.log(err);
      }
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<LineItemE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    // results: {},
    user: {
      username: 'sara@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
      accessKeyId: null,
      secretAccessKey: null,
    },
  };
};

const before00Setup = async (testObject: LineItemE2eTestObject) => {
  dLog('before00Setup');
  // Try/Catch for navigation issues
  try {
    testObject.browser = await puppeteer.launch({
      headless: false,
      args: [
        '--proxy-server="direct://"',
        '--proxy-bypass-list=*',
        '--disable-dev-shm-usage',
      ],
    });
    testObject.page = await testObject.browser.newPage();
    dLog('browser launched');

    // Navigation
    await testObject.page.goto(HUBBELL_BASE_URL, { waitUntil: 'load', timeout: 1200000 });
    await testObject.page.waitForNavigation({ waitUntil: 'load', timeout: 1200000 });
    dLog('running hubbellLogin');
    await hubbellLogin(testObject);
    dLog('Logged in, going to home page');
    await testObject.page.waitForNavigation({ waitUntil: 'load', timeout: 1200000 });
    dLog('waiting for selectors');
    await testObject.page.waitFor(RECEIVE_ORDER_HOME_BUTTON, { waitUntil: 'load', timeout: 1200000 });
    dLog('got selector');
    await testObject.page.click(RECEIVE_ORDER_HOME_BUTTON);
    dLog('clicked selector');
    await testObject.page.waitFor(RECEIVE_ORDER_HEADER, { waitUntil: 'load', timeout: 1200000 });
    await testObject.page.click(RECEIVE_ORDER_BUTTONS.ADD_NEW_ITEM_BUTTON);
    await testObject.page.waitFor(TEXTAREA.LINE_ITEM_NOTES, { waitUntil: 'load', timeout: 1200000 });
    dLog('got to page');

  } catch (err) {
    console.log(err.message);
  }
  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: LineItemE2eTestObject) => {
  await lineItemsSetup(testObject);
};

const beforeTests = async (testObject: LineItemE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => LineItemE2eTestObject) => {
  lineItemsTestSetup(getTestObject);
};

const after00Teardown = async (testObject: E2ETestObject) => {
  await testObject.page.click(RECEIVE_ORDER_BUTTONS.DISCARD_BUTTON);
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: E2ETestObject) => {
  await after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: (global as any).testCredentials,
  testName: 'End-to-end line items page test',
  parallelBefore: false,
});
