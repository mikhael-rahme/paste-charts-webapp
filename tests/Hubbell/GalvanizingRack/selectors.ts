
import { reduceConstants as rc } from '../../testHelpers/reduceConstants';
const pageName = 'GalvanizingRackForm';
const reduceConstants = rc(pageName);

type ButtonInputType = {
  CLEAR_LINE_ITEMS?: string,
  ADD_LINE_ITEMS?: string,
  ADD_RACK_ADDON?: string,
  SAVE_BUTTON?: string,
  RESET_BUTTON?: string,
};
type DropdownListInputType = {
  EXISTING_RACKS?: string,
  RACK_TYPE?: string,
  RACK_NUMBER?: string,
  ADDON_TYPE?: string,
};
type NumberPickerInputType = {
  RACK_WEIGHT?: string,
  RACK_WEIGHT_OVERRIDE?: string,
  ADDON_QUANTITY?: string,
};
type TextAreaInputType = {
  NOTES?: string,
};
type InputConstantType = DropdownListInputType
  & NumberPickerInputType
  & TextAreaInputType;

const inputsByType = {
  button: [
    'clearLineItems',
    'addLineItems',
    'addRackAddon',
    'saveButton',
    'resetButton',
  ],
  dropdownlist: [
    'existingRacks',
    'rackType',
    'rackNumber',
    'addonType',
  ],
  numberpicker: [
    'rackWeight',
    'rackWeightOverride',
    'addonQuantity',
  ],
  textarea: ['notes'],
};

export const BUTTONS: ButtonInputType = inputsByType.button.reduce(reduceConstants('button'), {});
export const DROPDOWNLISTS: DropdownListInputType = inputsByType.button.reduce(reduceConstants('button'), {});
export const NUMBERPICKERS: NumberPickerInputType = inputsByType.button.reduce(reduceConstants('button'), {});

const labels = Object.keys(inputsByType).reduce((acc, type) => [...acc, ...inputsByType[type]], []);
export const LABELS: InputConstantType = labels.reduce(reduceConstants('label', 'Label'), {});

export const RACK_HEADER = '#app > div > div > div:nth-child(3) > div > div:nth-child(1) > div > h5';
export const RACK_FORM_HEADER =
  '#app > div > div > div:nth-child(3) > div > div:nth-child(2) > div:nth-child(2) > form > div:nth-child(1) > div > h5';
