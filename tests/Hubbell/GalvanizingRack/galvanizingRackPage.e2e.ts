// tslint:disable:max-line-length
import {
   HUBBELL_BASE_URL,
} from '../constants';
import {
  BUTTONS,
  DROPDOWNLISTS,
  NUMBERPICKERS,
  LABELS,
  RACK_HEADER,
  RACK_FORM_HEADER,
} from './selectors';
import {
  Browser,
  Page,
} from 'puppeteer';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import { hubbellLogin } from '../hubbellLoginHelper';
import { BUTTONS as HOME_BUTTONS } from '../Home/selectors';

const puppeteer = require('puppeteer');
// Debug
const DEBUG = false;
const dLog = (message: string) => DEBUG && console.log(message);

export type HubbellHomeE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
};

// Tests
const homePageTestSetup = async (getTestObject: () => HubbellHomeE2eTestObject) => {
  const getDisabled = async id => getTestObject().page.$eval(id, (input: HTMLInputElement) => input.disabled);
  const getValue = async id => getTestObject().page.$eval(id, (input: HTMLInputElement) => input.value);
  const getInnerText = async id => getTestObject().page.$eval(id, (input: HTMLInputElement) => input.innerText);

  const labelHasText = async (id, label) => {
    const inner = await getInnerText(id);
    expect(inner).toEqual(label);
  };

  const fieldTakesInput = async (id, inputValue) => {
    const field = await getTestObject().page.$(id);
    await field.type(inputValue);
    const value = await getValue(id);
    expect(value).toEqual(inputValue);
  };

  describe('Tests for headers and labels', () => {
    // Header
    it('header should say "Galvanizing Rack"', async () =>
      expect(await getInnerText(RACK_HEADER)).toEqual('Galvanizing Rack'),
    );
    it('form header should say "Rack Form"', async () =>
      expect(await getInnerText(RACK_FORM_HEADER)).toEqual('Rack Form'),
    );

    // Labels
    // it('existingRacks field should have "Existing Racks" label', async () => labelHasText(LABELS.EXISTING_RACKS, 'Existing Racks'));
    it('rackType field should have "Rack Type *" label', async () => labelHasText(LABELS.RACK_TYPE, 'Rack Type *'));
    it('rackNumber field should have "Rack Number *" label', async () => labelHasText(LABELS.RACK_NUMBER, 'Rack Number *'));
    it('rackWeight fields should have "Rack Weight *" label', async () => labelHasText(LABELS.RACK_WEIGHT, 'Rack Weight *'));
    it('rackWeightOverride field should have "Rack Weight Override" label', async () => labelHasText(LABELS.RACK_WEIGHT_OVERRIDE, 'Rack Weight Override'));
    it('notes field should have "Notes" label', async () => labelHasText(LABELS.NOTES, 'Notes'));
  });

  // Buttons
  describe('Tests to click buttons', () => {
    it('"Save" button should be clickable but have disabled classes', async () => {
      const disabled = await getDisabled(BUTTONS.SAVE_BUTTON);
      const className = await getTestObject().page.$eval(BUTTONS.SAVE_BUTTON, (input: HTMLInputElement) => input.className);
      expect(className.split(' ')).toContain('btn-disabled');
      expect(disabled).toEqual(false);
    });
    // Disabled until order draft is initially saved
    it('"Discard Draft" button should be clickable', async () => {
      dLog('in test');
      const disabled = await getDisabled(BUTTONS.RESET_BUTTON);
      dLog(disabled.toString());
      expect(disabled).toEqual(false);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<HubbellHomeE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    user: {
      username: 'sara@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
      accessKeyId: null,
      secretAccessKey: null,
    },
  };
};

const before00Setup = async (testObject: HubbellHomeE2eTestObject) => {
  dLog('before00Setup');
  // Try/Catch for navigation issues
  try {
    testObject.browser = await puppeteer.launch({
      headless: true,
      args: [
        '--proxy-server="direct://"',
        '--proxy-bypass-list=*',
        '--disable-dev-shm-usage',
      ],
    });
    testObject.page = await testObject.browser.newPage();
    dLog('browser launched');

    // Navigation
    await testObject.page.goto(HUBBELL_BASE_URL, { waitUntil: 'load', timeout: 1200000 });
    await testObject.page.waitForNavigation({ waitUntil: 'load', timeout: 1200000 });
    dLog('running hubbellLogin');
    await hubbellLogin(testObject);
    dLog('Logged in, going to home page');
    await testObject.page.waitForNavigation({ waitUntil: 'load', timeout: 1200000 });
    dLog('waiting for selectors');
    await testObject.page.waitFor(HOME_BUTTONS.GALVANIZING_RACK, { waitUntil: 'load', timeout: 1200000 });
    dLog('got selector');
    await testObject.page.click(HOME_BUTTONS.GALVANIZING_RACK);
    dLog('clicked selector');
    await testObject.page.waitFor(RACK_HEADER, { waitUntil: 'load', timeout: 1200000 });
    dLog('got to page');

  } catch (err) {
    console.log(err.message);
  }
};

const before01Run = async (testObject: HubbellHomeE2eTestObject) => {};

const beforeTests = async (testObject: HubbellHomeE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);
  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => HubbellHomeE2eTestObject) => {
  homePageTestSetup(getTestObject);
};

const after00Teardown = async (testObject: HubbellHomeE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: HubbellHomeE2eTestObject) => {
  after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: (global as any).testCredentials,
  testName: 'End-to-end group admin page test',
  parallelBefore: false,
});
