
import { reduceConstants as rc } from '../../testHelpers/reduceConstants';
const pageName = 'HomePage';
const reduceConstants = rc(pageName);

type ButtonInputType = {
  RECEIVE_ORDER?: string,
  RECORDS?: string,
  REPORTS?: string,
  PROBLEMS?: string,
  SCHEDULE_BUILDER?: string,
  SCHEDULE_VIEWER?: string,
  GALVANIZING_RACK?: string,
  GALVANIZING_RUN?: string,
  GALVANIZING_FINISH?: string,
  SHIPPING?: string,
  LIST_MAINTENANCE?: string,
};
type LinkType = {
  HUBBELL_LOGO?: string,
  RECEIVE_ORDER?: string,
  RECORDS?: string,
  REPORTS?: string,
  PROBLEMS?: string,
  SCHEDULE_BUILDER?: string,
  SCHEDULE_VIEWER?: string,
  SHIPPING?: string,
};
type DropdownType = {
  GALVANIZING_DROPDOWN?: string,
  LOGIN_DROPDOWN?: string,
};

const selectorsByType = {
  button: [
    'receiveOrder',
    'records',
    'reports',
    'problems',
    'scheduleBuilder',
    'scheduleViewer',
    'galvanizingRack',
    'galvanizingRun',
    'galvanizingFinish',
    'shipping',
    'listMaintenance',
  ],
  a: [
    'hubbellLogo',
    'receiveOrder',
    'records',
    'reports',
    'problems',
    'scheduleBuilder',
    'scheduleViewer',
    'shipping',
  ],
  dropdown: [
    'galvanizingDropdown',
    'loginDropdown',
  ],
};

export const BUTTONS: ButtonInputType = selectorsByType.button.reduce(reduceConstants('button'), {});
export const LINKS: LinkType = selectorsByType.a.reduce(reduceConstants('a'), {});
export const DROPDOWNS: DropdownType = selectorsByType.dropdown.reduce(reduceConstants('dropdown'), {});

// Hubbell Home Load
export const LOAD = '#app > div > h2';

// Hubbell Navigation Bar
export const NAV_BAR = '#app > div > div > div:nth-child(1) > nav';
