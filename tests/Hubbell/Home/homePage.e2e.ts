// tslint:disable:max-line-length
import {
  HUBBELL_BASE_URL,
} from '../constants';
import {
  BUTTONS,
  LINKS,
  DROPDOWNS,
} from './selectors';
import {
  Browser,
  Page,
} from 'puppeteer';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import { hubbellLogin } from '../hubbellLoginHelper';

const puppeteer = require('puppeteer');
// Debug
const DEBUG = false ;
const dLog = (message: string) => DEBUG && console.log(message);

export type HubbellHomeE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
};

// Tests
const homePageTestSetup = async (getTestObject: () => HubbellHomeE2eTestObject) => {
  const getTextContent = async id => getTestObject().page.$eval(id, (input: HTMLInputElement) => input.textContent);
  const selectorHasText = async (id, label) => {
    const inner = await getTextContent(id);
    expect(inner).toEqual(label);
  };

  // Button Label Tests
  describe('Button Labels on the Hubbell home page', () => {
    it('Receive Order button text should equal "Receive Order"', async () => selectorHasText(BUTTONS.RECEIVE_ORDER, 'Receive Order'));
    it('Records button text should equal "Records"', async () => selectorHasText(BUTTONS.RECORDS, 'Records'));
    it('Reports button text should equal "Reports"', async () => selectorHasText(BUTTONS.REPORTS, 'Reports'));
    it('Problems button text should equal "Problems"', async () => selectorHasText(BUTTONS.PROBLEMS, 'Problems'));
    it('Schedule Builder button text should equal "Schedule Builder"', async () => selectorHasText(BUTTONS.SCHEDULE_BUILDER, 'Schedule Builder'));
    it('Schedule Viewer button text should equal "Schedule Viewer"', async () => selectorHasText(BUTTONS.SCHEDULE_VIEWER, 'Schedule Viewer'));
    it('Galvanizing Rack button text should equal "Galvanizing Rack"', async () => selectorHasText(BUTTONS.GALVANIZING_RACK, 'Galvanizing Rack'));
    it('Galvanizing Run button text should equal "Galvanizing Run"', async () => selectorHasText(BUTTONS.GALVANIZING_RUN, 'Galvanizing Run'));
    it('Galvanizing Finish button text should equal "Galvanizing Finish"', async () => selectorHasText(BUTTONS.GALVANIZING_FINISH, 'Galvanizing Finish'));
    it('Shipping button text should equal "Shipping"', async () => selectorHasText(BUTTONS.SHIPPING, 'Shipping'));
    it('List Maintenance button text should equal "List Maintenance"', async () => selectorHasText(BUTTONS.LIST_MAINTENANCE, 'List Maintenance'));
  });
  // Tab Option Labels
  describe('Tab Option labels on the Hubbell home page', () => {
    it('Receive Order link label should equal "Receive Order"', async () => selectorHasText(LINKS.RECEIVE_ORDER, 'Receive Order'));
    it('Records link label should equal "Records"', async () => selectorHasText(LINKS.RECORDS, 'Records'));
    it('Reports link label should equal "Reports"', async () => selectorHasText(LINKS.REPORTS, 'Reports'));
    it('Problems link label should equal "Problems"', async () => selectorHasText(LINKS.PROBLEMS, 'Problems'));
    it('Schedule Builder link label should equal "Schedule Builder"', async () => selectorHasText(LINKS.SCHEDULE_BUILDER, 'Schedule Builder'));
    it('Schedule Viewer link label should equal "Schedule Viewer"', async () => selectorHasText(LINKS.SCHEDULE_VIEWER, 'Schedule Viewer'));
    it('Shipping link label should equal "Shipping"', async () => selectorHasText(LINKS.SHIPPING, 'Shipping'));
  });
  describe('Dropdowns should be clickable', () => {
    it('Galvanizing dropdown label should equal "Galvanizing"', async () => selectorHasText(DROPDOWNS.GALVANIZING_DROPDOWN, 'Galvanizing'));
    it('User dropdown link label should equal "sara@coveycs.com"', async () => selectorHasText(DROPDOWNS.LOGIN_DROPDOWN, 'sara@coveycs.com'));
  //   it('Galvanizing Dropdown should be clickable', async () => {
  //     expect(getTestObject().results.galvanizingDrop.click).toContain('Rack' && 'Run' && 'Finish');
  //   });
  //   it('Login Dropdown should be clickable', async () => {
  //     expect(getTestObject().results.loginDrop.click);
  //   });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<HubbellHomeE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    user: {
      username: 'sara@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
      accessKeyId: null,
      secretAccessKey: null,
    },
  };
};

const before00Setup = async (testObject: HubbellHomeE2eTestObject) => {
  dLog('before00Setup');
  // Try/Catch for navigation issues
  try {
    testObject.browser = await puppeteer.launch({
      headless: true,
      args: [
        '--proxy-server="direct://"',
        '--proxy-bypass-list=*',
      ],
    });
    testObject.page = await testObject.browser.newPage();
    dLog('browser launched');
    await testObject.page.goto(HUBBELL_BASE_URL, { waitUntil: 'load', timeout: 1200000 });
    await testObject.page.waitForNavigation({ waitUntil: 'load', timeout: 1200000 });
    await hubbellLogin(testObject).catch(console.log);
    dLog('At login');
  } catch (err) {
    console.log(err);
  }
  await testObject.page.waitFor(BUTTONS.RECEIVE_ORDER, { waitUntil: 'load', timeout: 1200000 }),
  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: HubbellHomeE2eTestObject) => {};

const beforeTests = async (testObject: HubbellHomeE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);
  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => HubbellHomeE2eTestObject) => {
  homePageTestSetup(getTestObject);
};

const after00Teardown = async (testObject: HubbellHomeE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: HubbellHomeE2eTestObject) => {
  after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: (global as any).testCredentials,
  testName: 'End-to-end group admin page test',
  parallelBefore: false,
});
