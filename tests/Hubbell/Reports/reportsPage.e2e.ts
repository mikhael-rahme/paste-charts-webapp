import {
  HUBBELL_BASE_URL,
  REPORTS_URL,
} from '../../Hubbell/constants';
import {
  REPORTS_BUTTON,
  RECEIVED_ORDERS_OPTION,
  SCHEDULE_OPTION,
  PROBLEMS_OPTION,
  CRANE_SHEET_OPTION,
  PRODUCTION_REPORT_OPTION,
  LOST_PIECES_REPORT_OPTION,
} from './selectors';
import {
  hubbellLogin,
} from '../hubbellLoginHelper';
import {
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import {
  options,
} from '../../../app/containers/Hubbell/ReportsPage/hardcodedData';
import { IterrexRole } from 'toolkit';
import {
  E2ETestObject,
} from '../../E2ETestObject';
const puppeteer = require('puppeteer');
// Debug
const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export interface ReportsE2eTestObject extends E2ETestObject {
  results: {
    receivedOrders?: ElementHandle,
    schedule?: ElementHandle,
    problems?: ElementHandle,
    craneSheet?: ElementHandle,
    productionReport?: ElementHandle,
    lostPiecesReport?: ElementHandle,
    reportsLabel?: string,
  };
}

  // HAVE NOT DONE A TEST FOR REPORTS HEADER/ have not created id for header
export const reportsTestSetup = async (testObject: ReportsE2eTestObject) => {
  testObject.results.receivedOrders = await testObject.page.$(RECEIVED_ORDERS_OPTION);
  testObject.results.schedule = await testObject.page.$(SCHEDULE_OPTION);
  testObject.results.problems = await testObject.page.$(PROBLEMS_OPTION);
  testObject.results.craneSheet = await testObject.page.$(CRANE_SHEET_OPTION);
  testObject.results.productionReport = await testObject.page.$(PRODUCTION_REPORT_OPTION);
  testObject.results.lostPiecesReport = await testObject.page.$(LOST_PIECES_REPORT_OPTION);
};

const createListTest = async (getTestObject: () => ReportsE2eTestObject) => {
  // Hardcoded information from hardcodedData.tsx
  // Test WILL fail if hardcodedData.tsx changes
  describe('Tests for hard coded Reports Options', () => {
    it('Received Orders option should equal "Received Orders"', async () => {
      expect(options.find(option => option.title === 'Received Orders' && option.icon === 'truck'));
    });
    it('Schedule option should equal "Schedule"', async () => {
      expect(options.find(option => option.title === 'Schedule' && option.icon === 'calendar'));
    });
    it('Problems option should equal "Problems"', async () => {
      expect(options.find(option => option.title === 'Problems' && option.icon === 'exclamation-triangle'));
    });
    it('Crane Sheet option should equal "Crane Sheet"', async () => {
      expect(options.find(option => option.title === 'Crane Sheet' && option.icon === 'file-text'));
    });
    it('Production Report option should equal "Production Report"', async () => {
      expect(options.find(option => option.title === 'Production Report' && option.icon === 'file-text'));
    });
    it('Lost Pieces Report option should equal "Lost Pieces Report"', async () => {
      expect(options.find(option => option.title === 'Lost Pieces Report' && option.icon === 'question-circle'));
    });
  });

  describe('Tests to click selectors', () => {
    it('Received Orders option should be able to be clicked', async () => {
      expect(getTestObject().results.receivedOrders.click);
    });
    it('Schedule option should be able to be clicked', async () => {
      expect(getTestObject().results.schedule.click);
    });
    it('Problems option should be able to be clicked', async () => {
      expect(getTestObject().results.problems.click);
    });
    it('Crane Sheet option should be able to be clicked', async () => {
      expect(getTestObject().results.craneSheet.click);
    });
    it('Production Report option should be able to be clicked', async () => {
      expect(getTestObject().results.productionReport.click);
    });
    it('Lost Pieces Report option should be able to be clicked', async () => {
      expect(getTestObject().results.lostPiecesReport.click);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<ReportsE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'sara@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
      accessKeyId: null,
      secretAccessKey: null,
    },
  };
};

// New Page Opens When It's Closed
const newPage: (RecievedOrderE2eTestObject) => any = async (testObject: ReportsE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(REPORTS_URL);
  testObject.page.waitFor(REPORTS_BUTTON, { waitUntil: 'load', timeout: 1200000 }),
  await testObject.page.click(REPORTS_BUTTON);
  await testObject.page.waitForNavigation({ waitUntil: 'load', timeout: 1200000 }),
  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before00Setup = async (testObject: ReportsE2eTestObject) => {
  dLog('before00Setup');
  // Try/Catch for navigation issues
  try {
    testObject.browser = await puppeteer.launch({
      headless: true,
      args: [
        '--proxy-server="direct://"',
        '--proxy-bypass-list=*',
      ],
    });
    testObject.page = await testObject.browser.newPage();
    dLog('browser launched');

    // Navigation
    await testObject.page.goto(HUBBELL_BASE_URL, { waitUntil: 'load', timeout: 1200000 });
    await testObject.page.waitForNavigation({ waitUntil: 'load', timeout: 1200000 });
    dLog('running hubbellLogin');
    await hubbellLogin(testObject);
    dLog('Logged in, going to home page');
    await testObject.page.waitForNavigation({ waitUntil: 'load', timeout: 1200000 });
    dLog('waiting for selectors');
    await testObject.page.waitFor(REPORTS_BUTTON, { waitUntil: 'load', timeout: 1200000 });
    dLog('got selector');
    await testObject.page.click(REPORTS_BUTTON);
    dLog('clicked selector');
    await testObject.page.waitFor(RECEIVED_ORDERS_OPTION, { waitUntil: 'load', timeout: 1200000 });
    dLog('got to page');

  } catch (err) {
    console.log(err.message);
  }
  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: ReportsE2eTestObject) => {
  await reportsTestSetup(testObject);
};

const beforeTests = async (testObject: ReportsE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => ReportsE2eTestObject) => {
  createListTest(getTestObject);
};

const after00Teardown = async (testObject: E2ETestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: E2ETestObject) => {
  await after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: (global as any).testCredentials,
  testName: 'End-to-end list maintenance page test',
  parallelBefore: false,
});
