// tslint:disable:max-line-length
import {
  createOptionsSelector,
} from '../../testHelpers/createInputSelector';

export const REPORTS_BUTTON =
'#app > div > div > div:nth-child(3) > div > div:nth-child(4) > div > div > button';

export const REPORT_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div.col-2 > h3';

export const RECEIVED_ORDERS_OPTION =
createOptionsSelector({
  title: 'Received Orders',
  formFieldName: 'ReportsPage',
  inputType: 'li',
});

export const SCHEDULE_OPTION =
createOptionsSelector({
  title: 'Schedule',
  formFieldName: 'ReportsPage',
  inputType: 'li',
});

export const PROBLEMS_OPTION =
createOptionsSelector({
  title: 'Problems',
  formFieldName: 'ReportsPage',
  inputType: 'li',
});

export const CRANE_SHEET_OPTION =
createOptionsSelector({
  title: 'Crane Sheet',
  formFieldName: 'ReportsPage',
  inputType: 'li',
});

export const PRODUCTION_REPORT_OPTION =
createOptionsSelector({
  title: 'Production Report',
  formFieldName: 'ReportsPage',
  inputType: 'li',
});

export const LOST_PIECES_REPORT_OPTION =
createOptionsSelector({
  title: 'Lost Pieces Report',
  formFieldName: 'ReportsPage',
  inputType: 'li',
});
