
export const HUBBELL_BASE_URL = 'http://staging-sara.staging.iterrex.com:3000/';
const hubbellURL = suffix => `${HUBBELL_BASE_URL}${suffix}`;
export const HUBBELL_HOME_URL = hubbellURL('_');
export const HUBBELL_LOGIN_URL = hubbellURL('login');
export const RECEIVE_ORDER_URL = hubbellURL('receive-order');
export const RECORDS_URL = hubbellURL('records');
export const REPORTS_URL = hubbellURL('reports');
export const PROBLEMS_URL = hubbellURL('problems');
export const SCHEDULE_BUILDER_URL = hubbellURL('schedule-builder');
export const SCHEDULE_VIEWER_URL = hubbellURL('schedule-viewer');
export const GALVANIZING_RACK_URL = hubbellURL('galvanizing/rack');
export const GALVANIZING_RUN_URL = hubbellURL('galvanizing/run');
export const GALVANIZING_FINISH_URL = hubbellURL('galvanizing/finish');
export const SHIPPING_URL = hubbellURL('shipping');
export const HUB_USERNAME = 'sara@coveycs.com';
