import {
  HEADER_DROPDOWN_MENU,
  DROPDOWN_LOG_OUT_BUTTON,
} from './Header/selectors';
import {
  USER_INPUT,
  PASS_INPUT,
  LOGIN_BUTTON,
} from './Login/selectors';

export const waitForFunction = (
  periodicFunction: () => any,
  timeout: number = 5000,
  intervalLength: number = 100,
) => new Promise(async (resolve, reject) => {
  let currentTime = 0;
  const interval = setInterval(
    async () => {
      if (currentTime > timeout) {
        clearInterval(interval);
        return reject();
      }
      currentTime += intervalLength;
      if (await periodicFunction()) {
        clearInterval(interval);
        return resolve();
      }
    },
    intervalLength);
});

// export const waitForURLChange = (testObject: any, currentURL: string) => waitForFunction(async () => {
//   const newURL: string = await testObject.page.evaluate(() => location.href);
//   return currentURL !== newURL;
// });

export const isLoggedIn = async (testObject: any) => {
  if (await testObject.page.$(HEADER_DROPDOWN_MENU)) {
    return true;
  }
  return false;
};

export const login = async (testObject: any) => {
  await testObject.page.type(USER_INPUT, testObject.user.username);
  await testObject.page.type(PASS_INPUT, testObject.user.password);
  await testObject.page.click(LOGIN_BUTTON);
  await testObject.page.waitForSelector(HEADER_DROPDOWN_MENU);
};

export const logout = async (testObject: any) => {
  if (isLoggedIn) {
    await testObject.page.click(HEADER_DROPDOWN_MENU);
    await testObject.page.click(DROPDOWN_LOG_OUT_BUTTON);
  }
};
