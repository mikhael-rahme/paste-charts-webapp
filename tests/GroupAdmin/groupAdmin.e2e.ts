import {
  BASE_URL,
  GROUP_ADMIN_URL,
} from '../constants';
import {
  USER_GROUPS_HEADER,
  CREATE_GROUP_BUTTON,
  USER_GROUPS_TABLE,
  CREATE_GROUP_NAME_INPUT,
  CREATE_GROUP_DESCRIPTION_INPUT,
  CREATE_GROUP_SAVE_BUTTON,
} from './selectors';
import {
  isLoggedIn,
  login,
  waitForFunction,
} from '../helperFunctions';
import {
  Browser,
  ElementHandle,
  JSHandle,
  Page,
} from 'puppeteer';
import { headerTest } from '../Header/header.e2e';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';

const puppeteer = require('puppeteer');

const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type GroupsE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    title?: string,
    headerText?: string,
    button00?: ElementHandle,
    numberOfTableItems?: number,
  },
};

const elementQuerySetup = async (testObject: GroupsE2eTestObject) => {
  testObject.results.headerText = await testObject.page.$eval(USER_GROUPS_HEADER, (h1: HTMLHeadingElement) => h1.textContent);
  testObject.results.button00 = await testObject.page.$(CREATE_GROUP_BUTTON);
  testObject.results.numberOfTableItems =
    await testObject.page.$eval(USER_GROUPS_TABLE, (table: HTMLTableElement) => table.childNodes[1].childNodes.length);
};

const elementQueryTest = (getTestObject: () => GroupsE2eTestObject) => {
  describe('Newly opened Group Admin page contains:', () => {

    it('header with text "User Groups"', () => {
      expect(getTestObject().results.headerText).toEqual('User Groups');
    });

    it('Create Group button', () => {
      expect(getTestObject().results.button00).not.toBeNull();
    });

    it('table with more than 0 items', () => {
      expect(getTestObject().results.numberOfTableItems).toBeGreaterThan(0);
    });
  });
};

const createUserGroupSetup = async (testObject: GroupsE2eTestObject) => {
  await testObject.page.click(CREATE_GROUP_BUTTON);
  await testObject.page.type(CREATE_GROUP_NAME_INPUT, 'test-name');
  await testObject.page.type(CREATE_GROUP_DESCRIPTION_INPUT, 'test description');

  await testObject.page.click(CREATE_GROUP_SAVE_BUTTON);
};

const createUserGroupTest = async (testObject: GroupsE2eTestObject) => {

};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<GroupsE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (GroupsE2eTestObject) => any = async (testObject: GroupsE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(GROUP_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USER_GROUPS_HEADER),
    testObject.page.waitFor(CREATE_GROUP_BUTTON),
    testObject.page.waitFor(USER_GROUPS_TABLE),
  ]);
};

const before00Setup = async (testObject: GroupsE2eTestObject) => {
  dLog('before00Setup');

  testObject.browser = await puppeteer.launch();
  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');

  await testObject.page.goto(GROUP_ADMIN_URL);

  await login(testObject);
  dLog('At login');

  await testObject.page.goto(GROUP_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USER_GROUPS_HEADER),
    testObject.page.waitFor(CREATE_GROUP_BUTTON),
    testObject.page.waitFor(USER_GROUPS_TABLE),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: GroupsE2eTestObject) => {
  await elementQuerySetup(testObject);
};

const beforeTests = async (testObject: GroupsE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => GroupsE2eTestObject) => {
  elementQueryTest(getTestObject);
};

const after00Teardown = async (testObject: GroupsE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: GroupsE2eTestObject) => {
  after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end group admin page test',
  parallelBefore: false,
});
