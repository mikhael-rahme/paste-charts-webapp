// tslint:disable:max-line-length
export const USER_GROUPS_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 > div > h3';
export const CREATE_GROUP_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 > div > div > div > div:nth-child(1) > div > div > button';
export const USER_GROUPS_TABLE =
'#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 > div > div > div > div:nth-child(2) > div:nth-child(3) > table';

// create user group modal
export const CREATE_GROUP_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div > div > h3';

// export const CREATE_GROUP_HEADER =
// '#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div > div > h3';
export const CREATE_GROUP_NAME_INPUT =
'#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div > div > form > div.bottom-buttons.row > div > div:nth-child(1) > div:nth-child(2) > input';
export const CREATE_GROUP_DESCRIPTION_INPUT =
'#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div > div > form > div.bottom-buttons.row > div > div:nth-child(2) > div:nth-child(2) > input';
export const CREATE_GROUP_SAVE_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div > div > form > div.form-inline.save-buttons.row > div > div > button';
export const CREATE_GROUP_CANCEL_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div > div > form > div.form-inline.save-buttons.row > div > button';
export const GROUP_NAME_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div > div > form > div.bottom-buttons.row > div > div:nth-child(1) > label';
export const DESCRIPTION_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4 > div > div > form > div.bottom-buttons.row > div > div:nth-child(2) > label';

// Delete
export const DELETE_USER_GROUP_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 > div > div > div > div:nth-child(2) > div:nth-child(3) > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > button';
export const DELETE_USER_GROUP_HEADER =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-header > h4';
export const CONFIRM_DELETE_CHECKBOX =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > label > input';
export const CANCEL_DELETE_BUTTON =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > button.btn.btn-secondary';
export const DELETE_BUTTON =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > button.btn.btn-danger';

// Navigation
export const CREATE_USER_GROUP_NAVIGATION =
'#app > div > div > div:nth-child(3) > div > div > div > div.toggle-column.col.col-lg-4';

// Data Toggle
export const GROUP_NAME_TOGGLE =
'#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 > div > div > div > div:nth-child(2) > div:nth-child(3) > table > thead > tr > th:nth-child(1)';
export const DESCRIPTION_TOGGLE =
'#app > div > div > div:nth-child(3) > div > div > div > div.col.col-lg-8 > div > div > div > div:nth-child(2) > div:nth-child(3) > table > thead > tr > th:nth-child(2)';
