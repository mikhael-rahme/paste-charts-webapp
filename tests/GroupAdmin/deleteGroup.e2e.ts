import {
  BASE_URL,
  GROUP_ADMIN_URL,
} from '../constants';
import {
  USER_GROUPS_HEADER,
  CREATE_GROUP_BUTTON,
  USER_GROUPS_TABLE,
  CREATE_GROUP_NAME_INPUT,
  CREATE_GROUP_DESCRIPTION_INPUT,
  CREATE_GROUP_SAVE_BUTTON,
  DELETE_USER_GROUP_BUTTON,
  DELETE_BUTTON,
  DELETE_USER_GROUP_HEADER,
  CONFIRM_DELETE_CHECKBOX,
  CANCEL_DELETE_BUTTON,
  CREATE_GROUP_CANCEL_BUTTON,

} from './selectors';
import {
  isLoggedIn,
  login,
  waitForFunction,
} from '../helperFunctions';
import {
  Browser,
  ElementHandle,
  JSHandle,
  Page,
} from 'puppeteer';
import { headerTest } from '../Header/header.e2e';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import { reset } from 'redux-form';

const puppeteer = require('puppeteer');

const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type GroupsE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    title?: string,
    headerText?: string,
    button00?: ElementHandle,
    numberOfTableItems?: number,
    userGroupDeleteButton?: ElementHandle,
    deleteButton?: ElementHandle,
    deleteUserHeader?: ElementHandle,
    confirmDeleteCheckbox?: ElementHandle,
    cancelDeleteButton?: ElementHandle,
    saveButton?: ElementHandle,

  },
};

const elementQuerySetup = async (testObject: GroupsE2eTestObject) => {

  // Headers
  testObject.results.headerText = await testObject.page.$eval(USER_GROUPS_HEADER, (h1: HTMLHeadingElement) => h1.textContent);
  testObject.results.deleteUserHeader =
  await testObject.page.$eval(DELETE_USER_GROUP_HEADER, (header:HTMLHeadingElement) => header.textContent);

  // Buttons
  testObject.results.button00 = await testObject.page.$(CREATE_GROUP_BUTTON);
  testObject.results.userGroupDeleteButton = await testObject.page.$(DELETE_USER_GROUP_BUTTON);
  testObject.results.deleteButton = await testObject.page.$(DELETE_BUTTON);
  testObject.results.cancelDeleteButton = await testObject.page.$(CANCEL_DELETE_BUTTON);
  testObject.results.saveButton = await testObject.page.$(CREATE_GROUP_SAVE_BUTTON);

  // Checkboxes
  testObject.results.confirmDeleteCheckbox = await testObject.page.$(CONFIRM_DELETE_CHECKBOX);

  // Table
  testObject.results.numberOfTableItems =
    await testObject.page.$eval(USER_GROUPS_TABLE, (table: HTMLTableElement) => table.childNodes[1].childNodes.length);

};

const elementQueryTest = (getTestObject: () => GroupsE2eTestObject) => {
  describe('Delete Button Pop-Up Should Have:', () => {

    // Headers
    it('header with text "User Groups"', () => {
      expect(getTestObject().results.headerText).toEqual('User Groups');
    });

    it('Delete Group Header', () => {
      expect(getTestObject().results.deleteUserHeader).toEqual('Permanently Delete User Group');
    });

    // Buttons
    it('Create Group button', () => {
      expect(getTestObject().results.button00).not.toBeNull();
    });

    it('Delete Button' , () => {
      expect(getTestObject().results.deleteButton).not.toBeNull();
    });

    it('User Group Delete Button', () => {
      expect(getTestObject().results.userGroupDeleteButton).not.toBeNull();
    });

    it('Delete Cancel Button', () => {
      expect(getTestObject().results.cancelDeleteButton).not.toBeNull();
    });

    // Checkboxes
    it('Confirm Delete Checkbox', () => {
      expect(getTestObject().results.confirmDeleteCheckbox).not.toBeNull();
    });

    // Tables
    it('table with more than 0 items', () => {
      expect(getTestObject().results.numberOfTableItems).toBeGreaterThan(0);
    });
  });
};

// NOTE: There Must Be Items Within The Table For This To Work

// Test Setup For Delete Box, Confirm & Delete
const deleteUserGroupSetup = async (testObject: GroupsE2eTestObject) => {
  await testObject.page.click(DELETE_USER_GROUP_BUTTON);
  await testObject.page.type(DELETE_USER_GROUP_HEADER, 'Permanently Delete User Group');
  await testObject.page.click(CONFIRM_DELETE_CHECKBOX);
  await testObject.page.click(DELETE_BUTTON);
};

// Test For Delete Box, Confirm & Delete
const deleteUserGroupTest = async (getTestObject: () => GroupsE2eTestObject) => {
  describe('Delete Button Functionality, Confirm Check, Delete', () => {

    it('Delete User Group Button Click', () => {
      expect(getTestObject().results.userGroupDeleteButton.click);
    });
    it('Confirm Checkbox Clicked', () => {
      expect(getTestObject().results.confirmDeleteCheckbox.click);
    });
    it('Delete Button Clicked', () => {
      expect(getTestObject().results.deleteButton.click);
    });
  });
};

// Test Setup For Delete Box, Cancel
const deleteUserGroupCancelSetup = async (testObject: GroupsE2eTestObject) => {
  await testObject.page.click(DELETE_USER_GROUP_BUTTON);
  await testObject.page.type(DELETE_USER_GROUP_HEADER, 'Permanently Delete User Group');
  await testObject.page.click(CANCEL_DELETE_BUTTON);
};

// Test For Delete Box, Cancel
const deleteUserGroupCancelTest = async (getTestObject: () => GroupsE2eTestObject) => {
  describe('Delete Button Functionality, Cancel Button', () => {

    it('Delete User Group Button Click', () => {
      expect(getTestObject().results.userGroupDeleteButton.click);
    });
    it('Cancel Button Clicked', () => {
      expect(getTestObject().results.cancelDeleteButton.click);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<GroupsE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (GroupsE2eTestObject) => any = async (testObject: GroupsE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(GROUP_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USER_GROUPS_HEADER),
    testObject.page.waitFor(CREATE_GROUP_BUTTON),
    testObject.page.waitFor(USER_GROUPS_TABLE),
  ]);

  await testObject.page.click(DELETE_USER_GROUP_BUTTON);

  await Promise.all([
    testObject.page.waitFor(DELETE_USER_GROUP_HEADER),
    testObject.page.waitFor(DELETE_BUTTON),
    testObject.page.waitFor(CONFIRM_DELETE_CHECKBOX),
    testObject.page.waitFor(DELETE_BUTTON),
  ]);
  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before00Setup = async (testObject: GroupsE2eTestObject) => {

  // Navigation
  dLog('before00Setup');

  testObject.browser = await puppeteer.launch({
    // headless:false,
    // sloMo: 800,
  });

  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');

  await testObject.page.goto(GROUP_ADMIN_URL);

  await login(testObject);
  dLog('At login');

  await testObject.page.goto(GROUP_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USER_GROUPS_HEADER),
    testObject.page.waitFor(CREATE_GROUP_BUTTON),
    testObject.page.waitFor(USER_GROUPS_TABLE),
  ]);
  await testObject.page.click(DELETE_USER_GROUP_BUTTON);

  await Promise.all([
    testObject.page.waitFor(DELETE_USER_GROUP_HEADER),
    testObject.page.waitFor(DELETE_BUTTON),
    testObject.page.waitFor(CONFIRM_DELETE_CHECKBOX),
    testObject.page.waitFor(DELETE_BUTTON),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: GroupsE2eTestObject) => {
  await elementQuerySetup(testObject);
  await deleteUserGroupSetup(testObject);
  await deleteUserGroupCancelSetup(testObject);
};

const beforeTests = async (testObject: GroupsE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => GroupsE2eTestObject) => {
  elementQueryTest(getTestObject);
  deleteUserGroupTest(getTestObject);
  deleteUserGroupCancelTest(getTestObject);
};

const after00Teardown = async (testObject: GroupsE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: GroupsE2eTestObject) => {
  after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end group admin page test',
  parallelBefore: false,
});
