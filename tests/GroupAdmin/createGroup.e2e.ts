import {
  BASE_URL,
  GROUP_ADMIN_URL,
} from '../constants';
import {
  USER_GROUPS_HEADER,
  CREATE_GROUP_HEADER,
  CREATE_GROUP_BUTTON,
  CREATE_GROUP_SAVE_BUTTON,
  CREATE_GROUP_CANCEL_BUTTON,
  CREATE_GROUP_NAME_INPUT,
  CREATE_GROUP_DESCRIPTION_INPUT,
  GROUP_NAME_LABEL,
  DESCRIPTION_LABEL,
  USER_GROUPS_TABLE,

} from './selectors';
import {
  isLoggedIn,
  login,
  waitForFunction,
} from '../helperFunctions';
import {
  Browser,
  ElementHandle,
  JSHandle,
  Page,
} from 'puppeteer';
import { headerTest } from '../Header/header.e2e';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import { textSpanContainsPosition } from '../../node_modules/typescript';
import { Input, Label } from 'reactstrap';

const puppeteer = require('puppeteer');

const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type GroupsE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    title?: string,
    headerText?: string,
    button00?: ElementHandle,
    numberOfTableItems?: number,
    createUserGroupHeader?: string;
    groupNameLabel?: string;
    descriptionLabel?: string;
    saveButton?: ElementHandle;
    cancelButton?: ElementHandle;
    groupNameInput?: boolean;
    descriptionInput?: boolean
  },
};

const elementQuerySetup = async (testObject: GroupsE2eTestObject) => {

  // Headers
  testObject.results.headerText = await testObject.page.$eval(USER_GROUPS_HEADER, (h1: HTMLHeadingElement) => h1.textContent);
  testObject.results.createUserGroupHeader = await testObject.page.$eval(CREATE_GROUP_HEADER, (h1:HTMLHeadElement) => h1.textContent);

  // Buttons
  testObject.results.button00 = await testObject.page.$(CREATE_GROUP_BUTTON);
  testObject.results.saveButton = await testObject.page.$(CREATE_GROUP_SAVE_BUTTON);
  testObject.results.cancelButton = await testObject.page.$(CREATE_GROUP_CANCEL_BUTTON);

  // Inputs
  testObject.results.descriptionInput = (await testObject.page.$(CREATE_GROUP_DESCRIPTION_INPUT)) !== null;
  testObject.results.groupNameInput = (await testObject.page.$(CREATE_GROUP_NAME_INPUT)) !== null;

// Labels
  testObject.results.groupNameLabel = (await testObject.page.$eval(GROUP_NAME_LABEL, (label:HTMLLabelElement) => label.textContent));
  testObject.results.descriptionLabel = (await testObject.page.$eval(DESCRIPTION_LABEL, (label:HTMLLabelElement) => label.textContent));

  // Tables
  testObject.results.numberOfTableItems =
  await testObject.page.$eval(USER_GROUPS_TABLE, (table: HTMLTableElement) => table.childNodes[1].childNodes.length);

};

const elementQueryTest = (getTestObject: () => GroupsE2eTestObject) => {
  describe('Newly Opened Create User Group Modal', () => {

    // Test Headers
    it('header with text "User Groups"', () => {
      expect(getTestObject().results.headerText).toEqual('User Groups');
    });

    it('Create User Group Header ', () => {
      expect(getTestObject().results.createUserGroupHeader).toEqual('Create User Group');
    });

    // Test Labels
    it('Group name Label', () => {
      expect(getTestObject().results.groupNameLabel).toEqual('Group Name *');
    });

    it('Description Label', () => {
      expect(getTestObject().results.descriptionLabel).toEqual('Description');
    });

    // Test Buttons
    it('Create Group button', () => {
      expect(getTestObject().results.button00).not.toBeNull();
    });

    it('Save Button', () => {
      expect(getTestObject().results.saveButton).not.toBeNull();
    });

    it('Cancel Button', () => {
      expect(getTestObject().results.cancelButton).not.toBeNull();
    });

    // Test Input
    it('Group Name Input', () => {
      expect(getTestObject().results.groupNameInput).toEqual(true);
    });

    it('Description Input', () => {
      expect(getTestObject().results.descriptionInput).toEqual(true);
    });

    // Test Table
    it('table with more than 0 items', () => {
      expect(getTestObject().results.numberOfTableItems).toBeGreaterThan(0);
    });
  });
};

// Test Setup For Create Group Button Click & Save Button Click
const createUserGroupSetup = async (testObject: GroupsE2eTestObject) => {
  await testObject.page.click(CREATE_GROUP_BUTTON);
  await testObject.page.type(CREATE_GROUP_NAME_INPUT, 'test-name');
  await testObject.page.type(CREATE_GROUP_DESCRIPTION_INPUT, 'test description');

  await testObject.page.type(CREATE_GROUP_HEADER, 'test header');
  await testObject.page.click(CREATE_GROUP_SAVE_BUTTON);
};
// Test For Create Group Button Click & Save Button Click
const createUserGroupTest = async (getTestObject: () => GroupsE2eTestObject) => {
  describe('Create Group Button Click, Save Click', () => {

    it('Create Group Click', () => {
      expect(getTestObject().results.button00.click);
    });
    it('Save Create Group', () => {
      expect(getTestObject().results.saveButton.click);
    });
  });
};

// Test Setup For Create Group Button Click & Cancel Button Click
const createUserGroupCancelSetup = async (testObject: GroupsE2eTestObject) => {
  await testObject.page.click(CREATE_GROUP_BUTTON);
  await testObject.page.type(CREATE_GROUP_NAME_INPUT, 'test-name');
  await testObject.page.type(CREATE_GROUP_DESCRIPTION_INPUT, 'test description');

  await testObject.page.type(CREATE_GROUP_HEADER, 'test header');
  await testObject.page.click(CREATE_GROUP_CANCEL_BUTTON);
};
// Test For Create Group Button Click & Cancel Button Click
const createUserGroupCancelTest = async (getTestObject: () => GroupsE2eTestObject) => {
  describe('Create Group Button Click, Cancel Click', () => {

    it('Create Group Click', () => {
      expect(getTestObject().results.button00.click);
    });
    it('Cancel Create Group', () => {
      expect(getTestObject().results.cancelButton.click);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<GroupsE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (GroupsE2eTestObject) => any = async (testObject: GroupsE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(GROUP_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USER_GROUPS_HEADER),
    testObject.page.waitFor(CREATE_GROUP_BUTTON),
    testObject.page.waitFor(USER_GROUPS_TABLE),
  ]);

  await testObject.page.click(CREATE_GROUP_BUTTON);

  await Promise.all([
    testObject.page.waitFor(GROUP_NAME_LABEL),
    testObject.page.waitFor(CREATE_GROUP_NAME_INPUT),
    testObject.page.waitFor(DESCRIPTION_LABEL),
    testObject.page.waitFor(CREATE_GROUP_DESCRIPTION_INPUT),
    testObject.page.waitFor(CREATE_GROUP_SAVE_BUTTON),
    testObject.page.waitFor(CREATE_GROUP_CANCEL_BUTTON),
    testObject.page.waitFor(CREATE_GROUP_HEADER),
  ]);
  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before00Setup = async (testObject: GroupsE2eTestObject) => {

  // Navigation
  dLog('before00Setup');

  testObject.browser = await puppeteer.launch({
    // headless:false,
    // sloMo: 800,
  });

  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');

  await testObject.page.goto(GROUP_ADMIN_URL);

  await login(testObject);
  dLog('At login');

  await testObject.page.goto(GROUP_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USER_GROUPS_HEADER),
    testObject.page.waitFor(CREATE_GROUP_BUTTON),
    testObject.page.waitFor(USER_GROUPS_TABLE),
  ]);
  await testObject.page.click(CREATE_GROUP_BUTTON);

  await Promise.all([
    testObject.page.waitFor(GROUP_NAME_LABEL),
    testObject.page.waitFor(CREATE_GROUP_NAME_INPUT),
    testObject.page.waitFor(DESCRIPTION_LABEL),
    testObject.page.waitFor(CREATE_GROUP_DESCRIPTION_INPUT),
    testObject.page.waitFor(CREATE_GROUP_SAVE_BUTTON),
    testObject.page.waitFor(CREATE_GROUP_CANCEL_BUTTON),
    testObject.page.waitFor(CREATE_GROUP_HEADER),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: GroupsE2eTestObject) => {
  await elementQuerySetup(testObject);
  await createUserGroupSetup(testObject);
  await createUserGroupCancelSetup(testObject);
};

const beforeTests = async (testObject: GroupsE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => GroupsE2eTestObject) => {
  elementQueryTest(getTestObject);
  createUserGroupTest(getTestObject);
  createUserGroupCancelTest(getTestObject);
};

const after00Teardown = async (testObject: GroupsE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: GroupsE2eTestObject) => {
  after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end group admin page test',
  parallelBefore: false,
});
