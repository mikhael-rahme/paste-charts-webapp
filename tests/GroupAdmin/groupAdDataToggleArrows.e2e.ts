import {
  BASE_URL,
  GROUP_ADMIN_URL,
} from '../constants';
import {
  USER_GROUPS_HEADER,
  CREATE_GROUP_BUTTON,
  USER_GROUPS_TABLE,
  CREATE_GROUP_NAME_INPUT,
  CREATE_GROUP_DESCRIPTION_INPUT,
  CREATE_GROUP_SAVE_BUTTON,
  GROUP_NAME_TOGGLE,
  DESCRIPTION_TOGGLE,
} from './selectors';
import {
  isLoggedIn,
  login,
  waitForFunction,
} from '../helperFunctions';
import {
  Browser,
  ElementHandle,
  JSHandle,
  Page,
} from 'puppeteer';
import { headerTest } from '../Header/header.e2e';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';

const puppeteer = require('puppeteer');

const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type GroupsE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    title?: string,
    headerText?: string,
    button00?: ElementHandle,
    numberOfTableItems?: number,
    group_name_toggle?: ElementHandle,
    description_toggle?: ElementHandle,
    save_group_button?: ElementHandle,
  },
};

const elementQuerySetup = async (testObject: GroupsE2eTestObject) => {
  // Headers
  testObject.results.headerText = await testObject.page.$eval(USER_GROUPS_HEADER, (h1: HTMLHeadingElement) => h1.textContent);

  // Buttons
  testObject.results.button00 = await testObject.page.$(CREATE_GROUP_BUTTON);

  // Table
  testObject.results.numberOfTableItems =
    await testObject.page.$eval(USER_GROUPS_TABLE, (table: HTMLTableElement) => table.childNodes[1].childNodes.length);

  // Toggle
  testObject.results.group_name_toggle = await testObject.page.$(GROUP_NAME_TOGGLE);
  testObject.results.description_toggle = await testObject.page.$(DESCRIPTION_TOGGLE);
};

const elementQueryTest = (getTestObject: () => GroupsE2eTestObject) => {
  describe('Newly opened Group Admin page contains:', () => {

    // Toggle Tests
    it('Group Name Toggle', async () => {
      expect(getTestObject().results.group_name_toggle).not.toBeNull();
    });
    it('Description Toggle', async () => {
      expect(getTestObject().results.description_toggle).not.toBeNull();
    });

    // Original Page Content
    it('header with text "User Groups"', () => {
      expect(getTestObject().results.headerText).toEqual('User Groups');
    });

    it('Create Group button', () => {
      expect(getTestObject().results.button00).not.toBeNull();
    });

    it('table with more than 0 items', () => {
      expect(getTestObject().results.numberOfTableItems).toBeGreaterThan(0);
    });
  });
};

const dataToggleSetup = async (testObject: GroupsE2eTestObject) => {
  await testObject.page.click(GROUP_NAME_TOGGLE);
  await testObject.page.click(DESCRIPTION_TOGGLE);
};
// Create User Cancel Test
const dataToggleTest = async (getTestObject: () => GroupsE2eTestObject) => {
  describe('Data Toggle Functionality', () => {
    it('Group Name Arrow Toggle', async () => {
      expect(getTestObject().results.group_name_toggle.click);
    });
    it('Description Data Toggle', async () => {
      expect(getTestObject().results.description_toggle.click);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<GroupsE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (GroupsE2eTestObject) => any = async (testObject: GroupsE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(GROUP_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USER_GROUPS_HEADER),
    testObject.page.waitFor(CREATE_GROUP_BUTTON),
    testObject.page.waitFor(USER_GROUPS_TABLE),
    testObject.page.waitFor(GROUP_NAME_TOGGLE),
    testObject.page.waitFor(DESCRIPTION_TOGGLE),
  ]);
};

const before00Setup = async (testObject: GroupsE2eTestObject) => {
  dLog('before00Setup');

  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo: 800,
  });
  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');

  await testObject.page.goto(GROUP_ADMIN_URL);

  await login(testObject);
  dLog('At login');

  await testObject.page.goto(GROUP_ADMIN_URL);

  await Promise.all([
    testObject.page.waitFor(USER_GROUPS_HEADER),
    testObject.page.waitFor(CREATE_GROUP_BUTTON),
    testObject.page.waitFor(USER_GROUPS_TABLE),
    testObject.page.waitFor(GROUP_NAME_TOGGLE),
    testObject.page.waitFor(DESCRIPTION_TOGGLE),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: GroupsE2eTestObject) => {
  await elementQuerySetup(testObject);
  await dataToggleSetup(testObject);
};

const beforeTests = async (testObject: GroupsE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => GroupsE2eTestObject) => {
  elementQueryTest(getTestObject);
  dataToggleTest(getTestObject);
};

const after00Teardown = async (testObject: GroupsE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: GroupsE2eTestObject) => {
  after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end group admin page test',
  parallelBefore: false,
});
