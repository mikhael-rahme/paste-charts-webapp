export const REACTD3_GRAPH_HEADER = '#app > div > div > div:nth-child(3) > div > div.container__form > h4';
export const GRAPH_CONFIG_HEADER = '#app > div > div > div:nth-child(3) > div > div.container__form > h3';
export const GRAPH_CONFIG_INFO = '#app > div > div > div:nth-child(3) > div > div.container__form > div';
export const NODES_LINKS_INFO = '#app > div > div > div:nth-child(3) > div > div:nth-child(1) > span';
export const FULLSCREEN_BUTTON = '#app > div > div > div:nth-child(3) > div > div:nth-child(1) > button';
export const GRAPH = '#graph-graph-wrapper > svg';
export const GRAPH_DATA_HEADER = '#app > div > div > div:nth-child(3) > div > div.container__graph-data > h2';
export const GRAPH_DATA_INFO = '#app > div > div > div:nth-child(3) > div > div.container__graph-data > div > div';
