// import {
//   BASE_URL,
//   D3_GRAPH_URL,
// } from '../constants';
// import {
//   REACTD3_GRAPH_HEADER,
//   GRAPH_CONFIG_HEADER,
//   GRAPH_CONFIG_INFO,
//   NODES_LINKS_INFO,
//   FULLSCREEN_BUTTON,
//   GRAPH,
//   GRAPH_DATA_HEADER,
//   GRAPH_DATA_INFO,
// } from './selectors';
// import { isLoggedIn, login } from '../helperFunctions';
// import { Browser, Page, ElementHandle } from 'puppeteer';
// import { headerTest } from '../Header/header.e2e';
// import {
//   TestCredentials,
//   TestCredentialUser,
//   runTestsForAllUsers,
// } from 'toolkit/testUtils';
// import { IterrexRole, IterrexSystemRole } from 'toolkit';

// const puppeteer = require('puppeteer');

// export type D3GraphE2eTestObject = {
//   browser: Browser,
//   page: Page,
//   testCredentials: TestCredentials,
//   cognitoIdToken: string,
//   user?: TestCredentialUser,
//   results: {
//     d3GraphHeaderText?: string,
//     graphConfigHeaderText?: string,
//     graphDataHeaderText?: string,
//     graph?: ElementHandle,
//     button00?: ElementHandle,
//     nodesLinksInfo?: ElementHandle,
//     graphConfigInfo?: ElementHandle,
//     graphDataInfo?: ElementHandle,
//   },
// };

// const elementQuerySetup = async (testObject: D3GraphE2eTestObject) => {
//   testObject.results.d3GraphHeaderText = await testObject.page.$eval(REACTD3_GRAPH_HEADER, header => header.innerHTML);
//   testObject.results.graphConfigHeaderText = await testObject.page.$eval(GRAPH_CONFIG_HEADER, header => header.innerHTML);
//   testObject.results.graphDataHeaderText = await testObject.page.$eval(GRAPH_DATA_HEADER, header => header.innerHTML);
//   testObject.results.graph = await testObject.page.$(GRAPH);
//   testObject.results.button00 = await testObject.page.$(FULLSCREEN_BUTTON);
//   testObject.results.nodesLinksInfo = await testObject.page.$(NODES_LINKS_INFO);
//   testObject.results.graphConfigInfo = await testObject.page.$(GRAPH_CONFIG_INFO);
//   testObject.results.graphDataInfo = await testObject.page.$(GRAPH_DATA_INFO);
// };

// const elementQueryTest = (getTestObject: () => D3GraphE2eTestObject) => {
//   describe('Change password modal contains:', () => {
//     it('header with "Change Password" text', () => {
//       expect(getTestObject().results.d3GraphHeaderText).toEqual('Iterrex ReactD3 Graph');
//     });

//     it('old password input', () => {
//       expect(getTestObject().results.graphConfigHeaderText).toEqual('Graph Config');
//     });

//     it('new password input', () => {
//       expect(getTestObject().results.graphDataHeaderText).toEqual('Graph Data');
//     });

//     it('confirm new password input', () => {
//       expect(getTestObject().results.graph).not.toBeNull();
//     });

//     it('save button', () => {
//       expect(getTestObject().results.button00).not.toBeNull();
//     });

//     it('cancel button', () => {
//       expect(getTestObject().results.nodesLinksInfo).not.toBeNull();
//     });

//     it('save button', () => {
//       expect(getTestObject().results.graphConfigInfo).not.toBeNull();
//     });

//     it('cancel button', () => {
//       expect(getTestObject().results.graphDataInfo).not.toBeNull();
//     });
//   });
// };

// const createTestObject = async ({
//   cognitoIdToken,
//   testCredentials,
//   tenantName,
// }: {
//   cognitoIdToken: string,
//   testCredentials: TestCredentials,
//   tenantName?: string,
// }): Promise<D3GraphE2eTestObject> => {
//   // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
//   // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
//   return {
//     testCredentials,
//     cognitoIdToken,
//     browser: null,
//     page: null,
//     results: {},
//     user: {
//       username: 'garett@coveycs.com',
//       password: 'testPassword1!',
//       iterrexRole: null,
//       cognitoIdToken: null,
//       sessionToken: null,
//       bearerAuthorizationHeader: null,
//       basicAuthorizationHeader: null,
//     },
//   };
// };

// const newPage: (E2eTestObject) => any = async (testObject: D3GraphE2eTestObject) => {
//   await testObject.page.close();
//   testObject.page = await testObject.browser.newPage();

//   await testObject.page.goto(D3_GRAPH_URL);

//   await Promise.all([
//     testObject.page.waitFor(REACTD3_GRAPH_HEADER),
//     testObject.page.waitFor(GRAPH_CONFIG_HEADER),
//     testObject.page.waitFor(GRAPH_DATA_HEADER),
//     testObject.page.waitFor(GRAPH),
//   ]);
// };

// const before00Setup = async (testObject: D3GraphE2eTestObject) => {
//   testObject.browser = await puppeteer.launch({
//     headless: true,
//     sloMo: 800,
//   });
//   testObject.page = await testObject.browser.newPage();
//   await testObject.page.goto(BASE_URL);

//   await login(testObject);
//   await testObject.page.goto(D3_GRAPH_URL);
//   await Promise.all([
//     testObject.page.waitFor(REACTD3_GRAPH_HEADER),
//     testObject.page.waitFor(GRAPH_CONFIG_HEADER),
//     testObject.page.waitFor(GRAPH_DATA_HEADER),
//     testObject.page.waitFor(GRAPH),
//   ]);

//   await testObject.page.setViewport({ width: 1920, height: 1080 });
// };

// const before01Run = async (testObject: D3GraphE2eTestObject) => {
//   await elementQuerySetup(testObject);
// };

// const beforeTests = async (testObject: D3GraphE2eTestObject) => {
//   await before00Setup(testObject);
//   await before01Run(testObject);

//   return testObject;
// };

// const runTests = (iterrexRole: IterrexRole, getTestObject: () => D3GraphE2eTestObject) => {
//   elementQueryTest(getTestObject);
// };

// const after00Teardown = async (testObject: D3GraphE2eTestObject) => {
//   await testObject.page.close();
//   await testObject.browser.close();
// };

// const afterTests = async (testObject: D3GraphE2eTestObject) => {
//   after00Teardown(testObject);

//   return testObject;
// };

// runTestsForAllUsers({
//   createTestObject,
//   beforeTests,
//   afterTests,
//   runTests,
//   testCredentials: global.testCredentials,
//   testName: 'End-to-end home page test',
// });
