import {
  PASSWORD,
  BASE_URL,
  BODY_SELECTOR,
  HOME_URL,
} from '../constants';
import {
  CHANGE_PASSWORD_HEADER,
  OLD_INPUT,
  NEW_INPUT,
  CONFIRM_INPUT,
  SAVE_BUTTON,
  CANCEL_BUTTON,
  MISMATCH_WARNING,
} from './selectors';
import {
  HEADER_DROPDOWN_MENU,
  DROPDOWN_CHANGE_PASSWORD_BUTTON,
} from '../Header/selectors';
import {
  isLoggedIn,
  login,
  waitForFunction,
  logout,
} from '../helperFunctions';
import {
  Browser,
  Page,
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentials,
  TestCredentialUser,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import {
  IterrexRole,
  IterrexSystemRole,
} from 'toolkit';
const puppeteer = require('puppeteer');


export type ChangePasswordE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    // elementQuerySetup results
    headerText?: string,
    input00?: ElementHandle,
    input01?: ElementHandle,
    input02?: ElementHandle,
    button00?: ElementHandle,
    button01?: ElementHandle,
    // changePasswordMismatchSetup results
    inputValue00?: string,
    inputValue01?: string,
    inputValue02?: string,
    warning00?: string,
    header00?: ElementHandle,
    // changePasswordInvalidSetup results
    warning01?: string,
    passwordChanged00?: boolean,
    // changePasswordValidSetup results
    warning02?: string,
    passwordChanged01?: boolean,
  },
};

const elementQuerySetup = async (testObject: ChangePasswordE2eTestObject) => {
  testObject.results.headerText = await testObject.page.$eval(CHANGE_PASSWORD_HEADER, header => header.innerHTML);
  testObject.results.input00 = await testObject.page.$(OLD_INPUT);
  testObject.results.input01 = await testObject.page.$(NEW_INPUT);
  testObject.results.input02 = await testObject.page.$(CONFIRM_INPUT);
  testObject.results.button00 = await testObject.page.$(SAVE_BUTTON);
  testObject.results.button01 = await testObject.page.$(CANCEL_BUTTON);
};

const elementQueryTest = (getTestObject: () => ChangePasswordE2eTestObject) => {
  describe('Change password modal contains:', () => {
    it('header with "Change Password" text', () => {
      expect(getTestObject().results.headerText).toEqual('Change Password');
    });

    it('old password input', () => {
      expect(getTestObject().results.input00).not.toBeNull();
    });

    it('new password input', () => {
      expect(getTestObject().results.input01).not.toBeNull();
    });

    it('confirm new password input', () => {
      expect(getTestObject().results.input02).not.toBeNull();
    });

    it('save button', () => {
      expect(getTestObject().results.button00).not.toBeNull();
    });

    it('cancel button', () => {
      expect(getTestObject().results.button01).not.toBeNull();
    });
  });
};

const changePasswordMismatchSetup = async (testObject: ChangePasswordE2eTestObject) => {
  await testObject.page.type(OLD_INPUT, 'invalid1');
  testObject.results.inputValue00 = await testObject.page.$eval(OLD_INPUT, (input: HTMLInputElement) => input.value);

  await testObject.page.type(NEW_INPUT, 'invalid2');
  testObject.results.inputValue01 = await testObject.page.$eval(NEW_INPUT, (input: HTMLInputElement) => input.value);

  await testObject.page.type(CONFIRM_INPUT, 'invalid3');
  testObject.results.inputValue02 = await testObject.page.$eval(CONFIRM_INPUT, (input: HTMLInputElement) => input.value);

  testObject.results.warning00 = await testObject.page.$eval(MISMATCH_WARNING, warning => warning.innerHTML);

  await testObject.page.click(SAVE_BUTTON);
  testObject.results.header00 = await testObject.page.$(CHANGE_PASSWORD_HEADER);
};

const changePasswordMismatchTest = (getTestObject: () => ChangePasswordE2eTestObject) => {
  describe('Attempting to change password with mismatched confirmation input', () => {
    it('should allow old password input', () => {
      expect(getTestObject().results.inputValue00).toEqual('invalid1');
    });

    it('should allow new password input', () => {
      expect(getTestObject().results.inputValue01).toEqual('invalid2');
    });

    it('should allow confirm password input', () => {
      expect(getTestObject().results.inputValue02).toEqual('invalid3');
    });

    it('should display password mismatch warning', () => {
      expect(getTestObject().results.warning00).toEqual('Passwords must match');
    });

    it('should not succeed on save button click', () => {
      expect(getTestObject().results.header00).not.toBeNull();
    });
  });
};

const changePasswordInvalidSetup = async (testObject: ChangePasswordE2eTestObject) => {
  await testObject.page.type(OLD_INPUT, 'invalid');
  await testObject.page.type(NEW_INPUT, 'valid');
  await testObject.page.type(CONFIRM_INPUT, 'valid');

  testObject.results.warning01 = await testObject.page.$eval(MISMATCH_WARNING, warning => warning.innerHTML);

  await testObject.page.click(SAVE_BUTTON);
  await waitForFunction(async () => !(await testObject.page.$eval(BODY_SELECTOR, body => body.outerHTML)).includes('modal-open'))
  .then(() => testObject.results.passwordChanged00 = true)
  .catch(() => testObject.results.passwordChanged00 = false);
};

const changePasswordInvalidTest = (getTestObject: () => ChangePasswordE2eTestObject) => {
  describe('Attempting to change password with invalid old password', () => {
    it('should not display password mismatch warning', () => {
      expect(getTestObject().results.warning01).toEqual('');
    });

    it('should not succeed on save button click', () => {
      expect(getTestObject().results.passwordChanged00).toEqual(false);
    });
  });
};

const changePasswordValidSetup = async (testObject: ChangePasswordE2eTestObject) => {
  await testObject.page.type(OLD_INPUT, testObject.user.password);
  await testObject.page.type(NEW_INPUT, 'newPass');
  await testObject.page.type(CONFIRM_INPUT, 'newPass');

  testObject.results.warning02 = await testObject.page.$eval(MISMATCH_WARNING, warning => warning.innerHTML);

  await testObject.page.click(SAVE_BUTTON);
  await waitForFunction(async () => !(await testObject.page.$eval(BODY_SELECTOR, body => body.outerHTML)).includes('modal-open'))
  .then(() => testObject.results.passwordChanged00 = true)
  .catch(() => testObject.results.passwordChanged00 = false);
};

const changePasswordValidTest = (getTestObject: () => ChangePasswordE2eTestObject) => {
  describe('Attempting to change password with valid input', () => {
    it('should not display password mismatch warning', () => {
      expect(getTestObject().results.warning02).toEqual('');
    });

    it('should succeed on save button click', () => {
      expect(getTestObject().results.passwordChanged01).toEqual(true);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<ChangePasswordE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (E2eTestObject) => any = async (testObject: ChangePasswordE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(HOME_URL);

  await testObject.page.waitFor(HEADER_DROPDOWN_MENU);
  await testObject.page.click(HEADER_DROPDOWN_MENU);
  await testObject.page.click(DROPDOWN_CHANGE_PASSWORD_BUTTON);

  await Promise.all([
    testObject.page.waitFor(CHANGE_PASSWORD_HEADER),
    testObject.page.waitFor(OLD_INPUT),
    testObject.page.waitFor(NEW_INPUT),
    testObject.page.waitFor(CONFIRM_INPUT),
    testObject.page.waitFor(SAVE_BUTTON),
    testObject.page.waitFor(CANCEL_BUTTON),
  ]);
};

const before00Setup = async (testObject: ChangePasswordE2eTestObject) => {
  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo: 800,
  });
  testObject.page = await testObject.browser.newPage();
  await testObject.page.goto(BASE_URL);

  await login(testObject);
  await testObject.page.goto(HOME_URL);

  await testObject.page.waitFor(HEADER_DROPDOWN_MENU);
  await testObject.page.click(HEADER_DROPDOWN_MENU);
  await testObject.page.click(DROPDOWN_CHANGE_PASSWORD_BUTTON);

  await Promise.all([
    testObject.page.waitFor(CHANGE_PASSWORD_HEADER),
    testObject.page.waitFor(OLD_INPUT),
    testObject.page.waitFor(NEW_INPUT),
    testObject.page.waitFor(CONFIRM_INPUT),
    testObject.page.waitFor(SAVE_BUTTON),
    testObject.page.waitFor(CANCEL_BUTTON),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: ChangePasswordE2eTestObject) => {
  await elementQuerySetup(testObject);
  await newPage(testObject);
  await changePasswordMismatchSetup(testObject);
  await newPage(testObject);
  await changePasswordInvalidSetup(testObject);
  await newPage(testObject);
  await changePasswordValidSetup(testObject);
};

const beforeTests = async (testObject: ChangePasswordE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => ChangePasswordE2eTestObject) => {
  elementQueryTest(getTestObject);
  changePasswordMismatchTest(getTestObject);
  changePasswordInvalidTest(getTestObject);
  changePasswordValidTest(getTestObject);
};

const after00Teardown = async (testObject: ChangePasswordE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: ChangePasswordE2eTestObject) => {
  after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end home page test',
  parallelBefore: false,
});
