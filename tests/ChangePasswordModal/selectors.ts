export const CHANGE_PASSWORD_HEADER =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-header > h4';
export const OLD_INPUT =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-body > form > div:nth-child(1) > div:nth-child(2) > input';
export const NEW_INPUT =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-body > form > div:nth-child(2) > div:nth-child(2) > input';
export const CONFIRM_INPUT =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-body > form > div:nth-child(3) > div:nth-child(2) > input';
export const SAVE_BUTTON =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > button.btn.btn-success';
export const CANCEL_BUTTON =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > button.btn.btn-secondary';
export const MISMATCH_WARNING =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-body > form > div:nth-child(4) > small';
