
import {
  TestCredentialUser,
  TestCredentials,
} from 'toolkit/testUtils';
import {
  Browser,
  Page,
} from 'puppeteer';

export interface E2ETestObject {
  browser: Browser;
  page: Page;
  testCredentials: TestCredentials;
  cognitoIdToken: string;
  user?: TestCredentialUser;
}
