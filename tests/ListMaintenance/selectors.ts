// tslint:disable:max-line-length
// Original Page Content
export const LISTS_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div.col.col-sm-3 > div > h3';
export const CREATE_LIST_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div.col.col-sm-3 > div > div > div > div > button';
export const READ_ONLY_FILTER =
'#app > div > div > div:nth-child(3) > div > div > div.col.col-sm-3 > div > div > div > div > ul > li:nth-child(1) > a';
export const EDITABLE_FILTER =
'#app > div > div > div:nth-child(3) > div > div > div.col.col-sm-3 > div > div > div > div > ul > li:nth-child(2) > a';
export const DELETED_FILTER =
'#app > div > div > div:nth-child(3) > div > div > div.col.col-sm-3 > div > div > div > div > ul > li:nth-child(3) > a';
export const LISTS_TABLE =
'#app > div > div > div:nth-child(3) > div > div > div.col.col-sm-3 > div > div > div > div > div > div:nth-child(3) > table';
export const TABLE_PAGE_NAVIGATION =
'#app > div > div > div:nth-child(3) > div > div > div.col.col-sm-3 > div > div > div > div > div > div:nth-child(4) > nav';
export const NEXT_PAGE_TABLE_NAV =
'#app > div > div > div:nth-child(3) > div > div > div.col.col-sm-3 > div > div > div > div > div > div:nth-child(4) > nav > ul > li:nth-child(4) > a';
export const PREV_PAGE_TABLE_NAV =
'#app > div > div > div:nth-child(3) > div > div > div.col.col-sm-3 > div > div > div > div > div > div:nth-child(4) > nav > ul > li.page-item.disabled > a';
export const LIST_SAVED_OR_DELETED_SUCCESS =
'#app > div > div > span > span > div.bottom-right > span > div';

// Create List Constant Selectors
export const CREATE_LIST_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > h3';
export const LIST_PROPERTIES_TAB =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > ul > li:nth-child(1)';
export const VIEW_EDIT_LIST_ITEMS_TAB =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > ul > li:nth-child(2) > a';
export const GROUPS_TAB =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > ul > li:nth-child(3)';
export const SAVE_LIST_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.form-inline.save-buttons.row > div > div > button';
export const CANCEL_LIST_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.form-inline.save-buttons.row > div > button';

// Edit List
export const EDIT_LIST_BUTTON = '#app > div > div > div:nth-child(3) > div > div > div.col.col-sm-3 > div > div > div > div > div > div:nth-child(3) > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > button.btn.btn-warning > span';
export const EDIT_LIST_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > h3';
export const CREATED_BY_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > form > table > tbody > tr:nth-child(1)';
export const DATE_CREATED_LABEL =
'##app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > form > table > tbody > tr:nth-child(2) > th';
export const DATE_MOD_LABEL =
'##app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > form > table > tbody > tr:nth-child(3) > th';
export const CREATED_BY_INFO =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > form > table > tbody > tr:nth-child(1) > td';
export const DATE_CREATED_INFO =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > form > table > tbody > tr:nth-child(2) > td';
export const DATE_MOD_INFO =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > form > table > tbody > tr:nth-child(3) > td';
export const EDIT_INFO_TABLE =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > form > table';

// List Properties Tab Selectors
export const LIST_PROPERTIES_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > h5';
export const LIST_NAME_INPUT =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > form > div.row.form-group > div:nth-child(2) > input';
export const LIST_ITEM_PROPERTIES_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > form > h5';
export const ADD_PROPERTY_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > form > div:nth-child(5) > div > button';
export const LIST_PROPERTY_TABLE =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > form > div:nth-child(5) > div > div > div:nth-child(3) > table';
export const LIST_NAME_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > form > div.row.form-group > label';

// List Item Property Selectors
export const CREATE_LIST_ITEM_PROPERTY_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > h3';
export const PROPERTY_NAME_INPUT =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(2) > div:nth-child(2) > input';
export const TYPE_DROPDOWN =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(3) > div:nth-child(2)';
export const REQUIRED_CHECKBOX =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(4) > div:nth-child(2) > input';
export const CREATE_ANOTHER_CHECKBOX =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.form-inline.save-buttons.row > div > div > label > input';
export const SAVE_LIST_PROPERTY_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.form-inline.save-buttons.row > div > div > button';
export const CANCEL_ITEM_PROPERTY_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.form-inline.save-buttons.row > div > button';
export const DISABLED_SAVE_LIST_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.form-inline.save-buttons.row > div > div > button';
export const SUCCESS_SAVE_LIST =
'#app > div > div > span > span > div.bottom-right > span > div';
export const PROPERTY_NAME_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(2) > label';
export const TYPE_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(3) > label';
export const REQUIRED_CHECKBOX_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(4) > label';
export const CREATE_ANOTHER_CHECKBOX_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.form-inline.save-buttons.row > div > div > label > p';
export const LIST_PROPERTIES_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > h5';
export const LIST_ITEM_PROPERTIES_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > form > h5';

// Delete List Property Selectors
export const DELETE_LIST_PROPERTY_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > form > div:nth-child(5) > div > div > div:nth-child(3) > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > button.btn.btn-danger';
export const PERMANENTLY_DELETE_LIST_PROP_HEADER =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-header > h4';
export const DELETE_CONFIRM_QUESTION_LABEL =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-body';
export const DELETE_LIST_PROPERTY_POPUP_MODAL =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div';
export const CONFIRM_DELETE_LIST_PROP_LABEL =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > label';
export const CONFIRM_DELETE_LIST_PROP_CHECKBOX =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > label > input';
export const DELETE_LIST_PROP_BUTTON =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > button.btn.btn-danger';
export const CANCEL_DELETE_LIST_PROP_BUTTON =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > button.btn.btn-secondary';

// View/Edit List Items Constant Selectors
export const LIST_ITEMS_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > h5';
export const VIEW_EDIT_LIST_ITEMS_TABLE =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > div > div > div > div:nth-child(3) > table';
export const CREATE_LIST_ITEM_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > div > div > button';

// Create List Item Module
// Testing Properties & List Items
export const CHECKBOX_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(2) > label';
export const CHECKBOX_INPUT =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(2) > div:nth-child(2) > input';
export const DATE_TIME_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(3) > label';
export const GROUP_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(4) > label';
export const GROUP_INPUT =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(4) > div:nth-child(2) > div > div.rw-widget-input.rw-widget-picker.rw-widget-container';
export const NUMBER_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(5) > label';
export const NUMBER_PICKER =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(5) > div:nth-child(2) > div > div > input';
export const TEXT_INPUT_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(6) > label';
export const TEXT_INPUT =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(6) > div:nth-child(2) > input';
export const USER_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(7) > label';
export const USER_INPUT =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(7) > div:nth-child(2)';
export const CREATE_LIST_ITEM_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > h3';

export const SPECIAL_PERMISSIONS_CHECKBOX =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div.form-inline.save-buttons.row > div > label > input';
export const LIST_ITEM_PERMISSIONS_HEADING =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(9) > h4';
export const LIST_ITEM_PERMISSIONS_DESCRIPTION =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(9) > p';
export const READ_REC_INDIVIDUALS_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(9) > div:nth-child(4) > label';
export const READ_REC_INDIVIDUALS_DROPDOWN =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(9) > div:nth-child(4) > div:nth-child(2) > div';
export const READ_RECORD_GROUPS_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(9) > div:nth-child(5) > label';
export const READ_RECORD_GROUPS_INPUT =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(9) > div:nth-child(5) > div:nth-child(2) > div';
export const UPDATE_REC_INDIVIDUALS_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(9) > div:nth-child(6) > label';
export const UPDATE_REC_INDIVIDUALS_INPUT =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(9) > div:nth-child(6) > div:nth-child(2) > div > div.rw-widget-input.rw-widget-picker.rw-widget-container';
export const UPDATE_REC_GROUPS_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(9) > div:nth-child(7) > label';
export const UPDATE_REC_GROUPS_INPUT =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(9) > div:nth-child(7) > div:nth-child(2) > div > div.rw-widget-input.rw-widget-picker.rw-widget-container';
export const DELETE_REC_INDIVIDUALS_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(9) > div:nth-child(8) > label';
export const DELETE_REC_INDIVIDUALS_INPUT =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(9) > div:nth-child(8) > div:nth-child(2) > div > div.rw-widget-input.rw-widget-picker.rw-widget-container';
export const DELETE_REC_GROUPS_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(9) > div:nth-child(9) > label';
export const DELETE_REC_GROUPS_INPUT =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div:nth-child(9) > div:nth-child(9) > div:nth-child(2) > div > div.rw-widget-input.rw-widget-picker.rw-widget-container';
export const SAVE_LIST_ITEM_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.form-inline.save-buttons.row > div > div > button';
export const CANCEL_LIST_ITEM_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.form-inline.save-buttons.row > div > button';
export const CREATE_ANOTHER_CHECKBOX_LIST_ITEM =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.form-inline.save-buttons.row > div > div > label > input';
export const CREATE_ANOTHER_LIST_ITEM_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.form-inline.save-buttons.row > div > div > label > p';
export const SPECIAL_PERMISSIONS_CHECK_LABEL =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > div.form-inline.save-buttons.row > div > label > p';

// Delete List Item Selectors
export const DELETE_VIEW_EDIT_LIST_ITEM_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > div > div > div > div:nth-child(3) > table > tbody > tr:nth-child(1) > td > div > button.btn.btn-danger';
export const DELETE_LIST_ITEM_MODAL =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div';
export const CONFIRM_DELETE_QUESTION_LABEL =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-body';
export const PERMANENTLY_DELETE_HEADER =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-header > h4';
export const CONFIRM_DELETE_CHECKBOX =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > label > input';
export const CONFIRM_DELETE_LIST_ITEM_LABEL =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > label';
export const DELETE_LIST_ITEM_BUTTON =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > button.btn.btn-danger';
export const CANCEL_DELETE_BUTTON  =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > button.btn.btn-secondary';

// Edit List Item Selectors
export const EDIT_LIST_ITEM_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > div > div > div > div:nth-child(3) > table > tbody > tr:nth-child(1) > td > div > button.btn.btn-warning';
export const EDIT_LIST_ITEM_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > h3';
export const EDIT_LIST_ITEM_TABLE_INFO =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > table';

// Edit List Item Property Selectors
export const EDIT_LIST_PROPERTY_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > form > div:nth-child(5) > div > div > div:nth-child(3) > table > tbody > tr:nth-child(1) > td:nth-child(4) > div > button.btn.btn-warning';
export const EDIT_LIST_PROPERTY_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > h3';
export const EDIT_LIST_PROPERTY_INFO_TABLE =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-4 > div > form > div.bottom-buttons > table';

// Create List Groups tab selectors
export const GROUP_PERMISSIONS_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active > h5';
export const GROUP_PERMISSIONS_DIV_CONTAINER =
'#app > div > div > div:nth-child(3) > div > div > div.toggle-column.col.col-lg-5 > div > div.tab-content.bottom-buttons.top-tabs > div.tab-pane.active';

// Delete List modal selectors
export const DELETE_LIST_HEADER =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-header > h4';
export const DELETE_LIST_BODY =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-body';
export const DELETE_LIST_CONFIRM_CHECKBOX =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > label > input';
export const DELETE_BUTTON_DISABLED =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > button.btn.btn-danger.disabled';
export const DELETE_LIST_BUTTON =
'#app > div > div > div:nth-child(3) > div > div > div.col.col-sm-3 > div > div > div > div > div > div:nth-child(3) > table > tbody > tr:nth-child(1) > td:nth-child(3) > div > button.btn.btn-danger';
export const DELETE_BUTTON =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > button.btn.btn-danger';
export const DELETE_LIST_CANCEL_BUTTON =
'body > div:nth-child(7) > div > div.modal.fade.show > div > div > div.modal-footer > button.btn.btn-secondary';
