// tslint:disable:max-line-length
import {
  BASE_URL,
  LIST_MAINTENANCE_URL,
} from '../constants';
import {
  newListName,
} from './constants';
import {

  // Univsersal
  LISTS_HEADER,
  CREATE_LIST_BUTTON,
  READ_ONLY_FILTER,
  EDITABLE_FILTER,
  DELETED_FILTER,
  LISTS_TABLE,
  TABLE_PAGE_NAVIGATION,
  NEXT_PAGE_TABLE_NAV,
  PREV_PAGE_TABLE_NAV,
  CREATE_LIST_HEADER,
  SUCCESS_SAVE_LIST,

  // List Properties
  ADD_PROPERTY_BUTTON,

  // Property
  PROPERTY_NAME_INPUT,
  TYPE_DROPDOWN,
  REQUIRED_CHECKBOX,
  CREATE_ANOTHER_CHECKBOX,
  SAVE_LIST_PROPERTY_BUTTON,
  CANCEL_ITEM_PROPERTY_BUTTON,
  PROPERTY_NAME_LABEL,
  TYPE_LABEL,
  REQUIRED_CHECKBOX_LABEL,

  // Edit List Property
  EDIT_LIST_PROPERTY_BUTTON,
  EDIT_LIST_PROPERTY_HEADER,
  EDIT_LIST_PROPERTY_INFO_TABLE,
  EDIT_LIST_BUTTON,
  LIST_PROPERTY_TABLE,

}  from './selectors';
import { login } from '../helperFunctions';
import {
  Browser,
  Page,
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import expect from 'expect';
import { select } from 'async';

const puppeteer = require('puppeteer');
const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type ListE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    // elementQuery
    title?: string,
    headerText00?: string,
    createListButton?: ElementHandle,
    listsTable?: ElementHandle,
    listsTableLength00?: number,

    // List Properties
    addPropertyButton?: ElementHandle,
    propertyNameInput?: boolean,
    typeDropdown?: boolean,
    requiredCheckbox?: ElementHandle,

    // Edit List Property
    saveListPropButton?: ElementHandle,
    cancelListPropButton?: boolean,
    propNameLabel?: string,
    propTypeLabel?: string,
    reqCheckboxLabel?: string,
    editListPropHead?: string,
    editListPropButton?: ElementHandle,
    editListPropTableInfo?: boolean,
    listPropTable?: number,
  };
};

// ADD TEST FOR DROPDOWN OPTIONS

// Test Setup For Create List Properties
const editListPropertySetup = async (testObject: ListE2eTestObject) => {
  // Headers
  testObject.results.editListPropHead = await testObject.page.$eval(EDIT_LIST_PROPERTY_HEADER, (editListPropHead: HTMLHeadingElement) => editListPropHead.textContent);

  // Buttons
  testObject.results.createListButton = await testObject.page.$(CREATE_LIST_BUTTON);
  testObject.results.addPropertyButton =
  await testObject.page.$eval(ADD_PROPERTY_BUTTON, (button: HTMLButtonElement) => button.textContent);
  testObject.results.saveListPropButton = await testObject.page.$(SAVE_LIST_PROPERTY_BUTTON);
  testObject.results.cancelListPropButton = await testObject.page.$(CANCEL_ITEM_PROPERTY_BUTTON) !== null;
  testObject.results.addPropertyButton = await testObject.page.$(ADD_PROPERTY_BUTTON);
  testObject.results.editListPropButton = await testObject.page.$(EDIT_LIST_PROPERTY_BUTTON);

  // Checkboxes
  testObject.results.requiredCheckbox = await testObject.page.$(REQUIRED_CHECKBOX);

  // Table
  testObject.results.editListPropTableInfo = await testObject.page.$(EDIT_LIST_PROPERTY_INFO_TABLE) !== null;
  testObject.results.listPropTable = await testObject.page.$eval(LIST_PROPERTY_TABLE, (propertyTable: HTMLElement) => propertyTable.childNodes[1].childNodes.length);

  // Labels
  testObject.results.propNameLabel =
   (await testObject.page.$eval(PROPERTY_NAME_LABEL, (propLabel:HTMLLabelElement) => propLabel.textContent));
  testObject.results.propTypeLabel =
   (await testObject.page.$eval(TYPE_LABEL, (typeLabel:HTMLLabelElement) => typeLabel.textContent));
  testObject.results.reqCheckboxLabel =
   (await testObject.page.$eval(REQUIRED_CHECKBOX_LABEL, (reqCheckLabel:HTMLLabelElement) => reqCheckLabel.textContent));

  // Input
  testObject.results.propertyNameInput = await testObject.page.$(PROPERTY_NAME_INPUT) !== null;
  testObject.results.typeDropdown = await testObject.page.$(TYPE_DROPDOWN) !== null;

 // Add Property Tests
  await testObject.page.type(PROPERTY_NAME_INPUT, 'test-input');
  await testObject.page.click(REQUIRED_CHECKBOX);
  await testObject.page.click(TYPE_DROPDOWN);

  await testObject.page.click(SAVE_LIST_PROPERTY_BUTTON);
  // await testObject.page.click(CANCEL_ITEM_PROPERTY_BUTTON);

};
  // Test Setup For Edit List Properties
const editListPropertyTest = async (getTestObject: () => ListE2eTestObject) => {
  describe('Edit List Property Modal Contents', () => {
    // Headers
    it('Edit List Item Property', async () => {
      expect(getTestObject().results.editListPropHead).toEqual('Edit List Item Property');
    });
    // Labels
    it('Property Name * Label', async () => {
      expect(getTestObject().results.propNameLabel).toEqual('Property Name *');
    });
    it('Type * Label', async () => {
      expect(getTestObject().results.propTypeLabel).toEqual('Type *');
    });
    it('Required Checkbox Label', async () => {
      expect(getTestObject().results.reqCheckboxLabel).toEqual('Required');
    });
    // Checkboxes
    it('Required Checkbox Click', async () => {
      expect(getTestObject().results.requiredCheckbox.click);
    });
    // Input
    it('Property Name Input', async () => {
      expect(getTestObject().results.propertyNameInput).toEqual(true);
    });
    it('Select Dropdown', async () => {
      expect(getTestObject().results.typeDropdown).toEqual(true);
    });
    it('Save List Property Button Clicked', async () => {
      expect(getTestObject().results.saveListPropButton.click);
      expect(getTestObject().page.close);
    });
    it('Cancel List Property', async () => {
      expect(getTestObject().results.cancelListPropButton).toEqual(true);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<ListE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (ListE2eTestObject) => any = async (testObject: ListE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(EDIT_LIST_BUTTON),
    testObject.page.waitFor(LISTS_TABLE),
  ]);
  await testObject.page.click(EDIT_LIST_BUTTON);
  await Promise.all([
    testObject.page.waitFor(EDIT_LIST_PROPERTY_BUTTON),
  ]);
  await testObject.page.click(EDIT_LIST_PROPERTY_BUTTON);
  await Promise.all([
    testObject.page.waitFor(SAVE_LIST_PROPERTY_BUTTON),
    testObject.page.waitFor(CANCEL_ITEM_PROPERTY_BUTTON),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before00Setup = async (testObject: ListE2eTestObject) => {
  dLog('before00Setup');
  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo:800,
  });
  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');
  await testObject.page.goto(BASE_URL);

  await login(testObject);
  dLog('At login');
  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(EDIT_LIST_BUTTON),
    testObject.page.waitFor(LISTS_TABLE),
  ]);
  await testObject.page.click(EDIT_LIST_BUTTON);
  await Promise.all([
    testObject.page.waitFor(EDIT_LIST_PROPERTY_BUTTON),
  ]);
  await testObject.page.click(EDIT_LIST_PROPERTY_BUTTON);
  await Promise.all([
    testObject.page.waitFor(SAVE_LIST_PROPERTY_BUTTON),
    testObject.page.waitFor(CANCEL_ITEM_PROPERTY_BUTTON),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: ListE2eTestObject) => {
  await editListPropertySetup(testObject);
};

const beforeTests = async (testObject: ListE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => ListE2eTestObject) => {
  editListPropertyTest(getTestObject);
};

const after00Teardown = async (testObject: ListE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: ListE2eTestObject) => {
  await after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end list maintenance page test',
  parallelBefore: false,
});
