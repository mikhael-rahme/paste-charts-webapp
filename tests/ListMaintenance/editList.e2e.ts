import {
  BASE_URL,
  LIST_MAINTENANCE_URL,
} from '../constants';
import {
  newListName,
} from './constants';
import {

  // Univsersal
  LISTS_HEADER,
  CREATE_LIST_BUTTON,
  READ_ONLY_FILTER,
  EDITABLE_FILTER,
  DELETED_FILTER,
  LISTS_TABLE,
  TABLE_PAGE_NAVIGATION,
  NEXT_PAGE_TABLE_NAV,
  PREV_PAGE_TABLE_NAV,
  CREATE_LIST_HEADER,
  SUCCESS_SAVE_LIST,

  // List Properties
  LIST_PROPERTIES_TAB,
  VIEW_EDIT_LIST_ITEMS_TAB,
  GROUPS_TAB,
  SAVE_LIST_BUTTON,
  CANCEL_LIST_BUTTON,
  LIST_PROPERTIES_HEADER,
  LIST_NAME_INPUT,
  LIST_ITEM_PROPERTIES_HEADER,
  ADD_PROPERTY_BUTTON,
  LIST_ITEMS_HEADER,
  VIEW_EDIT_LIST_ITEMS_TABLE,
  GROUP_PERMISSIONS_HEADER,
  GROUP_PERMISSIONS_DIV_CONTAINER,
  LIST_NAME_LABEL,

    // EDIT LIST
    EDIT_LIST_BUTTON,
    EDIT_LIST_HEADER,
    EDIT_INFO_TABLE,

}  from './selectors';
import { login } from '../helperFunctions';
import {
  Browser,
  Page,
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import expect from 'expect';
import { ADD_PROPERTY_BUTTON } from 'tests/ListMaintenance/selectors';

const puppeteer = require('puppeteer');
const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type ListE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {

    // Headers
    headerText00?: string,
    editListHeader?: ElementHandle,
    title?: string,

    // Labels
    listNameLabel?: string,
    listPropertiesLabel?: string,
    listItemPropertiesLabel?: string,

    // Buttons
    saveListButton?: ElementHandle,
    cancelListButton?: ElementHandle,
    addPropertyButton?: ElementHandle,
    editListButton?: ElementHandle,

    // Table
    listsTable?: ElementHandle,
    listsTableLength00?: number,
    editInfoTable?: boolean,

    // Tabs
    listPropTab?: string,
    viewEditTab?: string,
    groupsTab?: string,

    // Input
    listNameInput?: boolean,
  };
};

// ADD TEST FOR DROPDOWN OPTIONS

// Test Setup For Edit List
const editListSetup = async (testObject: ListE2eTestObject) => {
  // Headers
  testObject.results.editListHeader = await testObject.page.$(EDIT_LIST_HEADER);

  // Buttons
  testObject.results.saveListButton = await testObject.page.$(SAVE_LIST_BUTTON);
  testObject.results.cancelListButton = await testObject.page.$(CANCEL_LIST_BUTTON);
  testObject.results.addPropertyButton = await testObject.page.$(ADD_PROPERTY_BUTTON);
  testObject.results.editListButton = await testObject.page.$(EDIT_LIST_BUTTON);

  // Labels
  testObject.results.listNameLabel =
  (await testObject.page.$eval(LIST_NAME_LABEL, (listNameLabel:HTMLLabelElement) =>  listNameLabel.textContent));
  testObject.results.listPropertiesLabel =
  await testObject.page.$eval(LIST_PROPERTIES_HEADER, (listPropLabel: HTMLLabelElement) => listPropLabel.textContent);
  testObject.results.listItemPropertiesLabel =
  await testObject.page.$eval(LIST_ITEM_PROPERTIES_HEADER, (listItemPropLabel: HTMLLabelElement) => listItemPropLabel.textContent);

  // Tabs
  testObject.results.listPropTab =
  await testObject.page.$eval(LIST_PROPERTIES_TAB, (listProp: HTMLLabelElement) => listProp.textContent);
  testObject.results.viewEditTab =
  await testObject.page.$eval(VIEW_EDIT_LIST_ITEMS_TAB, (viewTab: HTMLLabelElement) => viewTab.textContent);
  testObject.results.groupsTab = await testObject.page.$eval(GROUPS_TAB, (groupTab: HTMLLabelElement) => groupTab.textContent);

  // Input
  testObject.results.listNameInput = await testObject.page.$(LIST_NAME_INPUT) !== null;

  // Table
  testObject.results.editInfoTable = (await testObject.page.$(EDIT_INFO_TABLE)) !== null;

  await testObject.page.click(EDIT_LIST_BUTTON);
  // Just to test to see if it would save with new edited input
  await testObject.page.type(LIST_NAME_INPUT, '2');
  await testObject.page.click(SAVE_LIST_BUTTON);
  await testObject.page.click(ADD_PROPERTY_BUTTON);
  await testObject.page.click(CANCEL_LIST_BUTTON);

};
  // Test Setup For Edit List
const editListTest = async (getTestObject: () => ListE2eTestObject) => {
  describe('Create List Button Click/Under List Properties Tab', () => {
    it('Edit List Button Clicked', async () => {
      expect(getTestObject().results.editListButton.click);
    });

    // Labels
    it('header with text "List Properties"', async () => {
      expect(getTestObject().results.listPropertiesLabel).toEqual('List Properties');
    });
    it('header with text "List Item Properties"', async () => {
      expect(getTestObject().results.listItemPropertiesLabel).toEqual('List Item Properties');
    });
    it('List Name Label', async () => {
      expect(getTestObject().results.listNameLabel).toEqual('List Name');
    });

    // Table
    it('Edit Info Table', async () => {
      expect(getTestObject().results.editInfoTable).toEqual(true);
    });

    // Tabs
    it('Tab With Text "List Properties"', async () => {
      expect(getTestObject().results.listPropTab).toEqual('List Properties');
    });
    it('Tab With Text "View/Edit List Items"', async () => {
      expect(getTestObject().results.viewEditTab).toEqual('View/Edit List Items');
    });
    it('Tab With Text "Groups"', async () => {
      expect(getTestObject().results.groupsTab).toEqual('Groups');
    });

    // Buttons
    it('Add Property Button Clicked', async () => {
      expect(getTestObject().results.addPropertyButton.click);
    });
    it('Cancel List Button Clicked', async () => {
      expect(getTestObject().results.cancelListButton.click);
    });
    it('Save List Button Clicked', async () => {
      expect(getTestObject().results.saveListButton.click);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<ListE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (ListE2eTestObject) => any = async (testObject: ListE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
    testObject.page.waitFor(LISTS_TABLE),
  ]);
  await testObject.page.click(EDIT_LIST_BUTTON);
  await Promise.all([
    testObject.page.waitFor(EDIT_LIST_HEADER),
    testObject.page.waitFor(LIST_PROPERTIES_HEADER),
    testObject.page.waitFor(LIST_ITEM_PROPERTIES_HEADER),
    testObject.page.waitFor(ADD_PROPERTY_BUTTON),
    testObject.page.waitFor(SAVE_LIST_BUTTON),
    testObject.page.waitFor(CANCEL_LIST_BUTTON),
    testObject.page.waitFor(LIST_NAME_INPUT),
    testObject.page.waitFor(LIST_NAME_LABEL),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before00Setup = async (testObject: ListE2eTestObject) => {
  dLog('before00Setup');
  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo:800,
  });
  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');
  await testObject.page.goto(BASE_URL);

  await login(testObject);
  dLog('At login');
  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
    testObject.page.waitFor(LISTS_TABLE),
  ]);
  await testObject.page.click(EDIT_LIST_BUTTON);
  await Promise.all([
    testObject.page.waitFor(EDIT_LIST_HEADER),
    testObject.page.waitFor(LIST_PROPERTIES_HEADER),
    testObject.page.waitFor(LIST_ITEM_PROPERTIES_HEADER),
    testObject.page.waitFor(ADD_PROPERTY_BUTTON),
    testObject.page.waitFor(SAVE_LIST_BUTTON),
    testObject.page.waitFor(CANCEL_LIST_BUTTON),
    testObject.page.waitFor(LIST_NAME_INPUT),
    testObject.page.waitFor(LIST_NAME_LABEL),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: ListE2eTestObject) => {
  await editListSetup(testObject);
};

const beforeTests = async (testObject: ListE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => ListE2eTestObject) => {
  editListTest(getTestObject);
};

const after00Teardown = async (testObject: ListE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: ListE2eTestObject) => {
  await after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end list maintenance page test',
  parallelBefore: false,
});
