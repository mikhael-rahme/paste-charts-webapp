import {
  BASE_URL,
  LIST_MAINTENANCE_URL,
} from '../constants';
import { newListName } from './constants';
import {
  LISTS_HEADER,
  CREATE_LIST_BUTTON,
  LIST_SAVED_OR_DELETED_SUCCESS,
  LISTS_TABLE,
  TABLE_PAGE_NAVIGATION,
  NEXT_PAGE_TABLE_NAV,
  DELETE_LIST_HEADER,
  DELETE_LIST_BODY,
  DELETE_LIST_CONFIRM_CHECKBOX,
  DELETE_LIST_CANCEL_BUTTON,
  LIST_NAME_INPUT,
  SAVE_LIST_BUTTON,
  DELETE_BUTTON_DISABLED,
  DELETE_LIST_BUTTON,
  DELETE_BUTTON,
} from './selectors';
import {
  isLoggedIn,
  login,
  waitForFunction,
} from '../helperFunctions';
import {
  Browser,
  ElementHandle,
  JSHandle,
  Page,
} from 'puppeteer';
import { headerTest } from '../Header/header.e2e';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';

const puppeteer = require('puppeteer');
const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type DeleteListE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    header?: string,
    bodyWithListText?: boolean,
    checkbox?: ElementHandle,
    disabledDeleteButton?: boolean,
    cancelButton?: ElementHandle,
    disabledButton01?: ElementHandle,
    successModal?: ElementHandle,
    deleteListButton?: ElementHandle,
    deleteButton?: ElementHandle,

    // Original Page Content
    headerText00?: string,
    createListButton?: ElementHandle,
    listsTable?: ElementHandle,
    listsTableLength00?: number,
  },
};

// Element Query Setup
const elementQuerySetup = async (testObject: DeleteListE2eTestObject) => {

  // ORIGINAL PAGE CONTENT
    // Headers & Titles
  testObject.results.title = await testObject.page.title();
  testObject.results.headerText00 = await testObject.page.$eval(LISTS_HEADER, (header: HTMLElement) => header.innerText);
    // Buttons
  testObject.results.createListButton = await testObject.page.$(CREATE_LIST_BUTTON);
    // Tables
  testObject.results.listsTable = await testObject.page.$(LISTS_TABLE);
  testObject.results.listsTableLength00 =
        await testObject.page.$eval(LISTS_TABLE, (table: HTMLElement) => table.childNodes[1].childNodes.length);

  // DELETE MODAL CONTENT
   // Headers
  testObject.results.header = await testObject.page.$eval(DELETE_LIST_HEADER, (h1: HTMLHeadingElement) => h1.textContent);

   // Buttons/Checkbox
  testObject.results.disabledDeleteButton = await testObject.page.$(DELETE_BUTTON_DISABLED) !== null;
  testObject.results.cancelButton = await testObject.page.$(DELETE_LIST_CANCEL_BUTTON);
  testObject.results.checkbox = await testObject.page.$(DELETE_LIST_CONFIRM_CHECKBOX);
  testObject.results.deleteListButton = await testObject.page.$(DELETE_LIST_BUTTON);
  testObject.results.deleteButton = await testObject.page.$(DELETE_BUTTON);
  await testObject.page.click(DELETE_LIST_BUTTON);

   // List Saved Or Deleted
  testObject.results.successModal = await testObject.page.$(LIST_SAVED_OR_DELETED_SUCCESS);

  // Tests Table Navigation
  let rowHandles: ElementHandle[] = [];
  let found: boolean = false;
  let pageNum: number = 1;
  if (await testObject.page.$(TABLE_PAGE_NAVIGATION) !== null) {
    pageNum = await testObject.page.$eval(TABLE_PAGE_NAVIGATION, (nav: HTMLElement) => nav.childNodes[0].childNodes.length) - 2;
  }
  if (pageNum > 1) {
    for (let i = 0; (i < pageNum) && (!found); i++) {
      rowHandles = await testObject.page.$$('tr');
      for (const row of rowHandles) {
        const rowText: string = await (await row.getProperty('textContent')).jsonValue();
        if (rowText === newListName) {
          await (await row.$('td:nth-child(3) > div > button:nth-child(2)')).click();
          found = true;
          break;
        }
      }
      if (!found) {
        await testObject.page.click(NEXT_PAGE_TABLE_NAV);
      }
    }
  } else {
    rowHandles = await testObject.page.$$('tr');
    for (const row of rowHandles) {
      const rowText: string = await (await row.getProperty('textContent')).jsonValue();
      if (rowText === newListName) {
        await (await row.$('td:nth-child(3) > div > button:nth-child(2)')).click();
        break;
      }
    }
  }
};

// Element Query Test
const elementQueryTest = (getTestObject: () => DeleteListE2eTestObject) => {
  describe('Original Page Content & Delete Modal Should Have', () => {

    // Delete Modal Should Contain
    it('Header "Permanently Delete List"', async () => {
      expect(getTestObject().results.header).toEqual('Permanently Delete List');
    });

    it('Confirmation Checkbox', async () => {
      expect(getTestObject().results.checkbox).not.toBeNull();
    });

    it('Disabled Delete Button', async () => {
      expect(getTestObject().results.disabledDeleteButton).not.toBeNull();
    });

    it('Cancel Button', async () => {
      expect(getTestObject().results.cancelButton).not.toBeNull();
    });

    // Original Page Content
    it('Page Title', async () => {
      expect(getTestObject().results.title).toEqual('List Maintenance - iterrex');
    });

    it('Header with "Lists" text', async () => {
      expect(getTestObject().results.headerText00).toEqual('Lists');
    });

    it('Create List Button', async () => {
      expect(getTestObject().results.createListButton).not.toBeNull();
    });

    it('Table Containing Lists', async () => {
      expect(getTestObject().results.listsTable).not.toBeNull();
    });

    it('more than 0 list items', async () => {
      expect(getTestObject().results.listsTableLength00).toBeGreaterThan(0);
    });
  });
};

// Test Setup For Delete Box, Confirm & Delete
const deleteListSetup = async (testObject: DeleteListE2eTestObject) => {
  await testObject.page.click(DELETE_LIST_BUTTON);
  await testObject.page.waitFor(DELETE_BUTTON_DISABLED);
  await testObject.page.type(DELETE_LIST_HEADER, 'Permanently Delete List');
  await testObject.page.click(DELETE_LIST_CONFIRM_CHECKBOX);
  await testObject.page.click(DELETE_LIST_BUTTON);

    // Tests Page Navigation
  let rowHandles: ElementHandle[] = [];
  let found: boolean = false;
  let pageNum: number = 1;
  if (await testObject.page.$(TABLE_PAGE_NAVIGATION) !== null) {
    pageNum = await testObject.page.$eval(TABLE_PAGE_NAVIGATION, (nav: HTMLElement) => nav.childNodes[0].childNodes.length) - 2;
  }
  if (pageNum > 1) {
    for (let i = 0; (i < pageNum) && (!found); i++) {
      rowHandles = await testObject.page.$$('tr');
      for (const row of rowHandles) {
        const rowText: string = await (await row.getProperty('textContent')).jsonValue();
        if (rowText === newListName) {
          await (await row.$('td:nth-child(3) > div > button:nth-child(2)')).click();
          found = true;
          break;
        }
      }
      if (!found) {
        await testObject.page.click(NEXT_PAGE_TABLE_NAV);
      }
    }
  } else {
    rowHandles = await testObject.page.$$('tr');
    for (const row of rowHandles) {
      const rowText: string = await (await row.getProperty('textContent')).jsonValue();
      if (rowText === newListName) {
        await (await row.$('td:nth-child(3) > div > button:nth-child(2)')).click();
        break;
      }
    }
  }
};

// Test For Delete Box, Confirm & Delete
const deleteListTest = async (getTestObject: () => DeleteListE2eTestObject) => {
  describe('Delete Button Functionality, Confirm Check, Delete', () => {

    it('Delete List Button Click', () => {
      expect(getTestObject().results.deleteListButton.click);
    });
    it('Expect Delete Button Disabled', () => {
      expect(getTestObject().results.disabledDeleteButton).not.toBeNull();
    });
    it('Confirm Checkbox Clicked', () => {
      expect(getTestObject().results.checkbox.click);
    });
    it('Expect Delete Button Enabled', () => {
      expect(getTestObject().results.disabledDelete).not.toBeNull();
    });
    it('Delete Button Clicked', () => {
      expect(getTestObject().results.deleteButton.click);
    });
  });
};

// Test Setup For Delete Box, Cancel
const deleteListCancelSetup = async (testObject: DeleteListE2eTestObject) => {
  await testObject.page.click(DELETE_LIST_BUTTON);
  await testObject.page.waitFor(DELETE_BUTTON_DISABLED);
  await testObject.page.type(DELETE_LIST_HEADER, 'Permanently Delete List');
  await testObject.page.click(DELETE_LIST_CONFIRM_CHECKBOX);
  await testObject.page.waitFor(DELETE_BUTTON);
  await testObject.page.click(DELETE_LIST_CANCEL_BUTTON);
};

// Test For Delete Box, Cancel
const deleteListCancelTest = async (getTestObject: () => DeleteListE2eTestObject) => {
  describe('Delete Button Functionality, Confirm Check, Delete', () => {

    it('Delete List Button Click', () => {
      expect(getTestObject().results.deleteListButton.click);
    });
    it('Expect Delete Button Disabled', () => {
      expect(getTestObject().results.disabledDeleteButton).not.toBeNull();
    });
    it('Confirm Checkbox', () => {
      expect(getTestObject().results.checkbox).not.toBeNull();
    });
    it('Delete Button Clicked', () => {
      expect(getTestObject().results.cancelButton.click);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<DeleteListE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (DeleteListE2eTestObject) => any = async (testObject: DeleteListE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
  ]);

  await testObject.page.click(DELETE_LIST_BUTTON);
  await Promise.all([
    testObject.page.waitFor(DELETE_LIST_HEADER),
    testObject.page.waitFor(DELETE_LIST_BODY),
    testObject.page.waitFor(DELETE_LIST_CONFIRM_CHECKBOX),
    testObject.page.waitFor(DELETE_BUTTON_DISABLED),
    testObject.page.waitFor(DELETE_LIST_CANCEL_BUTTON),
  ]);
};

const before00Setup = async (testObject: DeleteListE2eTestObject) => {
  dLog('before00Setup');
  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo: 800,
  });
  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');

  await testObject.page.goto(BASE_URL);

  await login(testObject);
  dLog('At login');
  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
  ]);
  await testObject.page.click(DELETE_LIST_BUTTON);
  await Promise.all([
    testObject.page.waitFor(DELETE_LIST_HEADER),
    testObject.page.waitFor(DELETE_LIST_BODY),
    testObject.page.waitFor(DELETE_LIST_CONFIRM_CHECKBOX),
    testObject.page.waitFor(DELETE_BUTTON_DISABLED),
    testObject.page.waitFor(DELETE_LIST_CANCEL_BUTTON),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: DeleteListE2eTestObject) => {
// BOTH TESTS WORK SEPERATLY WITH ELEMENT QUERY SETUP/ TIMES OUT IF TESTING ALL
  await elementQuerySetup(testObject);
  // await deleteListCancelSetup(testObject);
  await deleteListSetup(testObject);
};

const beforeTests = async (testObject: DeleteListE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => DeleteListE2eTestObject) => {
  // BOTH TESTS WORK SEPERATLY WITH ELEMENT QUERY SETUP/ TIMES OUT IF TESTING ALL
  elementQueryTest(getTestObject);
  // deleteListCancelTest(getTestObject);
  deleteListTest(getTestObject);
};

const after00Teardown = async (testObject: DeleteListE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: DeleteListE2eTestObject) => {
  after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end delete list test',
  parallelBefore: false,
});
