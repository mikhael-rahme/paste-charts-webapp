// tslint:disable:max-line-length
import {
  BASE_URL,
  LIST_MAINTENANCE_URL,
} from '../constants';
import {
  newListName,
} from './constants';
import {

  // Univsersal
  LISTS_HEADER,
  CREATE_LIST_BUTTON,
  READ_ONLY_FILTER,
  EDITABLE_FILTER,
  DELETED_FILTER,
  LISTS_TABLE,
  TABLE_PAGE_NAVIGATION,
  NEXT_PAGE_TABLE_NAV,
  PREV_PAGE_TABLE_NAV,
  CREATE_LIST_HEADER,
  SUCCESS_SAVE_LIST,

  // List Properties
  LIST_PROPERTIES_TAB,
  VIEW_EDIT_LIST_ITEMS_TAB,
  GROUPS_TAB,
  SAVE_LIST_BUTTON,
  CANCEL_LIST_BUTTON,
  LIST_PROPERTIES_HEADER,
  LIST_NAME_INPUT,
  LIST_ITEM_PROPERTIES_HEADER,
  ADD_PROPERTY_BUTTON,
  LIST_ITEMS_HEADER,
  VIEW_EDIT_LIST_ITEMS_TABLE,
  GROUP_PERMISSIONS_HEADER,
  GROUP_PERMISSIONS_DIV_CONTAINER,
  LIST_NAME_LABEL,
  LIST_PROPERTIES_LABEL,
  LIST_ITEM_PROPERTIES_LABEL,

  // Create Property
  CREATE_LIST_ITEM_PROPERTY_HEADER,
  PROPERTY_NAME_INPUT,
  TYPE_DROPDOWN,
  REQUIRED_CHECKBOX,
  CREATE_ANOTHER_CHECKBOX,
  SAVE_LIST_PROPERTY_BUTTON,
  CANCEL_ITEM_PROPERTY_BUTTON,
  PROPERTY_NAME_LABEL,
  TYPE_LABEL,
  REQUIRED_CHECKBOX_LABEL,
  CREATE_ANOTHER_CHECKBOX_LABEL,
  DISABLED_SAVE_LIST_BUTTON,

}  from './selectors';
import { login } from '../helperFunctions';
import {
  Browser,
  Page,
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import expect from 'expect';
import { select } from 'async';

const puppeteer = require('puppeteer');
const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type ListE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    // elementQuery
    title?: string,
    headerText00?: string,
    createListButton?: ElementHandle,
    listsTable?: ElementHandle,
    listsTableLength00?: number,

    // Create List
    saveListButton?: ElementHandle,
    cancelListButton?: ElementHandle,
    listNameLabel?: string,
    successListSaved?: boolean,

    // List Properties
    listItemPropertiesHeader?: string,
    addPropertyButton?: ElementHandle,
    listPropertiesTab?: string,
    createListItemPropHead?: string,
    propertyNameInput?: boolean,
    typeDropdown?: boolean,
    requiredCheckbox?: ElementHandle,
    createAnotherCheckbox?: ElementHandle,
    saveListPropButton?: ElementHandle,
    cancelListPropButton?: ElementHandle,
    propNameLabel?: string,
    propTypeLabel?: string,
    reqCheckboxLabel?: string,
    createAnotherCheckLabel?: string,
    listPropertiesLabel?: string,
    listItemPropertiesLabel?: string,
    disabledSaveButton?: boolean,
    viewEditTab?: string,
    groupsTab?: string,
    listNameInput?: boolean,
    listPropertiesHeader?: string,
    createListHeader?: string,

  },
};

// ADD TEST FOR DROPDOWN OPTIONS

// Test Setup For Create List Properties
const createListSetup = async (testObject: ListE2eTestObject) => {
  // Headers
  testObject.results.createListHeader = await testObject.page.$eval(CREATE_LIST_HEADER, (h1: HTMLHeadingElement) => h1.textContent);
  testObject.results.listPropertiesHeader = await testObject.page.$eval(LIST_PROPERTIES_HEADER, (listPropHeader: HTMLHeadingElement) => listPropHeader.textContent);
  testObject.results.createListItemPropHead =
  await testObject.page.$eval(CREATE_LIST_ITEM_PROPERTY_HEADER, (createListPropHead: HTMLHeadingElement) => createListPropHead.textContent);

  // Buttons
  testObject.results.createListButton = await testObject.page.$(CREATE_LIST_BUTTON);
  testObject.results.saveListButton = await testObject.page.$(SAVE_LIST_BUTTON);
  testObject.results.cancelListButton = await testObject.page.$(CANCEL_LIST_BUTTON);
  testObject.results.addPropertyButton =
  await testObject.page.$eval(ADD_PROPERTY_BUTTON, (button: HTMLButtonElement) => button.textContent);
  testObject.results.saveListPropButton = await testObject.page.$(SAVE_LIST_PROPERTY_BUTTON);
  testObject.results.cancelListPropButton = await testObject.page.$(CANCEL_ITEM_PROPERTY_BUTTON);
  testObject.results.addPropertyButton = await testObject.page.$(ADD_PROPERTY_BUTTON);
  testObject.results.disabledSaveButton = await testObject.page.$(DISABLED_SAVE_LIST_BUTTON) !== null;
  testObject.results.successListSaved = await testObject.page.$(SUCCESS_SAVE_LIST) ! == null;

  // Checkboxes
  testObject.results.requiredCheckbox = await testObject.page.$(REQUIRED_CHECKBOX);
  testObject.results.createAnotherCheckbox = await testObject.page.$(CREATE_ANOTHER_CHECKBOX);

  // Labels
  testObject.results.listPropertiesLabel =
  (await testObject.page.$eval(LIST_PROPERTIES_LABEL, (listPropLabel:HTMLLabelElement) => listPropLabel.textContent));
  testObject.results.listItemPropertiesLabel =
  (await testObject.page.$eval(LIST_ITEM_PROPERTIES_LABEL, (listItemPropLabel:HTMLLabelElement) =>  listItemPropLabel.textContent));
  testObject.results.listNameLabel =
  (await testObject.page.$eval(LIST_NAME_LABEL, (listNameLabel:HTMLLabelElement) =>  listNameLabel.textContent));
  testObject.results.propNameLabel =
   (await testObject.page.$eval(PROPERTY_NAME_LABEL, (propLabel:HTMLLabelElement) => propLabel.textContent));
  testObject.results.propTypeLabel =
   (await testObject.page.$eval(TYPE_LABEL, (typeLabel:HTMLLabelElement) => typeLabel.textContent));
  testObject.results.reqCheckboxLabel =
   (await testObject.page.$eval(REQUIRED_CHECKBOX_LABEL, (reqCheckLabel:HTMLLabelElement) => reqCheckLabel.textContent));
  testObject.results.createAnotherCheckLabel =
   (await testObject.page.$eval(CREATE_ANOTHER_CHECKBOX_LABEL, (createAnotherLabel:HTMLLabelElement) => createAnotherLabel.textContent));

  // Tabs
  testObject.results.listPropertiesTab = await testObject.page.$eval(LIST_PROPERTIES_TAB, (tab: HTMLElement) => tab.textContent);
  testObject.results.viewEditTab = await testObject.page.$eval(VIEW_EDIT_LIST_ITEMS_TAB, (tab: HTMLElement) => tab.textContent);
  testObject.results.groupsTab = await testObject.page.$eval(GROUPS_TAB, (tab: HTMLElement) => tab.textContent);

  // Input
  testObject.results.listNameInput = await testObject.page.$(LIST_NAME_INPUT) !== null;
  testObject.results.propertyNameInput = await testObject.page.$(PROPERTY_NAME_INPUT) !== null;
  testObject.results.typeDropdown = await testObject.page.$(TYPE_DROPDOWN) !== null;

  await testObject.page.click(CREATE_LIST_BUTTON);
 // Add Property Tests
  await testObject.page.click(ADD_PROPERTY_BUTTON);
  await testObject.page.type(PROPERTY_NAME_INPUT, 'test-input');
  await testObject.page.waitFor(DISABLED_SAVE_LIST_BUTTON);
  await testObject.page.click(REQUIRED_CHECKBOX);
  await testObject.page.click(TYPE_DROPDOWN);

  await testObject.page.click(CREATE_ANOTHER_CHECKBOX);
  await testObject.page.click(SAVE_LIST_PROPERTY_BUTTON);
  await testObject.page.click(CANCEL_ITEM_PROPERTY_BUTTON);
    // Create Lists Test
  await testObject.page.type(LIST_NAME_INPUT, 'test_list_name');
  await testObject.page.click(SAVE_LIST_BUTTON);
  await testObject.page.click(CANCEL_LIST_BUTTON);

};
  // Test Setup For Create List Properties
const createListTest = async (getTestObject: () => ListE2eTestObject) => {
  // Create List Button Contents
  describe('Create List Button Click/Under List Properties Tab', () => {

    // Headers
    it('header with text "Create List"', async  () => {
      expect(getTestObject().results.createListHeader).toEqual('Create List');
    });
    it('header with text "List Properties"', async () => {
      expect(getTestObject().results.listPropertiesLabel).toEqual('List Properties');
    });
    it('header with text "List Item Properties"', async () => {
      expect(getTestObject().results.listItemPropertiesLabel).toEqual('List Item Properties');
    });
    // Tabs
    it('tab with text "List Properties"', async () => {
      expect(getTestObject().results.listPropertiesTab).toEqual('List Properties');
    });
    it('tab with text "View/Edit List Items"', async () => {
      expect(getTestObject().results.viewEditTab).toEqual('View/Edit List Items');
    });
    it('tab with text "Groups"', async () => {
      expect(getTestObject().results.groupsTab).toEqual('Groups');
    });
    // Input
    it('empty input for list name', async () => {
      expect(getTestObject().results.listNameInput).toEqual(true);
    });
    // Buttons
    it('Create List Button Clicked', async () => {
      expect(getTestObject().results.createListButton.click);
    });
    it('Add Property Button Click', async () => {
      expect(getTestObject().results.addPropertyButton.click);
    });
  });

  // Add Property Button Contents
  describe('Add Property Button Click & Content Of Add Property', () => {
    // Headers
    it('Create List Item Property Header', async () => {
      expect(getTestObject().results.createListItemPropHead).toEqual('Create List Item Property');
    });
    // Labels
    it('Property Name * Label', async () => {
      expect(getTestObject().results.propNameLabel).toEqual('Property Name *');
    });
    it('Type * Label', async () => {
      expect(getTestObject().results.propTypeLabel).toEqual('Type *');
    });
    it('Required Checkbox Label', async () => {
      expect(getTestObject().results.reqCheckboxLabel).toEqual('Required');
    });
    it('Create Another Checkbox Label', async () => {
      expect(getTestObject().results.createAnotherCheckLabel).toEqual('Create Another');
    });
    // Buttons
    it('Save List Property Button Should Be Diabled', async ()  => {
      expect(getTestObject().results.disabledSaveButton).toEqual(true);
    });
    // Checkboxes
    it('Required Checkbox Click', async () => {
      expect(getTestObject().results.requiredCheckbox.click);
    });
    it('Create Another Checkbox Click', async () => {
      expect(getTestObject().results.createAnotherCheckbox.click);
    });
    // Input
    it('Property Name Input', async () => {
      expect(getTestObject().results.propertyNameInput).toEqual(true);
    });
  });
  // Cancel List Property Functionality
  describe('Cancel List Property Functionality', () => {
    it('Cancel List Property', async () => {
      expect(getTestObject().results.cancelListPropButton.click);
    });
  });
  // Save List Functionality
  describe('Save List Functionality & Success', () => {
    it('Save List', async () => {
      expect(getTestObject().results.saveListButton.click);
    });
    it('Save Success', async () => {
      expect(getTestObject().results.successListSaved).toEqual(true);
    });
  });
      // Cancel List Functionality
  describe('Cancel List Functionality', () => {
    it('Cancel List', async () => {
      expect(getTestObject().results.cancelListButton.click);
    });
  });
  // Dropdown Functionality
  describe('Dropdown List Selection', () => {
    it('Select Dropdown', async () => {
      expect(getTestObject().results.typeDropdown).toEqual(true);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<ListE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (ListE2eTestObject) => any = async (testObject: ListE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
    testObject.page.waitFor(LISTS_TABLE),
  ]);
  await testObject.page.click(CREATE_LIST_BUTTON);
  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
    testObject.page.waitFor(LISTS_TABLE),
    testObject.page.waitFor(CREATE_LIST_HEADER),
    testObject.page.waitFor(LIST_NAME_LABEL),
    testObject.page.waitFor(LIST_NAME_INPUT),
    testObject.page.waitFor(ADD_PROPERTY_BUTTON),
    testObject.page.waitFor(SAVE_LIST_BUTTON),
    testObject.page.waitFor(CANCEL_LIST_BUTTON),
    testObject.page.waitFor(LIST_PROPERTIES_LABEL),
    testObject.page.waitFor(LIST_ITEM_PROPERTIES_LABEL),
  ]);
  await testObject.page.click(ADD_PROPERTY_BUTTON);
  await Promise.all([
    testObject.page.waitFor(SAVE_LIST_PROPERTY_BUTTON),
    testObject.page.waitFor(CANCEL_ITEM_PROPERTY_BUTTON),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before00Setup = async (testObject: ListE2eTestObject) => {
  dLog('before00Setup');
  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo:800,
  });
  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');
  await testObject.page.goto(BASE_URL);

  await login(testObject);
  dLog('At login');
  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
    testObject.page.waitFor(LISTS_TABLE),
  ]);
  await testObject.page.click(CREATE_LIST_BUTTON);
  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
    testObject.page.waitFor(LISTS_TABLE),
    testObject.page.waitFor(CREATE_LIST_HEADER),
    testObject.page.waitFor(LIST_NAME_LABEL),
    testObject.page.waitFor(LIST_NAME_INPUT),
    testObject.page.waitFor(ADD_PROPERTY_BUTTON),
    testObject.page.waitFor(SAVE_LIST_BUTTON),
    testObject.page.waitFor(CANCEL_LIST_BUTTON),
    testObject.page.waitFor(LIST_PROPERTIES_LABEL),
    testObject.page.waitFor(LIST_ITEM_PROPERTIES_LABEL),
  ]);
  await testObject.page.click(ADD_PROPERTY_BUTTON);
  await Promise.all([
    testObject.page.waitFor(SAVE_LIST_PROPERTY_BUTTON),
    testObject.page.waitFor(CANCEL_ITEM_PROPERTY_BUTTON),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: ListE2eTestObject) => {
  await createListSetup(testObject);
};

const beforeTests = async (testObject: ListE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => ListE2eTestObject) => {
  createListTest(getTestObject);
};

const after00Teardown = async (testObject: ListE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: ListE2eTestObject) => {
  await after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end list maintenance page test',
  parallelBefore: false,
});
