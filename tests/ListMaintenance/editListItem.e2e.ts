// tslint:disable:max-line-length
import {
  BASE_URL,
  LIST_MAINTENANCE_URL,
} from '../constants';
import {
  newListName,
} from './constants';
import {

  // Univsersal
  LISTS_HEADER,
  CREATE_LIST_BUTTON,
  READ_ONLY_FILTER,
  EDITABLE_FILTER,
  DELETED_FILTER,
  // LISTS_TABLE,
  TABLE_PAGE_NAVIGATION,
  NEXT_PAGE_TABLE_NAV,
  PREV_PAGE_TABLE_NAV,
  CREATE_LIST_HEADER,
  SUCCESS_SAVE_LIST,

  // Create List Item (View/Edit List Items Tab)
  CREATE_LIST_ITEM_BUTTON,
  SPECIAL_PERMISSIONS_CHECKBOX,
  SAVE_LIST_ITEM_BUTTON,
  CANCEL_LIST_ITEM_BUTTON,
  SPECIAL_PERMISSIONS_CHECK_LABEL,
  EDIT_LIST_BUTTON,
  VIEW_EDIT_LIST_ITEMS_TAB,

  CHECKBOX_LABEL,
  CHECKBOX_INPUT,
  DATE_TIME_LABEL,
  GROUP_LABEL,
  GROUP_INPUT,
  NUMBER_LABEL,
  NUMBER_PICKER,
  TEXT_INPUT_LABEL,
  TEXT_INPUT,
  USER_LABEL,
  USER_INPUT,

  // Edit List Item Selectors
  EDIT_LIST_ITEM_BUTTON,
  EDIT_LIST_ITEM_HEADER,
  EDIT_LIST_ITEM_TABLE_INFO,

}  from './selectors';
import { login } from '../helperFunctions';
import {
  Browser,
  Page,
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import expect from 'expect';

const puppeteer = require('puppeteer');
const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type ListE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {

    // Headers
    headerText00?: string,
    title?: string,
    createListItemHeader?: string,

    // Checkbox
    specialPermissionCheck?: ElementHandle,
    checkboxCheck?: ElementHandle,

    // Input
    textInput?: boolean,
    userInput?: boolean,
    numberPicker?: boolean,
    groupInput?: boolean

    // Labels
    specialPermCheckLabel?: string,
    checkboxLabel?: string,
    dateTimeLabel?: string,
    groupLabel?: string,
    numberPickerLabel?: string,
    textInputLabel?: string,
    userLabel?: string,

    // Buttons
    createListItemButton?: ElementHandle,
    saveListItemButton?: ElementHandle,
    cancelListItemButton?: ElementHandle,
    editListButton?: ElementHandle,
    viewEditListItemsTab?: ElementHandle,
    deleteListItemButton?: ElementHandle,

    // Modal & Table
    listsPropertyTableLength?: number,
    editInfoTable?: boolean,
    listItemTable?: boolean,

    // Edit List Item Selectors
    editListItemTableInfo?: boolean,
    editListItemButton?: ElementHandle,
    editListItemHeader?: string,

  };
};

// Test Setup For Edit List
const editListItemSetup = async (testObject: ListE2eTestObject) => {
  // Headers
  testObject.results.editListItemHeader = await testObject.page.$eval(EDIT_LIST_ITEM_HEADER, (editListItemHead: HTMLHeadingElement) => editListItemHead.textContent);

  // Buttons
  testObject.results.editListItemButton = await testObject.page.$(EDIT_LIST_ITEM_BUTTON);
  testObject.results.createListItemButton = await testObject.page.$(CREATE_LIST_ITEM_BUTTON);
  testObject.results.saveListItemButton = await testObject.page.$(SAVE_LIST_ITEM_BUTTON);
  testObject.results.cancelListItemButton = await testObject.page.$(CANCEL_LIST_ITEM_BUTTON);
  testObject.results.editListButton = await testObject.page.$(EDIT_LIST_BUTTON),
  testObject.results.viewEditListItemsTab = await testObject.page.$(VIEW_EDIT_LIST_ITEMS_TAB);

  // Table
  testObject.results.editListItemTableInfo = await testObject.page.$(EDIT_LIST_ITEM_TABLE_INFO) !== null;

  // Labels
  testObject.results.checkboxLabel =
  await testObject.page.$eval(CHECKBOX_LABEL, (checkLabel: HTMLLabelElement) => checkLabel.textContent);
  testObject.results.dateTimeLabel =
  await testObject.page.$eval(DATE_TIME_LABEL, (dateLabel: HTMLLabelElement) => dateLabel.textContent);
  testObject.results.groupLabel =
  await testObject.page.$eval(GROUP_LABEL, (groupLabel: HTMLLabelElement) => groupLabel.textContent);
  testObject.results.numberPickerLabel =
  await testObject.page.$eval(NUMBER_LABEL, (numberLabel: HTMLLabelElement) => numberLabel.textContent);
  testObject.results.textInputLabel =
  await testObject.page.$eval(TEXT_INPUT_LABEL, (textLabel: HTMLLabelElement) => textLabel.textContent);
  testObject.results.userLabel =
  await testObject.page.$eval(USER_LABEL, (userLabel: HTMLLabelElement) => userLabel.textContent);
  testObject.results.specialPermCheckLabel =
  await testObject.page.$eval(SPECIAL_PERMISSIONS_CHECK_LABEL, (specialPerChkLabel: HTMLLabelElement) => specialPerChkLabel.textContent);

  // Input
  testObject.results.textInput = await testObject.page.$(TEXT_INPUT) !== null;
  testObject.results.userInput = await testObject.page.$(USER_INPUT) !== null;
  testObject.results.groupInput = await testObject.page.$(GROUP_INPUT) !== null;
  testObject.results.numberPicker = await testObject.page.$(NUMBER_PICKER) !== null;

  // Checkbox
  testObject.results.specialPermissionCheck = await testObject.page.$(SPECIAL_PERMISSIONS_CHECKBOX);
  testObject.results.checkboxCheck = await testObject.page.$(CHECKBOX_INPUT);

  await testObject.page.click(CHECKBOX_INPUT);
  await testObject.page.type(TEXT_INPUT ,'test-text2');
  await testObject.page.click(SAVE_LIST_ITEM_BUTTON);
  await testObject.page.click(CANCEL_LIST_ITEM_BUTTON);
};
  // Test Setup For Edit List
const editListItemTest = async (getTestObject: () => ListE2eTestObject) => {

  describe('Edit List Item Module Contents', () => {
    // Headers
    it('Edit List Item Header', async () => {
      expect(getTestObject().results.editListItemHeader).toEqual('Edit List Item');
    });

    // Labels
    it('Checkbox Label Present', async () => {
      expect(getTestObject().results.checkboxLabel).toEqual('Checkbox');
    });
    it('Date Time Label Present', async () => {
      expect(getTestObject().results.dateTimeLabel).toEqual('Date Time');
    });
    it('Group Label Present', async () => {
      expect(getTestObject().results.groupLabel).toEqual('Group');
    });
    it('Number Picker Label Present', async () => {
      expect(getTestObject().results.numberPickerLabel).toEqual('Number');
    });
    it('Text Label Present', async () => {
      expect(getTestObject().results.textInputLabel).toEqual('Text');
    });
    it('User Label Present', async () => {
      expect(getTestObject().results.userLabel).toEqual('User');
    });
    it('Special Permissions Checkbox Label', async () => {
      expect(getTestObject().results.specialPermCheckLabel).toEqual('This Item Has Special Permissions');
    });

    // Input
    it('Text Input Present', async () => {
      expect(getTestObject().results.textInput).toEqual(true);
    });
    it('Group Input Present', async () => {
      expect(getTestObject().results.groupInput).toEqual(true);
    });
    it('Number Picker Present', async () => {
      expect(getTestObject().results.numberPicker).toEqual(true);
    });
    it('User Input Present', async () => {
      expect(getTestObject().results.userInput).toEqual(true);
    });

    // Table & Modal
    it('Edit List Table Info', async () => {
      expect(getTestObject().results.editListItemTableInfo).toEqual(true);
    });

    // Checkboxes
    it('Checkbox Checked', async () => {
      expect(getTestObject().results.checkboxCheck.click);
    });
    // Save List Item Functionality
    it('Save List Item Button Clicked',async() => {
      expect(getTestObject().results.saveListItemButton.click);
      expect(getTestObject().page.close);
    });
    // Cancel List Item Functionality
    it('Cancel List Button Clicked', async () => {
      expect(getTestObject().results.cancelListItemButton.click);
      expect(getTestObject().page.close);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<ListE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

// Selector Array Setup For Test
const selectorArraySetup = async (testObject: ListE2eTestObject) => {
  let rows: ElementHandle[] = [];
  let exists: boolean = false;
  const pageNumber: number = await testObject.page.$eval(TABLE_PAGE_NAVIGATION, (nav: HTMLElement) => nav.childNodes[0].childNodes.length) - 2;
  // If Statement For Page navigation If Page Number > 1
  if (pageNumber > 1) {
    for (let i = 0; (i < pageNumber) && (!exists); i++) {
      rows = await testObject.page.$$('tr');
      for (const row of rows) {
        // Get Values Of List Table
        const rowText: string = await (await row.getProperty('textContent')).jsonValue();
        // If rowText Is Equal To Test Name, Click Child Button
        if (rowText === 'Testing Properties & List Items') {
          await (await row.$('td:nth-child(3) > div > button:nth-child(1)')).click();
          exists = true;
          break;
        }
        // If Not Found, Click Next Page Navigation
        if (!exists) {
          await testObject.page.click(NEXT_PAGE_TABLE_NAV);
        }
      }
    }
    // If There's Only 1 Page Then Execute
  } else {
    rows = await testObject.page.$$('tr');
    for (const row of rows) {
      const rowText: string = await (await row.getProperty('textContent')).jsonValue();
      if (rowText === 'Testing Properties & List Items') {
        await (await row.$('td:nth-child(3) > div > button:nth-child(1)')).click();
        break;
      }
    }
  }
  // Navigation To Test
  await testObject.page.waitFor(VIEW_EDIT_LIST_ITEMS_TAB);
  await testObject.page.click(VIEW_EDIT_LIST_ITEMS_TAB);
  await testObject.page.waitFor(EDIT_LIST_ITEM_BUTTON);
  await testObject.page.click(EDIT_LIST_ITEM_BUTTON);
  await testObject.page.waitFor(EDIT_LIST_ITEM_HEADER);
};

const newPage: (ListE2eTestObject) => any = async (testObject: ListE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
  ]);
  await selectorArraySetup(testObject);
  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

// Before00Setup
const before00Setup = async (testObject: ListE2eTestObject) => {
  dLog('before00Setup');
  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo: 800,
  });
  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');
  await testObject.page.goto(BASE_URL);

  await login(testObject);
  dLog('At login');
  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
  ]);
  await selectorArraySetup(testObject);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: ListE2eTestObject) => {
  await editListItemSetup(testObject);
};

const beforeTests = async (testObject: ListE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => ListE2eTestObject) => {
  editListItemTest(getTestObject);
};

const after00Teardown = async (testObject: ListE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: ListE2eTestObject) => {
  await after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end list maintenance page test',
  parallelBefore: false,
});
