import {
  BASE_URL,
  LIST_MAINTENANCE_URL,
} from '../constants';
import { newListName } from './constants';
import {
  LISTS_HEADER,
  CREATE_LIST_BUTTON,
  CREATE_LIST_HEADER,
  LIST_PROPERTIES_TAB,
  VIEW_EDIT_LIST_ITEMS_TAB,
  GROUPS_TAB,
  LIST_PROPERTIES_HEADER,
  LIST_NAME_INPUT,
  LIST_ITEM_PROPERTIES_HEADER,
  ADD_PROPERTY_BUTTON,
  SAVE_LIST_BUTTON,
  CANCEL_LIST_BUTTON,
  LIST_ITEMS_HEADER,
  VIEW_EDIT_LIST_ITEMS_TABLE,
  GROUP_PERMISSIONS_HEADER,
  GROUP_PERMISSIONS_DIV_CONTAINER,
  LIST_SAVED_OR_DELETED_SUCCESS,
  LISTS_TABLE,
  TABLE_PAGE_NAVIGATION,
  NEXT_PAGE_TABLE_NAV,
  DELETE_LIST_HEADER,
  DELETE_LIST_BODY,
  DELETE_LIST_CONFIRM_CHECKBOX,
  // DELETE_LIST_DELETE_BUTTON,
  DELETE_LIST_CANCEL_BUTTON,
} from './selectors';
import {
  login,
  waitForFunction,
} from '../helperFunctions';
import {
  Browser,
  ElementHandle,
  JSHandle,
  Page,
} from 'puppeteer';
import { headerTest } from '../Header/header.e2e';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';

const puppeteer = require('puppeteer');

const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type CreateListE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    input01?: string,
    saveSuccessModal?: boolean,
    listAddSuccess?: boolean,
  },
};

export const createListSetup = async (testObject: CreateListE2eTestObject) => {
  await testObject.page.type(LIST_NAME_INPUT, newListName);
  testObject.results.input01 = await testObject.page.$eval(LIST_NAME_INPUT, (input: HTMLInputElement) => input.value);

  await testObject.page.click(SAVE_LIST_BUTTON);
  await waitForFunction(async () => (await testObject.page.$(SAVE_LIST_BUTTON)) === null, 15000, 1000);
  if (await testObject.page.$(LIST_SAVED_OR_DELETED_SUCCESS) !== null) {
    testObject.results.saveSuccessModal = true;
  } else {
    testObject.results.saveSuccessModal = false;
  }

  await testObject.page.reload();
  await testObject.page.waitFor(LISTS_TABLE);

  const rowHandles: ElementHandle[] = await testObject.page.$$('tr');

  if (await testObject.page.$(TABLE_PAGE_NAVIGATION) !== null) {
    const pageNum: number =
      await testObject.page.$eval(TABLE_PAGE_NAVIGATION, (nav: HTMLElement) => nav.childNodes[0].childNodes.length) - 2;
    for (let i = 1; i < pageNum; i++) {
      await testObject.page.click(NEXT_PAGE_TABLE_NAV);
      rowHandles.push(...(await testObject.page.$$('tr')));
    }
  }
  testObject.results.listAddSuccess = false;
  for (const row of rowHandles) {
    const rowText: string = await (await row.getProperty('textContent')).jsonValue();
    if (rowText === newListName) {
      testObject.results.listAddSuccess = true;
    }
  }
};

const createListTest = (getTestObject: () => CreateListE2eTestObject) => {
  describe('Creating a new list', () => {
    it('should allow list name input', async () => {
      expect(getTestObject().results.input01).toEqual(newListName);
    });

    // TO-DO: test adding properties to list when adding properties works

    it('should successfully save list', async () => {
      expect(getTestObject().results.saveSuccessModal).toEqual(true);
    });

    it('should add newly created list to lists table', async () => {
      expect(getTestObject().results.listAddSuccess).toEqual(true);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<CreateListE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (UsersE2eTestObject) => any = async (testObject: CreateListE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
  ]);

  await testObject.page.click(CREATE_LIST_BUTTON);
};

const before00Setup = async (testObject: CreateListE2eTestObject) => {
  dLog('before00Setup');
  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo: 800,
  });
  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');

  await testObject.page.goto(BASE_URL);

  await login(testObject);
  dLog('At login');
  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });

  await testObject.page.click(CREATE_LIST_BUTTON);
  await testObject.page.waitForSelector(LIST_NAME_INPUT);
};

const before01Run = async (testObject: CreateListE2eTestObject) => {
  await createListSetup(testObject);
};

const beforeTests = async (testObject: CreateListE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => CreateListE2eTestObject) => {
  createListTest(getTestObject);
};

const after00Teardown = async (testObject: CreateListE2eTestObject) => {
  await newPage(testObject);
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: CreateListE2eTestObject) => {
  after00Teardown(testObject);
  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end create list test',
  parallelBefore: false,
});
