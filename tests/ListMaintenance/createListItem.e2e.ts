// tslint:disable:max-line-length
import {
  BASE_URL,
  LIST_MAINTENANCE_URL,
} from '../constants';
import {
  newListName,
} from './constants';
import {

  // Univsersal
  LISTS_HEADER,
  CREATE_LIST_BUTTON,
  READ_ONLY_FILTER,
  EDITABLE_FILTER,
  DELETED_FILTER,
  // LISTS_TABLE,
  TABLE_PAGE_NAVIGATION,
  NEXT_PAGE_TABLE_NAV,
  PREV_PAGE_TABLE_NAV,
  CREATE_LIST_HEADER,
  SUCCESS_SAVE_LIST,
  LISTS_TABLE,

  // Create List Item (View/Edit List Items Tab)
  LIST_ITEMS_HEADER,
  VIEW_EDIT_LIST_ITEMS_TABLE,
  CREATE_LIST_ITEM_BUTTON,
  CREATE_LIST_ITEM_HEADER,
  SPECIAL_PERMISSIONS_CHECKBOX,
  LIST_ITEM_PERMISSIONS_HEADING,
  LIST_ITEM_PERMISSIONS_DESCRIPTION,
  SAVE_LIST_ITEM_BUTTON,
  CANCEL_LIST_ITEM_BUTTON,
  CREATE_ANOTHER_CHECKBOX_LIST_ITEM,
  CREATE_ANOTHER_LIST_ITEM_LABEL,
  SPECIAL_PERMISSIONS_CHECK_LABEL,
  VIEW_EDIT_LIST_ITEMS_TAB,
  LIST_NAME_INPUT,
  SAVE_LIST_BUTTON,
  CANCEL_LIST_BUTTON,
  LIST_PROPERTY_TABLE,

  CHECKBOX_LABEL,
  CHECKBOX_INPUT,
  DATE_TIME_LABEL,
  GROUP_LABEL,
  GROUP_INPUT,
  NUMBER_LABEL,
  NUMBER_PICKER,
  TEXT_INPUT_LABEL,
  TEXT_INPUT,
  USER_LABEL,
  USER_INPUT,

  READ_REC_INDIVIDUALS_LABEL,
  READ_REC_INDIVIDUALS_DROPDOWN,
  READ_RECORD_GROUPS_LABEL,
  READ_RECORD_GROUPS_INPUT,
  UPDATE_REC_INDIVIDUALS_LABEL,
  UPDATE_REC_INDIVIDUALS_INPUT,
  DELETE_REC_INDIVIDUALS_LABEL,
  DELETE_REC_INDIVIDUALS_INPUT,
  DELETE_REC_GROUPS_LABEL,
  DELETE_REC_GROUPS_INPUT,
  UPDATE_REC_GROUPS_LABEL,
  UPDATE_REC_GROUPS_INPUT,
  DELETE_LIST_ITEM_BUTTON,

}  from './selectors';
import { login } from '../helperFunctions';
import {
  Browser,
  Page,
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import expect from 'expect';
import { reset } from 'redux-form';

const puppeteer = require('puppeteer');
const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type ListE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {

    // Headers
    headerText00?: string,
    title?: string,
    createListItemHeader?: string,
    listItemPermissionHead?: string,

    // Checkbox
    specialPermissionCheck?: ElementHandle,
    createAnotherCheck?: ElementHandle,
    checkboxCheck?: ElementHandle,

    // Text Input
    textInput?: boolean,

    // Labels
    listItemPermDescription?: string,
    readRecIndividialsLabel?: string,
    createAnotherLabel?: string,
    specialPermCheckLabel?: string,
    readRecIndividualsLabel?: string,
    checkboxLabel?: string,
    dateTimeLabel?: string,
    groupLabel?: string,
    numberPickerLabel?: string,
    textInputLabel?: string,
    userLabel?: string,
    updateRecIndividualsLabel?: string,
    updateRecGroupsLabel?: string,
    readRecGroupsLabel?: string,
    deleteRecIndividualsLabel?: string,
    deleteRecGroupLabel?: string,

    // Buttons
    saveListButton?: ElementHandle,
    cancelListButton?: ElementHandle,
    createListItemButton?: ElementHandle,
    saveListItemButton?: ElementHandle,
    cancelListItemButton?: ElementHandle,
    editListButton?: ElementHandle,
    viewEditListItemsTab?: ElementHandle,
    deleteListItemButton?: ElementHandle,

    // Table
    listsPropertyTableLength?: number,
    editInfoTable?: boolean,
    listItemTable?: boolean,

    // Dropdown
    readRecIndividualDrop?: boolean,
    readRecGroupDrop?: boolean,
    updateRecIndividualsDrop?: boolean,
    updateRecGroupsDrop?: boolean,
    deleteRecIndividualsDrop?: boolean,
    deleteRecGroupsDrop?: boolean,
    groupDropdown?: boolean,
    numberPicker?: boolean,
    userInput?: boolean,

    listAddSuccess?: boolean,

    listLength?: number,

  };
};

// Test Setup For Edit List
const createListItemSetup = async (testObject: ListE2eTestObject) => {
  // Headers
  testObject.results.createListItemHeader =
  await testObject.page.$eval(CREATE_LIST_ITEM_HEADER, (createListItemHead: HTMLHeadingElement) => createListItemHead.textContent);
  testObject.results.listItemPermissionHead =
  await testObject.page.$eval(LIST_ITEM_PERMISSIONS_HEADING, (listItemPerHead: HTMLHeadingElement) => listItemPerHead.textContent);

  // Buttons
  testObject.results.saveListButton = await testObject.page.$(SAVE_LIST_BUTTON);
  testObject.results.cancelListButton = await testObject.page.$(CANCEL_LIST_BUTTON);
  testObject.results.createListItemButton = await testObject.page.$(CREATE_LIST_ITEM_BUTTON);
  testObject.results.saveListItemButton = await testObject.page.$(SAVE_LIST_ITEM_BUTTON);
  testObject.results.cancelListItemButton = await testObject.page.$(CANCEL_LIST_ITEM_BUTTON);
  // testObject.results.editListButton = await testObject.page.$(EDIT_LIST_BUTTON),
  testObject.results.viewEditListItemsTab = await testObject.page.$(VIEW_EDIT_LIST_ITEMS_TAB);
  testObject.results.deleteListItemButton = await testObject.page.$(DELETE_LIST_ITEM_BUTTON);

  // Labels
  testObject.results.listItemPermDescription =
  await testObject.page.$eval(LIST_ITEM_PERMISSIONS_DESCRIPTION, (listItemPermDes: HTMLLabelElement) => listItemPermDes.textContent);
  testObject.results.checkboxLabel =
  await testObject.page.$eval(CHECKBOX_LABEL, (checkLabel: HTMLLabelElement) => checkLabel.textContent);
  testObject.results.dateTimeLabel =
  await testObject.page.$eval(DATE_TIME_LABEL, (dateLabel: HTMLLabelElement) => dateLabel.textContent);
  testObject.results.groupLabel =
  await testObject.page.$eval(GROUP_LABEL, (groupLabel: HTMLLabelElement) => groupLabel.textContent);
  testObject.results.numberPickerLabel =
  await testObject.page.$eval(NUMBER_LABEL, (numberLabel: HTMLLabelElement) => numberLabel.textContent);
  testObject.results.textInputLabel =
  await testObject.page.$eval(TEXT_INPUT_LABEL, (textLabel: HTMLLabelElement) => textLabel.textContent);
  testObject.results.userLabel =
  await testObject.page.$eval(USER_LABEL, (userLabel: HTMLLabelElement) => userLabel.textContent);
  testObject.results.createAnotherLabel =
  await testObject.page.$eval(CREATE_ANOTHER_LIST_ITEM_LABEL, (createAnother: HTMLLabelElement) => createAnother.textContent);
  testObject.results.specialPermCheckLabel =
  await testObject.page.$eval(SPECIAL_PERMISSIONS_CHECK_LABEL, (specialPerChkLabel: HTMLLabelElement) => specialPerChkLabel.textContent);
  testObject.results.readRecIndividialsLabel =
  await testObject.page.$eval(READ_REC_INDIVIDUALS_LABEL, (readRecIndiLabel: HTMLLabelElement) => readRecIndiLabel.textContent);
  testObject.results.readRecGroupsLabel =
  await testObject.page.$eval(READ_RECORD_GROUPS_LABEL, (readRecGroupLabel: HTMLLabelElement) => readRecGroupLabel.textContent);
  testObject.results.updateRecIndividualsLabel =
  await testObject.page.$eval(UPDATE_REC_INDIVIDUALS_LABEL, (updateRecIndiLabel: HTMLLabelElement) => updateRecIndiLabel.textContent);
  testObject.results.updateRecGroupsLabel =
  await testObject.page.$eval(UPDATE_REC_GROUPS_LABEL, (updateRecGroupLabel: HTMLLabelElement) => updateRecGroupLabel.textContent);
  testObject.results.deleteRecIndividualsLabel =
  await testObject.page.$eval(DELETE_REC_INDIVIDUALS_LABEL, (deleteRecIndiLabel: HTMLLabelElement) => deleteRecIndiLabel.textContent);
  testObject.results.deleteRecGroupLabel =
  await testObject.page.$eval(DELETE_REC_GROUPS_LABEL, (deleteRecGroupLabel: HTMLLabelElement) => deleteRecGroupLabel.textContent);

  // Dropdown
  testObject.results.numberPicker = await testObject.page.$(NUMBER_PICKER) !== null;
  testObject.results.groupDropdown = await testObject.page.$(GROUP_INPUT) !== null;
  testObject.results.userInput = await testObject.page.$(USER_INPUT) !== null;
  testObject.results.readRecIndividualDrop = await testObject.page.$(READ_REC_INDIVIDUALS_DROPDOWN) !== null;
  testObject.results.readRecGroupDrop = await testObject.page.$(READ_RECORD_GROUPS_INPUT) !== null;
  testObject.results.updateRecIndividualsDrop = await testObject.page.$(UPDATE_REC_INDIVIDUALS_INPUT) !== null;
  testObject.results.updateRecGroupsDrop = await testObject.page.$(UPDATE_REC_GROUPS_INPUT) !== null;
  testObject.results.deleteRecIndividualsDrop = await testObject.page.$(DELETE_REC_INDIVIDUALS_INPUT) !== null;
  testObject.results.deleteRecGroupsDrop = await testObject.page.$(DELETE_REC_GROUPS_INPUT) !== null;

  // Input
  testObject.results.textInput = await testObject.page.$(TEXT_INPUT) !== null;

  // Checkbox
  testObject.results.specialPermissionCheck = await testObject.page.$(SPECIAL_PERMISSIONS_CHECKBOX);
  testObject.results.createAnotherCheck = await testObject.page.$(CREATE_ANOTHER_CHECKBOX_LIST_ITEM);
  testObject.results.checkboxCheck = await testObject.page.$(CHECKBOX_INPUT);

  await testObject.page.click(CREATE_ANOTHER_CHECKBOX_LIST_ITEM);
  await testObject.page.click(CHECKBOX_INPUT);
  await testObject.page.type(TEXT_INPUT ,'test-text');
  await testObject.page.click(SAVE_LIST_ITEM_BUTTON);
  await testObject.page.click(CANCEL_LIST_BUTTON);
};

  // Test Setup For Edit List
const createListItemTest = async (getTestObject: () => ListE2eTestObject) => {

  describe('Create List Item Module Contents', () => {
    // Headers
    it('Create List Item Header', async () => {
      expect(getTestObject().results.createListItemHeader).toEqual('Create List Item');
    });

    // Labels
    it('Checkbox Label Present', async () => {
      expect(getTestObject().results.checkboxLabel).toEqual('Checkbox');
    });
    it('Date Time Label Present', async () => {
      expect(getTestObject().results.dateTimeLabel).toEqual('Date Time');
    });
    it('Group Label Present', async () => {
      expect(getTestObject().results.groupLabel).toEqual('Group');
    });
    it('Number Picker Label Present', async () => {
      expect(getTestObject().results.numberPickerLabel).toEqual('Number');
    });
    it('Text Label Present', async () => {
      expect(getTestObject().results.textInputLabel).toEqual('Text');
    });
    it('User Label Present', async () => {
      expect(getTestObject().results.userLabel).toEqual('User');
    });
    it('Special Permissions Checkbox Label', async () => {
      expect(getTestObject().results.specialPermCheckLabel).toEqual('This Item Has Special Permissions');
    });
    it('Create Another Checkbox Label', async () => {
      expect(getTestObject().results.createAnotherLabel).toEqual('Create Another');
    });

    // Dropdowns
    it('Number Pricker Present', async () => {
      expect(getTestObject().results.numberPicker).toEqual(true);
    });
    it('Group Dropdown Present', async () => {
      expect(getTestObject().results.groupDropdown).toEqual(true);
    });
    it('User Input Present', async () => {
      expect(getTestObject().results.userInput).toEqual(true);
    });

    // Input
    it('Text Input Present', async () => {
      expect(getTestObject().results.textInput).toEqual(true);
    });

    // Checkboxes
    it('Create Another Checkbox Checked', async () => {
      expect(getTestObject().results.createAnotherCheck.click);
    });
    it('Checkbox Checked', async () => {
      expect(getTestObject().results.checkboxCheck.click);
    });
    // Save List Item Functionality
    it('Save List Item Button Clicked',async() => {
      expect(getTestObject().results.saveListItemButton.click);
      expect(getTestObject().page.close);
    });
    // Cancel List Item Functionality
    it('Cancel List Button Clicked', async () => {
      expect(getTestObject().results.cancelListItemButton.click);
      expect(getTestObject().page.close);
    });
  });

  // List Permissions Content
  describe('List Permissions Contains', () => {
    // Header
    it('List Item Permissions', async () => {
      expect(getTestObject().results.listItemPermissionHead).toEqual('List Item Permissions');
    });
    // Labels
    it('List Item Permissions Descriptions', async () => {
      expect(getTestObject().results.listItemPermDescription).toEqual('If this Item requires different permissions than other items in this list, set them here.');
    });
    it('Read Record Individuals', async () => {
      expect(getTestObject().results.readRecIndividialsLabel).toEqual('Read Record Individuals');
    });
    it('Read Record Groups', async () => {
      expect(getTestObject().results.readRecGroupsLabel).toEqual('Read Record Groups');
    });
    it('Update Record Individuals', async () => {
      expect(getTestObject().results.updateRecIndividualsLabel).toEqual('Update Record Individuals');
    });
    it('Update Record Groups', async () => {
      expect(getTestObject().results.updateRecGroupsLabel).toEqual('Update Record Groups');
    });
    it('Delete Record Individuals', async () => {
      expect(getTestObject().results.deleteRecIndividualsLabel).toEqual('Delete Record Individuals');
    });
    it('Delete Record Groups', async () => {
      expect(getTestObject().results.deleteRecGroupLabel).toEqual('Delete Record Groups');
    });
    // Dropdowns
    it('Read Record Individuals Dropdown', async () => {
      expect(getTestObject().results.readRecIndividualDrop).toEqual(true);
    });
    it('Read Record Groups Dropdown', async () => {
      expect(getTestObject().results.readRecGroupDrop).toEqual(true);
    });
    it('Update Record Individuals Dropdown', async () => {
      expect(getTestObject().results.updateRecIndividualsDrop).toEqual(true);
    });
    it('Update Record Groups Dropdown', async() => {
      expect(getTestObject().results.updateRecGroupsDrop).toEqual(true);
    });
    it('Delete Record Individuals Dropdown', async () => {
      expect(getTestObject().results.deleteRecIndividualsDrop).toEqual(true);
    });
    it('Delete Record Groups Dropdown', async () => {
      expect(getTestObject().results.deleteRecGroupsDrop).toEqual(true);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<ListE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

// Selector Array Setup For Test
const selectorArraySetup = async (testObject: ListE2eTestObject) => {
  let rows: ElementHandle[] = [];
  let exists: boolean = false;
  const pageNumber: number = await testObject.page.$eval(TABLE_PAGE_NAVIGATION, (nav: HTMLElement) => nav.childNodes[0].childNodes.length) - 2;
  // If Statement For Page navigation If Page Number > 1
  if (pageNumber > 1) {
    for (let i = 0; (i < pageNumber) && (!exists); i++) {
      rows = await testObject.page.$$('tr');
      for (const row of rows) {
        // Get Values Of List Table
        const rowText: string = await (await row.getProperty('textContent')).jsonValue();
        // If rowText Is Equal To Test Name, Click Child Button
        if (rowText === 'Testing Properties & List Items') {
          await (await row.$('td:nth-child(3) > div > button:nth-child(1)')).click();
          exists = true;
          break;
        }
        // If Not Found, Click Next Page Navigation
        if (!exists) {
          await testObject.page.click(NEXT_PAGE_TABLE_NAV);
        }
      }
    }
    // If There's Only 1 Page Then Execute
  } else {
    rows = await testObject.page.$$('tr');
    for (const row of rows) {
      const rowText: string = await (await row.getProperty('textContent')).jsonValue();
      if (rowText === 'Testing Properties & List Items') {
        await (await row.$('td:nth-child(3) > div > button:nth-child(1)')).click();
        break;
      }
    }
  }
  // Navigation To Test
  await testObject.page.waitFor(3000);
  await testObject.page.waitFor(VIEW_EDIT_LIST_ITEMS_TAB);
  await testObject.page.click(VIEW_EDIT_LIST_ITEMS_TAB);
  await testObject.page.waitFor(CREATE_LIST_ITEM_BUTTON);
  await testObject.page.click(CREATE_LIST_ITEM_BUTTON);
  await testObject.page.waitFor(SPECIAL_PERMISSIONS_CHECKBOX);
  await testObject.page.click(SPECIAL_PERMISSIONS_CHECKBOX);
};

const newPage: (ListE2eTestObject) => any = async (testObject: ListE2eTestObject) => {

  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
  ]);
  await selectorArraySetup(testObject),
  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

// Before00Setup
const before00Setup = async (testObject: ListE2eTestObject) => {
  dLog('before00Setup');
  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo: 800,
  });
  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');
  await testObject.page.goto(BASE_URL);

  await login(testObject);
  dLog('At login');
  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
  ]);
  await selectorArraySetup(testObject),
  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: ListE2eTestObject) => {
  await createListItemSetup(testObject);
};

const beforeTests = async (testObject: ListE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => ListE2eTestObject) => {
  createListItemTest(getTestObject);
};

const after00Teardown = async (testObject: ListE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: ListE2eTestObject) => {
  await after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end list maintenance page test',
  parallelBefore: false,
});
