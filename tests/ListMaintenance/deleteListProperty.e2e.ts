// tslint:disable:max-line-length
import {
  BASE_URL,
  LIST_MAINTENANCE_URL,
} from '../constants';
import {
  newListName,
} from './constants';
import {

  // Univsersal
  LISTS_HEADER,
  CREATE_LIST_BUTTON,
  READ_ONLY_FILTER,
  EDITABLE_FILTER,
  DELETED_FILTER,
  LISTS_TABLE,
  TABLE_PAGE_NAVIGATION,
  NEXT_PAGE_TABLE_NAV,
  PREV_PAGE_TABLE_NAV,
  CREATE_LIST_HEADER,
  SUCCESS_SAVE_LIST,

  // Delete List Property
  DELETE_LIST_PROPERTY_BUTTON,
  PERMANENTLY_DELETE_LIST_PROP_HEADER,
  DELETE_CONFIRM_QUESTION_LABEL,
  DELETE_LIST_PROPERTY_POPUP_MODAL,
  CONFIRM_DELETE_LIST_PROP_LABEL,
  CONFIRM_DELETE_LIST_PROP_CHECKBOX,
  DELETE_LIST_PROP_BUTTON,
  CANCEL_DELETE_LIST_PROP_BUTTON,
  EDIT_LIST_BUTTON,

}  from './selectors';
import { login } from '../helperFunctions';
import {
  Browser,
  Page,
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import expect from 'expect';
import { select } from 'async';

const puppeteer = require('puppeteer');
const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type ListE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    // elementQuery
    title?: string,
    headerText00?: string,
    createListButton?: ElementHandle,
    listsTable?: ElementHandle,
    listsTableLength00?: number,

    // List Properties
    addPropertyButton?: ElementHandle,
    propertyNameInput?: boolean,
    typeDropdown?: boolean,
    requiredCheckbox?: ElementHandle,

    // Delete List Property
    deleteListPropertyButton?: ElementHandle,
    permDeleteListPropHead?: string,
    deleteConfirmQuestionLabel?: boolean,
    deleteListPropModalPopUp?: boolean,
    confirmDeleteListPropLabel?: string,
    confirmDeleteListPropCheckbox?: ElementHandle,
    deleteListPropButtonConfirmed?: ElementHandle,
    cancelDeleteListPropButton?: boolean,
  };
};

// ADD TEST FOR DROPDOWN OPTIONS

// Test Setup For Create List Properties
const deleteListPropertySetup = async (testObject: ListE2eTestObject) => {
  // Headers
  testObject.results.permDeleteListPropHead = await testObject.page.$eval(PERMANENTLY_DELETE_LIST_PROP_HEADER, (permDeleteHeader: HTMLHeadingElement) => permDeleteHeader.textContent);

  // Buttons
  testObject.results.deleteListPropertyButton = await testObject.page.$(DELETE_LIST_PROP_BUTTON);
  testObject.results.deleteListPropButtonConfirmed = await testObject.page.$(DELETE_LIST_PROP_BUTTON);
  testObject.results.cancelDeleteListPropButton = await testObject.page.$(CANCEL_DELETE_LIST_PROP_BUTTON) !== null;

  // Checkboxes
  testObject.results.confirmDeleteListPropCheckbox = await testObject.page.$(CONFIRM_DELETE_LIST_PROP_CHECKBOX);

  // Labels
  testObject.results.confirmDeleteListPropLabel = await testObject.page.$eval(CONFIRM_DELETE_LIST_PROP_LABEL, (confirmDeleteLabel: HTMLElement) => confirmDeleteLabel.textContent);
  testObject.results.deleteConfirmQuestionLabel = await testObject.page.$(DELETE_CONFIRM_QUESTION_LABEL) !== null;

  // Modal
  testObject.results.deleteListPropModalPopUp = await testObject.page.$(DELETE_LIST_PROPERTY_POPUP_MODAL) !== null;

 // Delete Property Tests
  await testObject.page.click(CONFIRM_DELETE_LIST_PROP_CHECKBOX);
  await testObject.page.click(DELETE_LIST_PROP_BUTTON);

};
  // Test Setup For Edit List Properties
const deleteListPropertyTest = async (getTestObject: () => ListE2eTestObject) => {
  describe('Delete List Property Modal Contents', () => {
    // Headers
    it('Permanently Delete List Item Property', async () => {
      expect(getTestObject().results.permDeleteListPropHead).toEqual('Permanently Delete List Item Property');
    });
    // Labels
    it('Confirm Checkbox Label', async () => {
      expect(getTestObject().results.confirmDeleteListPropLabel).toEqual('Confirm');
    });
    it('Delete Confirm Question Label', async () => {
      expect(getTestObject().results.deleteConfirmQuestionLabel).toEqual(true);
    });
    // Checkboxes
    it('Confirm Checkbox Click', async () => {
      expect(getTestObject().results.confirmDeleteListPropCheckbox.click);
    });
    // Buttons
    it('Delete List Property Button Clicked', async () => {
      expect(getTestObject().results.deleteListPropButtonConfirmed.click);
    });
    it('Cancel List Property Button', async () => {
      expect(getTestObject().results.cancelDeleteListPropButton).toEqual(true);
    });
    // Modal
    it('Delete List Property Modal Pop Up', async () => {
      expect(getTestObject().results.deleteListPropModalPopUp).toEqual(true);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<ListE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (ListE2eTestObject) => any = async (testObject: ListE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(EDIT_LIST_BUTTON),
    testObject.page.waitFor(LISTS_TABLE),
  ]);
  await testObject.page.click(EDIT_LIST_BUTTON);
  await Promise.all([
    testObject.page.waitFor(DELETE_LIST_PROPERTY_BUTTON),
  ]);
  await testObject.page.click(DELETE_LIST_PROPERTY_BUTTON);
  await Promise.all([
    testObject.page.waitFor(DELETE_LIST_PROPERTY_POPUP_MODAL),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before00Setup = async (testObject: ListE2eTestObject) => {
  dLog('before00Setup');
  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo:800,
  });
  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');
  await testObject.page.goto(BASE_URL);

  await login(testObject);
  dLog('At login');
  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(EDIT_LIST_BUTTON),
    testObject.page.waitFor(LISTS_TABLE),
  ]);
  await testObject.page.click(EDIT_LIST_BUTTON);
  await Promise.all([
    testObject.page.waitFor(DELETE_LIST_PROPERTY_BUTTON),
  ]);
  await testObject.page.click(DELETE_LIST_PROPERTY_BUTTON);
  await Promise.all([
    testObject.page.waitFor(DELETE_LIST_PROPERTY_POPUP_MODAL),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: ListE2eTestObject) => {
  await deleteListPropertySetup(testObject);
};

const beforeTests = async (testObject: ListE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => ListE2eTestObject) => {
  deleteListPropertyTest(getTestObject);
};

const after00Teardown = async (testObject: ListE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: ListE2eTestObject) => {
  await after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end list maintenance page test',
  parallelBefore: false,
});
