// tslint:disable:max-line-length
import {
  BASE_URL,
  LIST_MAINTENANCE_URL,
} from '../constants';
import {
  newListName,
} from './constants';
import {

  // Univsersal
  LISTS_HEADER,
  CREATE_LIST_BUTTON,
  READ_ONLY_FILTER,
  EDITABLE_FILTER,
  DELETED_FILTER,
  CREATE_LIST_HEADER,
  SUCCESS_SAVE_LIST,
  EDIT_LIST_BUTTON,
  VIEW_EDIT_LIST_ITEMS_TAB,

  // Create List Item (View/Edit List Items Tab)
  LIST_ITEMS_HEADER,

  // Delete List Item Modal
  DELETE_VIEW_EDIT_LIST_ITEM_BUTTON,
  DELETE_LIST_ITEM_MODAL,
  CONFIRM_DELETE_QUESTION_LABEL,
  PERMANENTLY_DELETE_HEADER,
  CONFIRM_DELETE_CHECKBOX,
  CONFIRM_DELETE_LIST_ITEM_LABEL,
  DELETE_LIST_ITEM_BUTTON,
  CANCEL_DELETE_BUTTON,
  VIEW_EDIT_LIST_ITEMS_TABLE,

}  from './selectors';
import { login } from '../helperFunctions';
import {
  Browser,
  Page,
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentialUser,
  TestCredentials,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole } from 'toolkit';
import expect from 'expect';

const puppeteer = require('puppeteer');
const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type ListE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {

    // Headers
    headerText00?: string,
    title?: string,
    listItemHeader?: string,
    permanentlyDeleteHeader?: string,

    // Checkbox
    confirmDeleteCheckbox?: ElementHandle,

    // Modals & Tables
    deleteListModal?: boolean,
    viewEditListItemTable?: number,

    // Labels
    confirmDeleteQuestionLabel?: string,
    confirmCheckboxLabel?: string,

    // Buttons
    deleteViewListItemButton?: ElementHandle,
    deleteListItemButton?: ElementHandle,
    cancelDeleteButton?: boolean,

  };
};

const deleteListItemSetup = async (testObject: ListE2eTestObject) => {

  // Headers
  testObject.results.permanentlyDeleteHeader = await testObject.page.$eval(PERMANENTLY_DELETE_HEADER, (permDeleteHead: HTMLHeadingElement) => permDeleteHead.textContent);

  // Checkbox
  testObject.results.confirmDeleteCheckbox = await testObject.page.$(CONFIRM_DELETE_CHECKBOX);

  // Modals & Tables
  testObject.results.deleteListModal = await testObject.page.$(DELETE_LIST_ITEM_MODAL) !== null;
  testObject.results.viewEditListItemTable = await testObject.page.$eval(VIEW_EDIT_LIST_ITEMS_TABLE, (table: HTMLElement) => table.childNodes[1].childNodes.length);

  // Labels
  testObject.results.confirmDeleteQuestionLabel = await testObject.page.$eval(CONFIRM_DELETE_QUESTION_LABEL, (confirmQuestion: HTMLLabelElement) => confirmQuestion.textContent);
  testObject.results.confirmCheckboxLabel = await testObject.page.$eval(CONFIRM_DELETE_LIST_ITEM_LABEL, (confirmCheckLabel: HTMLLabelElement) => confirmCheckLabel.textContent);

  // Buttons
  testObject.results.deleteViewListItemButton = await testObject.page.$(DELETE_VIEW_EDIT_LIST_ITEM_BUTTON);
  testObject.results.deleteListItemButton = await testObject.page.$(DELETE_LIST_ITEM_BUTTON);
  testObject.results.cancelDeleteButton = await testObject.page.$(CANCEL_DELETE_BUTTON) !== null;

  await testObject.page.click(CONFIRM_DELETE_CHECKBOX);
  await testObject.page.click(DELETE_LIST_ITEM_BUTTON);
};

const deleteListItemTest = async (getTestObject: () => ListE2eTestObject) => {

  describe('Delete List Item Modal Contents', () => {
    // Headers
    it('Permanently Delete Header', async () => {
      expect(getTestObject().results.permanentlyDeleteHeader).toEqual('Permanently Delete List Item');
    });
    // Labels
    it('Delete Header Question', async () => {
      expect(getTestObject().results.confirmDeleteQuestionLabel).toEqual('Are you sure you want to delete this item? This action cannot be undone!');
    });
    it('Confirm Delete Checkbox Label', async () => {
      expect(getTestObject().results.confirmCheckboxLabel).toEqual('Confirm');
    });
    // Modals & Tables
    it('Delete List Item Modal Present', async () => {
      expect(getTestObject().results.deleteListModal).toEqual(true);
    });
    it('View/Edit List Table Should Have Content', async () => {
      expect(getTestObject().results.viewEditListItemTable).toBeGreaterThan(0);
    });
    // Checkboxes
    it('Confirm Delete Checkbox Checked', async () => {
      expect(getTestObject().results.confirmDeleteCheckbox.click);
    });
    // Buttons
    it('Delete List Item Button', async () => {
      expect(getTestObject().results.deleteListItemButton.click);
    });
    it('Cancel Delete List Item Button Clicked', async () => {
      expect(getTestObject().results.cancelDeleteButton).toEqual(true);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<ListE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (ListE2eTestObject) => any = async (testObject: ListE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
    testObject.page.waitFor(EDIT_LIST_BUTTON),
  ]);
  await testObject.page.click(EDIT_LIST_BUTTON);
  await Promise.all([
    testObject.page.waitFor(VIEW_EDIT_LIST_ITEMS_TAB),
  ]);

  await testObject.page.click(VIEW_EDIT_LIST_ITEMS_TAB);
  await Promise.all([
    testObject.page.waitFor(VIEW_EDIT_LIST_ITEMS_TABLE),
  ]);

  await testObject.page.click(DELETE_VIEW_EDIT_LIST_ITEM_BUTTON),
  await Promise.all([
    testObject.page.waitFor(PERMANENTLY_DELETE_HEADER),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

// Before00Setup
const before00Setup = async (testObject: ListE2eTestObject) => {
  dLog('before00Setup');
  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo: 800,
  });
  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');
  await testObject.page.goto(BASE_URL);

  await login(testObject);
  dLog('At login');
  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
    testObject.page.waitFor(EDIT_LIST_BUTTON),
  ]);
  await testObject.page.click(EDIT_LIST_BUTTON);
  await Promise.all([
    testObject.page.waitFor(VIEW_EDIT_LIST_ITEMS_TAB),
  ]);

  await testObject.page.click(VIEW_EDIT_LIST_ITEMS_TAB);
  await Promise.all([
    testObject.page.waitFor(VIEW_EDIT_LIST_ITEMS_TABLE),
  ]);

  await testObject.page.click(DELETE_VIEW_EDIT_LIST_ITEM_BUTTON),
  await Promise.all([
    testObject.page.waitFor(PERMANENTLY_DELETE_HEADER),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: ListE2eTestObject) => {
  await deleteListItemSetup(testObject);
};

const beforeTests = async (testObject: ListE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => ListE2eTestObject) => {
  deleteListItemTest(getTestObject);
};

const after00Teardown = async (testObject: ListE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: ListE2eTestObject) => {
  await after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end list maintenance page test',
  parallelBefore: false,
});
