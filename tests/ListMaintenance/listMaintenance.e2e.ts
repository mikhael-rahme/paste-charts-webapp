import {
  BASE_URL,
  LIST_MAINTENANCE_URL,
} from '../constants';
import {
  newListName,
} from './constants';
import {
  LISTS_HEADER,
  CREATE_LIST_BUTTON,
  READ_ONLY_FILTER,
  EDITABLE_FILTER,
  DELETED_FILTER,
  LISTS_TABLE,
  TABLE_PAGE_NAVIGATION,
  NEXT_PAGE_TABLE_NAV,
  PREV_PAGE_TABLE_NAV,
  CREATE_LIST_HEADER,
  LIST_PROPERTIES_TAB,
  VIEW_EDIT_LIST_ITEMS_TAB,
  GROUPS_TAB,
  SAVE_LIST_BUTTON,
  CANCEL_LIST_BUTTON,
  LIST_PROPERTIES_HEADER,
  LIST_NAME_INPUT,
  LIST_ITEM_PROPERTIES_HEADER,
  ADD_PROPERTY_BUTTON,
  LIST_ITEMS_HEADER,
  VIEW_EDIT_LIST_ITEMS_TABLE,
  GROUP_PERMISSIONS_HEADER,
  GROUP_PERMISSIONS_DIV_CONTAINER,
} from './selectors';
import { isLoggedIn, login, waitForFunction } from '../helperFunctions';
import { Browser, ElementHandle, JSHandle, Page } from 'puppeteer';
import { headerTest } from '../Header/header.e2e';
import { createListPropertyTest } from './createListProperty.e2e';
import { createListItemTest } from './createListItem.e2e';
import { TestCredentialUser, TestCredentials } from 'toolkit/lib/testUtils';
import { IterrexRole } from 'toolkit';
import runTestsForAllUsers from 'toolkit/lib/testUtils/runTestsForAllUsers';
import { isDisabled } from 'app/utils/utilities';

const puppeteer = require('puppeteer');
const DEBUG = true;
const dLog = (message: string) => DEBUG && console.log(message);

export type ListE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    // elementQuery
    title?: string,
    headerText00?: string,
    createListButton?: ElementHandle,
    listsTable?: ElementHandle,
    listsTableLength00?: number,
    // filterState
    filterState00?: boolean,
    filterState01?: boolean,
    filterState02?: boolean,
    // createList
    headerText01?: string,
    listPropertiesTab?: string,
    viewEditTab?: string,
    groupsTab?: string,
    saveListButtonText?: string,
    cancelListButtonText?: string,
    listPropertiesHeader?: string,
    listNameInput?: string,
    listItemPropertiesHeader?: string,
    addPropertyButton?: string,
    listItemsHeader?: string,
    viewEditListItemsTable?: ElementHandle,
    groupPermissionsHeader?: string,
    groupPermissionsDivContainer?: string,
    // filterFunction
    filterState03?: boolean,
    filterState04?: boolean,
    listsTableLength01?: number,
    tableItemText?: string,
    filterState05?: boolean,
    tableEditAndDelete?: boolean,
  },
};

// Element Query Setup
const elementQuerySetup = async (testObject: ListE2eTestObject) => {
  // Headers & Titles
  testObject.results.title = await testObject.page.title();
  testObject.results.headerText00 = await testObject.page.$eval(LISTS_HEADER, (header: HTMLElement) => header.innerText);
  // Buttons
  testObject.results.createListButton = await testObject.page.$(CREATE_LIST_BUTTON);
  // Tables
  testObject.results.listsTable = await testObject.page.$(LISTS_TABLE);
  testObject.results.listsTableLength00 =
      await testObject.page.$eval(LISTS_TABLE, (table: HTMLElement) => table.childNodes[1].childNodes.length);
};
// Element Query Tests
const elementQueryTest = (iterrexRole: IterrexRole, getTestObject: () => ListE2eTestObject) => {
  describe('Newly opened List Maintenance page contains:', () => {
    it('correct page title', async () => {
      expect(getTestObject().results.title).toEqual('List Maintenance - iterrex');
    });

    it('header with "Lists" text', async () => {
      expect(getTestObject().results.headerText00).toEqual('Lists');
    });

    it('Create List button', async () => {
      expect(getTestObject().results.createListButton).not.toBeUndefined();
    });

    it('table containing Lists', async () => {
      expect(getTestObject().results.listsTable).not.toBeUndefined();
    });

    it('more than 0 list items', async () => {
      expect(getTestObject().results.listsTableLength00).toBeGreaterThan(0);
    });
  });
};
// Filter State Setup
const filterStateSetup = async (testObject: ListE2eTestObject) => {
  testObject.results.filterState00 =
    (await testObject.page.$eval(READ_ONLY_FILTER, (button: HTMLElement) => button.className)).includes('active');
  testObject.results.filterState01 =
    (await testObject.page.$eval(EDITABLE_FILTER, (button: HTMLElement) => button.className)).includes('active');
  testObject.results.filterState02 =
    (await testObject.page.$eval(DELETED_FILTER, (button: HTMLElement) => button.className)).includes('active');
};
// Filter State Tests
const filterStateTest = (iterrexRole: IterrexRole, getTestObject: () => ListE2eTestObject) => {
  describe('Testing Filter initialization', () => {
    it('Readonly filter should be active', () => {
      expect(getTestObject().results.filterState00).toEqual(true);
    });

    it('Editable filter should be active', () => {
      expect(getTestObject().results.filterState01).toEqual(true);
    });

    it('Deleted filter should be inactive', () => {
      expect(getTestObject().results.filterState02).toEqual(false);
    });
  });
};

// Create List Setup
const createListSetup = async (testObject: ListE2eTestObject) => {
  // Create List Button Click & What's Expected
  await testObject.page.click(CREATE_LIST_BUTTON);
  testObject.results.headerText01 = await testObject.page.$eval(CREATE_LIST_HEADER, (h1: HTMLHeadingElement) => h1.textContent);
  testObject.results.listPropertiesTab = await testObject.page.$eval(LIST_PROPERTIES_TAB, (tab: HTMLElement) => tab.textContent);
  testObject.results.viewEditTab = await testObject.page.$eval(VIEW_EDIT_LIST_ITEMS_TAB, (tab: HTMLElement) => tab.textContent);
  testObject.results.groupsTab = await testObject.page.$eval(GROUPS_TAB, (tab: HTMLElement) => tab.textContent);
  testObject.results.saveListButtonText = await testObject.page.$eval(SAVE_LIST_BUTTON, (button: HTMLButtonElement) => button.textContent);
  testObject.results.cancelListButtonText =
      await testObject.page.$eval(CANCEL_LIST_BUTTON, (button: HTMLButtonElement) => button.textContent);

// List Properties Tab Click & What's Expected
  await testObject.page.click(LIST_PROPERTIES_TAB);
  testObject.results.listPropertiesHeader = await testObject.page.$eval(LIST_PROPERTIES_HEADER, (h1: HTMLElement) => h1.textContent);
  testObject.results.listNameInput = await testObject.page.$eval(LIST_NAME_INPUT, (input: HTMLInputElement) => input.value);
  testObject.results.listItemPropertiesHeader =
      await testObject.page.$eval(LIST_ITEM_PROPERTIES_HEADER, (h1: HTMLHeadingElement) => h1.textContent);
  testObject.results.addPropertyButton =
      await testObject.page.$eval(ADD_PROPERTY_BUTTON, (button: HTMLButtonElement) => button.textContent);

// View/Edit List Items Tab Click & What's Expected
  await testObject.page.click(VIEW_EDIT_LIST_ITEMS_TAB);
  testObject.results.listItemsHeader = await testObject.page.$eval(LIST_ITEMS_HEADER, (h1: HTMLHeadingElement) => h1.textContent);
  testObject.results.viewEditListItemsTable = await testObject.page.$(VIEW_EDIT_LIST_ITEMS_TABLE);

// Group Tab Click & What's Expected
  await testObject.page.click(GROUPS_TAB);
  testObject.results.groupPermissionsHeader = await testObject.page.$eval(GROUP_PERMISSIONS_HEADER, (h1: HTMLElement) => h1.textContent);
  testObject.results.groupPermissionsDivContainer =
      await testObject.page.$eval(GROUP_PERMISSIONS_DIV_CONTAINER, (div: HTMLElement) => div.childElementCount);
};

// Create List Test
const createListTest = async (iterrexRole: IterrexRole, getTestObject: () => ListE2eTestObject) => {
  describe('Newly opened Create List modal contains', () => {
    it('header with text "Create List"', async  () => {
      expect(getTestObject().results.headerText01).toEqual('Create List');
    });

    it('tab with text "List Properties"', async () => {
      expect(getTestObject().results.listPropertiesTab).toEqual('List Properties');
    });

    it('tab with text "View/Edit List Items"', async () => {
      expect(getTestObject().results.viewEditTab).toEqual('View/Edit List Items');
    });

    it('tab with text "Groups"', async () => {
      expect(getTestObject().results.groupsTab).toEqual('Groups');
    });

    it('button with text "Save List"', async () => {
      expect(getTestObject().results.saveListButtonText).toEqual(' Save List');
    });

    it('button with text "Cancel"', async () => {
      expect(getTestObject().results.cancelListButtonText).toEqual('Cancel');
    });

    // List Properties Tab Tests
    describe('Under List Properties tab', () => {
      it('header with text "List Properties"', async () => {
        expect(getTestObject().results.listPropertiesHeader).toEqual('List Properties');
      });

      it('empty input for list name', async () => {
        expect(getTestObject().results.listNameInput).toEqual('');
      });

      it('header with text "List Item Properties"', async () => {
        expect(getTestObject().results.listItemPropertiesHeader).toEqual('List Item Properties');
      });

      it('button with text "Add Property"', async () => {
        expect(getTestObject().results.addPropertyButton).toEqual(' Add Property');
      });
    });

    // View/Edit List Item Tests
    describe('Under View/Edit List Items tab', () => {
      it('header with text "List Items"', async () => {
        expect(getTestObject().results.listItemsHeader).toEqual('List Items');
      });

      it('no table of viewable list items', async () => {
        expect(getTestObject().results.viewEditListItemsTable).toBeNull();
      });
    });

    // Group Tab's Tests
    describe('Under Groups tab', () => {
      it('header with text "Group Permissions"', async () => {
        expect(getTestObject().results.groupPermissionsHeader).toEqual('Group Permissions');
      });

      it('div containing divs with group permission data', async () => {
        expect(getTestObject().results.groupPermissionsDivContainer).toBeGreaterThanOrEqual(1);
      });
    });
  });
};

// Filter Function Setup
const filterFunctionSetup = async (testObject: ListE2eTestObject) => {
// Read Only Click & What's Expected
  await testObject.page.click(READ_ONLY_FILTER);
  testObject.results.filterState03 =
      (await testObject.page.$eval(READ_ONLY_FILTER, (button: HTMLElement) => button.className)).includes('active');

// Editable Filter Click & What's Expected
  await testObject.page.click(EDITABLE_FILTER);
  testObject.results.filterState04 =
      (await testObject.page.$eval(EDITABLE_FILTER, (button: HTMLElement) => button.className)).includes('active');
  testObject.results.listsTableLength01 =
      await testObject.page.$eval(LISTS_TABLE, (table: HTMLElement) => table.childNodes[1].childNodes.length);
  testObject.results.tableItemText =
      await testObject.page.$eval(LISTS_TABLE, (table: HTMLElement) => table.childNodes[1].childNodes[0].textContent);

// Editable Filter & Table Navigation
  await testObject.page.click(EDITABLE_FILTER);
  testObject.results.filterState05 =
      (await testObject.page.$eval(EDITABLE_FILTER, (button: HTMLElement) => button.className)).includes('active');
  let rowButts: ElementHandle[] = [];
  rowButts = await testObject.page.$$('tbody tr');
  if ((await testObject.page.$(TABLE_PAGE_NAVIGATION)) !== null) {
    const pageNum = await testObject.page.$eval(TABLE_PAGE_NAVIGATION, (nav: HTMLElement) => nav.childNodes[0].childNodes.length) - 2;
    for (let i = 1; i < pageNum; i++) {
      await testObject.page.click(NEXT_PAGE_TABLE_NAV);
      rowButts.push(...(await testObject.page.$$('tbody tr')));
    }
  }
  testObject.results.tableEditAndDelete = true;
  for (const el of rowButts) {
    if ((await el.$$('button')).length < 2) {
      testObject.results.tableEditAndDelete = false;
    }
  }
};

// Filter Function Test
const filterFunctionTest = (iterrexRole: IterrexRole, getTestObject: () => ListE2eTestObject) => {
  describe('Attempting to navigate and filter table', () => {
    it('should deactivate Readonly filter on click', () => {
      expect(getTestObject().results.filterState03.isDisabled);
    });

    it('should deactivate Editable filter on click', () => {
      expect(getTestObject().results.filterState04.isDisabled);
    });

    it('should display 1 table item with all filters deactivated', () => {
      expect(getTestObject().results.listsTableLength01).toBeGreaterThan(0);
    });

    // it('table item should be "no data" notification', () => {
    //   expect(getTestObject().results.tableItemText).toEqual('There is no data available');
    // });

    // TO-DO: add tests for Readonly filter when working

    it('should activate Editable filter on click', () => {
      expect(getTestObject().results.filterState05).toEqual(true);
    });

    it('should display Readonly table items only', () => {
      expect(getTestObject().results.tableEditAndDelete).toEqual(false);
    });

    // TO-DO: add tests for Deleted filter when working
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<ListE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (ListE2eTestObject) => any = async (testObject: ListE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
    testObject.page.waitFor(LISTS_TABLE),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before00Setup = async (testObject: ListE2eTestObject) => {
  dLog('before00Setup');
  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo:800,
  });
  testObject.page = await testObject.browser.newPage();
  dLog('browser launched');
  await testObject.page.goto(BASE_URL);

  await login(testObject);
  dLog('At login');
  await testObject.page.goto(LIST_MAINTENANCE_URL);

  await Promise.all([
    testObject.page.waitFor(LISTS_HEADER),
    testObject.page.waitFor(CREATE_LIST_BUTTON),
    testObject.page.waitFor(LISTS_TABLE),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: ListE2eTestObject) => {
  await elementQuerySetup(testObject);
  await createListSetup(testObject);
  await filterStateSetup(testObject);
  await filterFunctionSetup(testObject);
};

const beforeTests = async (testObject: ListE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => ListE2eTestObject) => {
  elementQueryTest(iterrexRole, getTestObject);
  createListTest(iterrexRole, getTestObject);
  filterStateTest(iterrexRole, getTestObject);
  filterFunctionTest(iterrexRole, getTestObject);
};

const after00Teardown = async (testObject: ListE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: ListE2eTestObject) => {
  await after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end list maintenance page test',
  parallelBefore: false,
});
