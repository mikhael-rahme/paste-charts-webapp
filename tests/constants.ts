export const BASE_URL = 'http://staging-local.staging.iterrex.com:3000/';
const url = suffix => `${BASE_URL}${suffix}`;
export const HOME_URL = url('home');
export const LOGIN_URL = url('login');
export const UI_DASHBOARD_URL = url('ui-dashboard');
export const UI_BUILDER_URL = url('ui-builder');
export const LIST_MAINTENANCE_URL = url('list-maintenance');
export const USER_ADMIN_URL = url('user-admin');
export const GROUP_ADMIN_URL = url('group-admin');
export const D3_GRAPH_URL = url('d3-graph');

export const USERNAME = 'garett@coveycs.com';
export const PASSWORD = 'testPassword1!';

export const BODY_SELECTOR = 'body';
