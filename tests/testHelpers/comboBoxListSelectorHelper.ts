import { Page } from 'puppeteer';

export type ComboBoxListSelector = {
  page: Page,
  comboBoxListSelector: string,
  options?: {
    debug?: boolean,
  },
};

export const ComboBoxListSelectorHelp = ({
  comboBoxListSelector,
  page,
  options,
}: ComboBoxListSelector) => {
  const someFn = selector => Array.from(
    document.querySelectorAll(selector),
    e => e.textContent,
  );

  // Selectors
  const caretSelector = `${comboBoxListSelector} .rw-i-caret-down`;
  const selectOption = `${comboBoxListSelector} .rw-list-option`;

  // Debug
  const debug = options && options.debug || false;

  const dlog = (message:any) => {
    debug && console.log(message);
  };

  const focusOnInput = async () => {
    dlog('focusOnInput: waitForSelector');
    await page.waitForSelector(comboBoxListSelector);
    dlog('focusOnInput: focus');
    await page.focus(comboBoxListSelector);
    dlog('focusOnInput: done!');
  };

  dlog(comboBoxListSelector);
  const clickCaretDown = async (retry: number = 0) => {
    if (retry < 0) {
      throw new Error('You must provide a number less than 1!');
    }
    let retries = -1;
    while (retry > retries) {
      try {
        dlog('clickInput: waitForSelector');
        await page.waitForSelector(caretSelector);
        dlog('clickInput: hover');
        await page.hover(caretSelector);
        dlog('clickInput: click');
        await page.click(caretSelector);
        dlog('clickInput: done!');
        break;
      } catch (err) {
        if (retry > retries) {
          retries++;
          console.log(`error; retrying ${retry}...`);
        } else {
          throw err;
        }
      }
    }
  };
  const clickInput  = async (retry: number = 0) => {
    if (retry < 0) {
      throw new Error('You must provide a number less than 1!');
    }
    let retries = -1;
    while (retry > retries) {
      try {
        dlog('clickInput: waitForSelector');
        await page.waitForSelector(selectOption);
        dlog('clickInput: hover');
        await page.hover(selectOption);
        dlog('clickInput: click');
        await page.click(selectOption);
        dlog('clickInput: done!');
        break;
      } catch (err) {
        if (retry > retries) {
          retries++;
          console.log(`error; retrying ${retry}...`);
        } else {
          throw err;
        }
      }
    }
  };

  return {
    dlog,
    focusOnInput,
    clickCaretDown,
    clickInput,
  };
};
