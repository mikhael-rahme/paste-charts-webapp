import {
  createHtmlId,
} from '../../app/utils/createHtmlId';

import {
  createInputSelector,
  CreateInputSelectorInput,
} from './createInputSelector';

describe('createInputSelector', () => {
  it('should not throw', () => {
    const formFileName = 'SomeFile';
    const formFieldLabel = 'someField';
    expect(() => createInputSelector({
      formFileName,
      formFieldLabel,
      inputType: 'input',
    })).not.toThrow();
  });
  it('should return the correct output for inputs', () => {
    const formFileName = 'SomeFile';
    const formFieldLabel = 'someField';
    const path = `${formFileName}/${formFieldLabel}`;
    const expected = `input[id='${createHtmlId(path)}']`;
    expect(createInputSelector({
      formFileName,
      formFieldLabel,
      inputType: 'input',
    })).toEqual(expected);
  });

  it('should return the correct output for checkbox', () => {
    const formFileName = 'SomeFile';
    const formFieldLabel = 'someField';
    const path = `${formFileName}/${formFieldLabel}`;
    const expected = `input[id='${createHtmlId(path)}']`;
    expect(createInputSelector({
      formFileName,
      formFieldLabel,
      inputType: 'input',
    })).toEqual(expected);
  });

  it('should return the correct output for dropdownlist', () => {
    const formFileName = 'SomeFile';
    const formFieldLabel = 'someField';
    const path = `${formFileName}/${formFieldLabel}`;
    const expected = `div[id='${createHtmlId(path)}_input']`;
    expect(createInputSelector({
      formFileName,
      formFieldLabel,
      inputType: 'dropdownlist',
    })).toEqual(expected);
  });

  it('should return the correct output for buttons', () => {
    const formFileName = 'SomeFile';
    const formFieldLabel = 'someField';
    const path = `${formFileName}/${formFieldLabel}`;
    const expected = `button[id='${createHtmlId(path)}']`;
    expect(createInputSelector({
      formFileName,
      formFieldLabel,
      inputType: 'button',
    })).toEqual(expected);
  });
});
