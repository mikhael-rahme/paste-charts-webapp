import { snakeCase } from 'lodash';
import { createInputSelector } from './createInputSelector';

export const reduceConstants = formFileName => (inputType, suffix = '') => (acc, formField) => ({
  ...acc,
  [snakeCase(formField).toUpperCase()]: createInputSelector({
    formFileName,
    inputType,
    formFieldLabel: `${formField}${suffix}`,
  }),
});
export default reduceConstants;
