import {
  createHtmlId,
} from '../../app/utils/createHtmlId';

export type CreateInputSelectorInput = {
  formFileName: string; // eg. LoginForm
  formFieldLabel: string; // eg. username
  inputType:
  'input' |
  'button' |
  'li' |
  'h3' |
  'h5' |
  'textarea' |
  'dropdownlist' |
  'dropdown' |
  'checkbox' |
  'label' |
  'div' |
  'combobox' |
  'dropdownlistitem' |
  'numberpicker' |
  'multiselect' |
  'text' |
  'th' |
  'table';
};

export type CreateOptionsSelector = {
  title: string;
  formFieldName: string;
  inputType: 'li';
};

export const createInputSelector = ({
  formFileName,
  formFieldLabel,
  inputType,
}: CreateInputSelectorInput): string => {
  const path = `${formFileName}/${formFieldLabel}`;
  switch (inputType) {
    case 'dropdown':
      return `div[id='${createHtmlId(path)}'] .dropdown-toggle`;
    case 'dropdownlist':
      return `div[id='${createHtmlId(path)}_input']`;
    case 'dropdownlistitem':
      return `ul[id='${createHtmlId(path)}_listbox']`;
    case 'checkbox':
      return `input[id='${createHtmlId(path)}']`;
    case 'numberpicker':
      return `div[id='${createHtmlId(path)}']`;
    case 'text':
      return `input[id='${createHtmlId(path)}']`;
    case 'combobox':
      return `div[id='${createHtmlId(path)}']`;
    case 'th':
      return `th[id='${createHtmlId(path)}']`;
    case 'table':
      return `table[id='${createHtmlId(path)}']`;
    case 'div':
      return `div[id='${createHtmlId(path)}']`;
    default:
      return `${inputType}[id='${createHtmlId(path)}']`;
  }
};

export const createOptionsSelector = ({
  title,
  formFieldName,
  inputType,
}: CreateOptionsSelector): string => {
  const path = `${formFieldName}/${title}`;
  return `${inputType}[id='${createHtmlId(path)}']`;
};
