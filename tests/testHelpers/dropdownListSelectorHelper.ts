import { Page } from 'puppeteer';

export type DropDownListTesterInput = {
  page: Page,
  dropDownListSelector: string,
  options?: {
    debug?: boolean,
  },
};

export const DropDownListTester = ({
  dropDownListSelector,
  page,
  options,
}: DropDownListTesterInput) => {
  const someFn = selector => Array.from(
    document.querySelectorAll(selector),
    e => e.textContent,
  );

  const dropDownListItemSelector = `${dropDownListSelector} .rw-list-option`;
  const caretSelector = `${dropDownListSelector} .rw-i-caret-down`;
  const busySelector = `${dropDownListSelector} .rw-loading`;
  const emptyListSelector = `${dropDownListSelector} .rw-list-empty`;
  const dropDownListTextInputSelector = `${dropDownListSelector} .rw-input-reset`;
  const dropDownListInput = `${dropDownListSelector} .rw-input.rw-dropdown-list-input`;

  const debug = options && options.debug || false;

  const dlog = (message:any) => {
    debug && console.log(message);
  };

  const focusOnInput = async () => {
    dlog('focusOnInput: waitForSelector');
    await page.waitForSelector(dropDownListSelector);
    dlog('focusOnInput: focus');
    await page.focus(dropDownListSelector);
    dlog('focusOnInput: done!');
  };

  const clickCaretDown = async (retry: number = 0) => {
    if (retry < 0) {
      throw new Error('You must provide a number less than 1!');
    }
    let retries = -1;
    while (retry > retries) {
      try {
        dlog('clickInput: waitForSelector');
        await page.waitForSelector(caretSelector);
        dlog('clickInput: hover');
        await page.hover(caretSelector);
        dlog('clickInput: click');
        await page.click(caretSelector);
        dlog('clickInput: done!');
        break;
      } catch (err) {
        if (retry > retries) {
          retries++;
          console.log(`error; retrying ${retry}...`);
        } else {
          throw err;
        }
      }
    }
  };
  const clickInput  = async (retry: number = 0) => {
    if (retry < 0) {
      throw new Error('You must provide a number less than 1!');
    }
    let retries = -1;
    while (retry > retries) {
      try {
        dlog('clickInput: waitForSelector');
        await page.waitForSelector(dropDownListInput);
        dlog('clickInput: hover');
        await page.hover(dropDownListInput);
        dlog('clickInput: click');
        await page.click(dropDownListInput);
        dlog('clickInput: done!');
        break;
      } catch (err) {
        if (retry > retries) {
          retries++;
          console.log(`error; retrying ${retry}...`);
        } else {
          throw err;
        }
      }
    }
  };

  const type = async (text: string) => {
    dlog('type: waiting for input selector');
    await page.waitForSelector(dropDownListTextInputSelector);
    dlog('type: typing');
    return page.type(dropDownListTextInputSelector, text);
  };

  const waitForOptionsLoad = async () => {
    dlog('waitForOptionsLoad');
    await page.waitForSelector(busySelector);
    dlog('found busySelector');
    await page.waitForSelector(caretSelector);
    dlog('found caretSelector');
  };

  const getOptionsTextNew = async (fn): Promise<string[]> => {
    dlog('getOptionsText: evaluate');

    const options = await page.evaluate(
      fn,
      dropDownListItemSelector);
    dlog('getOptionsText: done!');
    return options;
  };
  const getOptionsText = async (): Promise<string[]> => {
    dlog('getOptionsText: evaluate');

    const options = await page.evaluate(`Array.from(
      document.querySelectorAll("${dropDownListItemSelector}"),
      e => e.textContent,
    );`);
    dlog(`typeof options: ${typeof options}`);
    dlog(`options: ${options}`);
    dlog(`stringify options: ${JSON.stringify(options, null, 2)}`);
    dlog('getOptionsText: done!');
    return options;
  };

  const selectDropDownItemByText = async (text: string) => {
    dlog('selectDropDownItemByText');
    dlog(dropDownListItemSelector);
    /*
    // const textOptions = await getOptionsText();
    const targetOption = textOptions.find(option => option === text);

    if (!targetOption) {
      throw new Error(`DropDownListTester: Could not find a list item with text ${text}`);
    }*/
    const escapedText = escapeXpathString(text || '');
    const linkHandlers = await page.$x(`${dropDownListSelector}/li[contains(text(), ${escapedText})]`);
    if (linkHandlers.length > 0) {
      await linkHandlers[0].click();
    } else {
      throw new Error(`DropDownListTester: Link not found: ${text}`);
    }
  };

  return {
    dlog,
    focusOnInput,
    clickCaretDown,
    clickInput,
    type,
    waitForOptionsLoad,
    getOptionsText,
    selectDropDownItemByText,
    dropDownListItemSelector,
    test: `() => Array.from(
        document.querySelectorAll(${dropDownListItemSelector}),
        e => e.textContent,
      );`,
  };
};
/*
export class DropDownListTester_OLD {
  dropDownListSelector: string; // main dropdownlist selector
  dropDownListItemSelector: string;
  dropDownListTextInputSelector: string; // where the text is typed
  dropDownListInput: string; // new
  busySelector: string;
  caretSelector: string;
  emptyListSelector: string;
  page: Page;
  debug: boolean;
  constructor(
    formFileName: string,
    formFieldLabel: string,
    page: Page,
    options?: {
      debug?: boolean,
    }) {
    this.dropDownListSelector = createInputSelector({
      formFileName,
      formFieldLabel,
      inputType: 'dropdownlist',
    });
    this.dropDownListItemSelector = `${this.dropDownListSelector} .rw-list-option`;
    this.caretSelector = `${this.dropDownListSelector} .rw-i-caret-down`;
    this.busySelector = `${this.dropDownListSelector} .rw-loading`;
    this.emptyListSelector = `${this.dropDownListSelector} .rw-list-empty`;
    this.dropDownListTextInputSelector = `${this.dropDownListSelector} .rw-input-reset`;
    this.dropDownListInput = `${this.dropDownListSelector} .rw-input.rw-dropdown-list-input`;

    this.page = page;
    this.debug = options && options.debug || false;

    this.dlog(JSON.stringify(
      {
        dropDownListSelector: this.dropDownListSelector,
        dropDownListItemSelector: this.dropDownListItemSelector,
      },
      null,
      2));
  }

  private dlog = (message:any) => {
    this.debug && console.log(message);
  }

  focusOnInput = async () => {
    this.dlog('focusOnInput: waitForSelector');
    await this.page.waitForSelector(this.dropDownListSelector);
    this.dlog('focusOnInput: focus');
    await this.page.focus(this.dropDownListSelector);
    this.dlog('focusOnInput: done!');
  }

  clickInput = async () => {
    this.dlog('clickInput: waitForSelector');
    await this.page.waitForSelector(this.dropDownListInput);
    this.dlog('clickInput: hover');
    await this.page.hover(this.dropDownListInput);
    this.dlog('clickInput: click');
    await this.page.click(this.dropDownListInput);
    this.dlog('clickInput: done!');
  }

  type = async (text: string) => {
    this.dlog('type: waiting for input selector');
    await this.page.waitForSelector(this.dropDownListTextInputSelector);
    this.dlog('type: typing');
    return this.page.type(this.dropDownListTextInputSelector, text);
  }

  waitForOptionsLoad = async () => {
    this.dlog('waitForOptionsLoad');
    await this.page.waitForSelector(this.busySelector);
    this.dlog('found busySelector');
    await this.page.waitForSelector(this.caretSelector);
    this.dlog('found caretSelector');
  }

  getOptionsText  = async () => {
    this.dlog('getOptionsText');
    // make sure it's not loading
    await this.page.waitForSelector(this.caretSelector);

    // see if it's either empty or not

    // const content: string[] = await this.page.evaluate('1 + 2', console.log(this.dropDownListItemSelector));

    // const content: string[] = await this.page.evaluate(() => {
    //   const element = Array.from(document.querySelectorAll("div[id='61ab68b8-f00c-5fc5-93d8-d94bdfc27c1a_input'] .rw-list-option"), );
    //   const link = element.map(element => element.textContent);
    //   return link;
    // },
    //   ) as string[];
    // return content;

    try {
      this.dlog(`getOptionsText; get any list items with: ${this.dropDownListItemSelector}`);
      await this.page.waitForSelector(this.dropDownListItemSelector);
      this.dlog('getOptionsText; evaluate all list item text');

      this.dlog('getOptionsText; done!');
      return ['fuuuu'];
    } catch (err) {
      console.log(err);
    }
  }

  // exact text match, uses click()
  selectDropDownItemByText = async (text: string) => {
    this.dlog('selectDropDownItemByText');

    const escapedText = escapeXpathString(text);
    const linkHandlers = await this.page.$x(`${this.dropDownListSelector}/li[contains(text(), ${escapedText})]`);
    if (linkHandlers.length > 0) {
      await linkHandlers[0].click();
    } else {
      throw new Error(`DropDownListTester: Link not found: ${text}`);
    }
  }
}
*/

const escapeXpathString = (str: string) => {
  const splitedQuotes = str.replace(/'/g, `', "'", '`);
  return `concat('${splitedQuotes}', '')`;
};
/*
const clickByText = async (page: Page, text: string) => {
  const escapedText = escapeXpathString(text);
  const linkHandlers = await page.$x(`${}//a[contains(text(), ${escapedText})]`);

  if (linkHandlers.length > 0) {
    await linkHandlers[0].click();
  } else {
    throw new Error(`Link not found: ${text}`);
  }
};
*/
