import {
  HOME_URL,
  LIST_MAINTENANCE_URL,
  UI_DASHBOARD_URL,
} from '../constants';
import {
  HEADER_HOME_BUTTON,
  HEADER_LIST_MAINTENANCE_BUTTON,
  HEADER_UI_BUILDER_BUTTON,
  HEADER_DROPDOWN_MENU,
} from './selectors';
import { dropdownTest } from './dropdown.e2e';
import { waitForURLChange, isLoggedIn, login } from '../helperFunctions';
import { e2eTestObject } from '../types';

const assert = require('assert');
const { expect } = require('chai');

export const headerTest = (testObject: e2eTestObject, pageURL: string) => {
  describe('Testing header on current page', () => {
    let page;

    const standardBefore = async () => {
      page = await testObject.browser.newPage();
      await console.log('\tPage opened');

      await page.goto(pageURL);

      if (!await isLoggedIn(page)) {
        await login(page);
      }

      await Promise.all([
        page.waitFor(HEADER_HOME_BUTTON),
        page.waitFor(HEADER_LIST_MAINTENANCE_BUTTON),
        page.waitFor(HEADER_UI_BUILDER_BUTTON),
        page.waitFor(HEADER_DROPDOWN_MENU),
      ]);
    };

    describe('Header contains:', () => {
      before(standardBefore);
      after(async () => await page.close().then(console.log('\tPage closed')));

      it('Home button', async () => {
        expect(await page.$(HEADER_HOME_BUTTON)).to.not.be.null;
      });

      it('List Maintenance button', async () => {
        expect(await page.$(HEADER_LIST_MAINTENANCE_BUTTON)).to.not.be.null;
      });

      it('UI Builder button', async () => {
        expect(await page.$(HEADER_UI_BUILDER_BUTTON)).to.not.be.null;
      });

      it('dropdown menu', async () => {
        expect(await page.$(HEADER_DROPDOWN_MENU)).to.not.be.null;
      });
    });

    describe('Header buttons:', () => {
      let currentURL: string;
      let isExpanded: boolean;
      before(standardBefore);
      beforeEach(async () => await page.goto(pageURL));
      after(async () => await page.close().then(console.log('\tPage closed')));

      it('should successfully navigate to /home', async () => {
        await page.click(HEADER_HOME_BUTTON);
        currentURL = await page.evaluate(() => location.href);

        expect(currentURL).to.eql(HOME_URL);
      });

      it('should successfully navigate to /list-maintenance', async () => {
        await page.click(HEADER_LIST_MAINTENANCE_BUTTON);
        currentURL = await page.evaluate(() => location.href);

        expect(currentURL).to.eql(LIST_MAINTENANCE_URL);
      });

      it('should successfully navigate to /ui-dashboard', async () => {
        await page.click(HEADER_UI_BUILDER_BUTTON);
        currentURL = await page.evaluate(() => location.href);

        expect(currentURL).to.eql(UI_DASHBOARD_URL);
      });

      it('should expand dropdown menu on click', async () => {
        await page.click(HEADER_DROPDOWN_MENU);
        isExpanded = (await page.$eval(HEADER_DROPDOWN_MENU, button => button.outerHTML)).includes('active');
        expect(isExpanded).to.eql(true);
      });

      it('should close dropdown menu on second click', async () => {
        await page.click(HEADER_DROPDOWN_MENU);
        await page.waitFor(100);
        await page.click(HEADER_DROPDOWN_MENU);
        isExpanded = (await page.$eval(HEADER_DROPDOWN_MENU, button => button.outerHTML)).includes('active');
        expect(isExpanded).to.eql(false);
      });
    });

    dropdownTest(testObject, pageURL);
  });
};
