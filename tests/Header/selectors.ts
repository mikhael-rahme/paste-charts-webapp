// Header elements
export const HEADER_HOME_BUTTON =
'#app > div > div > div:nth-child(1) > nav > ul:nth-child(1) > a';
export const HEADER_LIST_MAINTENANCE_BUTTON =
'#app > div > div > div:nth-child(1) > nav > ul:nth-child(1) > li > a';
export const HEADER_UI_BUILDER_BUTTON =
'#app > div > div > div:nth-child(1) > nav > ul:nth-child(1) > li:nth-child(3) > a';
export const HEADER_DROPDOWN_MENU =
'#app > div > div > div:nth-child(1) > nav > ul.ml-auto.navbar-nav > ul > div > button';

// Dropdown menu elements
export const DROPDOWN_ADMIN_MAINTENANCE =
'#app > div > div > div:nth-child(1) > nav > ul.ml-auto.navbar-nav > ul > div > div > h6';
export const DROPDOWN_USERS_BUTTON =
'#app > div > div > div:nth-child(1) > nav > ul.ml-auto.navbar-nav > ul > div > div > button:nth-child(2)';
export const DROPDOWN_GROUPS_BUTTON =
'#app > div > div > div:nth-child(1) > nav > ul.ml-auto.navbar-nav > ul > div > div > button:nth-child(3)';
export const DROPDOWN_UI_BUILDER_BUTTON =
'#app > div > div > div:nth-child(1) > nav > ul.ml-auto.navbar-nav > ul > div > div > button:nth-child(4)';
export const DROPDOWN_D3_GRAPHS_BUTTON =
'#app > div > div > div:nth-child(1) > nav > ul.ml-auto.navbar-nav > ul > div > div > button:nth-child(5)';
export const DROPDOWN_DIVIDER =
'#app > div > div > div:nth-child(1) > nav > ul.ml-auto.navbar-nav > ul > div > div > div';
export const DROPDOWN_CHANGE_PASSWORD_BUTTON =
'#app > div > div > div:nth-child(1) > nav > ul.ml-auto.navbar-nav > ul > div > div > button:nth-child(7)';
export const DROPDOWN_LOG_OUT_BUTTON =
'#app > div > div > div:nth-child(1) > nav > ul.ml-auto.navbar-nav > ul > div > div > button:nth-child(8)';

// Hubbell Stuff
export const HUBBELL_DROPDOWN =
'#app > div > div > div:nth-child(1) > nav > ul.ml-auto.navbar-nav > ul > div > button';
