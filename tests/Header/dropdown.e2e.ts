import {
  USER_ADMIN_URL,
  GROUP_ADMIN_URL,
  UI_BUILDER_URL,
  D3_GRAPH_URL,
} from '../constants';
import {
  HEADER_DROPDOWN_MENU,
  DROPDOWN_ADMIN_MAINTENANCE,
  DROPDOWN_USERS_BUTTON,
  DROPDOWN_GROUPS_BUTTON,
  DROPDOWN_UI_BUILDER_BUTTON,
  DROPDOWN_D3_GRAPHS_BUTTON,
  DROPDOWN_DIVIDER,
  DROPDOWN_CHANGE_PASSWORD_BUTTON,
  DROPDOWN_LOG_OUT_BUTTON,
} from './selectors';
import { changePasswordTest } from '../ChangePasswordModal/changePassword.e2e';
import { waitForURLChange, isLoggedIn, login } from '../helperFunctions';
import { e2eTestObject } from '../types';

const assert = require('assert');
const { expect } = require('chai');

export const dropdownTest = (testObject: e2eTestObject, pageURL: string) => {
  describe('Testing dropdown menu on current page', () => {
    let page;

    const standardBefore = async () => {
      page = await testObject.browser.newPage();
      await console.log('\tPage opened');

      await page.goto(pageURL);

      if (!await isLoggedIn(page)) {
        await login(page);
        await page.goto(pageURL);
      }

      await page.waitFor(HEADER_DROPDOWN_MENU);
    };

    describe('Dropdown menu contains:', () => {
      before(async () => {
        await standardBefore();
        await page.click(HEADER_DROPDOWN_MENU);
      });
      after(async () => await page.close().then(console.log('\tPage closed')));

      it('Admin Maintenance header', async () => {
        expect(await page.$(DROPDOWN_ADMIN_MAINTENANCE)).to.not.be.null;
      });

      it('Users button', async () => {
        expect(await page.$(DROPDOWN_USERS_BUTTON)).to.not.be.null;
      });

      it('Groups button', async () => {
        expect(await page.$(DROPDOWN_GROUPS_BUTTON)).to.not.be.null;
      });

      it('UI Builder button', async () => {
        expect(await page.$(DROPDOWN_UI_BUILDER_BUTTON)).to.not.be.null;
      });

      it('D3 Graphs button', async () => {
        expect(await page.$(DROPDOWN_D3_GRAPHS_BUTTON)).to.not.be.null;
      });

      it('Divider', async () => {
        expect(await page.$(DROPDOWN_DIVIDER)).to.not.be.null;
      });

      it('Change Password button', async () => {
        expect(await page.$(DROPDOWN_CHANGE_PASSWORD_BUTTON)).to.not.be.null;
      });

      it('Log Out button', async () => {
        expect(await page.$(DROPDOWN_LOG_OUT_BUTTON)).to.not.be.null;
      });
    });

    describe('Dropdown buttons should successfully navigate to:', () => {
      let currentURL: string;
      before(standardBefore);
      beforeEach(async () => {
        await page.goto(pageURL);
        await page.click(HEADER_DROPDOWN_MENU);
      });
      after(async () => await page.close().then(console.log('\tPage closed')));

      it('user admin page', async () => {
        await page.click(DROPDOWN_USERS_BUTTON);
        currentURL = await page.evaluate(() => location.href);

        expect(currentURL).to.eql(USER_ADMIN_URL);
      });

      it('group admin page', async () => {
        await page.click(DROPDOWN_GROUPS_BUTTON);
        currentURL = await page.evaluate(() => location.href);

        expect(currentURL).to.eql(GROUP_ADMIN_URL);
      });

      it('ui builder page', async () => {
        await page.click(DROPDOWN_UI_BUILDER_BUTTON);
        currentURL = await page.evaluate(() => location.href);

        expect(currentURL).to.eql(UI_BUILDER_URL);
      });

      it('d3 graph page', async () => {
        await page.click(DROPDOWN_D3_GRAPHS_BUTTON);
        currentURL = await page.evaluate(() => location.href);

        expect(currentURL).to.eql(D3_GRAPH_URL);
      });
    });

    changePasswordTest(testObject, pageURL);
  });
};
