export const UI_BUILDER_BUTTON =
'#app > div > div > div:nth-child(3) > div > div:nth-child(2) > div > button';
export const LIST_MAINTENANCE_BUTTON =
'#app > div > div > div:nth-child(3) > div > div:nth-child(3) > div > button';
export const USER_ADMIN_BUTTON =
'#app > div > div > div:nth-child(3) > div > div:nth-child(4) > div > button';
export const GROUP_ADMIN_BUTTON =
'#app > div > div > div:nth-child(3) > div > div:nth-child(5) > div > button';
export const DATABASE_BUTTON =
'#app > div > div > div:nth-child(3) > div > div:nth-child(6) > div > button';
