import {
  BASE_URL,
  HOME_URL,
  USERNAME,
  PASSWORD,
  UI_DASHBOARD_URL,
  LIST_MAINTENANCE_URL,
  USER_ADMIN_URL,
  GROUP_ADMIN_URL,
} from '../constants';
import {
  UI_BUILDER_BUTTON,
  LIST_MAINTENANCE_BUTTON,
  USER_ADMIN_BUTTON,
  GROUP_ADMIN_BUTTON,
  DATABASE_BUTTON,
} from './selectors';
import { UI_BUILDER_HEADER } from '../UIBuilder/selectors';
import { LISTS_HEADER } from '../ListMaintenance/selectors';
import { USERS_HEADER } from '../UserAdmin/selectors';
import { USER_GROUPS_HEADER } from '../GroupAdmin/selectors';
import { REACTD3_GRAPH_HEADER } from '../Database/selectors';
import { isLoggedIn, login } from '../helperFunctions';
import { Browser, Page, ElementHandle } from 'puppeteer';
import { headerTest } from '../Header/header.e2e';
import {
  TestCredentials,
  TestCredentialUser,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import { IterrexRole, IterrexSystemRole } from 'toolkit';

const puppeteer = require('puppeteer');

export type HomeE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    title?: string,
    button00?: ElementHandle,
    button01?: ElementHandle,
    button02?: ElementHandle,
    button03?: ElementHandle,
    button04?: ElementHandle,
    navigated00?: boolean,
    navigated01?: boolean,
    navigated02?: boolean,
    navigated03?: boolean,
    navigated04?: boolean,
  },
};

const elementQuerySetup = async (testObject: HomeE2eTestObject) => {
  testObject.results.title = await testObject.page.title();
  testObject.results.button00 = await testObject.page.$(UI_BUILDER_BUTTON);
  testObject.results.button01 = await testObject.page.$(LIST_MAINTENANCE_BUTTON);
  testObject.results.button02 = await testObject.page.$(USER_ADMIN_BUTTON);
  testObject.results.button03 = await testObject.page.$(GROUP_ADMIN_BUTTON);
  testObject.results.button04 = await testObject.page.$(DATABASE_BUTTON);
};

const elementQueryTestTenantUsers = (getTestObject: () => HomeE2eTestObject) => {
  describe('Newly opened home page contains:', () => {
    it('correct page title', () => {
      expect(getTestObject().results.title).toEqual('Home - iterrex');
    });

    it('UI Builder button', () => {
      expect(getTestObject().results.button00).not.toBeUndefined();
    });

    it('List Maintenance button', () => {
      expect(getTestObject().results.button01).not.toBeUndefined();
    });

    it('User Admin button', () => {
      expect(getTestObject().results.button02).not.toBeUndefined();
    });

    it('Group Admin button', () => {
      expect(getTestObject().results.button03).not.toBeUndefined();
    });

    it('Database button', () => {
      expect(getTestObject().results.button04).toBeNull();
    });
  });
};

const elementQueryTestSystemUsers = (getTestObject: () => HomeE2eTestObject) => {
  describe('Newly opened home page contains:', () => {
    it('correct page title', () => {
      expect(getTestObject().results.title).toEqual('Home - iterrex');
    });

    it('UI Builder button', () => {
      expect(getTestObject().results.button00).not.toBeUndefined();
    });

    it('List Maintenance button', () => {
      expect(getTestObject().results.button01).not.toBeUndefined();
    });

    it('User Admin button', () => {
      expect(getTestObject().results.button02).not.toBeUndefined();
    });

    it('Group Admin button', () => {
      expect(getTestObject().results.button03).not.toBeUndefined();
    });

    it('Database button', () => {
      expect(getTestObject().results.button04).not.toBeUndefined();
    });
  });
};

const navigationSetup = async (testObject: HomeE2eTestObject) => {
  await testObject.page.goto(HOME_URL);
  await testObject.page.click(UI_BUILDER_BUTTON);
  await testObject.page.waitForSelector(UI_BUILDER_HEADER);
  testObject.results.navigated00 = await testObject.page.$(UI_BUILDER_HEADER) !== null;

  await testObject.page.goto(HOME_URL);
  await testObject.page.click(LIST_MAINTENANCE_BUTTON);
  await testObject.page.waitForSelector(LISTS_HEADER);
  testObject.results.navigated01 = await testObject.page.$(LISTS_HEADER) !== null;

  await testObject.page.goto(HOME_URL);
  await testObject.page.click(USER_ADMIN_BUTTON);
  await testObject.page.waitForSelector(USERS_HEADER);
  testObject.results.navigated02 = await testObject.page.$(USERS_HEADER) !== null;

  await testObject.page.goto(HOME_URL);
  await testObject.page.click(GROUP_ADMIN_BUTTON);
  await testObject.page.waitForSelector(USER_GROUPS_HEADER);
  testObject.results.navigated03 = await testObject.page.$(USER_GROUPS_HEADER) !== null;

  await testObject.page.goto(HOME_URL);
  await testObject.page.click(DATABASE_BUTTON);
  await testObject.page.waitForSelector(REACTD3_GRAPH_HEADER);
  testObject.results.navigated04 = await testObject.page.$(REACTD3_GRAPH_HEADER) !== null;
};

const navigationTestTenantUsers = (iterrexRole: IterrexRole, getTestObject: () => HomeE2eTestObject) => {
  describe('Homepage buttons should successfully navigate to:', () => {
    it('UI Builder page', () => {
      expect(getTestObject().results.navigated00).toEqual(true);
    });

    it('List Maintenance page', () => {
      expect(getTestObject().results.navigated01).toEqual(true);
    });

    it('User Admin page', () => {
      expect(getTestObject().results.navigated02).toEqual(true);
    });

    it('Group Admin page', () => {
      expect(getTestObject().results.navigated03).toEqual(true);
    });

    it('Database page', () => {
      expect(getTestObject().results.navigated04).toEqual(false);
    });
  });
};

const navigationTestSystemUsers = (getTestObject: () => HomeE2eTestObject) => {
  describe('Homepage buttons should successfully navigate to:', () => {
    it('UI Builder page', () => {
      expect(getTestObject().results.navigated00).toEqual(true);
    });

    it('List Maintenance page', () => {
      expect(getTestObject().results.navigated01).toEqual(true);
    });

    it('User Admin page', () => {
      expect(getTestObject().results.navigated02).toEqual(true);
    });

    it('Group Admin page', () => {
      expect(getTestObject().results.navigated03).toEqual(true);
    });

    it('Database page', () => {
      expect(getTestObject().results.navigated04).toEqual(true);
    });
  });
};
const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<HomeE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (E2eTestObject) => any = async (testObject: HomeE2eTestObject) => {
  await testObject.page.close();
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(HOME_URL);

  await Promise.all([
    testObject.page.waitFor(UI_BUILDER_BUTTON),
    testObject.page.waitFor(LIST_MAINTENANCE_BUTTON),
    testObject.page.waitFor(USER_ADMIN_BUTTON),
    testObject.page.waitFor(GROUP_ADMIN_BUTTON),
    testObject.page.waitFor(DATABASE_BUTTON),
  ]);
};

const before00Setup = async (testObject: HomeE2eTestObject) => {
  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo: 800,
  });
  testObject.page = await testObject.browser.newPage();
  await testObject.page.goto(BASE_URL);

  await login(testObject);
  await testObject.page.goto(HOME_URL);
  await Promise.all([
    testObject.page.waitFor(UI_BUILDER_BUTTON),
    testObject.page.waitFor(LIST_MAINTENANCE_BUTTON),
    testObject.page.waitFor(USER_ADMIN_BUTTON),
    testObject.page.waitFor(GROUP_ADMIN_BUTTON),
    testObject.page.waitFor(DATABASE_BUTTON),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: HomeE2eTestObject) => {
  await elementQuerySetup(testObject);
  await newPage(testObject);
  await navigationSetup(testObject);
};

const beforeTests = async (testObject: HomeE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => HomeE2eTestObject) => {
  if (iterrexRole !== IterrexSystemRole.SYSTEM_ADMIN) {
    elementQueryTestTenantUsers(getTestObject);
    navigationTestTenantUsers(iterrexRole, getTestObject);
  } else {
    elementQueryTestSystemUsers(getTestObject);
    navigationTestSystemUsers(getTestObject);
  }
};

const after00Teardown = async (testObject: HomeE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: HomeE2eTestObject) => {
  after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end home page test',
  parallelBefore: false,
});
