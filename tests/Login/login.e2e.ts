import {
  BASE_URL,
  USERNAME,
  PASSWORD,
  HOME_URL,
} from '../constants';
import {
  HOME_HEADER,
  USER_INPUT,
  PASS_INPUT,
  LOGIN_BUTTON,
  INCORRECT_LOGIN_WARNING,
} from './selectors';
import { waitForFunction } from '../helperFunctions';
import {
  Browser,
  Page,
  ElementHandle,
} from 'puppeteer';
import {
  TestCredentials,
  TestCredentialUser,
  runTestsForAllUsers,
} from 'toolkit/testUtils';
import {
  IterrexRole,
  IterrexSystemRole,
  IterrexTenantRole,
} from 'toolkit';
import { HEADER_DROPDOWN_MENU } from '../Header/selectors';

const puppeteer = require('puppeteer');

export type LoginE2eTestObject = {
  browser: Browser,
  page: Page,
  testCredentials: TestCredentials,
  cognitoIdToken: string,
  user?: TestCredentialUser,
  results: {
    correctPageTitle?: boolean,
    correctHeaderText?: boolean,
    usernameInputExists?: boolean,
    passwordInputExists?: boolean,
    loginButtonExists?: boolean,
    isDisabled00?: boolean,
    isDisabled01?: boolean,
    isDisabled02?: boolean,
    isDisabled03?: boolean,
    isDisabled04?: boolean,
    isDisabled05?: boolean,
    isDisabled06?: boolean,
    isDisabled07?: boolean,
    isDisabled08?: boolean,
    isDisabled09?: boolean,
    incorrectLoginElem?: ElementHandle,
    newUrl?: string,
  };
};
const elementQuerySetup = async (testObject: LoginE2eTestObject) => {
  testObject.results.correctPageTitle = (await testObject.page.title()) === 'Login - iterrex';
  testObject.results.correctHeaderText = (await testObject.page.$eval(HOME_HEADER, header => header.innerHTML)) === 'Welcome!';
  testObject.results.usernameInputExists = (await testObject.page.$(USER_INPUT)) !== null;
  testObject.results.passwordInputExists = (await testObject.page.$(PASS_INPUT)) !== null;
  testObject.results.loginButtonExists = (await testObject.page.$(LOGIN_BUTTON)) !== null;
};

const elementQueryTest = (getTestObject: () => LoginE2eTestObject) => {
  describe('Newly opened page contains:', () => {
    it('correct page title', () => {
      expect(getTestObject().results.correctPageTitle).toEqual(true);
    });

    it('header with the text "Welcome!"', () => {
      expect(getTestObject().results.correctHeaderText).toEqual(true);
    });

    it('username input', () => {
      expect(getTestObject().results.usernameInputExists).toEqual(true);
    });

    it('password input', () => {
      expect(getTestObject().results.passwordInputExists).toEqual(true);
    });

    it('login button', () => {
      expect(getTestObject().results.loginButtonExists).toEqual(true);
    });
  });
};

const loginButtonStateSetup = async (testObject: LoginE2eTestObject) => {
  testObject.results.isDisabled00 = await testObject.page.$eval(LOGIN_BUTTON, (button: HTMLButtonElement) => button.disabled);

  await testObject.page.type(USER_INPUT, 'change1');
  testObject.results.isDisabled01 = await testObject.page.$eval(LOGIN_BUTTON, (button: HTMLButtonElement) => button.disabled);

  await testObject.page.click(LOGIN_BUTTON);
  testObject.results.isDisabled02 = await testObject.page.$eval(LOGIN_BUTTON, (button: HTMLButtonElement) => button.disabled);

  await testObject.page.type(PASS_INPUT, 'change2');
  testObject.results.isDisabled03 = await testObject.page.$eval(LOGIN_BUTTON, (button: HTMLButtonElement) => button.disabled);

  await testObject.page.click(LOGIN_BUTTON);
  testObject.results.isDisabled04 = await testObject.page.$eval(LOGIN_BUTTON, (button: HTMLButtonElement) => button.disabled);

  testObject.results.isDisabled05 = await testObject.page.$eval(LOGIN_BUTTON, (input: HTMLInputElement) => input.disabled);
  testObject.results.isDisabled06 = await testObject.page.$eval(LOGIN_BUTTON, (input: HTMLInputElement) => input.disabled);

  await waitForFunction(async () => !(await testObject.page.$eval(USER_INPUT, (input: HTMLInputElement) => input.disabled)));
  await testObject.page.type(USER_INPUT, 'change3');
  testObject.results.isDisabled07 = await testObject.page.$eval(LOGIN_BUTTON, (button: HTMLButtonElement) => button.disabled);

  await testObject.page.click(LOGIN_BUTTON);
  await waitForFunction(async () => !(await testObject.page.$eval(PASS_INPUT, (input: HTMLInputElement) => input.disabled)));
  await testObject.page.type(PASS_INPUT, 'change4');
  testObject.results.isDisabled08 = await testObject.page.$eval(LOGIN_BUTTON, (button: HTMLButtonElement) => button.disabled);
};

const loginButtonStateTest = (getTestObject: () => LoginE2eTestObject) => {
  describe('Login button state:', () => {
    it('should be disabled on new page', () => {
      expect(getTestObject().results.isDisabled00).toEqual(true);
    });

    it('should be enabled on username input change', () => {
      expect(getTestObject().results.isDisabled01).toEqual(false);
    });

    it('should be enabled after click with no password input', () => {
      expect(getTestObject().results.isDisabled02).toEqual(false);
    });

    it('should be enabled on password input change', () => {
      expect(getTestObject().results.isDisabled03).toEqual(false);
    });

    it('should be disabled after unsuccessful login', () => {
      expect(getTestObject().results.isDisabled04).toEqual(true);
    });

    it('should disable all input while querying', () => {
      expect(getTestObject().results.isDisabled05 && getTestObject().results.isDisabled06).toEqual(true);
    });

    it('should re-enable on username input change', () => {
      expect(getTestObject().results.isDisabled07).toEqual(false);
    });

    it('should re-enable on password input change', () => {
      expect(getTestObject().results.isDisabled08).toEqual(false);
    });
  });
};

const invalidLoginSetup = async (testObject: LoginE2eTestObject) => {
  const userIn = 'invalid1';
  const passIn = 'invalid2';

  await testObject.page.focus(USER_INPUT);
  await testObject.page.keyboard.type(userIn);

  await testObject.page.focus(PASS_INPUT);
  await testObject.page.keyboard.type(passIn);

  await Promise.all([
    waitForFunction(async () => await testObject.page.$eval(LOGIN_BUTTON, (button: HTMLButtonElement) => button.disabled)),
    testObject.page.click(LOGIN_BUTTON),
  ]);

  testObject.results.isDisabled09 = await testObject.page.$eval(LOGIN_BUTTON, (button: HTMLButtonElement) => button.disabled);

  await waitForFunction(async () => !(await testObject.page.$eval(USER_INPUT, (input: HTMLInputElement) => input.disabled)));

  testObject.results.incorrectLoginElem = await testObject.page.$(INCORRECT_LOGIN_WARNING);
};

const invalidLoginTest = (getTestObject: () => LoginE2eTestObject) => {
  describe('Attempting login with invalid credentials:', () => {
    it('should disable login button after failed login attempt', () => {
      expect(getTestObject().results.isDisabled09).toEqual(true);
    });

    it('should display incorrect login alert text', () => {
      expect(getTestObject().results.incorrectLoginElem).not.toBeUndefined();
    });
  });
};

const loginSetup = async (testObject: LoginE2eTestObject) => {
  await testObject.page.type(USER_INPUT, testObject.user.username);
  await testObject.page.type(PASS_INPUT, testObject.user.password);
  const currentURL: string = await testObject.page.evaluate(() => location.href);
  await Promise.all([
    testObject.page.waitForSelector(HEADER_DROPDOWN_MENU),
    testObject.page.click(LOGIN_BUTTON),
  ]);
  testObject.results.newUrl = await testObject.page.evaluate(() => location.href);
};

const loginTest = (getTestObject: () => LoginE2eTestObject) => {
  describe('Attempting login with valid credentials:', () => {
    it('should navigate to /home when login is successful', () => {
      expect(getTestObject().results.newUrl).toEqual(HOME_URL);
    });
  });
};

const createTestObject = async ({
  cognitoIdToken,
  testCredentials,
  tenantName,
}: {
  cognitoIdToken: string,
  testCredentials: TestCredentials,
  tenantName?: string,
}): Promise<LoginE2eTestObject> => {
  // const testCredential = testCredentials.getTestCredentialForCognitoToken(cognitoIdToken);
  // const user = testCredentials.getAllUsers().find(user => user.cognitoIdToken === cognitoIdToken);
  return {
    testCredentials,
    cognitoIdToken,
    browser: null,
    page: null,
    results: {},
    user: {
      username: 'garett@coveycs.com',
      password: 'testPassword1!',
      iterrexRole: null,
      cognitoIdToken: null,
      sessionToken: null,
      bearerAuthorizationHeader: null,
      basicAuthorizationHeader: null,
    },
  };
};

const newPage: (LoginE2eTestObject) => any = async (testObject: LoginE2eTestObject) => {
  if (testObject.page !== null) {
    await testObject.page.close();
  }
  testObject.page = await testObject.browser.newPage();

  await testObject.page.goto(HOME_URL);

  await Promise.all([
    testObject.page.waitFor(USER_INPUT),
    testObject.page.waitFor(PASS_INPUT),
    testObject.page.waitFor(LOGIN_BUTTON),
  ]);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before00Setup = async (testObject: LoginE2eTestObject) => {
  testObject.browser = await puppeteer.launch({
    // headless: false,
    // sloMo: 800,
  });
  await newPage(testObject);

  await testObject.page.setViewport({ width: 1920, height: 1080 });
};

const before01Run = async (testObject: LoginE2eTestObject) => {
  await elementQuerySetup(testObject);
  await newPage(testObject);
  await loginButtonStateSetup(testObject);
  await newPage(testObject);
  await invalidLoginSetup(testObject);
  await newPage(testObject);
  await loginSetup(testObject);
};

const beforeTests = async (testObject: LoginE2eTestObject) => {
  await before00Setup(testObject);
  await before01Run(testObject);

  return testObject;
};

const runTests = (iterrexRole: IterrexRole, getTestObject: () => LoginE2eTestObject) => {
  elementQueryTest(getTestObject);
  loginButtonStateTest(getTestObject);
  invalidLoginTest(getTestObject);
  loginTest(getTestObject);
};

const after00Teardown = async (testObject: LoginE2eTestObject) => {
  await testObject.page.close();
  await testObject.browser.close();
};

const afterTests = async (testObject: LoginE2eTestObject) => {
  await after00Teardown(testObject);

  return testObject;
};

runTestsForAllUsers({
  createTestObject,
  beforeTests,
  afterTests,
  runTests,
  testCredentials: global.testCredentials,
  testName: 'End-to-end login page test',
  parallelBefore: false,
});
