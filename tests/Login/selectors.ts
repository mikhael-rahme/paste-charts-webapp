// tslint:disable:max-line-length


export const HOME_HEADER =
'#app > div > div > div:nth-child(3) > div > div > div > h1';
export const USER_INPUT =
'#app > div > div > div:nth-child(3) > div > div > div > form > div:nth-child(1) > div:nth-child(2) > input';
export const PASS_INPUT =
'#app > div > div > div:nth-child(3) > div > div > div > form > div:nth-child(2) > div:nth-child(2) > input';
export const LOGIN_BUTTON =
'#btnLogin';
export const INCORRECT_LOGIN_WARNING =
'#app > div > div > div:nth-child(3) > div > div > div > form > div:nth-child(3) > div > small';


