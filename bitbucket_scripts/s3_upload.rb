require 'aws-sdk'

artifact,s3Bucket,key = ARGV
puts "#{artifact} #{s3Bucket} #{key}"
s3 = Aws::S3::Resource.new(region:'us-east-1')
obj = s3.bucket(s3Bucket).object(key)
obj.upload_file(artifact)