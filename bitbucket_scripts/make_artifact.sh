#!/bin/bash

zip -r $ARTIFACT_FILE . -x bitbucket_scripts/\* -x docs/\* -x node_modules/\* -x *.git -x *.md