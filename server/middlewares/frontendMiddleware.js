/* eslint-disable global-require */
const express = require('express');
const path = require('path');
const compression = require('compression');
const pkg = require(path.resolve(process.cwd(), 'package.json'));
const appConfigs = require('../../app/apps/config');
const webpack = require('webpack');
const fs = require('fs');
// Dev middleware
const addDevMiddlewares = (app, webpackConfig, appName) => {
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');
  const compiler = webpack(webpackConfig);
  const middleware = webpackDevMiddleware(compiler, {
    noInfo: false, // true,
    publicPath: webpackConfig.output.publicPath,
    silent: false, //true,
    stats: 'errors-only',
    logLevel: 'trace',
  });

  app.use((req, res, next) => {
    console.log(req.originalUrl);
    next();
  });

  app.use(middleware);
  app.use(webpackHotMiddleware(compiler));

  // Since webpackDevMiddleware uses memory-fs internally to store build
  // artifacts, we use it instead
  const middlewareFs = middleware.fileSystem;

  if (pkg.dllPlugin) {
    app.get(/\.dll\.js$/, (req, res) => {
      const filename = req.path.replace(/^\//, '');
      res.sendFile(path.join(process.cwd(), pkg.dllPlugin.path, filename));
    });
  }
  
  const appConfig = appConfigs[appName];
  console.log(`creating middleware for ${appName}`);
  const route = `/${appConfig.path.trim('/')}`;
  console.log(`route: ${route}`);
    
  /*
  const flatApps = Object.keys(appConfigs)
    .filter(configAppname => configAppname !== appName);
  flatApps.forEach(appname => { 
    console.log(`${appname} using public path ${appConfigs[appname].publicPath} for statics ${compiler.outputPath}${appConfigs[appname].publicPath}`)
    app.use(
    appConfigs[appname].publicPath, 
    express.static(`${compiler.outputPath}${appConfigs[appname].publicPath}`))
  });
*/
  const serveFile = (fileName, fileSystem, res) => {
    fileSystem.readFile(path.join(compiler.outputPath, fileName), (err, file) => {
      if (err) {
        res.sendStatus(404);
      } else {
        res.send(file.toString());
      }
    });
  }

  const serveFileFromDevServer = (fileName, res) => serveFile(fileName, middleware.fileSystem, res);
  const serveFileFromDisk = (fileName, res) => serveFile(fileName, fs, res);

  app.get('*', (req, res) => {
    // 
    // /admin/list-maintenance 
    // /some-real-route/some-subroute
    /*
      This will only match paths that aren't recognized by the webpack-dev-middleware

      The two cases where this will happen:
      1) A subroute of a page is loaded directly:
        - /admin/list-maintenance (should reolve to /admin.html)
        - will use in-memory file system
      2) A page not being served by the dev server
        - /login when hot-loading home
        - /someFileName.extension
        - use prebuild file from file system

      The steps to find which file to serve
      if the missed file is a subroute of the current app
        ? serve the base html file of the current app
        : serve the base html file of the 

    */
    console.log(`miss: ${req.originalUrl}`);

    // get path parts

    const isFile = req.originalUrl.indexOf('.') > -1;
    // serve static file from disk
    if (isFile) {
      return serveFileFromDisk(req.originalUrl, res);
    }

    // start path matching
    const {
      publicPath, // path prefix to match
      htmlFileName, // file to serve
    } = appConfigs[appName];
    console.log(JSON.stringify(appConfigs[appName]));
    // is it the current app?
    if (req.originalUrl.startsWith(publicPath)) {
      // current app, serve from memory
      console.log(`Serving ${htmlFileName} for ${req.originalUrl} from memory`);
      return serveFileFromDevServer(htmlFileName, res);
    } else {
      // a different app, serve from disk
      const htmlFile = appConfigs[Object.keys(appConfigs)
        .find(appNameToServe => req.originalUrl.startsWith(appConfigs[appNameToServe].publicPath))]
        .htmlFileName;
      console.log(`Serving ${htmlFile} for ${req.originalUrl} from disk`);
      return serveFileFromDisk(htmlFile, res);
    }
      /*
        a file belongs to an app if it matches the path exactly, or is in a subfolder of the path
        example:
       
        login:
          path: /
          file matches:
            / (index.html)
            /somefile.html
            /somefile.json
        home:
          path: /_
          file matches:
            /_ (index.html)
            /_/somefile.html
            /somefile.json
      
        1) look for an exact path match, which should resolve to an index.html file
          - the fast way to do is to look for a file extention
          - if no dot (.) exists, we can assume it's a path

        2) 
      */
      
    /*
      let appFileBelongsTo = 'login'; // assume it's login
      const isPath = req.originalUrl.indexOf('.') === -1;
      if (isPath) {
        appFileBelongsTo = flatApps.find(appname => appConfigs[appname].publicPath === req.originalUrl);
      } else {
        appFileBelongsTo = req.originalUrl.match(/\//g).length === 1
          ? 'login'
          : flatApps.find(appname => req.originalUrl.startsWith(appConfigs[appname].publicPath));
      }

      console.log(`${req.originalUrl} belongs to ${appFileBelongsTo}`);
      // we need to take 
      const rootUrl = '/';
      const isColdFile = flatApps.includes(appFileBelongsTo);
      console.log(`isColdFile: ${isColdFile}`);

      const fileToFetch = isPath
        ? `${req.originalUrl}/index.html`
        : req.originalUrl;
      (isColdFile ? realFs : fs).readFile(path.join(compiler.outputPath, fileToFetch), (err, file) => {
        if (err) {
          res.sendStatus(404);
        } else {
          res.send(file.toString());
        }
      });
      */
    });
  }

// Production middlewares
const addProdMiddlewares = (app, options) => {
  console.log('adding prod middleware');
  const publicPath = options.publicPath || '/';
  const outputPath = options.outputPath || path.resolve(process.cwd(), 'build');
  console.log(`outputPath: ${outputPath}`);
  // compression middleware compresses your server responses which makes them
  // smaller (applies also to assets). You can read more about that technique
  // and other good practices on official Express.js docs http://mxs.is/googmy
  app.use(compression());
  app.use(publicPath, express.static(outputPath));

  app.get('*', (req, res) => {
    console.log(`miss: ${req.originalUrl}`);

    const isPath = req.originalUrl.indexOf('.') === -1;
    const fileToFetch = isPath
    ? `${req.originalUrl.split('?')[0]}.html`
    : req.originalUrl;
    console.log(`fetching: ${fileToFetch}`);
    fs.readFile(path.join(outputPath, fileToFetch), (err, file) => {
      if (err) {
        res.sendFile(path.resolve(outputPath, 'index.html'))
      } else {
        res.send(file.toString());
      }
    });
    // return res.sendFile(path.resolve(outputPath, 'index.html'))
  });
};

/**
 * Front-end middleware
 */
module.exports = async (app, options) => {
  const isProd = process.env.NODE_ENV === 'production';
  if (isProd) {
    addProdMiddlewares(app, options);
  } else {
    /*
    const appsDir = path.resolve(__dirname, '../../app/apps');
    // console.log(`appsDir: ${appsDir}`);
    const filesInAppsDir = readdirSync(appsDir).filter(entry => !entry.includes('_'));
    // console.log(`filesInAppsDir: ${filesInAppsDir}`);
    const apps = filesInAppsDir.filter(entry => isDirectory(`${appsDir}/${entry}`));
    // console.log(apps);
    */
    const {
      appname,
      //skipcold,
    } = options;
    console.log('options:', JSON.stringify(options, null, 2));

    // only run hot reloaing for hotApp
    // others will be loaded from build
    console.log('Flat apps:');
    const flatAppsToBuild = Object.keys(appConfigs)
      .filter(configAppname => configAppname !== appname);
    console.log(JSON.stringify(flatAppsToBuild, null, 2));
    console.log(`Hot Load will build: ${appname}`);

    if (appname) {
      console.log(`Hot Load build starting for ${appname}`);
      const webpackConfig = require('../../internals/webpack/webpack.dev')(appname);
      addDevMiddlewares(app, webpackConfig, appname);
    }
  }

  return app;
};
