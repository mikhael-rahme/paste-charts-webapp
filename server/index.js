/* eslint consistent-return:0 */

const express = require('express');
const logger = require('./logger');

const argv = require('minimist')(process.argv.slice(2));
const setup = require('./middlewares/frontendMiddleware');
const resolve = require('path').resolve;
const app = express();
// const proxy = require('http-proxy-middleware');

/*
// If you need a backend, e.g. an API, add your custom backend-specific middleware here
// app.use('/api', myApi);
app.use('/graphql', proxy({
  target: process.env.API_URL,
  changeOrigin: true,
}));

const wsProxy = proxy({
  target: process.env.WS_URL,
  changeOrigin: true,
  ws: true,
  ignorePath: true,
});

app.use('/ws', wsProxy);
*/

// In production we need to pass these values in instead of relying on webpack
setup(app, {
  outputPath: resolve(process.cwd(), 'build'),
  publicPath: '/',
  appname: argv.app,
  skipcold: argv.skipcold
});

// get the intended port number, use port 3000 if not provided
const port = argv.port || process.env.PORT || 3000;


// Start your app.
/* const server = */ app.listen(port, (err) => {
  if (err) {
    return logger.error(err.message);
  }
  logger.appStarted(port);
});

// server.on('upgrade', wsProxy.upgrade);
