import * as express from 'express';
import * as proxy from 'http-proxy-middleware';
import { S3 } from 'aws-sdk';
const logger = require('./logger');
const argv = require('minimist')(process.argv.slice(2));
import Wait from 'toolkit/lib/main/Wait';

const debug = !!argv.debug
  ? (message => console.log(message))
  : () => {};

// we'll serve sourcemaps out of memory
const sourceMaps: {
  [key: string]: string,
} = {};

const s3 = new S3();

const getBucket = hostname => hostname.indexOf('staging.iterrex.com') > -1
  ? 'iterrex-webapp-staging'
  : 'iterrex-webapp-production';

const s3Prefix = key => `master${key}`;

const proxyMap: {
  [hostname: string]: any,
} = {};

const app = express();
app.use('*', async (req, res, next) => {
  debug(req.originalUrl);
  debug(req.hostname);
  if (req.originalUrl.includes('.map')) {
    debug('looking for map file');
    if (sourceMaps[req.originalUrl]) {
      debug('source map was cached');
      res.send(sourceMaps[req.originalUrl]);
    } else {
      debug('source map was not cached');
      const file = await s3.getObject({
        Bucket: getBucket(req.hostname),
        Key: s3Prefix(req.originalUrl),
      }).promise();
      debug('caching source map');
      try {
        sourceMaps[req.originalUrl] = (file.Body as Buffer).toString('utf-8');
        debug('source map is now cached');
        res.send(sourceMaps[req.originalUrl]);
      } catch (err) {
        console.error('Error caching source map:');
        console.error(err);
        next();
      }
    }
  } else {
    if (!proxyMap[req.hostname]) {
      console.log(`Adding a proxy to ${req.hostname}`);
      proxyMap[req.hostname] = proxy({
        target: `https://${req.hostname}`,
        changeOrigin: true,
        // without setting the referer explicity, the authorizer gets all f'ed up
        headers: {
          referer: `https://${req.hostname}`,
        },
      });
      app.use('*', (_req, _res, _next) => {
        debug('using new middleware');
        (_req.hostname === req.hostname && proxyMap[req.hostname](_req, _res, _next)) || next();
      });
      await Wait(3000);
      proxyMap[req.hostname](req, res, next);
    }
    next();
  }
});
const port = 3000;

app.listen(port, (err) => {
  if (err) {
    return logger.error(err.message);
  }
  logger.appStarted(port);
});
