const presets = [
  '@babel/react',
  '@babel/typescript',
  ['@babel/env', { 'modules': false }],
];
const plugins = [
  'react-hot-loader/babel',
  '@babel/plugin-proposal-object-rest-spread',
  '@babel/plugin-proposal-class-properties',
];

// {
//   'presets': [],
//   'plugins': [],
//   'env': {
//     'test': {
//       'presets': ['@babel/react', '@babel/typescript', ['@babel/env', { 'modules': 'commonjs' }]],
//       'plugins': ['@babel/plugin-transform-modules-commonjs']
//     }
//   }
// }

module.exports = {
  presets,
  plugins,
};
