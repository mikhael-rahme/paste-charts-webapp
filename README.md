## Quick start

1. Clone this repo using `git clone git@bitbucket.org:coveycomputersoftware/covey-core-webapp.git`
1. Run `yarn install` to install dependencies
1. Run `yarn build`, to be build the repo
1. Run `yarn start:home`, or `yarn start:admin`, or `yarn start:login`, depending on which pages you want to hot reload

## yarn-command to what-gets-hot-reloaded map
1. `yarn start:home` - hubbell-specific pages (records, scheduling, ...)
1. `yarn start:admin` - administrative pages (user, list, group maintenance, etc)
1. `yarn start:login` - login page


## Documentation

- [Intro](docs/general): What's included and why
- [**Commands**](docs/general/commands.md): Getting the most out of this boilerplate
- [Testing](docs/testing): How to work with the built-in test harness
- [Styling](docs/css): How to work with the CSS tooling
- [Your app](docs/js): Supercharging your app with Routing, Redux, simple
  asynchronicity helpers, etc.

## ENV Variables

Before running the project, copy the `.env.default` to `.env` and fill the following values:

| Key | Description/Default |
| ------ | ------ |
| API_URL | API URL: `https://dev.dynwatt.com`/`http://localhost:8080` |
| WS_URL | Websocket URL: `wss://dev-ws.dynwatt.com/ws1`/`http://localhost:8090` |
| NODE_ENV | Node Environment: `production`/`development` |

## Host File

### mac

add line
```
127.0.0.1 staging-local.staging.iterrex.com
```
to /etc/hosts

### windows

¯\\_(ツ)_/¯

## Hot Reloading
Want hot reloading to work for your module?

1. import 'hot' in your module
```
import { hot } from 'react-hot-loader'
```
2. wrap your default export
```
# old
export default /* the stuff you've exported */

# new
export default hot(module)(/* the stuff you've exported */)
```

