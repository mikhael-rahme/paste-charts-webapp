import { NormalizedData } from 'operation-service/lib/Types';

export type NMap = {
  [id: string]: NormalizedData,
};
