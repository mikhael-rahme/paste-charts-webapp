const uuidv5 = require('uuid/v5');

const UUID_NAMESPACE = '1b621a64-b0d5-4a1e-99be-da01ff1d3341';

export const createHtmlId = (pathToElement: string) => {
  return uuidv5(pathToElement, UUID_NAMESPACE);
};
