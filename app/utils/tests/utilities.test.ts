import expect from 'expect';
import {
  hoursSinceDate,
  getDateDifferenceInHours,
  getReadableBytes,
  enumToEnglish,
} from '../utilities';

context('utilities', () => {
  describe('hoursSinceDate', () => {
    let response;
    let error;
    const oneHour = 3.6e6;
    const numberOfHours = 5;

    before(() => {
      try {
        response = hoursSinceDate((new Date(Date.now() + (oneHour * numberOfHours))).toString());
      } catch (err) {
        error = err;
      }
    });

    it('should not error', () => {
      expect(error).toNotExist();
    });

    it('should return a value', () => {
      expect(response)
        .toExist();
    });

    it('should return the correct difference in hours within acceptable tolerance', () => {
      expect(Math.abs(response - numberOfHours)).toBeLessThan(0.001);
    });
  });
  describe('hoursSinceDate with bad date string', () => {
    it('should error', () => {
      expect(hoursSinceDate).toThrow(/You must supply a date string!/);
    });
  });

  describe('getDateDifferenceInHours', () => {
    let response;
    let error;
    const oneHour = 3.6e6;
    const numberOfHours = 5;

    before(() => {
      try {
        const date1 = new Date(Date.now() - (oneHour * numberOfHours)).toString();
        const date2 = new Date(Date.now() + (oneHour * numberOfHours)).toString();
        response = getDateDifferenceInHours(date1, date2);
      } catch (err) {
        error = err;
      }
    });

    it('should not error', () => {
      expect(error).toNotExist();
    });

    it('should return a value', () => {
      expect(response).toExist();
    });

    it('should return the correct difference in hours within acceptable tolerance', () => {
      expect(Math.abs(response - (2 * numberOfHours))).toBeLessThan(0.001);
    });
  });

  describe('getDateDifferenceInHours with a bad date string', () => {
    const oneHour = 3.6e6;
    const numberOfHours = 5;
    const newDate = new Date(Date.now() + (oneHour * numberOfHours)).toString();
    it('should error', () => {
      expect(() => getDateDifferenceInHours(null, newDate))
        .toThrow(/You must supply two date strings!/);
    });
  });

  describe('getReadableBytes', () => {
    it('should not have errored', () => {
      expect(() => getReadableBytes(1024)).toNotThrow();
    });
    it('should return correct base 2 units (GiB, TiB)', () => {
      expect(getReadableBytes(1024 * 1024 * 1000)).toEqual('1000MB');
    });
    it('should return correct base 2 units (GiB, TiB)', () => {
      expect(getReadableBytes(1024 * 1024 * 1024)).toEqual('1GB');
    });
  });

  describe('enumToEnglish', () => {
    it('should return a correctly formatted string', () => {
      expect(enumToEnglish('THIS_IS_A_TEST')).toEqual('This is a test');
    });
  });
});
