export const parseResetUrlArg = (str: string): {
  confirmationCode: string;
  username: string;
} => {
  const decoded = Buffer.from(str, 'base64').toString('utf8');
  const args = decoded.split(':');
  return {
    confirmationCode: args[0],
    username: args[1],
  };
};
