import {
  NormalizedData,
  TypeToFromParentEdgeMap,
} from 'operation-service/lib/Types';
import { NMap } from '../../Types';
import { arrayToMap } from '.';

export const getTreeFromMap = (
  map: NMap,
  id: string,
  selectedChildProps?: string[],
): NMap => {
  const root = map[id];

  if (!root) {
    return undefined;
  }

  const allChildren: NormalizedData[] = [];

  for (const prop of selectedChildProps || TypeToFromParentEdgeMap[root.type]) {
    const childIdens = root[prop];
    if (childIdens) {
      for (const ci of childIdens) {
        const lookup = map[ci.id];
        if (lookup) {
          allChildren.push(lookup);
        }
      }
    }
  }

  let children = allChildren;
  while (allChildren.length) {
    children = children.reduce((nextChildren, data) => {
      for (const prop of selectedChildProps || TypeToFromParentEdgeMap[data.type]) {
        const childIdens = data[prop];
        if (childIdens) {
          for (const ci of childIdens) {
            const lookup = map[ci.id];
            if (lookup) {
              nextChildren.push(lookup);
            }
          }
        }
      }
      return nextChildren;
    },                         []);
    allChildren.push(...children);
  }

  return arrayToMap(allChildren);
};
