import {
  NormalizedData,
  TypeToFromParentEdgeMap,
} from 'operation-service/lib/Types';
import { NMap } from '../../Types';
import { arrayToMap } from '.';

export const getChildrenFromMap = (
  map: NMap,
  id: string,
  selectedChildProps?: string[],
): NMap => {
  const root = map[id];

  if (!root) {
    return undefined;
  }

  const foundChildren: NormalizedData[] = [];
  for (const prop of selectedChildProps || TypeToFromParentEdgeMap[root.type]) {
    const childIdens = root[prop];
    if (childIdens) {
      for (const ci of childIdens) {
        const lookup = map[ci.id];
        if (lookup) {
          foundChildren.push(lookup);
        }
      }
    }
  }

  return arrayToMap(foundChildren);
};
