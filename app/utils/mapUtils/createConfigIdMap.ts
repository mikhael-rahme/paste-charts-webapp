import { NormalizedTypeArrayMap, NormalizedData } from 'operation-service/lib/Types';

export const createConfigIdMap = (
  tam: NormalizedTypeArrayMap,
): { [configId: string]: NormalizedData[] } => {
  const res = {};
  Object.keys(tam).forEach(key => tam[key].forEach((item) => {
    if (item.type.includes('Record')) {
      const configId: string = item.configId;
      if (typeof res[configId] === 'undefined') {
        res[configId] = [];
      }
      res[configId].push(item);
    }
  }));
  return res;
};
