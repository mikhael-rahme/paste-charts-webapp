import {
  NormalizedData,
} from 'operation-service/lib/Types';
import { NMap } from '../../Types';

export const arrayToMap = (
  array: NormalizedData[],
): NMap => array.reduce((map, item: NormalizedData) => {
  map[item.id] = item;
  return map;
},                      {});
