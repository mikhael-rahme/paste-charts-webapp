import {
  NormalizedData,
  TypeToChildPropertiesMap,
  VertexType,
} from 'operation-service/lib/Types';
import { uniq } from 'lodash';
import { NMap } from '../../Types';
import { dedup } from '../tamUtils/dedup';

/** WARNING!!!: not null-arg safe
 * Don't export this. It's assumign both prev/next are defined (which is checked in mergeTam)
 *  for efficency reasons; copy/paste and make it null-safe
 *
 * Merges the childProps of prev into next and dedups
 * @param prev
 * @param next
 */
const mergeNormData = (prev: NormalizedData, next: NormalizedData) => {
  let childProps = TypeToChildPropertiesMap[prev.type];
  if (prev.type === VertexType.PERMISSION) {
    childProps = childProps.filter(prop => prop !== VertexType.USER && prop !== VertexType.GROUP);
  }
  for (const prop of childProps) {
    const identifiers = dedup((prev[prop] || []).concat(next[prop] || []), (a, b) => a.id === b.id);
    next[prop] = identifiers;
  }
  return next;
};

export const mergeMap = (
  map: NMap,
  mapToMerge: NMap,
): NMap => {
  const prev = map || {};
  const next = mapToMerge || {};
  if (!next) {
    return prev;
  }
  if (!prev) {
    return next;
  }

  return uniq(Object.keys(prev).concat(Object.keys(next))).reduce((map, id) => {
    const prevItem = prev[id];
    const nextItem = next[id];

    if (prevItem) {
      map[id] = mergeNormData(prevItem, nextItem);
    } else {
      map[id] = nextItem;
    }

    return map;
  },                                                              map);

};
