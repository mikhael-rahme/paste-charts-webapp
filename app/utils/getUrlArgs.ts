export const getUrlArgs = <ReturnType = any>(): ReturnType => {
  // looks for <url>?<varName>=<varValue>...
  const regex = /.*\?((.*=.*)(&?))+/g;

  const regexRes = regex.exec(window.location.href);
  if (!regexRes) {
    return null;
  }

  return regexRes[1].split('&')
    .reduce<ReturnType>((vars, currentVarString) => {
      const currentVar = currentVarString.split('=');
      vars[currentVar[0]] = currentVar[1];
      return vars;
    },                  {} as ReturnType);
};
