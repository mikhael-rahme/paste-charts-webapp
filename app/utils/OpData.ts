import {
  NormalizedData,
  NormalizedRecord,
  NormalizedTypeArrayMap,
  TypeToChildPropertiesMap,
  ConfigRecordMap,
} from 'operation-service';

/* used to quickly access objects that are associated with the givne Id; say...
  if you want to rapidly look up all items who have a given parent

  this will make adding items to the map much easier
*/
export type IdAssociationMap = {
  [id: string]: NormalizedData[];
};

export type OpDataMap = {
  [id: string]: NormalizedData,
};

export type OpDataList = NormalizedData[];

export class OpData {
  map: OpDataMap;
  list: OpDataList;
  parentMap: IdAssociationMap;
  childMap: IdAssociationMap;
  defRecMap: ConfigRecordMap;
  constructor() {
    this.map = {};
    this.parentMap = {};
    this.childMap = {};
    this.list = [];
    this.defRecMap = {};
  }

  add (data: NormalizedData) {

  }
}
