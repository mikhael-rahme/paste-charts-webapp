import { CognitoIdToken } from 'toolkit/lib/main/CognitoIdToken';
import { IterrexTenantRole } from 'toolkit/lib/main/IterrexTenantRole';

export const credentialCheck = () => {
  try {
    const {
      accessToken,
      idToken,
      refreshToken,
    } = JSON.parse(localStorage.getItem('token'));
    if (accessToken && idToken && refreshToken) {
      const currentRoute = location.pathname;
      const routes = [
        '/login',
        '/_',
        '/admin',
        '/system',
        '/', // last for matching purposes
      ];
      const isBaseRoute = routes.find(route => currentRoute === route);

      const tokenExpired = JSON.parse(atob(accessToken.split('.')[1].replace('-', '+').replace('_', '/'))).exp < Date.now() / 1000;

      // instead of parseJwt, just use btoa
      const base64Url = idToken.split('.')[1];
      const base64 = base64Url.replace('-', '+').replace('_', '/');
      const congitoIdToken: CognitoIdToken = JSON.parse(atob(base64));

      if (congitoIdToken && isBaseRoute === '/') {
        return window.location.href = '/_';
      }
      /*
      const queryString = location.search.replace('?', '');
      // query string subpage

      if (queryString) {
        // handle query string routing
        const queryStringParts = queryString.split('&');
        if (queryStringParts[0].startsWith('route=')) {
          const nonRouteQueryString = queryStringParts.length > 1 && `?${queryStringParts.slice(1).join('&')}`;
          // check to see if we need to move the route from the querystring to the route path

          const historyObj: any = {};
          const newRoute = `${currentRoute}${queryStringParts[0].replace('route=', '')}`;
          const newRouteWithQueryString = `${newRoute}${nonRouteQueryString || ''}`;
          historyObj[`${currentRoute}?route=${queryStringParts[0][1]}`] = newRouteWithQueryString;
          return window.history.replaceState({}, document.title, newRouteWithQueryString);
        }
      }

      if (!isBaseRoute) {

        // const queryStringParts = queryString.split('&');
        // figure out which path is the base
        const pathParts = currentRoute.split('/').filter(Boolean);

        const newRoute = `/${pathParts[0]}/?route=${pathParts.slice(1).join('/')}/${queryString}`;
        window.location.href = newRoute;
      }*
      /*
      switch (isBaseRoute) {
        case '/':
        case '/login':
          // logged in, on base route
          return window.location.href = '/_';
        case '/_':
          return void 0;
        case '/admin':
          if (![
            IterrexTenantRole.TENANT_ADMIN,
            IterrexTenantRole.TENANT_OWNER,
            IterrexTenantRole.TENANT_PUBLISHER,
          ].find(role => cognitoId['cognito:groups'].includes(role))) {
            localStorage.removeItem('token');
            return window.location.href = '/login';
          }
        default:
        // not a base route
      }*/

      return;
    }
  } catch (err) { }
  if (window.location.pathname !== '/login/') {
    window.location.href = '/login/';
  }
};
