import { createHtmlId } from './createHtmlId';

describe('createHtmlId', () => {
  it('should return the same UUID for the same input', () => {
    const testInput = 'some/test/path';
    expect(createHtmlId(testInput)).toEqual(createHtmlId(testInput));
  });
  it('should return different UUIDs for different input', () => {
    const testInput1 = 'some/test/path1';
    const testInput2 = 'some/test/path2';
    expect(createHtmlId(testInput1)).not.toEqual(createHtmlId(testInput2));
  });
});
