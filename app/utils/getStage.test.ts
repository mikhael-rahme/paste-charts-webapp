import { getStage } from './getStage';

describe('getApiLocation', () => {
  it('should not error', () => {
    expect(() => getStage('dev-tenant.dev.iterrex.com')).not.toThrow();
  });
  it('should return the correct url for the dev environment', () => {
    expect(getStage('dev-tenant.dev.iterrex.com')).toBe('dev');
  });
  it('should return the correct url for the test environment', () => {
    expect(getStage('test-tenant.test.iterrex.com')).toBe('test');
  });
  it('should return the correct url for the staging environment', () => {
    expect(getStage('staging-tenant.staging.iterrex.com')).toBe('staging');
  });
  it('should return the correct url for the production environment', () => {
    expect(getStage('tenant.iterrex.com')).toBe('production');
  });
});
