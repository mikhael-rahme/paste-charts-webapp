import { setContext } from 'apollo-link-context';
import { createBasicAuthHeader } from './createBasicAuthHeader';
import { createResetAuthHeader } from './createResetAuthHeader';
import { parseResetUrlArg } from '../parseResetUrlArg';
import { getUrlArgs } from '../getUrlArgs';

export const withTokenLink = setContext((_) => {
  let authorization: string = null;
  if (_.operationName === 'login') {
    const {
      username,
      password,
    } = _.variables;
    authorization = createBasicAuthHeader({
      username,
      password,
    });

  } else if (_.operationName === 'resetPasswordLogin') {
    const {
      username,
      password,
    } = _.variables;
    const urlArgs = getUrlArgs<{ reset: string, code: string }>();
    const resetArgs = parseResetUrlArg(urlArgs.reset);
    authorization = createResetAuthHeader({
      username,
      password,
      confirmationCode: urlArgs.code,
    });
  } else {
    authorization = `Bearer ${localStorage.getItem('token')}` || null;
  }
  return {
    headers: {
      authorization,
    },
  };
});
