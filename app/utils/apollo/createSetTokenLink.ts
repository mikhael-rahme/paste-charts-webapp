import { ApolloLink } from 'apollo-link';
import { logChangedToken } from './logChangedToken';

export const createSetTokenLink = () => new ApolloLink((operation, forward) => {
  return forward(operation).map((response) => {
    const { response: { headers } } = operation.getContext();
    const authorizationHeader = headers.get('x-amzn-remapped-authorization');
    if (authorizationHeader) {
      const currentToken: {
        accessToken: string,
        idToken: string,
        refreshToken: string,
        accessKeyId: string,
        secretAccessKey: string,
        sessionToken: string,
      } = JSON.parse(localStorage.getItem('token')) || {};

      const parsedAuthHeader = JSON.parse(authorizationHeader);
      const {
        accessToken,
        idToken,
        refreshToken,
        accessKeyId,
        secretAccessKey,
        sessionToken,
      } = parsedAuthHeader;

      const newToken = {
        ...currentToken,
        accessToken,
        idToken,
      };
      if (refreshToken) {
        newToken.refreshToken = refreshToken;
      }

      if (process.env.NODE_ENV === 'development') {
        logChangedToken(currentToken, newToken);
      }

      localStorage.setItem('token', JSON.stringify(newToken));
      if (accessKeyId && secretAccessKey && sessionToken) {
        localStorage.setItem('creds', JSON.stringify({
          accessKeyId,
          secretAccessKey,
          sessionToken,
        }));
      }
    } else {
      localStorage.removeItem('token');
      localStorage.removeItem('creds');
    }
    return response;
  });
});
