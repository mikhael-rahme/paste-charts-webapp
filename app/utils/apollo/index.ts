import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import {
  from,
} from 'apollo-link';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { getApiLocation } from './getApiLocation';
import { withTokenLink } from './withTokenLink';
import { createSetTokenLink } from './createSetTokenLink';

const apiLocation = getApiLocation(window.location.hostname);

const cache = new InMemoryCache({
  addTypename: false,
});
const httpLink = createHttpLink({
  uri: `${apiLocation}`,
});

const setTokenLink = createSetTokenLink();

/*
const transportLink = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  httpLink);
  */
const transportLink = httpLink;

const link = from([
  withTokenLink,
  setTokenLink,
  transportLink,
]);

export default new ApolloClient({
  link,
  cache,
  queryDeduplication: true,
}) as any;
