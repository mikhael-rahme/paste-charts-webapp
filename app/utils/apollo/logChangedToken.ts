export type Token = {
  accessToken: string;
  refreshToken: string;
  idToken: string;
};

export const logChangedToken = (currentToken: Token, newToken: Token) => {
  const changedKeys = Object.keys(newToken).filter(key => newToken[key] !== currentToken[key]);
  if (changedKeys.length) {
    console.log('credentials updated:');
    // console.log(`newToken: ${JSON.stringify(newToken, null, 2)}`);
    // note, table is useless for long strings in chrome, not sure about firefox
    console.table(changedKeys.reduce(
      (outObj, key) => {
        outObj[key] = {
          old: currentToken[key],
          new: newToken[key],
        };
        return outObj;
      },
      {}));
  }
};
