import { createBasicAuthHeader } from './createBasicAuthHeader';

const username = 'testUsername';
const password = 'testPassword123!@#';
const dateNow = 100000;

describe('createBasicAuthHeader', () => {
  beforeAll(() => {
    (global as any).btoa = jest.fn(input => Buffer.from(input).toString('base64'));
    (global as any).Date.now = jest.fn(() => dateNow);
  });
  beforeEach(() => {
    ((global as any).btoa as jest.Mock).mockClear();
  });

  it('should error if a username is not provided', () => {
    expect(() => createBasicAuthHeader({
      password,
      username: null,
    })).toThrow();
  });
  it('should error if a password is not provided', () => {
    expect(() => createBasicAuthHeader({
      username,
      password: null,
    })).toThrow();
  });
  it('should not error if a username and password are provided', () => {
    expect(() => createBasicAuthHeader({
      username,
      password,
    })).not.toThrow();
  });
  it('should call btoa with the correct parameters', () => {
    createBasicAuthHeader({
      username,
      password,
    });
    expect(((global as any).btoa as jest.Mock).mock.calls[0][0]).toEqual(`${username}:${password}:${dateNow}`);
  });
  it('should return the correct output', () => {
    expect(createBasicAuthHeader({
      username,
      password,
    })).toBe(`Basic ${Buffer.from(`${username}:${password}:${dateNow}`).toString('base64')}`);
  });
});
