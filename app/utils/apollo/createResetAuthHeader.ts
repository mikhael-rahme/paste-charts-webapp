export const createResetAuthHeader = ({
  username,
  password,
  confirmationCode,
}: {
  username: string,
  password: string,
  confirmationCode: string,
}) => {
  if (!username) {
    throw new Error('createResetAuthHeader: username required!');
  }
  if (!password) {
    throw new Error('createResetAuthHeader: password required!');
  }
  if (!confirmationCode) {
    throw new Error('createResetAuthHeader: confirmationCode required!');
  }
  const encoded = btoa(`${username}:${password}:${confirmationCode}`);
  return `Reset ${encoded}`;
};
