import { createResetAuthHeader } from './createResetAuthHeader';

const username = 'testUsername';
const password = 'testPassword123!@#';
const confirmationCode = '12345';

describe('createResetAuthHeader', () => {
  beforeAll(() => {
    (global as any).btoa = jest.fn(input => Buffer.from(input).toString('base64'));
  });
  beforeEach(() => {
    ((global as any).btoa as jest.Mock).mockClear();
  });

  it('should error if a username is not provided', () => {
    expect(() => createResetAuthHeader({
      password,
      confirmationCode,
      username: null,
    })).toThrow();
  });
  it('should error if a password is not provided', () => {
    expect(() => createResetAuthHeader({
      username,
      confirmationCode,
      password: null,
    })).toThrow();
  });
  it('should error if a confirmationCode is not provided', () => {
    expect(() => createResetAuthHeader({
      username,
      password,
      confirmationCode: null,
    })).toThrow();
  });
  it('should not error if a username, password, and confirmationCode are provided', () => {
    expect(() => createResetAuthHeader({
      username,
      password,
      confirmationCode,
    })).not.toThrow();
  });
  it('should call btoa with the correct parameters', () => {
    createResetAuthHeader({
      username,
      password,
      confirmationCode,
    });
    expect(((global as any).btoa as jest.Mock).mock.calls[0][0]).toEqual(`${username}:${password}:${confirmationCode}`);
  });
  it('should return the correct output', () => {
    expect(createResetAuthHeader({
      username,
      password,
      confirmationCode,
    })).toBe(`Reset ${Buffer.from(`${username}:${password}:${confirmationCode}`).toString('base64')}`);
  });
});
