// import { WebSocketLink } from 'apollo-link-ws'; // can't use this link due to odd import error
import { WebSocketLink } from './WebsocketLink';
import { getStage } from '../getStage';
import { SubscriptionClient } from 'graphql-aws-iot-client';
import parseJwt from 'toolkit/lib/main/Authorization/parseJwt';
import { CognitoIdToken } from 'toolkit/lib/main/CognitoIdToken';
import { hardNavigate } from '../../apps/routing/hardNavigate';
import getAwsCredentials from 'toolkit/lib/main/Authorization/getAwsCredentials';
import { ApolloLink, execute, makePromise } from 'apollo-link';
import gql from 'graphql-tag';

export type IterrexConfig = {
  identityPoolId: string,
  region: string,
  userPoolId: string,
  userPoolWebClientId: string,
};

export const GET_VERSIONS = gql`
    query getVersions {
      getVersions
    }
`;

export const createIotWebsocketLink = (link: ApolloLink): WebSocketLink => {
  const iterrexConfigString: string = localStorage.getItem('iterrexConfig');
  if (!iterrexConfigString) {
    throw new Error('Could not retrieve Iterrex Config!');
  }

  const tokenString: string = localStorage.getItem('token');
  if (!tokenString) {
    console.error('Could not retrieve token!');
    hardNavigate('/');
  }

  let iterrexConfig: IterrexConfig;
  let cognitoIdToken: CognitoIdToken;

  try {
    iterrexConfig = JSON.parse(iterrexConfigString);
  } catch (err) {
    throw new Error('Could not parse Iterrex Config!');
  }

  try {
    const {
      idToken,
    } = JSON.parse(tokenString);
    cognitoIdToken = parseJwt(idToken);

    const client = new SubscriptionClient({
      debug: true,
      reconnect: true,
      region: iterrexConfig.region,
      stage: getStage(window.location.hostname),
      iotEndpoint: process.env.AWS_IOT_ENDPOINT_HOST,
      sub: cognitoIdToken.sub,
      tenantId: iterrexConfig.userPoolId,
      getCredentialsFunction: async () => {
        const credString = localStorage.getItem('creds');
        let creds = null;

        if (credString) {
          try {
            creds = JSON.parse(credString);
          } catch (err) {
            console.error(err);
            console.log('getCredentialsFunction: Could not parse stored credentials, fetching new');
          }
        }

        // get: current tokens in localStorage
        const tokenString: string = localStorage.getItem('token');
        if (!tokenString) {
          throw new Error('getCredentialsFunction: Could not retrieve token!');
        }
        const currentToken = JSON.parse(tokenString);
        const {
          idToken,
          refreshToken,
        } = currentToken;

        try {
          // call: cognito for new session
          const getAwsCredsRes = await getAwsCredentials(idToken, iterrexConfig.identityPoolId);
          creds = {
            accessKeyId: getAwsCredsRes.accessKeyId,
            secretAccessKey: getAwsCredsRes.secretAccessKey,
            sessionToken: getAwsCredsRes.sessionToken,
          };
          console.log('creds updated from token');
        } catch (err) {
          console.log(`Failed to get new session from cognito ${err}`);
          creds = null;
        }

        // failed: call for new session failed
        if (!creds) {
          try {
            await makePromise(
              execute(link, {
                query: GET_VERSIONS,
              }),
            );
          }  catch (err) {
            console.log(`Failed to refresh idToken via graphql getVersions call ${err}`);
            localStorage.clear();
            hardNavigate('/');
          }

          try {
            const idToken = JSON.parse(localStorage.getItem('token')).idToken;

            // call: cognito for new session
            const getAwsCredsRes = await getAwsCredentials(idToken, iterrexConfig.identityPoolId);
            creds = {
              accessKeyId: getAwsCredsRes.accessKeyId,
              secretAccessKey: getAwsCredsRes.secretAccessKey,
              sessionToken: getAwsCredsRes.sessionToken,
            };
          } catch (err) {
            console.log(`Failed to get new session from cognito after acquiring new idToken ${err}`);
            localStorage.clear();
            hardNavigate('/');
          }
        }

        // set: new session creds
        localStorage.setItem('creds', JSON.stringify(creds));
        console.log('localstorage creds updated');

        return creds;
      },
    });
    return new WebSocketLink(client);
  } catch (err) {
    console.log(err);
    throw new Error('Could not parse Iterrex Token!');
  }
};
