import { getApiLocation } from './getApiLocation';

describe('getApiLocation', () => {
  it('should not error', () => {
    expect(() => getApiLocation('dev-tenant.dev.iterrex.com')).not.toThrow();
  });
  it('should return the correct url for the dev environment', () => {
    expect(getApiLocation('dev-tenant.dev.iterrex.com')).toBe('https://dev.iterrex-api.com/graphql');
  });
  it('should return the correct url for the test environment', () => {
    expect(getApiLocation('test-tenant.test.iterrex.com')).toBe('https://test.iterrex-api.com/graphql');
  });
  it('should return the correct url for the staging environment', () => {
    expect(getApiLocation('staging-tenant.staging.iterrex.com')).toBe('https://staging.iterrex-api.com/graphql');
  });
  it('should return the correct url for the production environment', () => {
    expect(getApiLocation('tenant.iterrex.com')).toBe('https://iterrex-api.com/graphql');
  });
});
