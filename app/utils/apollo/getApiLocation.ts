import { getStage } from '../getStage';

export const getApiLocation = (hostname: string) => {
  const stage = getStage(hostname);
  const prefix = stage === 'production'
    ? ''
    : `${stage}.`;
  return `https://${prefix}iterrex-api.com/graphql`;
/*
  const inputParts = input.split('.');
  const inner = inputParts
    .slice(1, inputParts.length - 1)
    .join('.');
  return `https://${inner}-api.${inputParts[inputParts.length - 1]}/graphql`;*/
};
