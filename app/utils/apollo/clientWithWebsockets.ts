import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { CognitoIdentityServiceProvider } from 'aws-sdk';
import {
  ApolloLink,
  split,
  from,
} from 'apollo-link';
import { setContext } from 'apollo-link-context';
import ServiceRequest from 'toolkit/lib/main/ServiceRequest';
const { print } = require('graphql/language/printer');
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';
import { getApiLocation } from './getApiLocation';
import { withTokenLink } from './withTokenLink';
import { createIotWebsocketLink } from './createIotWebsocketLink';
import { getMainDefinition } from 'apollo-utilities';
import { createSetTokenLink } from './createSetTokenLink';

const apiLocation = getApiLocation(window.location.hostname);

const cache = new InMemoryCache({
  addTypename: false,
});
const httpLink = createHttpLink({
  uri: `${apiLocation}`,
});

const httpLinkChain = from([
  withTokenLink,
  createSetTokenLink(),
  httpLink,
]);

const wsLink = createIotWebsocketLink(httpLinkChain);

const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  httpLinkChain);

export default new ApolloClient({
  link,
  cache,
  queryDeduplication: true,
}) as any;
