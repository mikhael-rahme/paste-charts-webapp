
export const createBasicAuthHeader = ({
  username,
  password,
}: {
  username: string,
  password: string,
}) => {
  if (!username) {
    throw new Error('createBasicAuthHeader: username required!');
  }
  if (!password) {
    throw new Error('createBasicAuthHeader: password required!');
  }
  const encoded = btoa(`${username}:${password}:${Date.now()}`);
  return `Basic ${encoded}`;
};
