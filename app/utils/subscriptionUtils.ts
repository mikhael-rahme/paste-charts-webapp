import { mergeTam } from 'operation-service/lib/utils/tam';

export const updateListNormalizedRecords = (prev, { subscriptionData: { data } }, queryName) => {
  const result = data[queryName];
  if (!result) {
    return prev;
  }

  const operationRecords = mergeTam(
    prev.listNormalizedRecords && prev.listNormalizedRecords.operationRecords || {},
    result,
  );

  return {
    listNormalizedRecords: {
      operationRecords,
      paginationToken: prev.listNormalizedRecords && prev.listNormalizedRecords.paginationToken || '',
    },
  };
};

export const updateGetNormalizedRecord = (prev, { subscriptionData: { data: { updateRecordSuccess } } }) => {
  if (!updateRecordSuccess) {
    return prev;
  }

  const operationRecords = mergeTam(prev.getNormalizedOperationRecord, updateRecordSuccess);

  return {
    getNormalizedOperationRecord: operationRecords,
  };
};
