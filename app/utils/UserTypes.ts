
export const UserTypes = {
  SUPER_ADMINISTRATOR: 'SUPER ADMINISTRATOR',
  ADMINISTRATOR: 'ADMINISTRATOR',
  USER: 'USER',
};

export default UserTypes;
