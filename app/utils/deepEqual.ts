/* eslint-disable */
/*
  A function that determines if two objects are deep equal.
  Determines if arrays, regardless of order, are matching sets.
 */

export const deepEqual = (o0, o1) => {
  if (typeof o0 !== typeof o1) {
    return false;
  }
  if (typeof o0 !== 'object') {
    return o0 === o1;
  }
  if (Array.isArray(o0)) {
    return deepEqualArray(o0, o1);
  }
  return deepEqualObject(o0, o1);
};

export const deepEqualArray = (a0, array1) => {
  let equal = true;
  const a1 = array1.map(a => a);

  if (a0.length < a1.length) {
    return false;
  }

  a0.forEach((a0Item) => {
    !a1.find((a1Item, index) => {
      if (typeof a0Item !== 'object') {
        if (a0Item === a1Item) {
          a1.splice(index, 1);
          return true;
        } return false;
      }
      if (Array.isArray(a0Item)) {
        if (deepEqualArray(a0Item, a1Item)) {
          a1.splice(index, 1);
          return true;
        } return false;
      }
      if (deepEqualObject(a0Item, a1Item)) {
        a1.splice(index, 1);
        return true;
      } return false;

    }) ? equal = false : void 0;
  });

  return equal;
};

const deepEqualObject = (o0, o1) => {
  let equal = true;
  const keys = Object.keys(o0);

  if (!deepEqualArray(keys, Object.keys(o1))) {
    return false;
  }

  keys.forEach((key) => {
    if (typeof o0[key] !== 'object') {
      !(o0[key] === o1[key]) ? equal = false : void 0;
    } else if (Array.isArray(o0[key])) {
      !deepEqualArray(o0[key], o1[key]) ? equal = false : void 0;
    } else {
      !deepEqualObject(o0[key], o1[key]) ? equal = false : void 0;
    }
  });

  return equal;
};

export default deepEqual;
