import { parseResetUrlArg } from './parseResetUrlArg';

describe('parseResetUrlArg', () => {
  test('expected output', () => {
    const a = Buffer.from('confirmation:username').toString('base64');
    const r = parseResetUrlArg(a);
    expect(r).toEqual({
      confirmationCode: 'confirmation',
      username: 'username',
    });
  });
});
