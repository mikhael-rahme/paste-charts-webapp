import { getUrlArgs } from './getUrlArgs';

describe('getUrlArgs', () => {
  test('getUrlArgs returns expected', () => {
    (global as any).window = {
      location: { href: 'https://hubbell.iterrex.com?first=0&second=1' },
    };
    expect(getUrlArgs()).toEqual({
      first: '0',
      second: '1',
    });
  });
  test('returns null if no Url args are present', () => {
    (global as any).window = {
      location: { href: 'https://hubbell.iterrex.com' },
    };
    expect(getUrlArgs()).toBeNull();
  });
  test('returns null if args do not follow the expected format', () => {
    (global as any).window = {
      location: { href: 'https://hubbell.iterrex.com?test' },
    };
    expect(getUrlArgs()).toBeNull();
  });
});
