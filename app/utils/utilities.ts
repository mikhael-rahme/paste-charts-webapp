export { getUrlArgs } from './getUrlArgs';
export { parseResetUrlArg } from './parseResetUrlArg';
import * as dateTime from 'date-and-time';
import { OperationRecord, VertexType, Operation } from 'operation-service/lib/Types';
const bytes = require('bytes');

export const getFullDateString = (isoDateString) => {
  if (!isoDateString) {
    return 'None';
  }
  try {
    const date = new Date(isoDateString);
    return `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`;
  } catch (err) {
    return 'None';
  }
};

export const getReadableBytes = input => bytes(parseInt(input, 10), { decimalPlaces: 2 });

export const hoursSinceDate = (date: string) => {
  if (!date) {
    throw new Error('You must supply a date string!');
  }
  try {
    new Date(date); // eslint-disable-line no-new
  } catch (err) {
    throw new Error('You must supply a date string!');
  }
  return getDateDifferenceInHours((new Date().toString()), date);
};

export const getDateDifferenceInHours = (date1: string, date2: string) => {
  if (date1 && date2) {
    const date1Millis = (new Date(date1)).getTime();
    const date2Millis = (new Date(date2)).getTime();
    return Math.abs(date1Millis - date2Millis) / 36e5;
  }
  throw new Error('You must supply two date strings!');
};

type miscProps = {
  fieldName?: string,
  users?: any[],
};
export const getDisplayValue = (value, dt = null, misc: miscProps = { fieldName: null, users: [] }) => {
  let dataType = dt;
  if (misc.fieldName) {
    switch (misc.fieldName) {
      case 'dateCreated':
      case 'dateModified':
        dataType = 'datetime';
        break;
      case 'createdBy':
      case 'modifiedBy':
        dataType = 'user';
        break;
      default: break;
    }
  }

  if (Array.isArray(value)) {
    if (value.length > 0 && value[0].textField) {
      return value.map(({ textField }) => textField).join(', ');
    }
    return value.join(' ');
  }
  if (typeof value === 'object' && value && value.textField) {
    return value.textField;
  }
  const valueString = (value || '').toString();
  if (['true', 'false'].includes(valueString)) {
    return trueFalseToYesNo(valueString);
  }
  if (dataType) {
    if (dataType === 'boolean') {
      return value ? 'Yes' : 'No';
    }
    if (dataType === 'datetime') {
      if (!value) {
        return '';
      }
      return isNaN(value)
        ? new Date(value).toLocaleString()
        : new Date(parseInt(value, 10)).toLocaleString();
    }
    if (dataType === 'user') {
      return misc.users[value || ''];
    }
  }
  if ((value || '').toString() === 'NaN') {
    return '';
  }
  return value;
};

export const enumToEnglish = enumString => enumString
  .substr(0, 1) + enumString
    .substr(1).toLowerCase()
    .replace(/_/g, ' ');

export const groupsHaveWritePermission = (groupsThatCanWrite: string[], groupsToCheck: string[]) => {
  let groupCanWrite: boolean = false;
  groupsToCheck.forEach((group) => {
    if (!groupCanWrite) {
      groupCanWrite = groupsThatCanWrite.includes(group);
    }
  });
  return groupCanWrite;
};

export const groupsHavePermission = (groupsThatHavePermission: string[], groupsToCheck: string[]) => {
  let groupHasPermission: boolean = false;
  groupsToCheck.forEach((group) => {
    if (!groupHasPermission) {
      groupHasPermission = groupsThatHavePermission.includes(group);
    }
  });
  return groupHasPermission;
};

export const convertToJSONIfNeeded = (text) => {
  if (typeof text !== 'string') {
    return text;
  }
  try {
    const newText = JSON.parse(stripEscapeCharacters(text)); // TODO: This will need to change when we go to ref values
    return newText;
  } catch (error) {
    return text;
  }
};

export const operationRecordToFlatValues = (o: any, or: any) => { // TODO: fix types
  const res = {};
  if (o.fields) {
    o.fields.forEach((field) => {
      const find = or.fieldRecords.find(fr => fr.configId === field.id);
      if (find) {
        res[field.fieldName] = convertToJSONIfNeeded(find.value ? (find.value.textField || find.value) : '');
      }
    });
  }
  return res;
};

export const tableOperationRecordToFlatValues = (o: Operation, or: OperationRecord) => { // TODO: fix types
  const res = {};
  if (o.fields && or) {
    o.fields.forEach((field) => {
      const find = or.fieldRecords.find(fr => fr.configId === field.id) as &any;
      if (find) {
        if (field.dataType.toLowerCase() === 'date' || field.dataType.toLowerCase() === 'datetime') {
          const val = convertToJSONIfNeeded(find.value ? (find.value.textField || find.value) : '');
          res[field.fieldName] = { type: 'date', value: val };
        } else {
          res[field.fieldName] = convertToJSONIfNeeded(find.value ? (find.value.textField || find.value) : '');
        }
      }
    });
  }
  return res;
};

export const getTextForTables = (operation, operationRecord) => {
  const values = tableOperationRecordToFlatValues(operation, operationRecord);
  const res = {};
  Object.keys(values).forEach((key) => {
    if (values[key] !== null) {
      if (Array.isArray(values[key])) {
        // const tempVal = values[key].filter(f => f.textField && f.valueField).map(v => v.textField);
        const tempVal = values[key].map(v => v.textField);
        res[key] = tempVal.join(', ');
      } else if (values[key].textField) {
        res[key] = values[key].textField;
      } else if (values[key].type && values[key].type === 'date') {
        res[key] = `${dateDisplayFormat(values[key].value)} ${timeDisplayFormat(values[key].value)}`;
      } else {
        res[key] = values[key];
      }
    }
  });
  return res;
};

const dateDisplayFormat = (dateString) => {
  const date = new Date(dateString);
  return `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`;
};

export const pad = (number, length) => {
  let str = `${number}`;
  while (str.length < length) {
    str = `0${str}`;
  }
  return str;
};

const timeDisplayFormat = (dateString) => {
  const date = new Date(dateString);
  return `${date.getHours() > 12 ? date.getHours() - 12 : date.getHours() === 0 ? '12' :
    date.getHours()}:${pad(date.getMinutes(), 2)} ${date.getHours() >= 12 ? 'PM' : 'AM'}`;
};

export const operationRecordToTypedValues = (o: any, or: any) => { // TODO: fix types
  const res = {};
  if (o.fields) {
    o.fields.forEach((field) => {
      const find = (or.fieldRecords || []).find(fr => fr.configId === field.id && fr.configVersion === field.version);
      if (find) {
        res[field.fieldName] = convertToDataType(find.value);
      }
    });
  }
  return res;
};

export const flattenORtoFieldName = (o, or, fieldName): string[] => {
  const fieldNames: string[] = [];
  if (o && or) {
    or.forEach((r) => {
      const flattened = operationRecordToFlatValues(o, r);
      if (Object.keys(flattened).includes(fieldName)) {
        fieldNames.push(flattened[fieldName]);
      }
    });
  }
  return fieldNames;
};

export const lowercaseSort = (a, b) => {
  if (typeof a === 'string' && typeof b === 'string') {
    return a.toLowerCase().localeCompare(b.toLowerCase());
  }
  return a - b;
};

export const doDatesMatch = (date, day, time) => {
  const lineItemDate = dateTime.format(date, 'MM/DD h:00 A').replace(/\./g, '').toUpperCase();
  const scheduleSlotDate = `${day} ${time}`;
  return lineItemDate === scheduleSlotDate;
};

export const subtractArrays = (a, b) => a.filter(x => b.indexOf(x) < 0);
export const subtractArraysByProperty = (a, b, prop) => a.filter(x => b.find(y => y[prop] !== x[prop]));

export const flattenORtoFieldNameObj: any = (o, or, key, value) => {
  const fields = {};
  if (o && or) {
    or.forEach((r) => {
      const flattened = operationRecordToFlatValues(o, r);
      if (Object.keys(flattened).includes(key) && Object.keys(flattened).includes(value)) {
        if (!Object.keys(fields).includes(flattened[key])) {
          fields[flattened[key]] = [];
        }
        if (flattened[value] !== null && flattened[value] !== '') {
          fields[flattened[key]].push(flattened[value]);
        }
      }
    });
  }
  return fields;
};

export const unCamelize = (str: string) => {
  if (!str) {
    return null;
  }
  return str
    .replace(/([A-Z])/g, ' $1')
    .replace(/^./, str => str.toUpperCase())
    .replace(/([A-Z])\s(?=[A-Z]\b)/g, '$1')
    .replace(/ +(?= )/g, '').trim();
};

export const camelize = (str: string) => {
  return (str || '').replace(/(?:^\w|[A-Z]|\b\w)/g, (letter, index) => {
    return index === 0 ? letter.toLowerCase() : letter.toUpperCase();
  }).replace(/\s+/g, '');
};

export const objectFlip = (obj) => {
  const ret = {};
  Object.keys(obj).forEach((key) => {
    ret[obj[key]] = key;
  });
  return ret;
};

export const datatypeToFormType = (datatype) => {
  const dataTypes = {
    string: 'Text',
    boolean: 'Checkbox',
    number: 'Number',
    integer: 'Integer',
    object: 'Multiselect',
    reference: 'Reference to Parent List',
    user: 'User',
    date: 'Date',
    datetime: 'DateTime',
    group: 'Group',
  };
  const types = { ...dataTypes, ...objectFlip(dataTypes) };
  return types[datatype];
};

export const filterObject = (object, keys) => {
  const newObject = {};
  if (keys) {
    keys.forEach((key) => {
      if (object && key && nnnu(object[key])) {
        newObject[key] = object[key];
      }
    });
  }
  return newObject;
};

export const objectWithoutKey = (object, key: string) => {
  const { [key]: deletedKey, ...otherKeys } = object;
  return otherKeys;
};

export const objectWithoutKeys = (object, keys: string[]) => {
  let newObject = { ...object };
  keys.forEach(key => newObject = objectWithoutKey(newObject, key));
  return newObject;
};

export const stripEscapeCharactersFromObject = (object) => {
  Object.keys(object).forEach(key => object[key] = stripEscapeCharacters(object[key]));
  return object;
};

export const stripEscapeCharacters = (text) => {
  if (typeof text === 'string') {
    const string = text.replace(/\\"/g, '"').replace(/\\/g, '');
    return string;
  }
  return text;
};

export const stripNestedProperties = <T extends object>(o: T | T[], k: string | string[]) => {
  const keys: string[] = Array.isArray(k) ? k : [k];
  const newArray = [];
  let objects: object[];
  objects = Array.isArray(o)
    ? o
    : [o];

  objects.forEach((obj) => {
    let addedObject = { ...obj };
    keys.forEach((prop) => {
      if (Object.keys(addedObject).includes(prop)) {
        addedObject = objectWithoutKey(addedObject, prop);
      }
    });
    Object.keys(addedObject).forEach((key) => {
      if (Array.isArray(addedObject[key])) {
        addedObject[key] = stripNestedProperties(addedObject[key], keys);
      }
    });
    newArray.push(addedObject);
  });
  return Array.isArray(o) ? newArray : newArray[0];
};

export const stripUnwantedProperties = (o) => {
  const unwantedKeys = [
    'fieldId',
    'orderNumber',
    'modifiedNum',
    'index',
    'modified',
    'tenant',
    'customer',
    'lineItemNum',
  ];
  return stripNestedProperties(o, unwantedKeys);
};

export const stripUnwantedPropertiesForCopy = (o) => {
  const unwantedKeys = [
    'id',
    'version',
  ];
  return stripNestedProperties(o, unwantedKeys);
};

export const integerNormalize = (value, previousValue) => {
  if (!value || value === '') {
    return '';
  }
  if (!isNaN(value)) {
    return parseInt(value.toString(), 10);
  }

  const valid = !/^\s*$/.test(value);
  const previousValueValid = !/^\s*$/.test(previousValue) && !isNaN(previousValue);
  if (!valid) {
    return (previousValue && previousValueValid) ? parseInt(previousValue, 10) : '';
  }
  return value && parseInt(value.replace(/[^\d]/g, ''), 10);
};

export const convertToJS = (o) => {
  if (!nnnu(o)) {
    return o;
  }
  let obj = o;
  if (obj.size !== undefined) {
    obj = obj.toJS();
  }
  return obj;
};

export const noValue = (values, key) => {
  if (values.size !== undefined) {
    return !values.get(key) || values.get(key) === '' || values.get(key) === null;
  }
  return !Object.keys(values).includes(key) || values[key] === '' || values[key] === null;
};

export const convertToDataType = (value) => {
  if (value === null) {
    return null;
  }
  if (value === '') {
    return '';
  }
  if (!isNaN(value)) {
    const num = parseFloat(value);
    return num % 1 === 0 ? parseInt(value, 10) : num;
  }
  if (['true', 'false'].includes(value)) {
    return value === 'true';
  }
  return stringUnescape(value.toString());
};

const strongPassword = (password) => {
  const uc = password.match(/([A-Z])/g);
  const lc = password.match(/([a-z])/g);
  const n = password.match(/([\d])/g);
  return password.length >= 12 &&
    uc && uc.length > 0 &&
    lc && lc.length > 0 &&
    n && n.length > 0;
};

export const generateRandomPassword = () => {
  const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  let password = '';
  while (!strongPassword(password)) {
    password = `${Array(12)
      .fill(chars)
      .map(x => x[Math.floor(Math.random() * x.length)])
      .join('')}!`;
  }
  return password;
};

export const flattenArray = (arr) => {
  return arr.reduce(
    (flat, toFlatten) => {
      return flat.concat(Array.isArray(toFlatten) ? flattenArray(toFlatten) : toFlatten);
    },
    [],
  );
};

export type refType = {
  id: string,
  version?: number,
};

export const createRefsObject = (refs) => {
  const refsObject = [];
  refs.forEach((r) => {
    const ref: any = {};
    if (r.refType) { ref.refType = r.refType; }
    if (r.refId) {
      if (r.refId && Array.isArray(r.refId)) {
        ref.refId = r.refId[0].valueField;
      } else if (r.refId.valueField && Array.isArray(r.refId.valueField)) {
        ref.refId = r.refId.valueField[0].valueField;
      } else  {
        ref.refId = r.refId.valueField || r.refId;
      }
    }
    if (r.refVersion) { ref.refVersion = r.refVersion; }
    if (r.id) { ref.id = r.id; }
    if (r.version) { ref.version = r.version; }
    if (Object.keys(ref).length > 0) {
      ref.type = VertexType.REFERENCE;
      refsObject.push(ref);
    }
  });
  return refsObject;
};

export const findIndexWithAttr = (array, attr, value) => {
  for (let i = 0; i < array.length; i += 1) {
    if (array[i][attr] === value) {
      return i;
    }
  }
  return -1;
};
export const notNullOrUndefined = value => value !== null && typeof value !== 'undefined';
export const nnnu = value => value !== null && typeof value !== 'undefined';
export const labelWidth = width => ({
  labelWidth: width,
  labelClass: width === 12 ? 'text-left' : 'text-right',
  controlClass: 'form-control',
  controlWidth: 12 - width,
});

export const stringUnescape = str => str ? str.toString().replace(/\\'/g, "'").replace(/\\"/g, '"') : str;

export const getDayEstimatedWeight = lineItems =>
  lineItems.reduce((a: number, b) => a + parseInt(b.estimatedWeight, 10) || 0, 0);

export const getLineItemsEstimatedWeight = lineItems =>
  lineItems.reduce((a: number, b) => a + parseInt(b.estimatedWeight, 10) || 0, 0);

/**
 * For the given Date, set the time of day to 0 utc
 * @param time
 */
export const resetTime = (time: Date) => {
  time.setHours(0, 0, 0, 0);
  return time;
};

export const trueFalseToYesNo = (value) => {
  if (['true', 'false'].includes(value)) {
    return value === 'true' ? 'Yes' : 'No';
  }
  return value;
};

export const getTenant = () => window.location.hostname.split('.')[0];

// REPLACE WITH REDUCE
export const collectOperationRecords = (operationRecords: OperationRecord[], id: string, r = []): OperationRecord[] => {
  let records = r;
  operationRecords.forEach((record) => {
    if (record.configId === id) {
      records.push(record);
    } else if (record.operationRecords && record.operationRecords.length > 0) {
      records = collectOperationRecords(record.operationRecords, id, records);
    }
  });
  return records;
};

export const fetchNextOperationRecords = (
  fetchMore,
  paginationToken,
  additionalFunctionality = operationRecords => null,
  type = 'concat',
) => {
  if (!fetchMore) {
    return {};
  }

  fetchMore({
    variables: { paginationToken },
    updateQuery: (previousResult, result) => {
      const { fetchMoreResult } = result;
      if (!previousResult.listOperationRecords || !fetchMoreResult.listOperationRecords) {
        return previousResult;
      }

      const { listOperationRecords: { operationRecords, paginationToken } } = fetchMoreResult;

      additionalFunctionality(operationRecords);
      switch (type) {
        case 'concat':
          return {
            listOperationRecords: {
              paginationToken,
              operationRecords: previousResult.listOperationRecords.operationRecords
                .filter(({ id }) => !operationRecords.map(or => or.id).includes(id))
                .concat(operationRecords),
            },
          };
        case 'replace':
          return {
            listOperationRecords: {
              paginationToken,
              operationRecords,
            },
          };
        default: return {};
      }
    },
  });
};

export const fetchNextNormalizedRecords = (
  fetchMore,
  paginationToken,
  type = 'concat',
) => {
  if (!fetchMore) {
    return {};
  }

  fetchMore({
    variables: { paginationToken },
    updateQuery: (previousResult, result) => {
      const { fetchMoreResult } = result;
      if (!previousResult.listNormalizedRecords || !fetchMoreResult.listNormalizedRecords) {
        return previousResult;
      }

      const { listNormalizedRecords: { operationRecords, paginationToken } } = fetchMoreResult;

      switch (type) {
        case 'concat':
          return {
            listNormalizedRecords: {
              paginationToken,
              operationRecords: Object.keys(operationRecords).reduce(
                (obj, key) => ({
                  ...obj,
                  [key]: previousResult.listNormalizedRecords.operationRecords[key].concat(operationRecords[key]),
                }),
                {}),
            },
          };
        case 'replace':
          return {
            listNormalizedRecords: {
              paginationToken,
              operationRecords,
            },
          };
        default: return {};
      }
    },
  });
};

export const fetchNextNormalizedOperations = (
  fetchMore,
  paginationToken,
  type = 'concat',
) => {
  if (!fetchMore) {
    return {};
  }

  fetchMore({
    variables: { paginationToken },
    updateQuery: (previousResult, result) => {
      const { fetchMoreResult } = result;
      if (!previousResult.listNormalizedOperations || !fetchMoreResult.listNormalizedOperations) {
        return previousResult;
      }

      const { listNormalizedOperations: { operations, paginationToken } } = fetchMoreResult;

      switch (type) {
        case 'concat':
          return {
            listNormalizedOperations: {
              paginationToken,
              operations: Object.keys(operations).reduce(
                (obj, key) => ({
                  ...obj,
                  [key]: [
                    ...previousResult.listNormalizedOperations.operations[key],
                    ...operations[key],
                  ],
                }),
                {}),
            },
          };
        case 'replace':
          return {
            listNormalizedOperations: {
              paginationToken,
              operations,
            },
          };
        default: return {};
      }
    },
  });
};

export const getFieldId = (operation, fieldName) => {
  return ((operation.fields.find(o => o.fieldName === fieldName) || {}).id || '').toString();
};

export const getReactWidgetValue = (value) => {
  let valueString = typeof value === 'object' ? '' : value;
  if (!valueString) {
    for (const char in value) {
      if (typeof value[char] === 'string') {
        valueString += value[char];
      }
    }
  }
  return valueString;
};

export const replaceQueryProps = (query, props) => {
  if (!query) { return null; }
  const newQuery = { ...query };
  Object.keys(query).forEach((key) => {
    if (key === 'rules' && Array.isArray(query[key])) {
      const ruleProps = replaceQueryProps(newQuery[key], props);
      const ruleKeys = Object.keys(ruleProps);
      newQuery[key] = ruleKeys.map(k => ruleProps[k]);
    } else if (!Array.isArray(query[key]) && typeof query[key] === 'object') {
      newQuery[key] = replaceQueryProps(newQuery[key], props);
    } else if (key === 'value' && typeof query[key] === 'string' && query[key].startsWith('^^^props.')) {
      const getProps = query[key].replace('^^^', '').split('.');
      getProps.shift();
      let val = props;
      getProps.forEach(p => val = val[p]);
      newQuery[key] = val;
    } else if (key === 'value' && typeof query[key] === 'string' && query[key].startsWith('^^^date')) {
      newQuery[key] = new Date().toString();
    } else {
      newQuery[key] = query[key];
    }
  });
  return newQuery;
};

export const toArray = val => Array.isArray(val) ? val : [val];

export const isTablet = () => /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

export const toDate = (date) => {
  if (!date) {
    return null;
  }
  if (typeof date.getTime === 'function' && !isNaN(date.getTime())) {
    return date;
  }
  if (Number.isInteger(date)) {
    return new Date(date);
  }
  return isNaN(date) ? new Date(date.replace(/"/g, '')) : new Date(parseInt(date, 10));
};

export const intersectArrays = (a, b) => a.filter(value => -1 !== b.indexOf(value));
