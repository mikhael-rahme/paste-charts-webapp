
import {
  IterrexRole,
} from 'toolkit/lib/main/IterrexRole';
import isSystemRole from './isSystemRole';
import isTenantRole from './isTenantRole';
import capitalizeFirstLetter from '../capitalizeFirstLetter';

const mapRoleToPlaintext = (role: IterrexRole | string) => {
  if (isSystemRole(role)) {
    return role.split('-').map(capitalizeFirstLetter);
  }
  if (isTenantRole(role)) {
    return role.replace('tenant-', '').split('-').map(capitalizeFirstLetter).reduce((a, b) => `${a} ${b}`);
  }
  // user defined roles are just plaintext
  return role;
};

export default mapRoleToPlaintext;
