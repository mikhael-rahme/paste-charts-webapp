const expect = require('chai').expect;
import {
  IterrexSystemRole,
} from 'toolkit/lib/main/IterrexSystemRole';
import {
  IterrexTenantRole,
} from 'toolkit/lib/main/IterrexTenantRole';
import mapRoleToPlaintext from './mapRoleToPlantext';

describe('mapRoleToPlaintext', () => {
  it(`should work for ${IterrexSystemRole.SYSTEM_ADMIN}`, () => {
    expect(mapRoleToPlaintext(IterrexSystemRole.SYSTEM_ADMIN)).toEqual('System Admin');
  });
  it(`should work for ${IterrexSystemRole.SYSTEM_USER}`, () => {
    expect(mapRoleToPlaintext(IterrexSystemRole.SYSTEM_USER)).toEqual('System User');
  });
  it(`should work for ${IterrexTenantRole.TENANT_OWNER}`, () => {
    expect(mapRoleToPlaintext(IterrexTenantRole.TENANT_OWNER)).toEqual('Owner');
  });
  it(`should work for ${IterrexTenantRole.TENANT_ADMIN}`, () => {
    expect(mapRoleToPlaintext(IterrexTenantRole.TENANT_ADMIN)).toEqual('Admin');
  });
  it(`should work for ${IterrexTenantRole.TENANT_PUBLISHER}`, () => {
    expect(mapRoleToPlaintext(IterrexTenantRole.TENANT_PUBLISHER)).toEqual('Publisher');
  });
  it(`should work for ${IterrexTenantRole.TENANT_AGENT_USER}`, () => {
    expect(mapRoleToPlaintext(IterrexTenantRole.TENANT_AGENT_USER)).toEqual('Agent User');
  });
  it(`should work for ${IterrexTenantRole.TENANT_WRITE_USER}`, () => {
    expect(mapRoleToPlaintext(IterrexTenantRole.TENANT_WRITE_USER)).toEqual('Write User');
  });
  it(`should work for ${IterrexTenantRole.TENANT_READ_USER}`, () => {
    expect(mapRoleToPlaintext(IterrexTenantRole.TENANT_READ_USER)).toEqual('Read User');
  });
});
