
import {
  IterrexRole,
} from 'toolkit/lib/main/IterrexRole';
import {
  IterrexSystemRole,
} from 'toolkit/lib/main/IterrexSystemRole';
import {
  IterrexTenantRole,
} from 'toolkit/lib/main/IterrexTenantRole';
import isSystemRole from './isSystemRole';
import isTenantRole from './isTenantRole';
import capitalizeFirstLetter from '../capitalizeFirstLetter';

const unmapRoleToPlaintext = (role: string) => {
  if (isTenantRole(`tenant-${role.toLowerCase()}`)) {
    return `tenant-${role.toLowerCase()}`;
  }
  if (isSystemRole(`system-${role.toLowerCase()}`)) {
    return `system-${role.toLowerCase()}`;
  }
  // user defined roles are just plaintext
  return role;
};

export default unmapRoleToPlaintext;
