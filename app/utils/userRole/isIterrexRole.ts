import {
  IterrexRole,
} from 'toolkit/lib/main/IterrexRole';
import isSystemRole from './isSystemRole';
import isTenantRole from './isTenantRole';

const isIterrexRole = (role: IterrexRole | string) => (isSystemRole(role) || isTenantRole(role));
export default isIterrexRole;
