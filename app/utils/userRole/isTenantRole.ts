import { EnumValues } from 'enum-values';
import {
  IterrexRole,
} from 'toolkit/lib/main/IterrexRole';
import {
  IterrexTenantRole,
} from 'toolkit/lib/main/IterrexTenantRole';

const isSystemRole = (role: IterrexRole | string) => EnumValues.getValues(IterrexTenantRole).includes(role);
export default isSystemRole;
