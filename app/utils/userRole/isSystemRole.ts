import { EnumValues } from 'enum-values';
import {
  IterrexRole,
} from 'toolkit/lib/main/IterrexRole';
import {
  IterrexSystemRole,
} from 'toolkit/lib/main/IterrexSystemRole';

const isSystemRole = (role: IterrexRole | string) => EnumValues.getValues(IterrexSystemRole).includes(role);
export default isSystemRole;
