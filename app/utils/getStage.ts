import { Stage } from 'toolkit/lib/main/Stage';

export const getStage = (hostname: string): Stage => {
  const hostnameParts = hostname.split('.');
  if (hostnameParts.length === 3) {
    return 'production';
  }
  return hostnameParts[1] as Stage;
};
