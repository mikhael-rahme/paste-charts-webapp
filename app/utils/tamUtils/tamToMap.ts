import { NormalizedTypeArrayMap } from 'operation-service/lib/Types';

export const tamToMap = (tam: NormalizedTypeArrayMap): NormalizedTypeArrayMap => Object.keys(tam).reduce((map, prop) => {
  for (const item of tam[prop]) {
    map[item.id] = item;
  }
  return map;
},                                                                                                       {});
