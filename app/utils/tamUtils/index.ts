/*
  TAM: TypeArrayMap

  Utils to work directly with TAM, to save resources.
*/

export { denormalizeTam } from './denormalizeTam';
export { mergeTam } from './mergeTam';
export { nonNormalizedToTam } from './normalizeToTam';
export { getChildrenFromTam } from './getChildrenFromTam';
export { childPropsToType } from './childPropsToType';
export { getFromTam } from './getFromTam';
export { getTreeFromTam } from './getTreeFromTam';
export { tamToMap } from './tamToMap';
