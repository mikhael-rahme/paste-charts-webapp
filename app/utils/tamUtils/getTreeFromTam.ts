import {
  NormalizedTypeArrayMap,
  Identifier,
  TypeToChildPropertiesMap,
} from 'operation-service';

import {
  getFromTam,
  getChildrenFromTam,
  childPropsToType,
} from '.';

export const getTreeFromTam = (
  tam: NormalizedTypeArrayMap,
  parent: { id: string, type: string },
  selectedChildProps?: string[],
) => {
  const newTam: NormalizedTypeArrayMap = {
    operations: [],
    operationRecords: [],
    fields: [],
    fieldRecords: [],
    users: [],
    groups: [],
    permissions: [],
    references: [],
  };

  const root = getFromTam(tam, parent);
  newTam[childPropsToType[parent.type]].push(root);

  let children = [];
  for (const childProp of selectedChildProps || TypeToChildPropertiesMap[parent.type]) {
    const childType = childPropsToType[childProp];
    const childIdentifiers = root[childProp];
    if (childIdentifiers) {
      for (const childIden of childIdentifiers) {
        const getResult = getFromTam(tam, childIden.id, childType);
        if (getResult) {
          children.push(getResult);
          newTam[childProp].push(getResult);
        }
      }
    }
  }

  while (children.length) {
    children = children.reduce((nextChildren, data) => {
      for (const childProp of selectedChildProps || TypeToChildPropertiesMap[data.type]) {
        const dataType = childPropsToType[childProp];
        const childIdens = data[childProp];
        if (childIdens) {
          for (const ci of childIdens) {
            const getResult = getFromTam(tam, ci.id, dataType);
            if (getResult) {
              nextChildren.push(getResult);
              newTam[childProp].push(getResult);
            }
          }
        }
      }
      return nextChildren;
    },                         []);
  }
  return newTam;
};
