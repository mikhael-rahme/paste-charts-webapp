export const dedup = (array, predicate?) =>
  array.filter((a, i) => array.slice(0, i).findIndex(b => predicate ? predicate(a, b) : a === b) === -1);
/*
  mutanizer: if you want to mutate the object as its dup'd
*/
export const dup = (
  item: any,
  times: number,
  mutanizer: (item: any, index: any) => any = a => a,
) => {
  const res = [];
  for (let x = 0; x < times; x++) {
    if (typeof item === 'object' && item !== undefined && item !== null) {
      res.push(JSON.parse(JSON.stringify(mutanizer(item, x))));
    } else {
      res.push(mutanizer(item, x));
    }
  }
  return res;
};
