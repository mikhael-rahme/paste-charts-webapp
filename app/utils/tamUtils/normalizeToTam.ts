import {
  NonNormalizedData,
  NormalizedTypeArrayMap,
  TypeToChildPropertiesMap,
  TypeToFromParentEdgeMap,
  EdgeType,
  VertexType,
} from 'operation-service/lib/Types';

/** WARNING Mutates the provided tam. Don't export dingus (ryan)
 *
 * @param tam
 * @param parentOfObj
 * @param objToNormalize
 */
const doNormalizeToTam = (
  tam: NormalizedTypeArrayMap,
  parentOfObj: NonNormalizedData,
  objToNormalize: NonNormalizedData): NormalizedTypeArrayMap => {
  const nObj = {
    ...objToNormalize,
    parents: [],
  };

  if (parentOfObj) {
    nObj.parents.push({ id: parentOfObj.id, version: parentOfObj.type === VertexType.PERMISSION ? parentOfObj.version : 0 });
  }

  for (const prop of TypeToChildPropertiesMap[objToNormalize.type]) {
    const childIdentifiers = [];
    for (const child of objToNormalize[prop] || []) {
      childIdentifiers.push({ id: child.id, version: 0 });
      doNormalizeToTam(tam, objToNormalize, child);
    }
    nObj[prop] = childIdentifiers;
  }
  const tamPropForType = TypeToFromParentEdgeMap[objToNormalize.type][0];
  tam[tamPropForType].push(nObj);

  return tam;
};

/**
 * For the provided nonNormalizedData, return a NormalizedTypeArrayMap corresponding to the provided obj.
 * @param obj
 */
export const nonNormalizedToTam = (obj: NonNormalizedData): NormalizedTypeArrayMap => {
  const tam: NormalizedTypeArrayMap = {
    operations: [],
    operationRecords: [],
    fields: [],
    fieldRecords: [],
    users: [],
    groups: [],
    permissions: [],
    references: [],
  };
  return doNormalizeToTam(tam, null, obj);
};
