export const childPropsToType = {
  operations: 'Operation',
  operationRecords: 'OperationRecord',
  fields: 'Field',
  fieldRecords: 'FieldRecord',
  permissions: 'Permission',
  references: 'Reference',
  users: 'User',
  groups: 'Group',
};
