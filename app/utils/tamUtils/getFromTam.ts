import {
  NormalizedData,
  NormalizedTypeArrayMap,
  TypeToChildPropertiesMap,
} from 'operation-service/lib/Types';

export type getFromTam = {
  (tam: NormalizedTypeArrayMap, id: string, type: string): NormalizedData;
  (tam: NormalizedTypeArrayMap, item: { id: string, type: string }): NormalizedData;
};

export const getFromTam: getFromTam = (
  tam: NormalizedTypeArrayMap,
  arg0: string | { id: string, type: string },
  arg1?: string,
) => {
  let id;
  let type;
  if (typeof arg0 === 'object') {
    id = arg0.id;
    type = arg0.type;
  } else {
    id = arg0;
    type = arg1;
  }
  const prop = TypeToChildPropertiesMap[type];
  return tam[prop].find(item => item.id === id);
};
