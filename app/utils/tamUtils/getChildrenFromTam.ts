import {
  NormalizedTypeArrayMap,
  Identifier,
  TypeToChildPropertiesMap,
} from 'operation-service';

/**
 * From the given TAM, access the childrne of the given parent.
 * Optionally provide an array of childProps to lookup from the tam and return
 * @param tam
 * @param parent
 * @param selectedChildProps
 */
export const getChildrenFromTam = (tam: NormalizedTypeArrayMap, parent: { id: string, type: string }, selectedChildProps?: string[]) => {
  const newTam: NormalizedTypeArrayMap = {
    operations: [],
    operationRecords: [],
    fields: [],
    fieldRecords: [],
    users: [],
    groups: [],
    permissions: [],
    references: [],
  };

  const root = tam[parent.type].find(p => p.id === parent.id);
  const childProps = Array.isArray(selectedChildProps) ? selectedChildProps : TypeToChildPropertiesMap[root.type];

  if (!root) {
    return newTam;
  }

  for (const prop of childProps) {
    for (const childIden of root[prop] || []) {
      const find = tam[prop].find(data => data.id === childIden.id);
      if (find) {
        newTam[prop].push(find);
      }
    }
  }

  return newTam;
};
