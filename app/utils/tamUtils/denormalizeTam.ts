import {
  NormalizedTypeArrayMap,
  NormalizedData,
  TypeToChildPropertiesMap,
  NormalizedRecord,
} from 'operation-service/lib/Types';
import { uniqBy } from 'lodash';

export const denormalizedItem = (map, item: NormalizedData) => {
  if (!item) {
    return null;
  }
  const childProps = TypeToChildPropertiesMap[item.type];
  const childMap = {};
  for (const prop of childProps) {
    childMap[prop] = item[prop].map(childIdentifier => denormalizedItem(map, map[childIdentifier.id])).filter(Boolean);
  }
  return {
    ...item,
    ...childMap,
    parents: undefined,
  };
};

/**
 * Provide an optional object for overriding the default top-node selector.
 * configId?: string -- select trees starting from vertices that have the specified configId
 */
export const denormalizeTam = (tam: NormalizedTypeArrayMap, options?: { configId?: string }): any[] => {
  if (!tam) {
    return [];
  }
  const { configId } = options || { configId: undefined };
  const keys = Object.keys(tam);
  const list: NormalizedData[] = keys.reduce((full, key) => full.concat(tam[key]), []);
  const map = {};
  list.forEach((li) => {
    if (li && li.id) {
      map[li.id] = li;
    }
  });
  const roots = list.filter((li) => {
    if (li) {
      if (configId) {
        return (li as NormalizedRecord).configId === configId;
      }
      if (li.parents) {
        return !li.parents.length || li.parents.every(pid => !map[pid.id]);
      }

    }
    return false;
  });
  return roots.map(root => denormalizedItem(map, root));
};
