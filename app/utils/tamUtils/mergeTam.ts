import {
  NormalizedTypeArrayMap,
  NormalizedData,
  TypeToChildPropertiesMap,
  VertexType,
  TypeToFromParentEdgeMap,
  NormalizedTypeArrayMapDataKeys,
} from 'operation-service/lib/Types';
import { dedup } from './dedup';
import { tamToArray } from 'operation-service/lib/utils/tam';
import { ensureChildIdenExists, ensureParentIdenExists } from 'operation-service/lib/utils/tam/ensureParentChildIntegrity';

/**
 * Merges the childProps of prev into next and dedups
 * @param prev
 * @param next
 */
const mergeNormData = (prev: NormalizedData, next: NormalizedData) => {
  if (!prev) {
    return next;
  }
  if (!next) {
    return prev;
  }
  let childProps = TypeToChildPropertiesMap[prev.type];
  if (prev.type === VertexType.PERMISSION) {
    childProps = childProps.filter(prop => prop !== VertexType.USER && prop !== VertexType.GROUP);
  }
  for (const prop of childProps) {
    const identifiers = dedup((prev[prop] || []).concat(next[prop] || []), (a, b) => a.id === b.id);
    next[prop] = identifiers;
  }
  if (prev.parents || next.parents) {
    const identifiers = dedup((prev.parents || []).concat(next.parents || []), (a, b) => a.id === b.id && a.version === b.version);
    next.parents = identifiers;
  }
  return next;
};

/**
 * Merges the source into the target
 * If ids collide, the source item replaces the target item. Identifier lists are merged and deduped.
 * @param target
 * @param source
 */
export const mergeTam = (target: NormalizedTypeArrayMap, source: NormalizedTypeArrayMap) => {
  const tam = {
    operations: [],
    operationRecords: [],
    fields: [],
    fieldRecords: [],
    users: [],
    groups: [],
    permissions: [],
    references: [],
  };

  if (!source) {
    if (!target) {
      return tam;
    }
    return target;
  }
  if (!target) {
    return source;
  }

  const tarArray = tamToArray(target);
  const srcArray = tamToArray(source);
  let finArray = tarArray;

  for (const src of srcArray) {
    let found = false;
    const srcParentEdge = TypeToFromParentEdgeMap[src.type][0];
    finArray = finArray.reduce((list, fin) => {
      const finParentEdge = TypeToFromParentEdgeMap[src.type][0];
      if (src.id === fin.id) {
        list.push(mergeNormData(fin, src));
        found = true;
      } else {
        if (
          // ensure identifiers in both child/parent if src is a child of fin
          src.parents.find(par => par.id === fin.id &&
            (src.type === 'Group' || src.type === 'User' ? par.version === fin.version : true)) ||
          fin[srcParentEdge] && fin[srcParentEdge].find(chi => chi.id === src.id)
        ) {
          ensureChildIdenExists(fin, src);
          ensureParentIdenExists(fin, src);
        } else if (
          // ensure identifiers in both child/parent if fin is a child of src
          fin.parents.find(par => par.id === src.id &&
            (fin.type === 'Group' || fin.type === 'User' ? par.version === src.version : true)) ||
          src[finParentEdge] && src[finParentEdge].find(chi => chi.id === fin.id)
        ) {
          ensureChildIdenExists(src, fin);
          ensureParentIdenExists(src, fin);
        }
        list.push(fin);
      }
      return list;
    },                         []);
    if (!found) {
      finArray.push(src);
    }
  }

  // TODO: remove this and return a new TAM; this is here because I'm too lazy/lack time to go through everything and repair where
  //  this function is used as non-pure function
  NormalizedTypeArrayMapDataKeys.forEach(key => target[key] = []);

  finArray.forEach((item) => {
    switch (item.type) {
      case 'Operation':
        target.operations.push(item);
        break;
      case 'OperationRecord':
        target.operationRecords.push(item);
        break;
      case 'Field':
        target.fields.push(item);
        break;
      case 'FieldRecord':
        target.fieldRecords.push(item);
        break;
      case 'Permission':
        target.permissions.push(item);
        break;
      case 'Reference':
        target.references.push(item);
        break;
      case 'Group':
        target.groups.push(item);
        break;
      case 'User':
        target.users.push(item);
      default: break;
    }
  });

  return target;
};
