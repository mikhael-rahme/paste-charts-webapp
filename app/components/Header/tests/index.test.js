
import * as React from 'react';
import { shallow } from 'enzyme';
import Header from '../index.tsx';
const { describe, it } = require('mocha');
const expect = require('expect');

describe('<Header />', () => {
  let navigateClicked = false; // eslint-disable-line no-unused-vars
  const setNavigateClicked = () => {
    navigateClicked = true;
  };
  let showChangePasswordModalClicked = false; // eslint-disable-line no-unused-vars
  const setShowChangePasswordModalClicked = () => {
    showChangePasswordModalClicked = true;
  };

  const renderHeader = (options) => <Header {...options} />;

  it('should render', () => {
    const renderedComponent = shallow(renderHeader({
      currentRoute: '/home',
      navigate: setNavigateClicked,
      showChangePasswordModal: setShowChangePasswordModalClicked,
    }));
    expect(renderedComponent.length).toEqual(1);
  });
});
