import * as React from 'react';
import { getTenant } from '../../utils/utilities';

export const getLogo = () => {
  switch (getTenant()) {
    case 'staging-local':
    default:
      return <span>iterrex</span>;
  }
};

export const getTitle = () => {
  switch (getTenant()) {
    case 'hubbell':
    case 'staging-local':
    default:
      return 'iterrex';
  }
};
