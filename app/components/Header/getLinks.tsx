import * as React from 'react';
import * as FontAwesome from 'react-fontawesome';
import {
  Button,
} from 'reactstrap';
import UserNavDropdown from './UserNavDropdown';
import {
  adminPrefix,
} from '../../apps/routing';
import { createHtmlId } from '../../utils/createHtmlId';
const pjson = require('../../../package.json');

export const requiresRedirect = (newLocation: string): boolean => {
  const newLocationPrefix = newLocation.split('/')[newLocation.startsWith('/') ? 1 : 0];
  const currentLocationPrefix = window.location.pathname.split('/')[1];
  return newLocationPrefix !== currentLocationPrefix;
};

export const getLinks = (props) => {
  const {
    username,
    logout,
    versions,
  } = props;

  const navLinks = [];
  const userLinkItems: any = [];

  const adminLinks = [];

  if (adminLinks.length) {
    adminLinks.unshift({
      name: 'Admin Maintenance',
      type: 'header',
    });
  }

  const version = versions && JSON.parse(versions) || {};
  version.webapp = pjson.version;

  userLinkItems.push(
    ...adminLinks,
    adminLinks.length && { type: 'divider' },
    {
      key: 'log-out',
      name: 'Log Out',
      handler: logout,
    },
  );

  const userLinks = [
    <Button
      key="refresh"
      color="link"
      style={{ color: 'white' }}
      onClick={() => location.reload()}>
      <FontAwesome name="refresh"/>
    </Button>,
    <UserNavDropdown
      id={createHtmlId('HomePage/loginDropdown')}
      key="user-nav"
      name={username}
      items={userLinkItems.filter(Boolean)}
    />,
  ];

  return { navLinks, userLinks };
};

export const getHomepageButtons = (userGroups, userRole) => {
  return [
    { text: 'UI Builder', icon: 'paint-brush', path: adminPrefix('ui-dashboard') },
    { text: 'List Maintenance', icon: 'list', path: adminPrefix('list-maintenance') },
    { text: 'User Admin', icon: 'user', path: adminPrefix('user-admin') },
    { text: 'Group Admin', icon: 'users', path: adminPrefix('group-admin') },
    { text: 'Database', icon: 'database' },
  ];
};
