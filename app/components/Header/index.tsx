import * as React from 'react';
import {
  Navbar,
  NavbarBrand,
  Nav,
} from 'reactstrap';
import styles from './styles';
import { getLogo } from './getBranding';
import { getLinks } from './getLinks';
import { homePrefix } from '../../apps/routing';

export type HeaderPropTypes = {
  props?: object,
  username?: string,
  iterrexRole?: string,
  userGroups?: string[],
  userRole?: string,
  logout?: () => void,
  navigate?: (string) => void,
  currentRoute?: string,
  showChangePasswordModal?: () => void,
  versions?: string,
};

class Header extends React.PureComponent<HeaderPropTypes, {}> {
  render() {
    const {
      username,
      navigate,
    } = this.props;

    const { navLinks, userLinks } = getLinks(this.props);
    const navbarItemsStyle = { style: styles.navbarItems };

    return (
      <div style={styles.navbar} >
        <Navbar
          className="primaryNavbar"
          {...navbarItemsStyle}>
          <Nav navbar style={styles.navbarNav}>
            <NavbarBrand
              style={{ cursor: 'pointer' }}
              className="primaryNavBrand"
              onClick={() => username && navigate(homePrefix(''))}>
              {getLogo()}
            </NavbarBrand>
            {username && navLinks}
          </Nav>
          <Nav
            style={{ flexDirection: 'row' }}
            className="ml-auto"
            navbar>
            {username && userLinks.map(link =>
              <li key={link.key}>
                {link}
              </li>)}
          </Nav>
        </Navbar>
      </div>
    );
  }
}

export default Header;
