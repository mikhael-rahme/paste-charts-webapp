const styles : any = {
  navbar: {
    marginBottom: '.25em',
    hyphens: 'none',
  },
  navbarItems: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  navbarNav: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  userLinks: {
    marginRight: '2.5em',
  },
};

export default styles;
