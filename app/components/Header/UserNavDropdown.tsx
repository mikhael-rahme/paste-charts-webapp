import * as React from 'react';
import {
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Nav,
} from 'reactstrap';

export type props = {
  id?: string,
  name: string,
  items: {
    name?: string,
    type?: string,
    handler?: () => void,
  }[],
};

const UserNavDropdown = (props: props) => {
  const {
    id,
    items,
    name,
  } = props;

  return (
    <Nav>
      <UncontrolledDropdown id={id}>
        <DropdownToggle caret>
          {name}
        </DropdownToggle>
        <DropdownMenu>
          {items.map(({ name, type, handler }) => {
            switch (type) {
              case 'header':
                return (
                  <DropdownItem
                    header
                    key={name}>
                    {name}
                  </DropdownItem>
                );
              case 'divider':
                return (
                  <DropdownItem
                    divider
                    key={name || Date.now()} />
                );
              case 'link':
              default:
                return (
                  <DropdownItem
                    key={name}
                    onClick={() => {
                      handler();
                    }}>
                    {name}
                  </DropdownItem>
                );
            }
          })}
        </DropdownMenu>
      </UncontrolledDropdown>
    </Nav>
  );
};

export default UserNavDropdown;
