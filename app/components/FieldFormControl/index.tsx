/**
 *
 * FieldFormControl
 * https://github.com/react-bootstrap/react-bootstrap/issues/2210#issuecomment-246933578
 *
 */

import moment from 'moment';
import * as momentLocalizer from 'react-widgets-moment';
import * as numberLocalizer from 'react-widgets-simple-number';
import * as React from 'react';
import { ErrorText, WarningText } from './Wrappers';
import {
  Col,
  FormGroup,
  FormText,
  Input,
  Label,
  Button,
} from 'reactstrap';
import {
  convertToJS,
  filterObject,
  lowercaseSort,
} from '../../utils/utilities';
import {
  Combobox,
  DateTimePicker,
  DropdownList,
  Multiselect,
  NumberPicker,
  SelectList,
} from 'react-widgets';
import {
  createHtmlId,
} from '../../utils/createHtmlId';

export type FieldFormControlProptypes = {
  alreadySorted?: boolean,
  controlWidth?: number,
  data?: {
    valueField?: string,
    textField?: string,
  }[],
  hidden: boolean,
  input?: React.InputHTMLAttributes<HTMLInputElement> | any, // BOOO
  label?: string,
  labelClass?: string,
  labelWidth?: number | string,
  meta?: {
    error?: any,
    touched?: boolean,
    warning?: any,
  },
  multiple: boolean,
  rowComponent?: string | React.ComponentClass<any> | React.StatelessComponent<any>,
  sortData: any,
  type: string,
  textField?: string,
  valueField?: string,
  formName?: string,
  id?: string,
  dateFormat?: any,
} & any;

momentLocalizer();
numberLocalizer();

const FieldFormControl = (props: FieldFormControlProptypes) => {
  const {
    alreadySorted,
    controlWidth,
    hidden,
    input,
    label,
    labelClass,
    labelWidth,
    meta: {
      touched,
      error,
      warning,
    },
    multiple,
    rowComponent,
    sortData,
    formName,
  } = props;

  const id = `${createHtmlId(`${formName}/${input.name}`)}` || '';
  const labelId = `${createHtmlId(`${formName}/${input.name}Label`)}` || '';

  let type = props.type;
  if (type === 'routeselector' && multiple) {
    type = 'routeselector_multiple';
  }

  if (type === 'permissionselector' && multiple) {
    type = 'permissionselector_multiple';
  }

  let value = convertToJS(input.value);
  value = ['true', 'false'].includes(value) ? value === 'true' : value;

  const originalData = [...convertToJS(props.data || [])];
  const widgetsValidation = {
    onBlur: input.onBlur ? () => input.onBlur() : () => null,
    className: (touched && error ? 'form-control-error' : ''),
  };

  const lowercaseObjectSort = (a, b) => a.textField && a.textField.toLowerCase().localeCompare(b.textField && b.textField.toLowerCase());
  const sortedData = alreadySorted
    ? originalData
    : (typeof originalData[0] === 'string'
      ? originalData.sort(lowercaseSort)
      : originalData.sort(lowercaseObjectSort));

  const getInputComponent = () => {
    // const inputTypes = ['text', 'textarea', 'checkbox', 'email', 'password', 'select'];
    // const componentKey = inputTypes.includes(type) ? ItemTypes.INPUT : ItemTypes[type.toUpperCase()];

    // if (!componentKey) {
    //   return null;
    // }

    const inputProps = {
      // ...(filterObject(props, ValidComponentProps[componentKey]) || {}),
      ...props,
      onChange: input.onChange,
    };
    const data = sortData === false ? convertToJS(props.data || []) : sortedData;

    switch ((type || '').toLowerCase()) {
      case 'combobox':
        return (
          <Combobox
            {...widgetsValidation}
            {...inputProps}
            id={id}
            value={value}
            data={data}
            onSelect={input.onChange}
            itemComponent={rowComponent} />
        );
      case 'selectlist':
        return (
          <SelectList
          {...widgetsValidation}
          {...inputProps}
          id={id}
          value={value}
          data={data}
          onChange={input.onChange}
          itemComponent={rowComponent}
          multiple={multiple}
          />
        );
      case 'dropdownlist':
      case 'routeselector':
      case 'permissionselector':
        return (
          <DropdownList
            {...widgetsValidation}
            {...inputProps}
            id={id}
            value={value}
            data={data}
            onSelect={input.onChange}
            itemComponent={rowComponent}
          />
        );

      case 'multiselect':
      case 'routeselector_multiple':
      case 'permissionselector_multiple':
        return (
          <Multiselect
            {...widgetsValidation}
            {...inputProps}
            id={id}
            value={Array.isArray(value)
              ? value
              : ((value || '').includes(',')
                ? value.split(',')
                : (value || []))}
            data={data}
            onSelect={input.onChange}
            itemComponent={rowComponent}
            />
        );
      case 'datetime':
      case 'datetimepicker':
      case 'calendar':
        if (props.dateFormat) {
          inputProps['format'] = props.dateFormat;
        }
        return (
          <DateTimePicker
            {...inputProps}
            id={id}
            value={value ? new Date(value) : null}
            onSelect={input.onChange} />
        );

      case 'number':
      case 'numberpicker':
        return (
          <NumberPicker
            {...inputProps}
            id={id}
            value={value ? Number(value) : null} />
        );

      case 'readonly':
        return (
          <label style={{ paddingTop: 6 }}>{value.textField || value}</label>
        );

      case 'textarea':
      case 'text':
      case 'select':
      case 'checkbox':
      case 'email':
      case 'password':
        return (
          <Input
            {...input}
            {...inputProps}
            autoComplete="off"
            id={id}
            state={(touched && error) ? 'error' : null}
            onChange={input.onChange}
            value={value}
            checked={value}
            hidden={hidden} />
        );

      default: return null;
    }
  };

  return (
    <FormGroup row>
      {labelWidth > 0 &&
        <Label
          id={labelId}
          sm={labelWidth}
          className={labelClass}
          hidden={hidden}>
          {label}
        </Label>}
      <Col sm={controlWidth}>
        {getInputComponent()}
      </Col>
      <Col sm={{ size: controlWidth || 12, offset: 12 - controlWidth }}>
        {touched && (
          (error &&
            <FormText>
              <ErrorText>
                {error}
              </ErrorText>
            </FormText>) ||
          (warning &&
            <FormText>
              <WarningText>
                {warning}
              </WarningText>
            </FormText>)
        )}
      </Col>
    </FormGroup>
  );
};

export default FieldFormControl;
