import styled, { StyledComponentClass } from 'styled-components';

export const ErrorText: StyledComponentClass<any, any> = styled.p`
  color: #d9534f;
  margin-top: -2px;
  margin-bottom: -8px;
`;

export const WarningText: StyledComponentClass<any, any> = styled.p`
  color: #ea8b23;
  margin-top: -2px;
  margin-bottom: -8px;
`;
