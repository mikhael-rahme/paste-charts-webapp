import * as React from 'react';
import { mount } from 'enzyme';
import { Field, reduxForm } from 'redux-form/immutable';
import FieldFormControl from '../index.tsx';
const { describe, it } = require('mocha');
const expect = require('expect');

/*
export type meta = {
  touched: boolean,
  error: any,
  warning: any,
};

const meta = {
  touched: false,
  error: null,
  warning: null,
};
*/

class TestForm extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <Field
        key="test"
        name="test"
        type="text"
        label="Test"
        component={FieldFormControl}
      />);
  }
}
const reduxTestForm = reduxForm()(TestForm);

describe('<FieldFormControl />', () => {
  /*
  it('should render text types', () => {
    const renderedComponent = mount(
      <FieldFormControl type="text" input={<input />} meta={meta} />,
    );
    expect(renderedComponent.find('input').props().type).toEqual('text');
  });

  it('should render select types with options', () => {
    const options = [
      <option key="1">1</option>,
    ];
    const renderedComponent = mount(
      <FieldFormControl options={options} input={<select />} type="select" meta={meta} />,
    );
    expect(renderedComponent.find('select option').length).toEqual(1);
  });

  it('should render checkboxes', () => {
    const renderedComponent = mount(
      <FieldFormControl input={<input />} type="checkbox" meta={meta} />,
    );
    expect(renderedComponent.find('input').props().type).toEqual('checkbox');
  });

  it('should render radio buttons', () => {
    const renderedComponent = mount(
      <FieldFormControl input={<input />} type="radio" meta={meta} />,
    );
    expect(renderedComponent.find('input').props().type).toEqual('radio');
  });
  */
  it('should render as field in a redux form', () => {
    const renderedComponent = mount(<reduxTestForm />);
    expect(renderedComponent.length).toEqual(1);
  });
});
