export type ReactWidgetDataType = {
  textField: string | number,
  valueField: string | number,
  groupBy?: string,
};
