/**
 *
 * ToggleColumn
 *
 */

import * as React from 'react';
import { Col } from 'reactstrap';

const ToggleColumn = (props: ToggleColumnTypes) => {
  const {
    visible,
    width,
    children,
    style,
    noBorder,
    noVisibleBorder,
    noHiddenBorder,
  } = props;

  if (visible) {
    return (
      <Col
        className="toggle-column"
        lg={width}
        style={{
          borderLeft: (noBorder || noVisibleBorder) ? '' : '1px solid #ddd',
          ...style,
        }}>
        {children}
      </Col>
    );
  }
  return (
    <Col
      lg={width}
      style={{
        backgroundColor: '#eee',
        height: 'calc(100vh - 80px)',
        marginTop: 0,
        borderLeft: (noBorder || noHiddenBorder) ? '' : '1px solid #bbb',
        ...style,
      }}>
    </Col>
  );
};

export type ToggleColumnTypes = {
  children?: any,
  visible: boolean,
  width: number,
  style?: any,
  noVisibleBorder?: boolean,
  noHiddenBorder?: boolean,
  noBorder?: boolean,
};

export default ToggleColumn;
