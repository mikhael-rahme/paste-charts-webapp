import * as React from 'react';
import ToggleColumn from '../index.tsx';
const expect = require('expect');
const { describe, it } = require('mocha');
const { render } = require('enzyme');

describe('<ToggleColumn />', () => {
  it('should render ', () => {
    const renderedComponent = render(
      <ToggleColumn width={12} visible={false} />,
    );
    expect(renderedComponent.length).toEqual(1);
  });
});
