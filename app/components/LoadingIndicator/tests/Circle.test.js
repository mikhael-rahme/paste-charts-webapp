import * as React from 'react';
import Circle from '../Circle.tsx';
const expect = require('expect');
const { describe, it } = require('mocha');
const { mount } = require('enzyme');

describe('<LoadingIndicator Circle />', () => {
  it('should render a <div> tag', () => {
    const renderedComponent = mount(<Circle />);
    expect(renderedComponent.find('div').length).toEqual(1);
  });

  it('should have a className attribute', () => {
    const renderedComponent = mount(<Circle />);
    expect(renderedComponent.find('div').prop('className')).toExist();
  });

  it('should not adopt attributes', () => {
    const id = 'test';
    const renderedComponent = mount(<Circle id={id} />);
    expect(renderedComponent.find('div').prop('id')).toNotExist();
  });
});
