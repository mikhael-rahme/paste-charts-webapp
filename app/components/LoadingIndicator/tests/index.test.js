import * as React from 'react';
import LoadingIndicator from '../index.tsx';
const expect = require('expect');
const { describe, it } = require('mocha');
const { render } = require('enzyme');

describe('<LoadingIndicator />', () => {
  it('should render 13 divs', () => {
    const renderedComponent = render(
      <LoadingIndicator />,
    );
    expect(renderedComponent.find('div').length).toEqual(13);
  });
});
