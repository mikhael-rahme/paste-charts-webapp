import styled, { StyledComponentClass } from 'styled-components';

export const Wrapper: StyledComponentClass<any, any> = styled.div`
  margin: 2em auto;
  width: 50px;
  height: 50px;
  position: relative;
`;
export const CustomWrapper: StyledComponentClass<any, any> = styled.div`
  display: inline-block;
  position: relative;
`;

export default Wrapper;
