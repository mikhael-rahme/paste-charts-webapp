import * as React from 'react';
import LoadingWrapper from '../index.tsx';
const expect = require('expect');
const { describe, it } = require('mocha');
const { render } = require('enzyme');

describe('<LoadingWrapper />', () => {
  it('should render ', () => {
    const renderedComponent = render(
      <LoadingWrapper loading={false} />,
    );
    expect(renderedComponent.length).toEqual(1);
  });
});
