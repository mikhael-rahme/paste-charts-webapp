
import * as React from 'react';
import LoadingIndicator from '../LoadingIndicator';
import styled, { StyledComponentClass } from 'styled-components';

const MessageWrapper: StyledComponentClass<any, any> = styled.h2`
  color: #777;
  font-weight: 100;
  font-family: sans-serif;
`;

export type LoadingWrapperPropTypes = {
  loading: boolean,
  children?: any,
  message?: string,
  style?: any,
  indicatorStyle?: any,
  indicatorSmall?: boolean,
};

const LoadingWrapper = (props: LoadingWrapperPropTypes) => {
  const {
    loading,
    children,
    message,
    style,
    indicatorStyle,
    indicatorSmall,
  } = props;

  const defaultStyle = { textAlign: 'center', marginTop: message ? 100 : 50 };

  if (loading) {
    return (
      <div style={style || defaultStyle}>
        {message && <MessageWrapper>{message}</MessageWrapper>}
        <LoadingIndicator style={indicatorStyle} small={indicatorSmall} width={indicatorStyle && indicatorStyle.width}/>
      </div>
    );
  }
  return (
    <div>
      {children}
    </div>
  );
};
export default LoadingWrapper;
