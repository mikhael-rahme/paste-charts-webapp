/**
 *
 * Empty
 * A null component to pass to redux-form fields that will not be rendered.
 * Might be used in a scenario where you want redux-form to handle a piece of data, but said data will not be derived from user input.
 */

const HiddenComponent = () => {
  return null;
};

export default HiddenComponent;
