/**
 * app.js
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */

declare const System: any;

// Needed for redux-saga es6 generator support
import 'babel-polyfill';

/* eslint-disable import/no-unresolved, import/extensions */
// Load the favicon, the manifest.json file and the .htaccess file
import 'file-loader?name=[name].[ext]!./favicon.ico';
import '!file-loader?name=[name].[ext]!./manifest.json';
import 'file-loader?name=[name].[ext]!./.htaccess';
/* eslint-enable import/no-unresolved, import/extensions */

// Import all the third party stuff
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import { Provider } from 'react-redux';
import apolloClient from './utils/apollo';
import { applyRouterMiddleware, Router, browserHistory } from 'react-router';
import * as FontFaceObserver from'fontfaceobserver';
import { syncHistoryWithStore } from 'react-router-redux';
import { useScroll } from 'react-router-scroll';
import configureStore from './store';
import { AppContainer } from 'react-hot-loader';
import { shim as finallyShim } from 'promise.prototype.finally';
finallyShim();

// Import Language Provider
import LanguageProvider from './containers/LanguageProvider';

// Import CSS reset and Global Styles
import 'react-widgets/dist/css/react-widgets.css';
import 'sanitize.css/sanitize.css';
import 'react-select/dist/react-select.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import 'react-virtualized/styles.css';
import 'react-toastify/dist/ReactToastify.css';
import './global-styles';

// Import Authorization Provider
import AuthorizationProvider from './containers/AuthorizationProvider';

// Observe loading of Open Sans (to remove open sans, remove the <link> tag in
// the index.html file and this observer)
const openSansObserver: any = new FontFaceObserver('Open Sans', {});

// When Open Sans is loaded, add a font-family using Open Sans to the body
openSansObserver.load().then(() => {
  document.body.classList.add('fontLoaded');
},                           () => {
  document.body.classList.remove('fontLoaded');
});

// Import i18n messages
import { translationMessages } from './i18n';

// Create redux store with history
// this uses the singleton browserHistory provided by react-router
// Optionally, this could be changed to leverage a created history
// e.g. `const browserHistory = useRouterHistory(createBrowserHistory)();`
const initialState = {};
const store = configureStore(initialState, browserHistory);

// Sync history and store, as the react-router-redux reducer
// is under the non-default key ("routing"), selectLocationState
// must be provided for resolving how to retrieve the "route" in the state
import { selectLocationState } from './containers/App/selectors';
const history = syncHistoryWithStore(browserHistory, store, {
  selectLocationState: selectLocationState(),
});

// Set up the router, wrapping all Routes in the App component
import App from './containers/App';
import createRoutes from './routes';
const rootRoute = {
  component: App,
  childRoutes: createRoutes(store),
};

require('offline-plugin/runtime').install();

const render = (messages) => {
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <ApolloProvider client={apolloClient}>
          <AuthorizationProvider>
            {/* Put init stuff here */}
            <LanguageProvider messages={messages}>
                <Router
                  history={history}
                  routes={rootRoute}
                  render={
                    // Scroll to top when going to a new page, imitating default browser
                    // behaviour
                    applyRouterMiddleware(useScroll())
                  }
                />
            </LanguageProvider>
          </AuthorizationProvider>
        </ApolloProvider>
      </Provider>
    </AppContainer>,
    document.getElementById('app'),
  );
};

// Hot reloadable translation json files

const anyModule = module as any;

if (anyModule.hot) {
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
  anyModule.hot.accept('./i18n', () => {
    render(translationMessages);
  });
}

// Chunked polyfill for browsers without Intl support
const thisWindow = window as any;
if (!(thisWindow).Intl) {
  (new Promise((resolve) => {
    resolve(import('intl'));
  }))
    .then(() => Promise.all([
      import('intl/locale-data/jsonp/en.js'),
      import('intl/locale-data/jsonp/de.js'),
    ]))
    .then(() => render(translationMessages))
    .catch((err) => {
      throw err;
    });
} else {
  render(translationMessages);
}
