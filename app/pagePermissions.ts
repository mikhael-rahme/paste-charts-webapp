import {
  IterrexTenantRole,
  IterrexSystemRole,
} from 'toolkit';

export const pagePermissions = {
  userAdmin: [
    IterrexTenantRole.TENANT_ADMIN,
    IterrexTenantRole.TENANT_OWNER,
    IterrexSystemRole.SYSTEM_ADMIN,
  ],
  userGroupsAdmin: [
    IterrexTenantRole.TENANT_ADMIN,
    IterrexTenantRole.TENANT_OWNER,
    IterrexSystemRole.SYSTEM_ADMIN,
  ],
  listMaintenance: [
    IterrexTenantRole.TENANT_ADMIN,
    IterrexTenantRole.TENANT_OWNER,
    IterrexSystemRole.SYSTEM_ADMIN,
  ],
};
