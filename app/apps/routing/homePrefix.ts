import {
  HOME_ROUTE_PREFIX,
} from './constants';
export const homePrefix = (route: string) => `/${HOME_ROUTE_PREFIX}/${route}`;
