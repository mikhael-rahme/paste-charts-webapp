import { withAuthorization } from '../../containers/AuthorizationProvider';

export const loadAuthorizedModule = cb => (componentModule) => {
  cb(null, props => withAuthorization(componentModule.default, props));
};
