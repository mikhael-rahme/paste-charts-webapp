import * as config from '../config';

// We need to hard naviage when switching apps
export const requiresHardNavigate = (path: string): boolean => {
  const routePath = path.split('?')[0];
  const routePathParts = routePath.split('/').filter(Boolean);

  return (window.location.pathname.indexOf(routePathParts[0]) !== 1);
};
