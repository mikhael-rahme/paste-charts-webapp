import {
  SYSTEM_ROUTE_PREFIX,
} from './constants';
export const systemPrefix = (route: string) => `/${SYSTEM_ROUTE_PREFIX}/${route}`;
