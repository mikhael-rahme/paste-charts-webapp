// Navigate to a page using the proper query string insertion for cross-SPA naviagion
export const hardNavigate = (newLocation: string): void => {

  // make sure that there is a leading slash
  const href = newLocation.indexOf('/') === 0
    ? newLocation
    : `/${newLocation}`;
  window.location.href = href;
  /*
  const [routePath, queryString] = newLocation.split('?');

  const routePathParts = routePath
    .split('/')
    .filter(Boolean);

  const subRouteParts = routePathParts.slice(1);

  let newQueryString = '';
  if (subRouteParts.length) {
    // Add route query stand-in
    newQueryString = `?route=${subRouteParts.join('/')}`;
  }
  if (queryString) {
    // append the initial query string
    if (newQueryString && queryString) {
      newQueryString = `${newQueryString}&${queryString}`;
    } else {
      newQueryString = `?${queryString}`;
    }
  }

  const newPath = `/${routePathParts[0]}/${newQueryString}`;
  console.log(newPath);*/
  // window.location.href = newPath;
};
