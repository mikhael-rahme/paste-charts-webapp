import { RouteProps } from 'react-router';
export interface IExtendedRouteProps extends RouteProps {
  name?: string;
  authorize?: string[];
}
