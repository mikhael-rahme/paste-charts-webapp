import {
  ADMIN_ROUTE_PREFIX,
} from './constants';
export const adminPrefix = (route: string) => `/${ADMIN_ROUTE_PREFIX}/${route}`;
