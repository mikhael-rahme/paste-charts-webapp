module.exports = {
  login: {
    path: 'login',
    publicPath: '/login',
    // regex: /(\/$)|(\/index)/,
    htmlFileName: 'login.html',
  },
  home: {
    path: '_',
    publicPath: '/_',
    // regex: '^/_',
    htmlFileName: '_.html',
  },
  admin: {
    path: 'admin',
    publicPath: '/admin',
    // regex: '^/admin',
    htmlFileName: 'admin.html',
  },
  // this needs to be last, as it will catch all unmatched paths
  index: {
    path: 'index',
    publicPath: '/',
    // regex: '^/admin',
    htmlFileName: 'index.html',
  },
}
