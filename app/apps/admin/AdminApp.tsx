import * as React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { push } from 'react-router-redux';
import Helmet from 'react-helmet';
import styled, { StyledComponentClass } from 'styled-components';
import {
  selectCurrentUsername,
  selectRoute,
  selectAuthorization,
  selectCurrentIterrexRole,
  selectCurrentGroups,
} from './selectors';
import { logout } from './actions';
import { Header } from '../../components';
import { graphql, compose } from 'react-apollo';
import { GET_VERSIONS } from './queries';
import { getTitle } from '../../components/Header/getBranding';
import { hardNavigate } from '../routing/hardNavigate';
import { requiresHardNavigate } from '../routing/requiresHardNavigate';
import { hot } from 'react-hot-loader';
import { ToastContainer, toast } from 'react-toastify';

const AppStyleWrapper: StyledComponentClass<any, any> = styled.div`
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  flex-direction: column;
`;

export type AppPropTypes = {
  children?: JSX.Element,
  currentUsername?: string,
  currentIterrexRole?: string,
  currentGroups?: string[],
  logout?: () => any,
  navigate: (string) => any,
  currentRoute?: string,
  showChangePasswordModal?: () => any,
  versions: string,
};

export class App extends React.Component<AppPropTypes, {}> {
  render() {
    const {
      currentUsername,
      currentIterrexRole,
      currentGroups,
      children,
      logout,
      navigate,
      currentRoute,
      showChangePasswordModal,
      versions,
    } = this.props;

    const title = getTitle();

    return (
      <AppStyleWrapper>
        <Helmet
          titleTemplate={`%s - ${title}`}
          defaultTitle={title}
          meta={[
            { name: 'description', content: title },
          ]}
        />
        <Header
          username={currentUsername}
          iterrexRole={currentIterrexRole}
          userGroups={currentGroups}
          logout={logout}
          currentRoute={currentRoute}
          navigate={navigate}
          showChangePasswordModal={showChangePasswordModal}
          versions={versions}
        />

        <ToastContainer autoClose={4000} position={toast.POSITION.TOP_RIGHT} />
        <div>{React.Children.toArray(children)}</div>
      </AppStyleWrapper>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    logout: () => {
      dispatch(logout());
    },
    navigate: path => requiresHardNavigate(path)
      ? hardNavigate(path)
      : dispatch(push(path)),
  };
};

const mapStateToProps = createStructuredSelector({
  authorization: selectAuthorization(),
  currentUsername: selectCurrentUsername(),
  currentIterrexRole: selectCurrentIterrexRole(),
  currentGroups: selectCurrentGroups(),
  currentRoute: selectRoute(),
});

type Variables = {};

type Response = {
  getVersions: string;
};

type Props = {
  versions: string;
  loadingVersions: boolean;
};

// type Props = ChildProps<{}, Response, {}>;

export default hot(module)(compose(
  connect(mapStateToProps, mapDispatchToProps),

  graphql<{}, Response, Variables, Props>(GET_VERSIONS, {
    props: ({ data: { loading, getVersions } }) => ({
      versions: getVersions,
      loadingVersions: loading,
    }),
  }),
)(App));
