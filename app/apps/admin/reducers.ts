/**
 * Combine all reducers in this file and export the combined reducers.
 * If we were to do this in store.js, reducers wouldn't be hot reloadable.
 */

import { fromJS } from 'immutable';
import {
  ReducersMapObject,
  Reducer,
} from 'redux';
import { combineReducers } from 'redux-immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import authorization from '../../containers/AuthorizationProvider/reducer';
import { reducer as form } from 'redux-form/immutable';

import { reducer as sematable } from 'sematable/immutable';
// const sematable = reducer;
/*
 * routeReducer
 *
 * The reducer merges route location changes into our immutable state.
 * The change is necessitated by moving to react-router-redux@4
 *
 */

// Initial routing state
const routeInitialState = fromJS({
  locationBeforeTransitions: null,
});

/**
 * Merge route into the global application state
 */
function route(state = routeInitialState, action) {
  switch (action.type) {
    /* istanbul ignore next */
    case LOCATION_CHANGE:
      return state.merge({
        locationBeforeTransitions: action.payload,
      });
    default:
      return state;
  }
}

/**
 * Creates the main reducer with the asynchronously loaded ones
 */
export default function createReducer(asyncReducers: ReducersMapObject = {}): Reducer<any> {
  return combineReducers({
    sematable,
    route,
    ...asyncReducers,
    // apollo: apolloClient.reducer(),
    form,
    authorization,
  });
}
