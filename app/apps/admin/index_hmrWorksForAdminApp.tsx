
import '../globalImports';

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';
import { Provider } from 'react-redux';
import {
  applyRouterMiddleware,
  Router,
  RouteProps,
  browserHistory,
} from 'react-router';
import * as FontFaceObserver from'fontfaceobserver';
import { useScroll } from 'react-router-scroll';
import { AppContainer } from 'react-hot-loader';

// Import Authorization Provider
import AuthorizationProvider from '../../containers/AuthorizationProvider';

// Observe loading of Open Sans (to remove open sans, remove the <link> tag in
// the index.html file and this observer)
const openSansObserver: any = new FontFaceObserver('Open Sans', {});

// When Open Sans is loaded, add a font-family using Open Sans to the body
openSansObserver.load().then(() => {
  document.body.classList.add('fontLoaded');
},                           () => {
  document.body.classList.remove('fontLoaded');
});

// Import i18n messages
import { translationMessages } from '../../i18n';

export interface IExtendedRouteProps extends RouteProps {
  name?: string;
  authorize?: string[];
}

import configureStore from './store';
import { syncHistoryWithStore } from 'react-router-redux';
import { selectLocationState } from '../../containers/App/selectors';
import AdminApp from './AdminApp';
import createRoutes from './routes';
import apolloClient from '../../utils/apollo/clientWithWebsockets';

const initialState = {};
const store = configureStore(initialState, browserHistory);

const history = syncHistoryWithStore(browserHistory, store, {
  selectLocationState: selectLocationState(),
});

const rootRoute = {
  component: AdminApp,
  childRoutes: createRoutes(store),
};
/*
type RenderAppArgs = {
  apolloClient: ApolloClient<any>,
  rootRoute: {
    component: any,
    childRoutes: IExtendedRouteProps[];
  },
  store: any,
  history: History,
};*/

const renderApp = () => ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <ApolloProvider client={apolloClient}>
          <AuthorizationProvider >
            {/* Put init stuff here */}
            <Router
                history={history}
                routes={rootRoute}
                render={
                  // Scroll to top when going to a new page, imitating default browser
                  // behavioFur
                  applyRouterMiddleware(useScroll())
                }
              />
          </AuthorizationProvider>
        </ApolloProvider>
      </Provider>
    </AppContainer>,
    document.getElementById('app'),
  );

const anyModule = module as any;
if (anyModule.hot) {
  anyModule.hot.accept(['./apps/admin/AdminApp.tsx'], () => {
    rootRoute.component = require('./AdminApp').default;
    renderApp();
  });
}

if (process.env.NODE_ENV === 'production') {
  require('offline-plugin/runtime').install();
}

renderApp();
