import { renderApp } from '../authenticatedBasePage';
import configureStore from './store';
import { applyRouterMiddleware, Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { selectLocationState } from '../../containers/App/selectors';
import AdminApp from './AdminApp';
import createRoutes from './routes';
import apolloClient from '../../utils/apollo/clientWithWebsockets';

const initialState = {};
const store = configureStore(initialState, browserHistory);

const history = syncHistoryWithStore(browserHistory, store, {
  selectLocationState: selectLocationState(),
});

const rootRoute = {
  component: AdminApp,
  childRoutes: createRoutes(store),
};

renderApp({
  store,
  history,
  apolloClient,
  rootRoute,
});
