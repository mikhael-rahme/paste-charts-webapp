import { fromJS } from 'immutable';
import expect from 'expect';
import {
  // selectAuthorization,
  selectCurrentUser,
  selectCurrentUsername,
  selectCurrentUserRole,
/*
  selectLoading,
  selectError,
  selectRoute,
  selectLocationState,
*/
} from '../selectors.ts';

describe('App Selectors', () => {
  describe('selectCurrentUser', () => {
    const selectCurrentUserSelector = selectCurrentUser();
    it('should select the current user', () => {
      const mockedState = fromJS({
        currentUser: {
          username: '',
          userRole: '',
          userId: '',
          groups: [],
        },
      });
      expect(selectCurrentUserSelector(mockedState)).toEqual({
        currentUser: {
          username: '',
          userRole: '',
          userId: '',
          groups: [],
        },
      });
    });
  });
  describe('selectCurrentUser', () => {
    const selectCurrentUsernameSelector = selectCurrentUsername();
    it('should select the current username', () => {
      const mockedState = fromJS({
        currentUser: {
          username: '',
          userRole: '',
          userId: '',
          groups: [],
        },
      });
      expect(selectCurrentUsernameSelector(mockedState)).toEqual('');
    });
  });
  describe('selectCurrentUser', () => {
    const selectCurrentUserRoleSelector = selectCurrentUserRole();
    it('should select the current user role', () => {
      const mockedState = fromJS({
        currentUser: {
          username: '',
          userRole: '',
          userId: '',
          groups: [],
        },
      });
      expect(selectCurrentUserRoleSelector(mockedState)).toEqual('');
    });
  });
});
