import { renderApp } from '../unauthenticatedBasePage';
import configureStore from './store';
const initialState = {};
const store = configureStore(initialState);
// apolloClient with no websockets
import apolloClient from '../../utils/apollo/index';

// Set up the router, wrapping all Routes in the App component
import LoginApp from './LoginApp';

renderApp({
  store,
  // history,
  apolloClient,
  pageComponent: LoginApp,
});
