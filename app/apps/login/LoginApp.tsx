import * as React from 'react';
import Helmet from 'react-helmet';
import styled, { StyledComponentClass } from 'styled-components';
import { Header } from '../../components';
import { getTitle } from '../../components/Header/getBranding';
import Login from '../../containers/Login';

const StyleWrapper: StyledComponentClass<any, any> = styled.div`
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  flex-direction: column;
`;

const LoginApp = () => {

  const title = getTitle();
  return (
    <StyleWrapper>
      <Helmet
        titleTemplate={`%s - ${title}`}
        defaultTitle={title}
        meta={[
          { name: 'description', content: title },
        ]}
      />
      <Header />
      <Login />
    </StyleWrapper>
  );
};

export default LoginApp;
