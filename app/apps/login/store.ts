/**
 * Create the store with asynchronously loaded reducers
 */

import {
  createStore,
  applyMiddleware,
  compose,
  Store,
  ReducersMapObject,
  Middleware,
} from 'redux';
import { fromJS } from 'immutable';
import createSagaMiddleware, { Task, SagaIterator } from 'redux-saga';
import createReducer from './reducers';
// import apolloClient from './utils/apollo';

declare const System: any;

const sagaMiddleware = createSagaMiddleware();

export interface IStore<T> extends Store<T> {
  runSaga?: (saga: (...args: any[]) => SagaIterator, ...args: any[]) => Task; // TODO: cleanup
  asyncReducers?: ReducersMapObject;
}

declare interface IWindow extends Window {
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: typeof compose;
}

declare const window: IWindow;

export default function configureStore<T>(initialState = {}): IStore<T> {
  // Create the store with three middlewares
  // 1. sagaMiddleware: Makes redux-sagas work
  // 2. routerMiddleware: Syncs the location/URL path to the state
  // 3. apolloClient: GraphQL state!
  const middlewares: Middleware[] = [
    sagaMiddleware,
    // apolloClient.middleware(),
  ];

  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const store: IStore<T> = createStore<T>(
    createReducer(),
    fromJS(initialState),
    composeEnhancers(
      applyMiddleware(...middlewares),
    ),
  );

  // Extensions
  store.runSaga = sagaMiddleware.run;
  store.asyncReducers = {}; // Async reducer registry

  // Make reducers hot reloadable, see http://mxs.is/googmo
  /* istanbul ignore next */
  if ((module as any).hot) {
    (module as any).hot.accept('./reducers', () => {
      import('./reducers').then((reducerModule) => {
        const createReducers = reducerModule.default;
        const nextReducers = createReducers(store.asyncReducers);

        store.replaceReducer(nextReducers);
      });
    });
  }

  return store;
}
