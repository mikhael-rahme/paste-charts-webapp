// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from '../../utils/asyncInjectors';
import { Route, RouteProps } from 'react-router';
import { withAuthorization } from '../../containers/AuthorizationProvider';

declare const System: any;

export interface IExtendedRouteProps extends RouteProps {
  name?: string;
  authorize?: string[];
}

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = cb => (componentModule) => {
  cb(null, componentModule.default);
};

const loadAuthorizedModule = cb => (componentModule) => {
  cb(null, props => withAuthorization(componentModule. default, props));
};

export default function createRoutes(store): IExtendedRouteProps[] {
  // create reusable async injectors using getAsyncInjectors factory
  const { injectReducer } = getAsyncInjectors(store);
  return [
    {
      path: '/',
      name: 'login',
      getComponent(location, cb) {

        const importModules = Promise.all([
          import('containers/Login/reducer'),
          import('containers/Login'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, component]) => {
          injectReducer('login', reducer.default);

          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        import('containers/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}
