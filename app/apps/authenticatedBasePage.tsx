/**
 * app.js
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */

declare const System: any;
import './globalImports';

/* eslint-disable import/no-unresolved, import/extensions */
// Load the favicon, the manifest.json file and the .htaccess file
// import 'file-loader?name=[name].[ext]!../../.htaccess';
/* eslint-enable import/no-unresolved, import/extensions */

// Import all the third party stuff
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { ApolloClient } from 'apollo-client';
import { ApolloProvider } from 'react-apollo';
import { Provider } from 'react-redux';
import {
  applyRouterMiddleware,
  Router,
  RouteProps,
} from 'react-router';
import * as FontFaceObserver from'fontfaceobserver';
import { History } from 'history';
import { useScroll } from 'react-router-scroll';
import { AppContainer } from 'react-hot-loader';

// Import Authorization Provider
import AuthorizationProvider from '../containers/AuthorizationProvider';

// Observe loading of Open Sans (to remove open sans, remove the <link> tag in
// the index.html file and this observer)
const openSansObserver: any = new FontFaceObserver('Open Sans', {});

// When Open Sans is loaded, add a font-family using Open Sans to the body
openSansObserver.load().then(() => {
  document.body.classList.add('fontLoaded');
},                           () => {
  document.body.classList.remove('fontLoaded');
});

// Import i18n messages
import { translationMessages } from '../i18n';

export interface IExtendedRouteProps extends RouteProps {
  name?: string;
  authorize?: string[];
}

export type RenderAppArgs = {
  apolloClient: ApolloClient<any>,
  rootRoute: {
    component: any,
    childRoutes: IExtendedRouteProps[];
  },
  store: any,
  history: History,
};

export const renderApp = ({
  rootRoute,
  store,
  history,
  apolloClient,
}: RenderAppArgs) => {
  const render = (messages) => {
    ReactDOM.render(
      <AppContainer>
        <Provider store={store}>
          <ApolloProvider client={apolloClient}>
            <AuthorizationProvider >
              {/* Put init stuff here */}
              <Router
                  history={history}
                  routes={rootRoute}
                  render={
                    // Scroll to top when going to a new page, imitating default browser
                    // behavioFur
                    applyRouterMiddleware(useScroll())
                  }
                />
            </AuthorizationProvider>
          </ApolloProvider>
        </Provider>
      </AppContainer>,
      document.getElementById('app'),
    );
  };

  // Hot reloadable translation json files
/*
  const anyModule = module as any;
  if (anyModule.hot) {
    // modules.hot.accept does not accept dynamic dependencies,
    // have to be constants at compile-time
    anyModule.hot.accept('./i18n', () => {
      render(translationMessages);
    });
  }
*/
  // Chunked polyfill for browsers without Intl support
  const thisWindow = window as any;
  if (!(thisWindow).Intl) {
    (new Promise((resolve) => {
      resolve(import('intl'));
    }))
      .then(() => Promise.all([
        import('intl/locale-data/jsonp/en.js'),
        import('intl/locale-data/jsonp/de.js'),
      ]))
      .then(() => render(translationMessages))
      .catch((err) => {
        throw err;
      });
  } else {
    render(translationMessages);
  }
};

// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installe
if (process.env.NODE_ENV === 'production') {
  require('offline-plugin/runtime').install();
}
