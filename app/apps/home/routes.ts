// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from '../../utils/asyncInjectors';
import { withAuthorization } from '../../containers/AuthorizationProvider';
import {
  IExtendedRouteProps,
  loadModule,
  errorLoadingRoute,
} from '../routing';

declare const System: any;

export default function createRoutes(store): IExtendedRouteProps[] {
  // create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store);

  const loadAuthorizedModule = cb => (componentModule) => {
    cb(null, props => withAuthorization(componentModule.default, props));
  };

  return [
    {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        console.log(nextState);
        import('containers/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoadingRoute);
      },
    },
  ];
}
