import expect from 'expect';
import {
  LOGOUT,
} from '../constants';
import {
  logout,
} from '../actions.ts';
const { describe, it } = require('mocha');

describe('App Actions', () => {
  describe('logout', () => {
    it('should return the correct type', () => {
      const expectedResult = {
        type: LOGOUT,
      };
      expect(logout()).toEqual(expectedResult);
    });
  });
});
