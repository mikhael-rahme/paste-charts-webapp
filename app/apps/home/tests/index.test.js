import * as React from 'react';
import expect, { createSpy } from 'expect';
import { shallow } from 'enzyme';
import Header from 'app/components/Header/index.tsx';
import { App } from '../index.tsx';
const { describe, it } = require('mocha');

describe('<App />', () => {
  let logout;
  let navigate;

  beforeEach(() => {
    logout = createSpy();
    navigate = createSpy();
  });

  const renderComponent = (showChangePassword) => {
    const props = {
      logout,
      navigate,
      currentUsername: 'test',
      currentUserRole: 'test',
      showChangePasswordModal: showChangePassword,
    };

    return shallow(<App {...props} />);
  };

  describe('App Container', () => {
    it('should render the header', () => {
      const renderedComponent = renderComponent(true);
      expect(renderedComponent.find(Header).length).toEqual(1);
    });

    it('should render a ConfirmModal container', () => {
      const renderedComponent = renderComponent(true);
      expect(renderedComponent.find('Connect(ConfirmModal)').length).toEqual(1);
    });

    it('should render a ChangePasswordModal container', () => {
      const renderedComponent = renderComponent(true);
      // console.log( util.inspect(renderedComponent));

      expect(renderedComponent.find('Connect(Apollo(ChangePasswordModal))').length).toEqual(1);
    });

    it('should pass the correct information to the header', () => {
      const renderedComponent = renderComponent(true);

      expect(renderedComponent.find('Header').first().prop('username')).toEqual('test');
      expect(renderedComponent.find('Header').first().prop('userRole')).toEqual('test');
      expect(renderedComponent.find('Header').first().prop('showChangePasswordModal')).toEqual(true);
    });

    it('should only show the ChangePasswordModal container if necessary', () => {
      const renderedComponent = renderComponent(false);

      expect(renderedComponent.find('Header').first().prop('showChangePasswordModal')).toEqual(false);
    });
  });
});
