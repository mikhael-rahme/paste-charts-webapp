import gql from 'graphql-tag';
import { Query } from 'react-apollo';

export const GET_VERSIONS = gql`
  query getVersions {
    getVersions
  }
`;

export interface GetVersionsData {
  versions: string;
}

export class GetVersionsQuery extends Query<GetVersionsData> {}
