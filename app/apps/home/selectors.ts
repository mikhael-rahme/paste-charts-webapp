/*
 * Global state selectors, used to retrieve
 * import aspects of the application's state
 * from the Redux store (currently selected users,
 * authorization conditions, the currently accessed route,
 * etc.)
 */

const { createSelector } = require('reselect');

const selectAuthorization = () => state => state.get('authorization');
const selectRoute = () => (state, props) => props.router.location.pathname;

const selectCurrentUser = () => createSelector(
  selectAuthorization(),
  authorizationState => authorizationState.get('currentUser'),
);

const selectCurrentUsername = () => createSelector(
  selectCurrentUser(),
  currentUser => currentUser.get('username'),
);

const selectCurrentIterrexRole = () => createSelector(
  selectCurrentUser(),
  currentUser => currentUser.get('iterrexRole'),
);

const selectCurrentGroups = () => createSelector(
  selectCurrentUser(),
  currentUser => currentUser.get('groups'),
);

const selectLoading = () => createSelector(
  selectAuthorization(),
  authorizationState => authorizationState.get('loading'),
);

const selectError = () => createSelector(
  selectAuthorization(),
  authorizationState => authorizationState.get('error'),
);

const selectLocationState = () => {
  let prevRoutingState;
  let prevRoutingStateJS;

  return (state) => {
    const routingState = state.get('route'); // or state.route

    if (!routingState.equals(prevRoutingState)) {
      prevRoutingState = routingState;
      prevRoutingStateJS = routingState.toJS();
    }

    return prevRoutingStateJS;
  };
};

export {
  selectAuthorization,
  selectCurrentUser,
  selectCurrentUsername,
  selectCurrentIterrexRole,
  selectCurrentGroups,
  selectLoading,
  selectError,
  selectRoute,
  selectLocationState,
};
