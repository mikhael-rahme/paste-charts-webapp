/**
 * Create the store with asynchronously loaded reducers
 */

import {
  createStore,
  applyMiddleware,
  compose,
  Store,
  ReducersMapObject,
  Middleware,
} from 'redux';
import { fromJS } from 'immutable';
import { routerMiddleware } from 'react-router-redux';
import createSagaMiddleware, { Task, SagaIterator, Effect } from 'redux-saga';
import createReducer from './reducers';
import rootSaga from './sagas';
// import apolloClient from './utils/apollo';

declare const System: any;

const sagaMiddleware = createSagaMiddleware();

export interface IStore<T> extends Store<T> {
  runSaga?: (saga: (...args: any[]) => SagaIterator, ...args: any[]) => Task; // TODO: cleanup
  asyncReducers?: ReducersMapObject;
}

declare interface IWindow extends Window {
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: typeof compose;
}

declare const window: IWindow;

export default function configureStore<T>(initialState = {}, history): IStore<T> {
  // Create the store with three middlewares
  // 1. sagaMiddleware: Makes redux-sagas work
  // 2. routerMiddleware: Syncs the location/URL path to the state
  // 3. apolloClient: GraphQL state!
  const middlewares: Middleware[] = [
    sagaMiddleware,
    routerMiddleware(history),
    // apolloClient.middleware(),
  ];

  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const store: IStore<T> = createStore<T>(
    createReducer(),
    fromJS(initialState),
    composeEnhancers(
      applyMiddleware(...middlewares),
    ),
  );

  // Extensions
  store.runSaga = sagaMiddleware.run;
  store.asyncReducers = {}; // Async reducer registry
  store.runSaga(rootSaga as any); // sorry

  // Make reducers hot reloadable, see http://mxs.is/googmo
  /* istanbul ignore next */
  const anyModule = module as any;
  if (anyModule.hot) {
    anyModule.hot.accept('./reducers', () => {
      import('./reducers').then((reducerModule) => {
        const createReducers = reducerModule.default;
        const nextReducers = createReducers(store.asyncReducers);

        store.replaceReducer(nextReducers);
      });
    });
  }

  return store;
}
