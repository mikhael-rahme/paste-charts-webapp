/*
 * App Actions
 *
 */

import {
  LOGOUT,
} from '../../containers/AuthorizationProvider/constants';

export function logout() {
  return {
    type: LOGOUT,
  };
}
