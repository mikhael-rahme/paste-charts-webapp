
// Needed for redux-saga es6 generator support
import 'babel-polyfill';
import 'file-loader?name=[name].[ext]!../favicon.ico';
// import '!file-loader?name=[name].[ext]!../manifest.json';

// Import CSS reset and Global Styles
import 'react-widgets/dist/css/react-widgets.css';
import 'sanitize.css/sanitize.css';
import 'react-select/dist/react-select.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import 'react-virtualized/styles.css';
import 'react-toastify/dist/ReactToastify.css';
import '../global-styles';
import * as FontFaceObserver from'fontfaceobserver';

// Observe loading of Open Sans (to remove open sans, remove the <link> tag in
// the index.html file and this observer)
const openSansObserver: any = new FontFaceObserver('Open Sans', {});
// When Open Sans is loaded, add a font-family using Open Sans to the body
openSansObserver.load().then(() => {
  document.body.classList.add('fontLoaded');
},                           () => {
  document.body.classList.remove('fontLoaded');
});
