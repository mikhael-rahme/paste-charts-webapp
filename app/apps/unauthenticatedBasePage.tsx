/**
 * app.js
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */

declare const System: any;
import './globalImports';

// Import all the third party stuff
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { ApolloClient } from 'apollo-client';
import { ApolloProvider } from 'react-apollo';
import { Provider } from 'react-redux';
import {
  RouteProps,
} from 'react-router';
import { AppContainer } from 'react-hot-loader';

// Import Authorization Provider
import AuthorizationProvider from '../containers/AuthorizationProvider';

// Import i18n messages
import { translationMessages } from '../i18n';

export interface IExtendedRouteProps extends RouteProps {
  name?: string;
  authorize?: string[];
}

export type RenderAppArgs = {
  apolloClient: ApolloClient<any>,
  pageComponent: () => JSX.Element,
  store: any,
};

export const renderApp = ({
  store,
  apolloClient,
  pageComponent,
}: RenderAppArgs) => {
  const render = (messages) => {
    ReactDOM.render(
      <AppContainer>
        <Provider store={store}>
          <ApolloProvider client={apolloClient}>
            <AuthorizationProvider >
              {/* Put init stuff here */}
              { pageComponent() }
            </AuthorizationProvider>
          </ApolloProvider>
        </Provider>
      </AppContainer>,
      document.getElementById('app'),
    );
  };

  // Hot reloadable translation json files
/*
  const anyModule = module as any;
  if (anyModule.hot) {
    // modules.hot.accept does not accept dynamic dependencies,
    // have to be constants at compile-time
    anyModule.hot.accept('./i18n', () => {
      render(translationMessages);
    });
  }
*/
  // Chunked polyfill for browsers without Intl support
  const thisWindow = window as any;
  /*
  if (!(thisWindow).Intl) {
    (new Promise((resolve) => {
      resolve(import('intl'));
    }))
      .then(() => Promise.all([
        import('intl/locale-data/jsonp/en.js'),
        import('intl/locale-data/jsonp/de.js'),
      ]))
      .then(() => render(translationMessages))
      .catch((err) => {
        throw err;
      });
  } else {
    render(translationMessages);
  }
  */
  render(translationMessages);
};

// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installe
if (process.env.NODE_ENV === 'production') {
  require('offline-plugin/runtime').install();
}
