import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
    overflow: hidden;
  }

  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    -webkit-hyphens: auto;
    -moz-hyphens: auto;
    -ms-hyphens: auto;
    hyphens: auto;
    background-color: #fafafa;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    background-color: #fafafa;
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    line-height: 1.5em;
  }

  label.notes-label,
  label.text-left,
  label.text-right {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-size: 0.9em;
    line-height: 2em;
    font-weight: bold;
  }

  input[type=radio],
  input[type=checkbox] {
    -ms-transform: scale(1.1);
    -moz-transform: scale(1.1);
    -webkit-transform: scale(1.1);
    -o-transform: scale(1.1);
    transform: scale(1.1);
    padding: 10px;
    cursor: pointer;
    margin-top: 10px;
    width: auto;
  }

  .form-control:focus,
  .form-control:active {
    box-shadow: 0 0 2pt 2pt #5cb3fd30;
  }

  input[type=radio]:focus,
  input[type=radio]:active,
  input[type=checkbox]:focus,
  input[type=checkbox]:active {
    box-shadow: 0 0 2pt 1pt #5cb3fd8a;
  }

  .form-group label {
    pointer-events: all !important;
  }

  .primaryNavbar {
    background-color: #232323;
    color: #cccccc;
  }

  .primaryNavlink {
    cursor: pointer;
    color: #cccccc !important;
    transition: .5s;
  }

  .primaryNavlink:hover {
    color: white !important;
  }

  .primaryDropdownNavlink {
    cursor: pointer;
    transition: .5s;
    color: black !important;
  }

  .primaryDropdownNavlink:hover {
    color: #606060 !important;
  }

  .primaryNavBrand {
    color: #cccccc !important;
    transition: .5s;
  }

  .primaryNavBrand:hover {
    color: white !important;
  }

  .btn,
  [type="checkbox"],
  [type="radio"],
  .nav-item,
  .dropdown-item,
   .list-table-view-modes li,
   .list-table-view-modes .nav-link {
    cursor: pointer;
  }

  .primaryNavbar .nav-item {
    display: flex;
    align-items: center;
    justify-content: center;
    padding-left: 10px;
  }

  .primaryNavbar .nav-item .nav-link {
    padding-left: 10px !important;
    padding-right: 10px !important;
    border-radius: 4px;
  }

  .primaryNavbar .nav-item.active .nav-link {
    color: white !important;
    background: rgba(255, 255, 255, 0.1);
  }

  .primaryNavBrand img {
    height: 30px;
    border-radius: 4px;
    cursor: pointer;
  }

  .list-table-view-modes {
    flex: 1;
    text-align: center;
    justify-content: space-between;
    margin-top: 10px;
    margin-bottom: 10px;
  }

  .list-table-view-modes li {
    flex: 1;
    margin-left: 5px;
    margin-right: 5px;
    font-size: 0.8em;
    user-select: none;
  }

  .list-table-view-modes a.nav-link.active:not([href]):not([tabindex]):focus,
  .list-table-view-modes a.nav-link.active:not([href]):not([tabindex]):hover {
    color: #fff;
    text-decoration: none;
    padding-left: 10px;
    padding-right: 10px;
    border-radius: 5px;
  }
  .table-sm th, .table-sm td {
    vertical-align: middle !important;
  }

  .pagination.pagination-sm {
    overflow: auto;
  }

  .order-line-item {
    box-sizing: border-box;
    padding: 10px;
  }

  .toggle-column {
    box-sizing: border-box;
    padding: 10px;
    overflow-y: auto;
    overflow-x: hidden;
    height: calc(100vh - 70px);
  }

  .form-check-input {
    margin-left: 0 !important;
  }

  .bottom-buttons {
    height: calc(100vh - 225px);
    overflow-x: hidden;
    overflow-y: auto;
  }
  .bottom-buttons.top-tabs {
    height: calc(100vh - 300px);
  }
  .bottom-buttons.receive {
    height: calc(100vh - 270px);

  .bottom-buttons.line-item {
    height: calc(100vh - 230px);
  }}

  .save-buttons .col {
    text-align: right;
  }
  .save-buttons .col div {
    display: inline-block;
    margin-right: 10px;
  }
  .save-buttons .col div label {
    display: inline-block;
    margin-right: 20px;
    text-align: right;
  }

  .save-buttons .btn {
    min-width: 200px;
  }
  .save-buttons .btn-secondary {
    min-width: 150px;
  }

  .nopadding {
    padding: 0 !important;
  }
  .lowpadding {
    padding: 4px !important;
  }

  .text-right.col-form-label {
    padding-right: 0 !important;
  }

  .required-help {
    float: right;
    margin-top: -45px;
    color: #888;
    font-size: 0.8em;
  }

  .form-control-error,
  .form-control-error .rw-widget-picker {
    border-color: #dc3545 !important;
  }

  .form-control-error:focus,
  .form-control-error.rw-state-focus .rw-widget-picker {
    border-color: #dc3545 !important;
    box-shadow: 0 0 0 0.2rem rgba(220,53,69,.25);
  }

  .table-order-report tbody tr th,
  .table-order-report tbody tr td {
    font-size: 0.9em;
    padding: 10px;
  }

  .table-order-report tbody tr th {
    width: 50%;
  }

  #receive-order-records-table .table {
    font-size: 0.9em;
  }

  .modal-footer label.form-check-label input {
    margin-right: 10px !important;
  }

  .modal-footer label.form-check-label {
    padding-right: 5px;
  }

  .table tbody tr td:last-child .btn-group {
    float: right;
  }

  .modal-title .fa {
    padding-right: 15px;
  }

  .audit-info-table {
    margin-top: -0.5rem;
  }

  .audit-info-table th,
  .audit-info-table td {
    border-top: none !important;
  }

  .audit-info-table tbody tr th {
    text-align: right;
    width: 25%;
    font-size: 0.9em;
  }

  .receive-save .col {
    padding-left: 5px;
    padding-right: 5px;
  }

  .fa.fa-async {
    margin-right: 5px;
  }

  .btn-disabled {
    cursor: not-allowed;
    opacity: .65;
  }

  .primaryNavbar .dropdown-toggle {
    min-width: 180px;
  }

  .edit-component-panel label {
    font-size: 0.9em;
  }

  .btn-grey {
    color: #fff !important;
    background-color: #696969 !important;;
    border-color: #585858 !important;;
  }

  .btn-grey:hover,
  .btn-grey:active,
  .btn-grey:focus {
    color: #fff !important;
    background-color: #585858 !important;;
    border-color: #585858 !important;;
  }

  .modal-title {
    width: 100%;
  }

  .pageModal {
    max-width: 99vw;
  }

  .progress-bar-striped {
    background-repeat: repeat;
  }

  .design-mode input,
  .design-mode select,
  .design-mode checkbox,
  .design-mode .rw-widget-input,
  .design-mode textarea {
    cursor: pointer !important;
  }

  .parent-checkbox {
    border-top: 1px solid #ddd;
  }

  .child-checkbox .form-group {
    margin-top: -10px;
  }

  .disabled-label {
    color: #777;
  }

  .schedule-row {
    padding-right: 6px;
    padding-left: 6px;
  }

  .schedule-row .schedule-day {
    padding-left: 6px !important;
    padding-right: 6px !important;
    background: #fff;
  }
  .schedule-row .schedule-day:nth-child(odd) {
    background: #f3f3f3;
  }

  @keyframes rotation {
    0% {
      transform: rotate(-.7deg);
    } 100% {
      transform: rotate(.7deg);
    }
  }

  @keyframes spin {
    from {transform:rotate(0deg);}
    to {transform:rotate(360deg);}
  }

  .flat-input .rw-widget-input {
    border-radius: 0 !important;
  }

  .flat-right,
  .flat-right .rw-widget-input {
    border-radius: 4px 0 0 4px !important;
    border-right: none !important;
  }

  .flat-left,
  .flat-left .rw-widget-input {
    border-radius: 0 4px 4px 0 !important;
    border-left: none !important;
  }

  .navbar-nav:first-child .dropdown-toggle {
    background-color: transparent;
    color: #cccccc;
    border: none;
    min-width: auto;
    margin-top: 3px;
  }

  .table thead th {
    white-space: nowrap
  }

  .queryBuilder .form-group {
    margin-top: 2px;
    margin-bottom: 2px;
  }

  .flat-left .rw-widget {
    width: 100%;
  }

  input.rw-select-list-input {
    top: 0;
  }

  @media only screen
  and (max-device-height : 768px)
  and (max-device-width : 1024px) {
    .primaryNavbar {
      display: none !important;
    }
  }
  @media only screen
  and (max-device-height : 1024px)
  and (max-device-width : 768px) {
    .primaryNavbar {
      display: none !important;
    }
  }
  .rw-list-option {
    min-height: 30px;
  }

  .schedule-galv-location .rw-multiselect-tag>* {
    font-size: 0.8em;
  }

  .reports-status .rw-multiselect-tag>* {
    font-size: 0.7em;
  }

  .ReactVirtualized__Table__row:nth-child(odd) {
    background: #eee;
  }
  .ReactVirtualized__Table__row:hover {
    background: #ddd;
  }
  .ReactVirtualized__Table__headerRow {
    text-transform: none;
  }

  .printPage {
    width: 7.5in;
    height: 10in;
    overflow: hidden;
    margin: 0.5in;
  }

  @media print {
    .printPage {
      width: 7.5in;
      height: 10in;
      overflow: hidden;
      margin: 0.5in;
      page-break-before: always;
      page-break-after: always;
      break-before: always;
      break-after: always;
    }
  }

  #user-table {
    font-size: 0.85em;
  }

  #receive-records-table .ReactVirtualized__Table__Grid,
  #shipping-table .ReactVirtualized__Table__Grid
   {
    height: calc(100vh - 200px) !important;
    overflow-y: auto !important;
  }

  #reports-table .ReactVirtualized__Table__Grid
   {
    height: calc(100vh - 180px) !important;
    overflow-y: auto !important;
  }
  .Toastify__toast {
    font-weight: bold;
    border-radius: 5px;
    overflow: hidden;
    padding-left: 12px;
  }

  .Toastify__toast--success {
    background: #28a745;
  }

  .Toastify__toast--error {
    background: #dc3545;
  }

  .Toastify__toast--warning {
    background: #ffc107;
  }

  .Toastify__toast--info {
    background: #007bff;
  }

  #receive-records-table .ReactVirtualized__Grid__innerScrollContainer {
    font-size: 0.9em;
  }
`;

export {};
