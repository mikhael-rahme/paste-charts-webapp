/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 */

import * as React from 'react';
import messages from './messages';
import { FormattedMessage } from 'react-intl';
import { hot } from 'react-hot-loader';

export const NotFound = () => {
  return (
    <article>
      <FormattedMessage {...messages.header} />
    </article>
  );
};

export default hot(module)(NotFound);
