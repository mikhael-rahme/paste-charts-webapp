import * as React from 'react';
import {
  Button,
  FormText,
  Row,
  Col,
} from 'reactstrap';
import FieldFormControl from '../../components/FieldFormControl/index';
import { Field, reduxForm } from 'redux-form/immutable';
import validate from './resetFormValidate';

export class ResetForm extends React.Component<ResetFormPropTypes, {}> { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      handleSubmit,
      submitting,
      pristine,
      error,
    } = this.props;

    const labelwidth3 = {
      labelWidth: 3,
      labelClass: 'text-right',
      controlClass: 'form-control',
      controlWidth: 9,
    };

    return (
      <form onSubmit={handleSubmit} >
        <Field
          name="username"
          type="text"
          label="Username"
          disabled={submitting}
          component={FieldFormControl}
          {...labelwidth3}
        />
        <Field
          type="password"
          name="newPassword"
          label="New Password"
          disabled={submitting}
          component={FieldFormControl}
          {...labelwidth3}
        />
        <Field
          type="password"
          name="confirmPassword"
          label="Confirm Password"
          disabled={submitting}
          component={FieldFormControl}
          {...labelwidth3}
        />

        <Row>
          <Col>
            <Button
              id="btnLogin"
              block
              color="info"
              disabled={pristine || submitting || !!error}
              type="submit"
            >Login</Button>
            {error &&
              <FormText className="text-left text-danger" >
                {error}
              </FormText>}
          </Col>
        </Row>
      </form>
    );
  }
}

export type ResetFormPropTypes = {

} & any;

const exportedForm: any = reduxForm({
  validate,
  form: 'resetForm',
})(ResetForm);

export default exportedForm;
