/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  UPDATE_PASSWORD,
  UPDATE_USERNAME,
  LOGIN,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
} from './constants';
import { LoginResponse } from './mutation';

/**
 * Update the username input - REMOVE when redux forms is implemented
 *
 * @param  {string} username The current username
 *
 * @return {username} An action object with a type of UPDATE_USERNAME passing the username
 */
export function updateUsername(username) {
  return {
    username,
    type: UPDATE_USERNAME,
  };
}

/**
 * Update the password input - REMOVE when redux forms is implemented
 *
 * @param  {string} password The current username
 *
 * @return {password} An action object with a type of UPDATE_PASSWORD passing the password
 */
export function updatePassword(password) {
  return {
    password,
    type: UPDATE_PASSWORD,
  };
}

/**
 * Login succeeded
 *
 * @param  {array} repos The repository data
 * @param  {string} username The current username
 *
 * @return {object}      An action object with a type of LOGIN_SUCCESS passing the tokens
 */
export function loginSuccess(token: LoginResponse) {
  return {
    token,
    type: LOGIN_SUCCESS,
  };
}

/**
 * Login failed - passes error object from server
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of UPDATE_PASSWORD passing the password
 */
export function loginFail(error) {
  return {
    error,
    type: LOGIN_FAIL,
  };
}

/**
 * Login
 *
 * @return {object}       An action object with a type of LOGIN
 */
export function login() {
  return {
    type: LOGIN,
  };
}
