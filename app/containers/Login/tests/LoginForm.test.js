import * as React from 'react';
import expect, { createSpy } from 'expect';
import { shallow, mount } from 'enzyme';
import { reducer as formReducer } from 'redux-form/immutable';
import { combineReducers } from 'redux-immutable';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import LoginFormContainer, { LoginForm } from '../LoginForm.tsx';

describe('Login <LoginForm />', () => {
  let handleSubmit;
  let error;
  let touched;
  let reset;
  let onSave;
  let onSaveResponse;

  let store;

  let propsWithProvider;
  let mountedContainer;

  beforeEach(() => {
    handleSubmit = createSpy();
    error = null;
    touched = false;
    onSave = createSpy();
    onSaveResponse = Promise.resolve();
    reset = createSpy().andReturn(onSaveResponse);

    propsWithProvider = {
      error,
      touched,
      onSave,
      reset,
      handleSubmit,
      submitting: false,
    };

    store = createStore(combineReducers({ form: formReducer }));

    mountedContainer = mount(
      <Provider store={store} >
        <LoginFormContainer{...propsWithProvider} />
      </Provider>,
    );
  });

  const renderComponent = () => {
    const props = {
      handleSubmit,
      submitting: true,
    };

    return shallow(
      <LoginForm {...props} />,
    );
  };

  it('renders correctly', () => {
    const renderedComponent = renderComponent();

    expect(renderedComponent.find('form').length).toEqual(1);
    // expect(renderedComponent.find('FormText').length).toEqual(1);
    // expect(renderedComponent.find('Field').length).toEqual(1);
    // expect(renderedComponent.find('Button').length).toEqual(1);
  });

  it('handles all form submissions correctly', () => {
    const renderedComponent = renderComponent();

    renderedComponent.find('form').first().simulate('submit');
    expect(handleSubmit.calls.length).toEqual(1);
  });

  it('shows help text when the username field is blank', () => {
    mountedContainer.find('input').first().simulate('blur');
    expect(mountedContainer.find('FormText').first().text()).toEqual('Required');
  });

  it('shows help text when the password field is blank', () => {
    mountedContainer.find('input').at(1).simulate('blur');
    expect(mountedContainer.find('FormText').first().text()).toEqual('Required');
  });

  it('should disable both fields when submitting', () => {
    const renderedComponent = renderComponent();

    expect(renderedComponent.find('Field').first().prop('disabled')).toEqual(true);
    expect(renderedComponent.find('Field').at(1).prop('disabled')).toEqual(true);
  });

  it('fails validation of login information when the provided username and password are incorrect', () => {
    mountedContainer.find('input').first().simulate('change', { target: { value: 'test' } });
    mountedContainer.find('input').at(1).simulate('change', { target: { value: 'test' } });
    mountedContainer.find('form').simulate('submit');

    expect(handleSubmit.calls.length).toEqual(1);
  });
});
