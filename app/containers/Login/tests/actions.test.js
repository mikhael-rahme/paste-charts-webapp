import expect from 'expect';
import {
  updateUsername,
  updatePassword,
  loginSuccess,
  loginFail,
  login,
} from '../actions.ts';
import {
  UPDATE_PASSWORD,
  UPDATE_USERNAME,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGIN,
} from '../constants';

describe('Login actions', () => {
  describe('updateUsername action', () => {
    it('should return the correct type and the username', () => {
      const expected = {
        type: UPDATE_USERNAME,
        username: 'test',
      };
      expect(updateUsername('test')).toEqual(expected);
    });
  });
  describe('updatePassword action', () => {
    it('should return the correct type and the password', () => {
      const expected = {
        type: UPDATE_PASSWORD,
        password: 'test',
      };
      expect(updatePassword('test')).toEqual(expected);
    });
  });
  describe('loginSuccess action', () => {
    it('should return the correct type and a token', () => {
      const expected = {
        type: LOGIN_SUCCESS,
        token: 'test',
      };
      expect(loginSuccess('test')).toEqual(expected);
    });
  });
  describe('loginFail action', () => {
    it('should return the correct type and the error', () => {
      const expected = {
        type: LOGIN_FAIL,
        error: 'test',
      };
      expect(loginFail('test')).toEqual(expected);
    });
  });
  describe('Login action', () => {
    it('should return the correct type', () => {
      const expected = {
        type: LOGIN,
      };
      expect(login()).toEqual(expected);
    });
  });
});
