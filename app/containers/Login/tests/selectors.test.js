import { fromJS } from 'immutable';
import expect from 'expect';
import {
  selectLogin,
  selectUsername,
  selectPassword,
  selectLoading,
  selectError,
} from '../selectors.ts';

describe('Login Selectors', () => {
  describe('selectLogin', () => {
    const loginSelector = selectLogin();
    it('should select the state of \'Login\'', () => {
      const expectedResult = 'test';
      const mockedState = fromJS({
        login: expectedResult,
      });

      expect(loginSelector(mockedState)).toEqual(expectedResult);
    });
  });

  describe('selectUsername', () => {
    const loginSelector = selectUsername();
    it('should select the username', () => {
      const expectedResult = 'test';
      const mockedState = fromJS({
        login: {
          username: expectedResult,
        },
      });

      expect(loginSelector(mockedState)).toEqual(expectedResult);
    });
  });

  describe('selectPassword', () => {
    const loginSelector = selectPassword();
    it('should select the password', () => {
      const expectedResult = 'test';
      const mockedState = fromJS({
        login: {
          password: expectedResult,
        },
      });

      expect(loginSelector(mockedState)).toEqual(expectedResult);
    });
  });

  describe('selectLoading', () => {
    const loginSelector = selectLoading();
    it('should select \'loading\'', () => {
      const expectedResult = 'test';
      const mockedState = fromJS({
        login: {
          loading: expectedResult,
        },
      });

      expect(loginSelector(mockedState)).toEqual(expectedResult);
    });
  });

  describe('selectError', () => {
    const loginSelector = selectError();
    it('should select the error', () => {
      const expectedResult = 'test';
      const mockedState = fromJS({
        login: {
          error: expectedResult,
        },
      });

      expect(loginSelector(mockedState)).toEqual(expectedResult);
    });
  });
});
