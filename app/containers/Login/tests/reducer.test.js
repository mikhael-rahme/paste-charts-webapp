import expect from 'expect';
import { fromJS } from 'immutable';
import {
  LOGOUT,
} from '../constants';
import {
  updateUsername,
  updatePassword,
  loginSuccess,
  login,
  loginFail,
} from '../actions.ts';
import loginReducer from '../reducer.ts';

const initialState = fromJS({
  userId: '',
  username: '',
  password: '',
  loading: false,
  error: null,
  userRole: '',
});

describe('Login reducer', () => {
  it('returns the initial state', () => {
    expect(loginReducer(undefined, {})).toEqual(initialState);
  });

  it('modifies \'username\' when the action is updateUsername', () => {
    const currentState = fromJS({
      userId: '',
      username: 'test',
      password: '',
      loading: false,
      error: null,
      userRole: '',
    });

    expect(loginReducer(undefined, updateUsername('test')).get('username')).toEqual(currentState.get('username'));
  });

  it('modifies \'password\' when the action is updatePassword', () => {
    const currentState = fromJS({
      userId: '',
      username: '',
      password: 'test',
      loading: false,
      error: null,
      userRole: '',
    });

    expect(loginReducer(undefined, updatePassword('test')).get('password')).toEqual(currentState.get('password'));
  });

  it('sets \'loading\' to true when the action is login', () => {
    const currentState = fromJS({
      userId: '',
      username: '',
      password: '',
      loading: true,
      error: null,
      userRole: '',
    });

    expect(loginReducer(undefined, login()).get('loading')).toEqual(currentState.get('loading'));
  });

  it('returns a modified state when the action is loginSuccess', () => {
    const currentState = fromJS({
      userId: '',
      username: '',
      password: '',
      loading: false,
      error: null,
      userRole: '',
    });

    expect(loginReducer(undefined, loginSuccess('test'))).toEqual(currentState);
  });

  it('returns a modified state and indicates an error when the action is loginFail', () => {
    const currentState = fromJS({
      userId: '',
      username: '',
      password: '',
      loading: false,
      error: 'test',
      userRole: '',
    });

    expect(loginReducer(undefined, loginFail('test')).get('error')).toEqual(currentState.get('error'));
  });

  it('modifies localStorage when the action is logout', () => {
    const token = {
      accessToken: 'something',
      refreshToken: 'something',
    };

    localStorage.setItem('token', JSON.stringify(token));

    loginReducer(undefined, { type: LOGOUT });

    expect(localStorage.token).toNotExist();
  });
});

