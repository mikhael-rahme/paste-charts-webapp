import expect, { createSpy } from 'expect';
import { shallow } from 'enzyme';
import React from 'react';
import { Login } from '../index.tsx';

describe('<Login />', () => {
  let loginSuccess;
  let serverLogin;

  beforeEach(() => {
    loginSuccess = createSpy();
    serverLogin = createSpy();
  });

  const renderComponent = () => {
    const props = {
      loginSuccess,
      serverLogin,
    };

    return shallow(
      <Login {...props} />,
    );
  };

  it('renders correctly', () => {
    const renderedComponent = renderComponent();

    expect(renderedComponent.find('HelmetWrapper').length).toEqual(1);
    // expect(renderedComponent.find('h1').length).toEqual(1);
    // expect(renderedComponent.find('div').length).toEqual(2);
    // expect(renderedComponent.find('ReduxForm').length).toEqual(1);
  });
});
