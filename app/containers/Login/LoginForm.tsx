import * as React from 'react';
import {
  Button,
  FormText,
  Row,
  Col,
} from 'reactstrap';
import FieldFormControl from '../../components/FieldFormControl/index';
import { Field, reduxForm } from 'redux-form/immutable';
import validate from './loginFormValidate';
import { createHtmlId } from '../../../app/utils/createHtmlId';

export class LoginForm extends React.Component<LoginFormPropTypes, {}> { // eslint-disable-line react/prefer-stateless-function
  render() {
    const {
      handleSubmit,
      submitting,
      pristine,
      error,
    } = this.props;

    const labelwidth3 = {
      labelWidth: 3,
      labelClass: 'text-right',
      controlClass: 'form-control',
      controlWidth: 9,
    };

    return (
      <form onSubmit={handleSubmit} >
        <Field
          name="username"
          type="text"
          label="Username"
          disabled={submitting}
          formName={'LoginForm'}
          component={FieldFormControl}
          {...labelwidth3}
        />
        <Field
          type="password"
          name="password"
          label="Password"
          disabled={submitting}
          formName={'LoginForm'}
          component={FieldFormControl}
          {...labelwidth3}
        />

        <Row>
          <Col>
            <Button
              block
              id={createHtmlId('LoginForm/login')}
              name="login"
              color="info"
              disabled={pristine || submitting || !!error}
              type="submit"
            >Login</Button>
            {error &&
              <FormText className="text-left text-danger" >
                {error}
              </FormText>}
          </Col>
        </Row>
      </form>
    );
  }
}

export type LoginFormPropTypes = {

} & any;

const exportedForm: any = reduxForm({
  validate,
  form: 'login',
})(LoginForm);

export default exportedForm;
