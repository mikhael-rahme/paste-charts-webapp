export default function validate(values, props) {
  const errors: errorsProps = {};
  if (values) {
    if (!values.get('username')) {
      errors.username = 'Required';
    }
    if (!values.get('newPassword')) {
      errors.newPassword = 'Required';
    }
    if (!values.get('confirmPassword')) {
      errors.confirmPassword = 'Required';
    }
    if (values.get('newPassword') !== values.get('confirmPassword')) {
      errors.confirmPassword = 'Password fields must match';
    }
  }
  return errors;
}

type errorsProps = {
  username?: string,
  newPassword?: string,
  confirmPassword?: string,
};
