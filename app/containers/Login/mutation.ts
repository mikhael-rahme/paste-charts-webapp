import gql from 'graphql-tag';

export type LoginResponse = {
  accessToken: string;
  idToken: string;
  refreshToken: string;
  // config data
  region: string;
  userPoolId: string;
  identityPoolId: string;
  userPoolWebClientId: string;
};

export const LOGIN_MUTATION = gql`
mutation login($username: String!, $password: String!){
  token(username: $username, password: $password){
    accessToken
    idToken
    refreshToken
    region
    userPoolId
    identityPoolId
    userPoolWebClientId
  }
}
`;
export const RESET_PASSWORD_MUTATION = gql`
  mutation resetPasswordLogin($username: String!, $password: String!){
    token(username: $username, password: $password){
      accessToken
      idToken
      refreshToken
      region
      userPoolId
      identityPoolId
      userPoolWebClientId
    }
  }
`;
