/*
 *
 * Login
 *
 */

import * as React from 'react';
import { connect } from 'react-redux';
import { SubmissionError, formValueSelector } from 'redux-form/immutable';
import Helmet from 'react-helmet';
import {
  LOGIN_MUTATION,
  RESET_PASSWORD_MUTATION,
  LoginResponse,
} from './mutation';
import { graphql, compose } from 'react-apollo';
import { push } from 'react-router-redux';
import { selectError, selectCurrentlyLoggedIn } from './selectors';
import { loginSuccess as loginSuccessAction } from './actions';
import { createStructuredSelector } from 'reselect';
import LoginForm from './LoginForm';
import ResetForm from './ResetForm';
import { Row, Col } from 'reactstrap';
import { actions as reduxFormActions } from '../../../node_modules/redux-form';
import { isTablet, getUrlArgs } from '../../utils/utilities';

export class Login extends React.Component<LoginPropTypes, {}> { // eslint-disable-line react/prefer-stateless-function
  componentWillMount() {
    const {
      currentlyLoggedIn,
      goToReceiveOrder,
      loginFocusField,
      // error,
    } = this.props;
    if (currentlyLoggedIn) {
      goToReceiveOrder();
    }
  }

  componentWillUpdate(nextProps) {
    const urlArgs = getUrlArgs<Partial<{ reset: string }>>();
    const isReset = !!urlArgs.reset;

    if (isReset) {
      this.props.resetFocusField('username');
    } else {
      this.props.loginFocusField('username');
    }
    // this.props.loginFocusField(this.props.error ? 'password' : 'user');
  }

  loginFormSubmit = async (values) => {
    const {
      serverLogin,
      loginSuccess,
      goToReceiveOrder,
    } = this.props;
    const { username, password } = values.toJS();
    try {
      const {
        data: {
          token,
        },
      }: {
        data: {
          token: LoginResponse,
        },
      } = await serverLogin({ username, password });
      loginSuccess(token);
      goToReceiveOrder();
    } catch (err) {
      throw new SubmissionError({ _error: 'Incorrect login information' });
    }
  }
  resetFormSubmit = async (values) => {
    const {
      resetPassword,
      loginSuccess,
      goToReceiveOrder,
    } = this.props;
    const { username, newPassword } = values.toJS();
    console.log(values.toJS());
    try {
      const {
        data: {
          token,
        },
      }: {
        data: {
          token: LoginResponse,
        },
      } = await resetPassword({ username, newPassword });
      loginSuccess(token);
      goToReceiveOrder();
    } catch (err) {
      throw new SubmissionError({ _error: 'Invalid ' });
    }
  }

  render() {
    const urlArgs = getUrlArgs<Partial<{ reset: string }>>();
    const isReset = urlArgs && !!urlArgs.reset;

    const loginForm = (<LoginForm
      onSubmit={this.loginFormSubmit.bind(this)}
    />);
    const resetForm = (<ResetForm
      onSubmit={this.resetFormSubmit.bind(this)}
    />);

    const message = isReset ? 'Reset Password' : 'Welcome!';
    const form = isReset ? resetForm : loginForm;

    return (
      <div>
        <Helmet
          title="Login"
          meta={[
            { name: 'description', content: 'Login' },
          ]}
        />

       {/* Insert Stuff Here  */}
      </div>
    );
  }
}

export type LoginPropTypes = {
  serverLogin: (any) => any,
  resetPassword: (any) => any,
  loginSuccess: (any) => any,
  resetFocusField: (any) => any,
  currentlyLoggedIn?: boolean,
  goToReceiveOrder?: () => any,
  loginFocusField: (string) => void,
};

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    loginSuccess: (tokens) => {
      dispatch(loginSuccessAction(tokens));
      // window.location.href = '_';
    },
    goToReceiveOrder: () => {
      // window.location.href = '_';
    },
    loginFocusField: (field: string) => dispatch(reduxFormActions.focus('login', field)),
    resetFocusField: (field: string) => dispatch(reduxFormActions.focus('username', field)),
  };
};

const mapStateToProps = createStructuredSelector({
  error: selectError(),
  currentlyLoggedIn: selectCurrentlyLoggedIn(),
});
const forcefetch: any = { forcefetch: true };

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  graphql(LOGIN_MUTATION, {
    options: forcefetch,
    props: ({ mutate }) => ({
      serverLogin: ({ username, password }) => mutate({ variables: { username, password } }),
    }),
  }),
  graphql(RESET_PASSWORD_MUTATION, {
    options: forcefetch,
    props: ({ mutate }) => ({
      resetPassword: ({ username, newPassword }) => mutate({ variables: { username, password: newPassword } }),
    }),
  }),
)(Login);
