/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';

const selectLogin = () => state => state.get('login');
// const selectApollo = state => state.get('apollo');

const selectUsername = () => createSelector(
  selectLogin(),
  loginState => loginState.get('username'),
);
const selectPassword = () => createSelector(
  selectLogin(),
  loginState => loginState.get('password'),
);

const selectLoading = () => createSelector(
  selectLogin(),
  loginState => loginState.get('loading'),
);

const selectError = () => createSelector(
  selectLogin(),
  loginState => loginState.get('error'),
);

const selectAuthorization = () => state => state.get('authorization');
const selectCurrentUser = () => createSelector(
  selectAuthorization(),
  authorizationState => authorizationState.get('currentUser'),
);
const selectCurrentlyLoggedIn = () => createSelector(
  selectCurrentUser(),
  userState => !!userState.get('username'),
);

export {
  selectLogin,
  selectUsername,
  selectPassword,
  selectLoading,
  selectError,
  selectCurrentlyLoggedIn,
};
