export default function validate(values, props) {
  const errors: errorsProps = {};
  if (values) {
    if (!values.get('username')) {
      errors.username = 'Required';
    }
    if (!values.get('password')) {
      errors.password = 'Required';
    }
  }
  return errors;
}

type errorsProps = {
  username?: string,
  password?: string,
};
