/*
 * LoginReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import {
  UPDATE_USERNAME,
  UPDATE_PASSWORD,
  LOGIN_FAIL,
  LOGIN_SUCCESS,
  LOGIN,
  LOGOUT,
} from './constants';
import { LoginResponse } from './mutation';
import { fromJS } from 'immutable';

// The initial state of the App
const initialState = fromJS({
  userId: '',
  username: '',
  password: '',
  loading: false,
  error: null,
  userRole: '',
});

function loginReducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_USERNAME:
      return state
        .set('username', action.username);
    case UPDATE_PASSWORD:
      return state
        .set('password', action.password);
    case LOGIN:
      return state
        .set('loading', true);
    case LOGIN_SUCCESS:
      const {
        identityPoolId,
        region,
        userPoolId,
        userPoolWebClientId,
      } = action.token as LoginResponse;
      if (identityPoolId && region && userPoolId && userPoolWebClientId) {
        // Cache config data, if sent in response.
        // TODO: find better caching method;
        localStorage.setItem('iterrexConfig', JSON.stringify({
          identityPoolId,
          region,
          userPoolId,
          userPoolWebClientId,
        }));
      }
      return state
        .set('username', '')
        .set('loading', false)
        .set('password', '')
        .set('error', null);
    case LOGIN_FAIL:
      return state
        .set('loading', false)
        .set('error', action.error);
    case LOGOUT:
      localStorage.removeItem('token');
      localStorage.removeItem('creds');
      return state
        .set('user', null)
        .set('error', null);
    default:
      return state;
  }
}

export default loginReducer;
