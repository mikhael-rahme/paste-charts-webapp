/*
 * App Actions
 *
 */

import {
  LOGOUT,
} from '../AuthorizationProvider/constants';

export function logout() {
  return {
    type: LOGOUT,
  };
}
