import * as React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { push } from 'react-router-redux';
import Helmet from 'react-helmet';
import styled, { StyledComponentClass } from 'styled-components';
import {
  selectCurrentUsername,
  selectRoute,
  selectAuthorization,
  selectCurrentIterrexRole,
  selectCurrentGroups,
} from './selectors';
import { logout } from './actions';
import { show as showChangePasswordModalAction } from '../ChangePasswordModal/actions';
import { Header } from '../../components';
import ConfirmModal from '../ConfirmModal';
import ChangePasswordModal from '../ChangePasswordModal';
import { graphql, compose, ChildDataProps } from 'react-apollo';
import { GET_VERSIONS, GetVersionsQuery } from './queries';
import { getTitle } from '../../components/Header/getBranding';
import { selectAppConfig } from '../AuthorizationProvider/selectors';
import { hot } from 'react-hot-loader';

const AppWrapper: StyledComponentClass<any, any> = styled.div`
  margin: 0 auto;
  display: flex;
  min-height: 100%;
  flex-direction: column;
`;

export type AppPropTypes = {
  children?: JSX.Element,
  currentUsername?: string,
  currentIterrexRole?: string,
  logout?: () => any,
  navigate: (string) => any,
  currentRoute?: string,
  showChangePasswordModal?: () => any,
  versions: string,
  // currentGroups: any,
  // appConfig: any,
};

export class App extends React.Component<AppPropTypes, {}> {
  render() {
    const {
      currentUsername,
      currentIterrexRole,
      children,
      logout,
      navigate,
      currentRoute,
      showChangePasswordModal,
      versions,
      // currentGroups,
      // appConfig,
    } = this.props;

    const title = getTitle();

    return (
      <AppWrapper>
        <Helmet
          titleTemplate={`%s - ${title}`}
          defaultTitle={title}
          meta={[
            { name: 'description', content: title },
          ]}
        />
        <Header
          username={currentUsername}
          iterrexRole={currentIterrexRole}
          logout={logout}
          currentRoute={currentRoute}
          navigate={navigate}
          showChangePasswordModal={showChangePasswordModal}
          versions={versions}
          {/*userGroups={currentGroups}*/}
          {/*appConfig={appConfig}*/}
        />
        {/*
        <ConfirmModal />
        <ChangePasswordModal />
        */}
        <div>{React.Children.toArray(children)}</div>
      </AppWrapper>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch,
    logout: () => {
      dispatch(logout());
    },
    navigate: path => dispatch(push(path)),
    showChangePasswordModal: options => dispatch(showChangePasswordModalAction(options)),
  };
};

const mapStateToProps = createStructuredSelector({
  authorization: selectAuthorization(),
  currentUsername: selectCurrentUsername(),
  currentIterrexRole: selectCurrentIterrexRole(),
  currentGroups: selectCurrentGroups(),
  currentRoute: selectRoute(),
  appConfig: selectAppConfig(),
});

type Variables = {};

type Response = {
  getVersions: string;
};

type Props = {
  versions: string;
  loadingVersions: boolean;
};

// type Props = ChildProps<{}, Response, {}>;

export default hot(module)(compose(
  connect(mapStateToProps, mapDispatchToProps),

  graphql<{}, Response, Variables, Props>(GET_VERSIONS, {
    props: ({ data: { loading, getVersions } }) => ({
      versions: getVersions,
      loadingVersions: loading,
    }),
  }),
)(App));
