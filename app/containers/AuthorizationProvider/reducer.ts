/*
 * AuthorizationProvider Reducer -
 * Redux reducer to handle Redux state management
 * for the AuthorizationProvider container
 */

import {
  LOGOUT,
  LOAD_TOKEN,
  AUTHORIZATION_INIT_COMPLETE,
  SET_APP_CONFIG,
} from './constants';
import {
  LOGIN_SUCCESS,
} from '../Login/constants';
import { fromJS } from 'immutable';
import * as jwtDecode from 'jwt-decode';
import {
  IterrexSystemRole,
} from 'toolkit/lib/main/IterrexSystemRole';
import {
  IterrexTenantRole,
} from 'toolkit/lib/main/IterrexTenantRole';
import { EnumValues } from 'enum-values';
import { hardNavigate } from '../../apps/routing/hardNavigate';
import parseJwt from 'toolkit/lib/main/Authorization/parseJwt';

// The initial state of the App
const initialState = fromJS({
  initialized: false,
  currentUser: {
    username: '',
    userRole: '',
    userId: '',
    iterrexRole: '',
    groups: [],
    allGroups: [],
  },
  appConfig: null,
});

const setToken = action => localStorage.setItem('token', JSON.stringify({
  accessToken: action.token.accessToken,
  refreshToken: action.token.refreshToken,
  idToken: action.token.idToken,
}));

function authorizationReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_SUCCESS: // eslint-disable-line no-case-declarations
      // setToken(action);
      hardNavigate('/_'); // home
      return initialState;
    case LOAD_TOKEN: // eslint-disable-line no-fallthrough, no-case-declarations
      // setToken(action);
      // const token = jwtDecode(action.token.accessToken);
      // const { id, userRole, username } = token;

      // const cognitoGroups: string[] = token['cognito:groups'];
      const cognitoIdToken = parseJwt(action.token.idToken);
      const iterrexRole = cognitoIdToken['cognito:groups'].find(group => [
        ...EnumValues.getValues(IterrexSystemRole),
        ...EnumValues.getValues(IterrexTenantRole),
      ].includes(group));

      const tenantDefinedGroups: string[] = cognitoIdToken['cognito:groups'].filter(group => group !== iterrexRole);
      const username = cognitoIdToken['cognito:username'];
      return state
        .setIn(['currentUser', 'username'], username)
        // .setIn(['currentUser', 'userRole'], userRole)
        .setIn(['currentUser', 'iterrexRole'], iterrexRole)
        .setIn(['currentUser', 'groups'], tenantDefinedGroups)
        .setIn(['currentUser', 'userId'], cognitoIdToken.sub)
        .setIn(['currentUser', 'allGroups'], [...tenantDefinedGroups, iterrexRole]);
    case LOGOUT:
      localStorage.removeItem('token');
      localStorage.removeItem('creds');
      hardNavigate('/');
      return initialState;
    case AUTHORIZATION_INIT_COMPLETE:
      return state.set('initialized', true);
    case SET_APP_CONFIG:
      return state.set('appConfig', action.appConfig);
    default:
      return state;
  }
}

export default authorizationReducer;
