/*
 * AuthorizationProvider constants -
 * Constants used to represent the types
 * of Redux actions used within the
 * AuthorizationProvider container
 */

export const LOGOUT = 'boilerplate/App/LOGOUT';
export const LOAD_TOKEN = 'boilerplate/App/LOAD_TOKEN';
export const AUTHORIZATION_INIT_COMPLETE = 'boilerplate/App/AUTHORIZATION_INIT_COMPLETE';
export const SET_APP_CONFIG = 'boilerplate/App/SET_APP_CONFIG';
