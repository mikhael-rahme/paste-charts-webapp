/*
 * AuthorizationProvider actions -
 * Redux actions used within the
 * AuthorizationProvider container to
 * manipulate state
 */

import {
  LOGOUT,
  LOAD_TOKEN,
  AUTHORIZATION_INIT_COMPLETE,
  SET_APP_CONFIG,
} from './constants';

export function logout() {
  return {
    type: LOGOUT,
  };
}

export function loadToken(token) {
  return {
    token,
    type: LOAD_TOKEN,
  };
}

export function completeInitialization() {
  return {
    type: AUTHORIZATION_INIT_COMPLETE,
  };
}

export function setAppConfig(appConfig) {
  return {
    appConfig,
    type: SET_APP_CONFIG,
  };
}
