import styled, { StyledComponentClass } from 'styled-components';

export const AppConfigErrorContainer: StyledComponentClass<any, any> = styled.div`
  padding: 30px;
  width: 80%;
  margin: auto;
  margin-top: 12px;
`;

export const AppConfigErrorMessage: StyledComponentClass<any, any> = styled.div`
  text-align: center;
  padding: 30px;
  border-radius: 6px;
  user-select: none;
`;

export const AppConfigErrorStackTrace: StyledComponentClass<any, any> = styled.pre`
  text-align: left;
  font-family: monospace;
  background: #eee;
  border-radius: 6px;
  padding: 12px;
  margin-top: 12px;
`;
