// Apollo/GraphQL mutation used to refresh the access and refresh JSON web tokens used to authenticate users

import gql from 'graphql-tag';

export const REFRESH_TOKEN = gql`
mutation refreshToken($username: String!, $refreshToken: String!){
    refreshToken(username: $username, refreshToken: $refreshToken){
    accessToken
    refreshToken
  }
}
`;
