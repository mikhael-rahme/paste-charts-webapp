import gql from 'graphql-tag';

export const GET_APP_CONFIG = gql`query getAppConfig {
  getAppConfig {
    type
    id
    version
    schema
    operationName
    operationType
    status
    dateCreated
    dateModified

    fields {
      id
      version
      fieldName
      dataType
      status
      required
      type
      references {
        refId
        refType
        id
        version
        type
      }
      permissions {
        id
        version
        permissionName
        type
        groups {
          groupName
          type
          id
          version
        }
        users {
          sub
          type
          id
          version
        }
      }
    }
    operations
    parents
    references {
      refId
      refType
      id
      version
      type
    }
    permissions {
      id
      version
      permissionName
      type
      id
      version
      groups {
        groupName
        type
        id
        version
      }
      users {
        sub
        type
        id
        version
      }
    }
  }
}`;
