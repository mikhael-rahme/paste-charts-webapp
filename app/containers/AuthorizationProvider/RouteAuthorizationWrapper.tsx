// @flow

/*
* Wrapper container used to
* authorize attempts to access a route
* in the application
* */

import { AuthorizedComponent } from 'react-router-role-authorization';
import { Redirect, RedirectProps } from 'react-router';
import * as React from 'react';
import { connect } from 'react-redux';
import {
  selectCurrentUserRole,
  selectCurrentUserGroups,
} from './selectors';
import { createStructuredSelector } from 'reselect';

export class RouteAuthorizationWrapper extends AuthorizedComponent<RouteAuthorizationWrapperPropTypes, {}> {
  allowedRoles: Array<string>;
  userRoles: Array<string>;
  notAuthorizedPath: string;
  context: any;
  props: RouteAuthorizationWrapperPropTypes;
  constructor(props: any, context?: any) {
    super(props);
    this.allowedRoles = props.route.authorize;
    this.userRoles = [props.currentUserRole, ...props.currentUserGroups];
    this.notAuthorizedPath = '/_/notfound';
  }

  handleUnauthorizedRole(routeRoles, userRoles) {
    // handle unsuccessful authorization somehow
    console.log(`Route is available for roles: ${routeRoles}, but your roles are: ${userRoles}...`);

    // you should still redirect somewhere else if you want to prevent from accessing the restricted route ...
    // ... or just use the default behaviour by calling `super.handleUnauthorizedRole()`
    const { router } = this.context;
    router.push(this.notAuthorizedPath);
  }

  render() {
    const {
      children,
    } = this.props;

    const authorized = this.allowedRoles.some(role => this.userRoles.includes(role));

    return authorized ? React.cloneElement(React.Children.only(children), {
      ...this.props,
    }) : (<div/>);
  }
}

export type RouteAuthorizationWrapperPropTypes = {
  currentUserRole?: string, // from redux
  currentUserGroups?: string[],
  notAuthorizedPath?: string, // definded internally
  children?: JSX.Element,
  location: { pathname: string },
};

const mapStateToProps = createStructuredSelector({
  currentUserRole: selectCurrentUserRole(),
  currentUserGroups: selectCurrentUserGroups(),
});

export function mergeProps(stateProps: Object, dispatchProps: Object, ownProps: Object) {
  return Object.assign({}, ownProps, stateProps, dispatchProps);
}

const ConnectedAuthorization: any = connect(mapStateToProps, {}, mergeProps)(RouteAuthorizationWrapper as any);

export default ConnectedAuthorization;

export const withAuthorization = (WrappedComponent, props) => (
  <ConnectedAuthorization {...props} >
    <WrappedComponent />
  </ConnectedAuthorization>
);
