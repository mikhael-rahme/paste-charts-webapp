import expect from 'expect';
import {
  LOGOUT,
  LOAD_TOKEN,
  AUTHORIZATION_INIT_COMPLETE,
} from '../constants';
import {
  logout,
  loadToken,
  completeInitialization,
} from '../actions.ts';
const { describe, it } = require('mocha');

describe('AuthorizationProvider Actions', () => {
  describe('logout', () => {
    it('should return the correct type', () => {
      const expectedResult = {
        type: LOGOUT,
      };
      expect(logout()).toEqual(expectedResult);
    });
  });
  describe('loadToken', () => {
    it('should return the correct type', () => {
      const token = {
        accessToken: 'something',
        refreshToken: 'something',
      };
      const expectedResult = {
        token,
        type: LOAD_TOKEN,
      };
      expect(loadToken(token)).toEqual(expectedResult);
    });
  });
  describe('completeInitialization', () => {
    it('should return the correct type', () => {
      const expectedResult = {
        type: AUTHORIZATION_INIT_COMPLETE,
      };
      expect(completeInitialization()).toEqual(expectedResult);
    });
  });
});
