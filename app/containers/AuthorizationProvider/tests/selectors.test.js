import { fromJS } from 'immutable';
import expect from 'expect';
import {
  selectAuthorization,
  selectCurrentUser,
  selectCurrentUserRole,
  selectAuthorizationInitialized,
} from '../selectors.ts';

describe('AuthorizationProvider selectors', () => {
  describe('selectAuthorization', () => {
    const authorizationSelector = selectAuthorization();
    it('should select the authorization state', () => {
      const authorizationState = fromJS({});
      const mockedState = fromJS({
        authorization: authorizationState,
      });
      expect(authorizationSelector(mockedState)).toEqual(authorizationState);
    });
  });

  describe('selectCurrentUser', () => {
    const currentUserSelector = selectCurrentUser();
    it('should select the current user', () => {
      const currentUser = {
        username: 'test1',
        userRole: 'test2',
      };
      const mockedState = fromJS({
        authorization: {
          currentUser,
        },
      });
      expect(currentUserSelector(mockedState).get('username')).toEqual(currentUser.username);
      expect(currentUserSelector(mockedState).get('userRole')).toEqual(currentUser.userRole);
    });
  });

  describe('selectCurrentUserRole', () => {
    const currentUserRoleSelector = selectCurrentUserRole();
    it('should select the current userRole', () => {
      const userRole = 'mxstbr';
      const mockedState = fromJS({
        authorization: {
          currentUser: {
            userRole,
          },
        },
      });
      expect(currentUserRoleSelector(mockedState)).toEqual(userRole);
    });
  });

  describe('selectAuthorizationInitialized', () => {
    const authorizationInitializedSelector = selectAuthorizationInitialized();
    it('should select the loading', () => {
      const initialized = true;
      const mockedState = fromJS({
        authorization: {
          initialized,
        },
      });
      expect(authorizationInitializedSelector(mockedState)).toEqual(initialized);
    });
  });
});
