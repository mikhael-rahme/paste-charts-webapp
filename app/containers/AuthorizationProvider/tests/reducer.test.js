import expect from 'expect';
import { fromJS } from 'immutable';
import { LOGIN_SUCCESS } from 'app/containers/Login/constants.ts';
import {
  loadToken,
  logout,
  completeInitialization,
} from '../actions.ts';
import authorizationReducer from '../reducer.ts';
const { describe, it } = require('mocha');

describe('AuthorizationProvider Reducer', () => {
  const initialState = fromJS({
    initialized: false,
    currentUser: {
      username: '',
      userRole: '',
      userId: '',
      groups: [],
    },
  });

  // tslint:disable:max-line-length
  const token = {
    accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3RfdXNlcm5hbWUiLCJ1c2VyUm9sZSI6InRlc3Rfcm9sZSIsImlkIjoidGVzdF9pZCJ9.Cbn3EKORf-4g8EDeYcVfa9f1XlUwDgztEcIm5XtFrOk',
    refreshToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3RfdXNlcm5hbWUiLCJ1c2VyUm9sZSI6InRlc3Rfcm9sZSIsImlkIjoidGVzdF9pZCJ9.Cbn3EKORf-4g8EDeYcVfa9f1XlUwDgztEcIm5XtFrOk',
  };

  it('returns the initial state', () => {
    expect(authorizationReducer(undefined, {})).toEqual(initialState);
  });
  it('modifies localStorage when the action type is LOGIN_SUCCESS', () => {
    authorizationReducer(undefined, { token, type: LOGIN_SUCCESS });
    expect(JSON.parse(localStorage.getItem('token'))).toEqual(token);
  });
  it('returns a modified state when the action is loadToken', () => {
    const currentState = fromJS({
      initialized: false,
      currentUser: {
        username: 'test_username',
        userRole: 'test_role',
        userId: 'test_id',
      },
    });

    expect(authorizationReducer(undefined, loadToken(token)).getIn(['currentUser', 'username'])).toEqual(currentState.getIn(['currentUser', 'username']));
    expect(authorizationReducer(undefined, loadToken(token)).getIn(['currentUser', 'userRole'])).toEqual(currentState.getIn(['currentUser', 'userRole']));
    expect(authorizationReducer(undefined, loadToken(token)).getIn(['currentUser', 'userId'])).toEqual(currentState.getIn(['currentUser', 'userId']));
  });
  it('clears the state when the action is logout', () => {
    expect(authorizationReducer(undefined, logout())).toEqual(initialState);
  });
  it('returns a modified state when the action is completeInitialization', () => {
    expect(authorizationReducer(undefined, completeInitialization())
      .get('initialized'))
      .toEqual(true);
  });
});
