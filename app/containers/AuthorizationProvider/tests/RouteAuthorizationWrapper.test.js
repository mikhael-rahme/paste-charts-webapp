import * as React from 'react';
import expect, { createSpy } from 'expect';
import { shallow } from 'enzyme';
import RouteAuthorizationWrapper from '../RouteAuthorizationWrapper.tsx';
const { describe, it } = require('mocha');

describe('<RouteAuthorizationWrapper />', () => {
  let replace;

  beforeEach(() => {
    replace = createSpy();
  });
  const renderComponent = (overrideProps = {}) => {
    const props = {
      currentUserRole: 'Admin',
      notAuthorizedPath: '/',
      route: {
        authorize: ['Admin', 'User'],
      },
      ...overrideProps,
    };
    const context = {
      router: {
        replace,
      },
    };

    return shallow((
      <RouteAuthorizationWrapper {...props}>
        React.Children.only(children)
      </RouteAuthorizationWrapper>
    ), { context });
  };

  describe('RouteAuthorizationWrapper Container', () => {
    it('should render', () => {
      expect(renderComponent().not(null));
    });

    it('should render children when the current user is authorized', () => {
      const child = <div>Test</div>;
      expect(renderComponent(child).find('<div>Test</div>').length).toEqual(1);
    });
  });
});
