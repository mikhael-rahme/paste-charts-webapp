/*
 * Selectors used within the AuthorizationProvider container
 * to retrieve data from the Authorization Provider's
 * section of the Redux store, such as the current logged in
 * user and their user role
 */

import { createSelector } from 'reselect';

const selectAuthorization = () => state => state.get('authorization');

const selectCurrentUser = () => createSelector(
  selectAuthorization(),
  authorizationState => authorizationState.get('currentUser').toJS(),
);

const selectCurrentUserId = () => createSelector(
  selectCurrentUser(),
  currentUser => currentUser && currentUser.userId,
);

const selectCurrentUserRole = () => createSelector(
  selectCurrentUser(),
  currentUser => currentUser.iterrexRole,
);

const selectCurrentUserGroups = () => createSelector(
  selectCurrentUser(),
  currentUser => currentUser.groups,
);

const selectAuthorizationInitialized = () => createSelector(
  selectAuthorization(),
  authorizationState => authorizationState.get('initialized'),
);

const selectAppConfig = () => createSelector(
  selectAuthorization(),
  state => state.get('appConfig'),
);

export {
  selectAuthorization,
  selectCurrentUser,
  selectCurrentUserId,
  selectCurrentUserRole,
  selectCurrentUserGroups,
  selectAuthorizationInitialized,
  selectAppConfig,
};
