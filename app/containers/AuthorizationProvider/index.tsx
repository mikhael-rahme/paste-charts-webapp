/*
 * AuthorizationProvider -
 * Wraps other containers/routes with a higher-order
 * container that prevents access by users who aren't
 * logged in and grants varying levels of access
 * based on the user role of a logged in user
 */

import * as React from 'react';
import { connect } from 'react-redux';
import { selectAuthorizationInitialized, selectCurrentUser, selectAppConfig } from './selectors';
import {
  logout as logoutAction,
  loadToken as loadTokenAction,
  completeInitialization as completeInitializationAction,
  setAppConfig as setAppConfigAction,
} from './actions';
import { REFRESH_TOKEN } from './mutation';
import { GET_APP_CONFIG } from './queries';
import * as jwtDecode from 'jwt-decode';
import { push } from 'react-router-redux';
import { createStructuredSelector } from 'reselect';
import { withAuthorization } from './RouteAuthorizationWrapper';
import { LoadingWrapper } from '../../components';
import {
  graphql,
  compose,
  OperationVariables,
} from 'react-apollo';
import { ApolloQueryResult } from 'apollo-client';
import { AppConfigErrorMessage, AppConfigErrorStackTrace, AppConfigErrorContainer } from './wrappers';
import { Button } from 'reactstrap';
import { hot } from 'react-hot-loader';

export class AuthorizationProvider extends React.Component<AuthorizationProviderPropTypes, {}> {
  componentWillReceiveProps(nextProps) {
    if (!this.props.appConfig && nextProps.appConfig) {
      try {
        this.props.setAppConfig(nextProps.appConfig);
      } catch {
        console.error('Invalid AppConfig:', nextProps.appConfig);
      }
    }
    // if (!this.props.appConfigError && nextProps.appConfigError) {
    //   this.props.logoutNoRouteChange();
    // }
  }

  async componentDidUpdate() {
    const {
      setAppConfig,
      // refetchAppConfig,
      currentUser,
      newAppConfig,
    } = this.props;

    if (currentUser.username !== '') {
      // await refetchAppConfig();
      setAppConfig(newAppConfig);
    }
  }

  async componentWillMount() {
    const {
      logout,
      loadToken,
      refreshToken,
      navigate,
      completeInitialization,
      appConfig,
      setAppConfig,
      refetchAppConfig,
      newAppConfig,
      currentUser,
    } = this.props;

    // Since this fires on initial load (before the router in instantiated) we
    // just grab the path from the browser directly
    const route = location.pathname;

    const tokenString = localStorage.getItem('token');
    if (route === '/reset') {
      // when we're resetting a password, make sure that we remove the access token
      localStorage.removeItem('token');
      completeInitialization();
    } else if (tokenString) {
      const token = JSON.parse(tokenString);
      let decodedToken;
      try {
        // TODO

        // Load all permissions here!!!

        decodedToken = jwtDecode(token.accessToken);
        // console.log(`decodedToken: ${JSON.stringify(decodedToken, null, 2)}`); // eslint-disable-line no-console
        const now = Date.now();
        /*
        if (now > decodedToken.exp * 1000) {
          // console.log('old access token, try and refresh'); // eslint-disable-line no-console
          const { data } = await refreshToken({ username: decodedToken.username, refreshToken: token.refreshToken });
          // console.log('token refreshed'); // eslint-disable-line no-console
          // console.log(`newToken: ${JSON.stringify(data.refreshToken, null, 2)}`); // eslint-disable-line no-console
          token = data.refreshToken;
        }*/
        // console.log('load token'); // eslint-disable-line no-console
        loadToken(token);
        newAppConfig && setAppConfig(newAppConfig);

        completeInitialization();
        if (currentUser.username !== '') {
          // await refetchAppConfig();
          setAppConfig(newAppConfig);
        }
        // setAppConfig(appConfig);
        /*
        if (route === '/') {
          // navigate('/home');
          (window.location as any) = `${window.location.host}/home`;
        }
          route '/' is now default

        */
      } catch (err) {
        localStorage.removeItem('token');
        completeInitialization();
        logout();
      }
    } else {
      localStorage.removeItem('token');
      completeInitialization();
      // window.location.href = 'login';
    }
  }
  render() {
    const {
      appConfig,
      appConfigError,
      authorizationInitialized,
      currentUser,
    } = this.props;

    if (appConfigError) {
      return (
        <AppConfigErrorContainer>
          <AppConfigErrorMessage>
            An error occurred while loading the application. Contact support if problem persists.
            <Button color="link" block onClick={() => window.location.reload(false)}>
              Reload page or click here to re-attempt.
            </Button>
          </AppConfigErrorMessage>

          <h6>Debug Stacktrace:</h6>
          <AppConfigErrorStackTrace>
            {new Date().toISOString()} {appConfigError.stack}
          </AppConfigErrorStackTrace>
        </AppConfigErrorContainer>
      );
    }

    const component = (
      <LoadingWrapper loading={currentUser.username.length > 0 && !appConfig} message="Loading...">
        {React.Children.only(this.props.children)}
      </LoadingWrapper>
    );
    return authorizationInitialized ? component : null;
  }
}

export type AuthorizationProviderPropTypes = {
  appConfigError?: Error,
  children?:  JSX.Element,
  logout: () => any,
  logoutNoRouteChange: () => any,
  loadToken: (string) => any,
  navigate: (string) => any,
  refreshToken: (string) => any,
  authorizationInitialized?: Boolean,
  completeInitialization?: () => any,
  appConfig?: any,
  newAppConfig?: string,
  setAppConfig: (string) => any,
  refetchAppConfig?: () => any,
  currentUser: {
    username: string,
    userRole: string,
    userId: string,
    iterrexRole: string,
    groups: any[],
  },
};

function mapDispatchToProps(dispatch) {
  return {
    logout: () => {
      dispatch(logoutAction());
    },
    logoutNoRouteChange: () => dispatch(logoutAction()),
    loadToken: token => dispatch(loadTokenAction(token)),
    navigate: path => dispatch(push(path)),
    completeInitialization: () => dispatch(completeInitializationAction()),
    setAppConfig: newAppConfig => dispatch(setAppConfigAction(newAppConfig)),
  };
}

const mapStateToProps = createStructuredSelector({
  authorizationInitialized: selectAuthorizationInitialized(),
  currentUser: selectCurrentUser(),
  appConfig: selectAppConfig(),
});

export {
  withAuthorization,
};

type InputProps = {};

type Response = {
  getAppConfig: string;
};

type Variables = {};

type Props = {
  loadingAppConfig: boolean;
  newAppConfig: string;
  refetchAppConfig: (variables?: OperationVariables) => Promise<ApolloQueryResult<any>>;
};

export default hot(module)(compose(
  connect(mapStateToProps, mapDispatchToProps),
  graphql(REFRESH_TOKEN, {
    props: ({ mutate }) => ({
      refreshToken: ({ username, refreshToken }) => mutate({ variables: { username, refreshToken } }),
    }),
  }),
  graphql<InputProps, Response, Variables, Props>(GET_APP_CONFIG, {
    props: ({ data: { refetch, loading, getAppConfig, error } }) => {
      return {
        appConfigError: error,
        loadingAppConfig: loading,
        newAppConfig: getAppConfig,
        refetchAppConfig: refetch,
      };
    },
    skip: ownProps => ownProps.currentUser.username === '',
  }),
)(AuthorizationProvider));
