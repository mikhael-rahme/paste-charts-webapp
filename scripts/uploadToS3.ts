import { S3 } from 'aws-sdk';
const argv = require('minimist')(process.argv.slice(2));
const { promisify } = require('util');
const fs = require('fs');
const path = require('path');
import * as mime from 'mime-types';
import backoff from 'toolkit/lib/main/backoff';

// only production or staging
const stage = argv.stage === 'production'
  ? 'production'
  : 'staging';

const bucketName = `iterrex-webapp-${stage}`;

// start build
const skipBuild = !!argv.skipBuild;

const s3 = new S3();

const FOLDER_PREFIX = 'master/';
const folderPrefix = fileName => `${FOLDER_PREFIX}${fileName}`;

const CK = '\u2713';
const printDone = message => console.log(`${message} ${CK}`);

const run = async () => {
  const targetBucketList: S3.ListObjectsV2Output = await s3.listObjectsV2({ Bucket: bucketName }).promise();

  const existingS3Files: string[] = targetBucketList.Contents.map(s3Object => s3Object.Key);
  console.log(`existingS3Files: ${JSON.stringify(existingS3Files, null, 2)}`);
  let latestBuildFiles;
  try {
    latestBuildFiles = await promisify(fs.readdir)(path.join(__dirname, '../build'));

  } catch (err) {
    console.error('Could not read build folder!');
    console.error(err);
    process.exit(1);
  }
  console.log(`latestBuildFiles: ${JSON.stringify(latestBuildFiles, null, 2)}`);

  // Create a list of files that will be overwritten
  const [
    filesToOverwrite,
    filesToDelete,
  ] = existingS3Files.reduce(
    (output, currentFile) => {
      if (latestBuildFiles.includes(currentFile.replace('master/', ''))) {
        output[0].push(currentFile);
      } else {
        output[1].push(currentFile);
      }
      return output;
    },
    [[], []]);

  const filesToAdd: string[] = latestBuildFiles
    .filter(newFile => !filesToOverwrite.includes(folderPrefix(newFile)) && !filesToDelete.includes(folderPrefix(newFile)));

  const noCacheFileRegex = /(.+\.html|sw\.js)/;
  console.log(`files to be overwritten: ${JSON.stringify(filesToOverwrite.filter(file => file.match(noCacheFileRegex)), null, 2)}`);
  console.log(`files to be deleted: ${JSON.stringify(filesToDelete, null, 2)}`);
  console.log(`files to ADD ${JSON.stringify(filesToAdd, null, 2)}`);

  /* Order of update
    1) Upload new files
    2) Overwrite existing noCache files
    3) Delete extra unused files
  */

  const getACL = (fileName): 'private' | 'public-read' => fileName.indexOf('.map') > -1
    ? 'private'
    : 'public-read';

  const getBody = async (fileName) : Promise<Buffer> => promisify(fs.readFile)(path.join(__dirname, `../build/${fileName}`));

  const getContentType = fileName => mime.lookup(fileName) || 'text/plain';

  const getCacheControl = (fileName: string) => fileName.match(noCacheFileRegex)
    ? 'no-cache'
    : 'max-age=31536000';

  // TODO Parallelize all S3 calls using backoff()

  console.log('Uploading new files...');
  for (const file of filesToAdd) {
    try {
      await s3.putObject({
        Bucket: bucketName,
        Key: folderPrefix(file),
        Body: await getBody(file),
        ACL: getACL(file),
        CacheControl: getCacheControl(file),
        ContentType: getContentType(file),
      }).promise();
    } catch (err) {
      console.error(err);
      process.exit(1);
    }
    printDone(file);
  }
  console.log('Uploading new files done!');

  console.log('Overwriting existing files...');
  for (const prefixedFile of filesToOverwrite) {
    const file = prefixedFile.replace(FOLDER_PREFIX, '');
    try {
      await s3.putObject({
        Bucket: bucketName,
        Key: folderPrefix(file),
        Body: await getBody(file),
        ACL: getACL(file),
        CacheControl: getCacheControl(file),
        ContentType: getContentType(file),
      }).promise();
    } catch (err) {
      console.error(err);
      process.exit(1);
    }
    printDone(file);
  }
  console.log('Overwriting existing files done!');

  console.log('Deleting old files...');
  for (const file of filesToDelete) {
    await s3.deleteObject({
      Key: file,
      Bucket: bucketName,
    }).promise();
  }
  console.log('old files deleted successfully!');

  console.log('All done!');
};

run();
