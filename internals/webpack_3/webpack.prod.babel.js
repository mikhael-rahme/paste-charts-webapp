// Important modules this config uses
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OfflinePlugin = require('offline-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
webpack.optimize.UglifyJsPlugin = UglifyJsPlugin;
module.exports = require('./webpack.base.babel')({
  // In production, we skip all hot-reloading stuff
  entry: {
    home: path.join(process.cwd(), 'app/apps/home/index.tsx'),
    login: path.join(process.cwd(), 'app/apps/login/index.tsx'),
    admin: path.join(process.cwd(), 'app/apps/admin/index.tsx'),
    credentialCheck: path.join(process.cwd(), 'app/apps/_credentialCheck/index.tsx'),
  },

  // Utilize long-term caching by adding content hashes (not compilation hashes) to compiled assets
  output: {
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].chunk.js',
  },

  tsLoaders: [
    'babel-loader',
    'awesome-typescript-loader'
  ],

  cssLoadersLocal: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [
      {
        loader: 'css-loader',
        options: {
          modules: true,
          '-autoprefixer': true,
          importLoaders: 1,
        },
      },
      'postcss-loader',
    ],
  }),

  // We use ExtractTextPlugin so we get a separate CSS file instead
  // of the CSS being in the JS and injected as a style tag
  cssLoaders: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [
      {
        loader: 'css-loader',
        options: {
          modules: false,
          '-autoprefixer': true,
          importLoaders: 1,
        },
      },
      'postcss-loader',
    ],
  }),

  plugins: [

    function CustomErrorHandlerPlugin() {
      this.plugin(
        'done', (stats) => {
          if (process.argv.indexOf('--watch') > -1) {
            return;
          }
          if (stats.compilation.errors && stats.compilation.errors.length) {
            console.error(stats.compilation.errors.join('\n\n')); // eslint-disable-line no-console
            process.exit(1); // or throw new Error('webpack build failed.');
          }
          process.exit(0);
        }
      );
    },

    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      children: true,
      minChunks: 2,
      async: true,
    }),

    // OccurrenceOrderPlugin is needed for long-term caching to work properly.
    // See http://mxs.is/googmv
    new webpack.optimize.OccurrenceOrderPlugin(true),

    // Minify and optimize the JavaScript
    /*
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false, // ...but do not show warnings in the console (there is a lot of them)
      },
    }),*/

    new UglifyJsPlugin({
      uglifyOptions: {
        compress: {
          warnings: false
        },
        ecma: 8,
        verbose: true,
      },
      parallel: true,
    }),

    // Minify and optimize the index.html
    new HtmlWebpackPlugin({
      template: 'app/template.html',
      filename: 'index.html',
      inject: true,
      minify: false,
      chunks: [
        'login'
      ],
    }),

    // Minify and optimize the index.html
    new HtmlWebpackPlugin({
      template: 'app/template.html',
      filename: '_',
      inject: true,
      minify: false,
      chunks: [
        'credentialCheck',
        'home'
      ],
      chunksSortMode: 'manual',
    }),
    // Minify and optimize the index.html
    /*
    new HtmlWebpackPlugin({
      template: 'app/template.html',
      filename: 'admin',
      inject: true,
      minify: false,
      chunks: [
        'credentialCheck',
        'admin'
      ],
      chunksSortMode: 'manual',
    }),*/

    // Extract the CSS into a separate file
    new ExtractTextPlugin('[name].[contenthash].css'),

    // Put it in the end to capture all the HtmlWebpackPlugin's
    // assets manipulations and do leak its manipulations to HtmlWebpackPlugin
    new OfflinePlugin({
      relativePaths: false,
      publicPath: '/',

      // No need to cache .htaccess. See http://mxs.is/googmp,
      // this is applied before any match in `caches` section
      excludes: ['.htaccess'],

      caches: {
        main: [':rest:'],

        // All chunks marked as `additional`, loaded after main section
        // and do not prevent SW to install. Change to `optional` if
        // do not want them to be preloaded at all (cached only when first loaded)
        additional: ['*.chunk.js'],
      },

      // Removes warning for about `additional` section usage
      safeToUseOptionalCaches: true,

      AppCache: false,

      updateStrategy: 'changed',
      autoUpdate: 1000 * 60 * 15,

      ServiceWorker: {
        events: true,
        navigateFallbackURL: '/',
      },
    }),
  ],
});
