var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const appsConfig = require('../../app/apps/config');
console.log(`appsConfig: ${JSON.stringify(appsConfig, null, 2)}`);
const path = require('path');
const pkg = require(path.resolve(process.cwd(), 'package.json'));
const dllPlugin = pkg.dllPlugin;
const fs = require('fs');
const cheerio = require('cheerio');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');


/**
* We dynamically generate the HTML content in development so that the different
* DLL Javascript files are loaded in script tags and available to our application.
*/
function templateContent() {
 const html = fs.readFileSync(
   path.resolve(process.cwd(), 'app/template.html')
 ).toString();

 if (!dllPlugin) { return html; }

 const doc = cheerio(html);
 const body = doc.find('body');
 const dllNames = !dllPlugin.dlls ? ['reactBoilerplateDeps'] : Object.keys(dllPlugin.dlls);

 dllNames.forEach((dllName) => body.append(`<script data-dll='true' src='/${dllName}.dll.js'></script>`));

 return doc.toString();
}

const getHtmlWebpackOptions = (appname) => {
  const options = {
    inject: true, // Inject all files that are generated by webpack, e.g. bundle.js
    templateContent: templateContent(), // eslint-disable-line no-use-before-define
    // template: 'template.html',
  };
  try {
    console.log(`appConfig: ${JSON.stringify(appsConfig[appname])}`)
    if (appsConfig[appname].htmlFileName) {
      console.log(`setting htmlFileName for ${appname}`);
      options.filename = appsConfig[appname].htmlFileName;
    }
  } catch (err) {
    console.warn(err);
  }
  return options;
};

const getAppFolderPath = (appname) => `app/apps/${appname}`;

module.exports = (appname) => require('./webpack.base')({
  entry: [
    'webpack-hot-middleware/client',
    'react-hot-loader/patch',
    `./apps/${appname}/index.tsx`,
  ],
  mode: 'development',
  optimization: {
    minimize: false,
  },
  performance: {
    hints: false,
  },
  output: {
    /*filename: '[name].js',
    chunkFilename: '[name].chunk.js',
    path: path.resolve(process.cwd(), 'build', appsConfig[appname].path),
    publicPath: `${appsConfig[appname].path}/`,*/
    filename: '[name].js',
    chunkFilename: '[name].chunk.js',
  },
  plugins: dependencyHandlers().concat([
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin(getHtmlWebpackOptions(appname)),
    new ForkTsCheckerWebpackPlugin({
      tsconfig: path.join(__dirname, '../../tsconfig.json'),
    })
  ]),
  tsLoaders: [
    {
      loader: 'babel-loader',
      options: { plugins: ['react-hot-loader/babel'] }
    },
    {
      loader: 'ts-loader',
      options: {
        transpileOnly: true //HMR doesn't work without this
      }
    },
  ],
  cssLoaders: [
    // isProduction ? MiniCssExtractPlugin.loader : 'style-loader',
    'style-loader',
    {
      loader: 'css-loader',
      query: {
        modules: true,
        sourceMap: true,
        importLoaders: 1,
        localIdentName: '[local]__[name]'
      }
    },
    {
      loader: 'postcss-loader',
      options: {
        ident: 'postcss',
        plugins: [
          require('postcss-import')({ addDependencyTo: webpack }),
          require('postcss-url')(),
          require('postcss-preset-env')({
            /* use stage 2 features (defaults) */
            stage: 2,
          }),
          require('postcss-reporter')(),
          require('postcss-browser-reporter')({
            disabled: false,
          })
        ]
      }
    }
  ],
  devtool: 'inline-source-map',
});


/*
* Select which plugins to use to optimize the bundle's handling of
* third party dependencies.
*
* If there is a dllPlugin key on the project's package.json, the
* Webpack DLL Plugin will be used.  Otherwise the CommonsChunkPlugin
* will be used.
*
*/
function dependencyHandlers() {
  // Don't do anything during the DLL Build step
  if (process.env.BUILDING_DLL) { return []; }

  const dllPath = path.resolve(process.cwd(), dllPlugin.path || 'node_modules/react-boilerplate-dlls');

  /**
   * If DLLs aren't explicitly defined, we assume all production dependencies listed in package.json
   * Reminder: You need to exclude any server side dependencies by listing them in dllConfig.exclude
   *
   * @see https://github.com/mxstbr/react-boilerplate/tree/master/docs/general/webpack.md
   */
  if (!dllPlugin.dlls) {
    const manifestPath = path.resolve(dllPath, 'reactBoilerplateDeps.json');

    if (!fs.existsSync(manifestPath)) {
      console.error(
        `The following Webpack DLL manifest is missing: ${path.basename(
          manifestPath,
        )}`,
      );
      console.error(`Expected to find it in ${dllPath}`);
      console.error('Please run: npm run build:dll');
      process.exit(0);
    }

    return [
      new webpack.DllReferencePlugin({
        context: process.cwd(),
        manifest: require(manifestPath),
        // name: path.resolve(__dirname, '../../app/dll'),
      }),
    ];
  }
 }

