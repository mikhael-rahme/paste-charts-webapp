var webpack = require('webpack');
var MiniCssExtractPlugin = require('mini-css-extract-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const appsConfig = require('../../app/apps/config');
const path = require('path');
var HappyPack = require('happypack');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const OfflinePlugin = require('offline-plugin');

module.exports = (env, argv) => 
  require('./webpack.base')({
    mode: 'production',
    entry: {
      login: './apps/login/index.tsx',
      home: './apps/home/index.tsx',
      admin: './apps/admin/index.tsx',
      base: './apps/index/index.tsx',
    },
    output: {
      filename: '[name].[chunkhash].js',
      chunkFilename: '[name].[chunkhash].chunk.js',
      // filename: '[name].js',
      // chunkFilename: '[name].chunk.js',
      path: path.resolve(__dirname, '../../build'),
      publicPath: `/`,
    },
    tsLoaders: [
      'babel-loader',
      'happypack/loader?id=ts',
    ],
    plugins: [
      new HappyPack({
        id: 'ts',
        threads: 2,
        loaders: [
            {
                path: 'ts-loader',
                query: {
                  happyPackMode: true,
                  transpileOnly: true,
                }
  
            }
        ]
      }),
      new ForkTsCheckerWebpackPlugin({ 
        tsconfig: path.join(__dirname, '../../tsconfig.json'),
      }),
      new HtmlWebpackPlugin({
        template: 'template.html',
        filename: 'login.html',
        inject: true,
        chunks: [
          'base',
          'login'
        ],
        chunksSortMode: 'manual',
      }),
  
      // Minify and optimize the index.html
      new HtmlWebpackPlugin({
        template: 'template.html',
        filename: '_.html',
        inject: true,
        chunks: [
          'base',
          'home'
        ],
        chunksSortMode: 'manual',
      }),
      // Minify and optimize the index.html
      new HtmlWebpackPlugin({
        template: 'template.html',
        filename: 'admin.html',
        inject: true,
        chunks: [
          'base',
          'admin',
        ],
        chunksSortMode: 'manual',
      }),
      // Minify and optimize the index.html
      new HtmlWebpackPlugin({
        template: 'template.html',
        filename: 'index.html',
        inject: true,
        chunks: [
          'base',
        ],
      }),
      new OfflinePlugin({
        relativePaths: false,
        publicPath: '/',
  
        // No need to cache .htaccess. See http://mxs.is/googmp,
        // this is applied before any match in `caches` section
        excludes: ['.htaccess', '**/*.map'],
  
        caches: {
          main: [':rest:'],
  
          // All chunks marked as `additional`, loaded after main section
          // and do not prevent SW to install. Change to `optional` if
          // do not want them to be preloaded at all (cached only when first loaded)
          additional: ['*.chunk.js'],
        },
  
        // Removes warning for about `additional` section usage
        safeToUseOptionalCaches: true,
  
        AppCache: false,
  
        updateStrategy: 'changed',
        autoUpdate: 1000 * 60 * 15,
  
        ServiceWorker: {
          events: true,
          minify: false,
          navigateFallbackURL: '/',
        },
      }),
    ],
    cssLoaders: [
      // isProduction ? MiniCssExtractPlugin.loader : 'style-loader',
      MiniCssExtractPlugin.loader,
      {
        loader: 'css-loader',
        query: {
          modules: true,
          sourceMap: false,
          importLoaders: 1,
          localIdentName: '[hash:base64:5]'
        }
      },
      {
        loader: 'postcss-loader',
        options: {
          ident: 'postcss',
          plugins: [
            require('postcss-import')({ addDependencyTo: webpack }),
            require('postcss-url')(),
            require('postcss-preset-env')({
              /* use stage 2 features (defaults) */
              stage: 2,
            }),
            require('postcss-reporter')(),
            require('postcss-browser-reporter')({
              disabled: true,
            })
          ]
        }
      }
    ],
    miniCss: {
      filename: '[contenthash].css',
      disable: false,
    },
    devtool: 'source-map',
    optimization: {
      nodeEnv: 'production',
      splitChunks: {
        chunks: 'all',
        name: true,
        cacheGroups: {
          commons: {
            chunks: 'initial',
            minChunks: 2,
          },
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            chunks: 'all',
            priority: -10,
          },
        },
      },
      runtimeChunk: true,
      occurrenceOrder: true,
    },
  });

