const webpack = require('webpack');
const path = require('path');

// constiables
const sourcePath = path.join(__dirname, '../../app');
const outPath = path.join(__dirname, '../../build');

// plugins
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackCleanupPlugin = require('webpack-cleanup-plugin');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');

const loadOperationIds = require('../loadOperationIds');

// Remove this line once the following warning goes away (it was meant for webpack loader authors not users):
// 'DeprecationWarning: loaderUtils.parseQuery() received a non-string value which can be problematic,
// see https://github.com/webpack/loader-utils/issues/56 parseQuery() will be replaced with getOptions()
// in the next major version of loader-utils.'
process.noDeprecation = true;

module.exports = (options) => ({
  mode: options.mode || 'production',
  context: sourcePath,
  entry: options.entry,
  output: Object.assign({
    path: outPath,
    // filename: 'bundle.js', // breaks dll
    // chunkFilename: '[chunkhash].chunk.js',
    publicPath: '/',
  }, options.output),
  target: 'web',
  resolve: {
    extensions: ['.js', '.ts', '.tsx'],
    // Fix webpack's default behavior to not load packages with jsnext:main module
    // (jsnext:main directs not usually distributable es6 format, but es6 sources)
    mainFields: ['module', 'browser', 'main'],
    alias: {
      app: path.resolve(__dirname, '../../app'),
      components: path.resolve(__dirname, '../../app/components'),
      containers: path.resolve(__dirname, '../../app/containers'),
      immutable: 'immutable/dist/immutable.js',
      moment$: 'moment/moment.js',
    }
  },
  module: {
    rules: [
      // .ts, .tsx
      {
        test: /\.tsx?$/,
        use: options.tsLoaders,
      },
      // css
      {
        // Do not transform vendor's CSS with CSS-modules
        // The point is that they remain in global scope.
        // Since we require these CSS files in our JS or CSS files,
        // they will be a part of our compilation either way.
        // So, no need for ExtractTextPlugin here.
        test: /\.css$/,
        include: /node_modules/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },
      {
        test: /\.css$/,
        exclude: [/node_modules/, /build/, /dist/],
        use: options.cssLoaders
      }, {
        test: /\.(scss)$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader'],
      }, {
        test: /\.less$/,
        use: [{
          loader: 'style-loader',
        }, {
          loader: 'css-loader',
        }, {
          loader: 'less-loader',
          options: {
            javascriptEnabled: true,
          },
        }],
      },
      // static assets
      { test: /\.html$/, use: 'html-loader' },
      { test: /\.(a?png|svg)$/, use: 'url-loader?limit=10000' },
      { test: /\.(jpe?g|gif|bmp|mp3|mp4|ogg|wav|eot|ttf|woff|woff2)$/, use: 'file-loader' },
      /*{
        type: 'javascript/auto',
        test: /\.json$/,
        use: [
          {
            loader: 'file-loader',
            options: {
                name: "[name].[ext]",
            },
          },
        ],
      },*/
      {
        type: 'javascript/auto',
        test: /\.mjs$/,
        use: [],
      },
    ]
  },
  optimization: options.optimization,
  plugins: (options.plugins || []).concat([
    new webpack.EnvironmentPlugin(Object.assign({
      // NODE_ENV: process.env.NODE_ENV || 'development', // use 'development' unless process.env.NODE_ENV is defined
      AWS_IOT_ENDPOINT_HOST: JSON.stringify(process.env.AWS_IOT_ENDPOINT_HOST) || `''`,
      DEBUG: false,
    }, loadOperationIds(process.env.HUBBELL_STAGE || 'staging'))),
    new MomentLocalesPlugin({ // remove extra locales from built code
      localesToKeep: ['en'],
    }),
    new webpack.ContextReplacementPlugin( // moment sucks
      /moment[/\\]locale$/,
      /en/,
    ),
    //new WebpackCleanupPlugin(),
    options.miniCss && new MiniCssExtractPlugin(options.miniCss),
  ]).filter(Boolean),
  /*
  devServer: {
    disableHostCheck: true,
    host: '0.0.0.0',
    contentBase: sourcePath,
    hot: true,
    inline: true,
    historyApiFallback: {
      disableDotRule: true,
    },
    stats: 'minimal',
    clientLogLevel: 'warning',
  },*/
  // https://webpack.js.org/configuration/devtool/
  devtool: options.devtool,
  node: {
    // workaround for webpack-dev-server issue
    // https://github.com/webpack/webpack-dev-server/issues/60#issuecomment-103411179
    fs: 'empty',
    fsevents: 'empty',
    net: 'empty',
    tls: 'empty',
    dns: 'empty',
    readline: 'empty',
    child_process: 'empty',
  },
});
