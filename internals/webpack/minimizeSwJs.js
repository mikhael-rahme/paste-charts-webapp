const UglifyJS = require('uglify-es');
const fs = require('fs');
const path = require('path');

const serviceworkerPath = path.resolve(__dirname, '../../build/sw.js') // path to your outputted serviceworker
const { code: minifiedServiceworker } = UglifyJS.minify(fs.readFileSync(serviceworkerPath, 'utf8'), {
  // options taken from https://github.com/NekR/offline-plugin/blob/master/src/service-worker.js#L71
  compress: {
    warnings: false,
    dead_code: true,
    drop_console: true,
    unused: true
  },

  output: {
    comments: false
  }
});

fs.writeFileSync(serviceworkerPath, minifiedServiceworker)