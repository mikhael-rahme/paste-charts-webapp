module.exports = {
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  testEnvironment: 'node',
  verbose: true,
  collectCoverage: true,
  rootDir: process.cwd(),
  testRegex: '(/__tests__/.*|(\\.|/)(integrationTest|spec))\\.(jsx?|tsx?)$',
  setupTestFrameworkScriptFile: './jest/jest.setupTestFrameworkScriptFile.ts',
  globalSetup: './jest/jest.globalSetup.ts',
  moduleFileExtensions: [
    'ts',
    'tsx',
    'js',
    'jsx',
    'json',
    'node',
  ],
};
