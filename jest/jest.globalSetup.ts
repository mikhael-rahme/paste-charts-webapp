const {
  TestCredentials,
  getTestStage,
} = require('toolkit/testUtils');
const STS = require('aws-sdk/clients/sts');

const sts = new STS({
  region: process.env.AWS_REGION || 'us-east-1',
});

console.log('running setup!');
module.exports = async () => {
  console.log('fetching test credentials');
  const testCredentials = await TestCredentials.getTestCredentials({
    stage: getTestStage(),
    region: process.env.AWS_REGION || 'us-east-1',
  });
  if (!testCredentials) {
    throw new Error('Error loading test credentials');
  }
  process.testCredentials = testCredentials;
  // console.log(JSON.stringify(process.testCredentials))
  console.log('fetch test credentials complete!');
};
