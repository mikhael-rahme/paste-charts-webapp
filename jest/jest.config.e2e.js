module.exports = {
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  testEnvironment: 'node',
  verbose: true,
  collectCoverage: true,
  rootDir: process.cwd(),
  // testRegex: '(/__tests__/.*|(\\.|/)(e2e))\\.(jsx?|tsx?)$',
  testRegex: 'galvanizingRackPage.e2e.ts',
  preset: 'jest-puppeteer',
  setupTestFrameworkScriptFile: './jest/jest.setupTestFrameworkScriptFile.e2e.ts',
  globalSetup: './jest/jest.globalSetup.ts',
  moduleFileExtensions: [
    'ts',
    'tsx',
    'js',
    'jsx',
    'json',
    'node',
  ],
};
