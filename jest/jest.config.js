module.exports = {
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  testEnvironment: 'node',
  verbose: true,
  collectCoverage: true,
  rootDir: process.cwd(),
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$',
  setupTestFrameworkScriptFile: './jest/jest.setupTestFrameworkScriptFile.ts',
  // globalSetup: './jest/jest.globalSetup.ts', // shouldn't need credentials for unit tests
  moduleFileExtensions: [
    'ts',
    'tsx',
    'js',
    'jsx',
    'json',
    'node',
  ],
};
