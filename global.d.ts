import {
  TestCredentials
} from 'toolkit/testUtils';

declare module NodeJS {
  interface Process {
      testCredentials: TestCredentials,
  }
}
